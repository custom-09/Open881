/* Class411 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class411
{
    public static final int anInt4588 = 28;
    public static final int anInt4589 = 38;
    public static final int anInt4590 = 46;
    public static final int anInt4591 = 26;
    public static final int anInt4592 = 37;
    public static final int anInt4593 = 47;
    public static final int anInt4594 = 11;
    public static final int anInt4595 = 21;
    public static final int anInt4596 = 19;
    public static final int anInt4597 = 3;
    public static final int anInt4598 = 51;
    public static final int anInt4599 = 29;
    public static final int anInt4600 = 14;
    static final int anInt4601 = 15;
    public static final int anInt4602 = 48;
    public static final int anInt4603 = 12;
    public static final int anInt4604 = 24;
    public static final int anInt4605 = 49;
    public static final int anInt4606 = 1;
    public static final int anInt4607 = 6;
    public static final int anInt4608 = 7;
    public static final int anInt4609 = 42;
    public static final int anInt4610 = 30;
    public static final int anInt4611 = 25;
    public static final int anInt4612 = 41;
    public static final int anInt4613 = 16;
    public static final int anInt4614 = 35;
    public static final int anInt4615 = 33;
    public static final int anInt4616 = 9;
    public static final int anInt4617 = 27;
    public static final int anInt4618 = 13;
    public static final int anInt4619 = 52;
    public static final int anInt4620 = 50;
    public static final int anInt4621 = 5;
    public static final int anInt4622 = 44;
    public static final int anInt4623 = 17;
    public static final int anInt4624 = 36;
    public static final int anInt4625 = 20;
    public static final int anInt4626 = 32;
    public static final int anInt4627 = 22;
    public static final int anInt4628 = 10;
    public static final int anInt4629 = 39;
    public static final int anInt4630 = 45;
    public static final int anInt4631 = 23;
    public static final int anInt4632 = 31;
    public static final int anInt4633 = 43;
    public static final int anInt4634 = 8;
    public static final int anInt4635 = 34;
    public static final int anInt4636 = 40;
    public static final int anInt4637 = 2;
    public static final int anInt4638 = 18;
    public static final int anInt4639 = 4;
    
    Class411() throws Throwable {
	throw new Error();
    }
    
    public static void method6716(long[] ls, Object[] objects, int i) {
	Class605.method10034(ls, objects, 0, ls.length - 1, 2104058479);
    }
    
    static final void method6717(Class669 class669, int i) {
	Class9 class9
	    = (class669.aClass534_Sub18_Sub8_8614.aClass9Array11759
	       [class669.anIntArray8591[class669.anInt8613 * 662605117]]);
	Class534_Sub39 class534_sub39
	    = ((Class534_Sub39)
	       class9.method579((long) class669.anIntArray8595
				       [((class669.anInt8600 -= 308999563)
					 * 2088438307)]));
	if (class534_sub39 != null)
	    class669.anInt8613 += class534_sub39.anInt10807 * -717556733;
    }
    
    static final void method6718(Class669 class669, byte i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_0_, -33627732);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_0_ >> 16];
	Class580.method9816(class247, class243, class669, 16711680);
    }
    
    static final void method6719(Class669 class669, int i) {
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = Class627.method10377(Class260.method4812((class669.anIntArray8595
							[((class669.anInt8600
							   -= 308999563)
							  * 2088438307)]),
						       (byte) 0),
				   Class539.aClass672_7171.method93(),
				   1395176417);
    }
}
