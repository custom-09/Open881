/* Class131 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class131 implements Interface76
{
    static Class131 aClass131_1526;
    static Class131 aClass131_1527;
    static Class131 aClass131_1528;
    static Class131 aClass131_1529;
    static Class131 aClass131_1530;
    static Class131 aClass131_1531;
    static Class131 aClass131_1532;
    static Class131 aClass131_1533;
    static Class131 aClass131_1534;
    static Class131 aClass131_1535;
    static Class131 aClass131_1536;
    static Class131 aClass131_1537;
    static Class131 aClass131_1538;
    static Class131 aClass131_1539;
    static Class131 aClass131_1540;
    static Class131 aClass131_1541;
    static Class131 aClass131_1542;
    static Class131 aClass131_1543;
    static Class131 aClass131_1544;
    static Class131 aClass131_1545;
    static Class131 aClass131_1546;
    static Class131 aClass131_1547;
    static Class131 aClass131_1548;
    static Class131 aClass131_1549;
    static Class131 aClass131_1550;
    static Class131 aClass131_1551;
    static Class131 aClass131_1552;
    static Class131 aClass131_1553;
    static Class131 aClass131_1554;
    static Class131 aClass131_1555;
    static Class131 aClass131_1556;
    static Class131 aClass131_1557;
    static Class131 aClass131_1558;
    static Class131 aClass131_1559;
    static Class131 aClass131_1560
	= new Class131(0, "COMPRESSED_RGB_S3TC_DXT1_EXT (0x83f0)", 33776);
    static Class131 aClass131_1561;
    static Class131 aClass131_1562;
    static Class131 aClass131_1563;
    static Class131 aClass131_1564;
    static Class131 aClass131_1565;
    static Class131 aClass131_1566;
    static Class131 aClass131_1567;
    static Class131 aClass131_1568;
    static Class131 aClass131_1569;
    static Class131 aClass131_1570;
    static Class131 aClass131_1571;
    static Class131 aClass131_1572;
    static Class131 aClass131_1573;
    static Class131 aClass131_1574;
    static Class131 aClass131_1575;
    public int anInt1576;
    static Class131 aClass131_1577;
    int anInt1578;
    static Class131 aClass131_1579;
    
    Class131(int i, String string, int i_0_) {
	anInt1578 = i;
	anInt1576 = i_0_;
    }
    
    static {
	aClass131_1527
	    = new Class131(1, "COMPRESSED_RGBA_S3TC_DXT1_EXT (0x83f1)", 33777);
	aClass131_1528
	    = new Class131(2, "COMPRESSED_RGBA_S3TC_DXT3_EXT (0x83f2)", 33778);
	aClass131_1529
	    = new Class131(3, "COMPRESSED_RGBA_S3TC_DXT5_EXT (0x83f3)", 33779);
	aClass131_1530 = new Class131(4, "PALETTE4_RGB8_OES (0x8b90)", 35728);
	aClass131_1531 = new Class131(5, "PALETTE4_RGBA8_OES (0x8b91)", 35729);
	aClass131_1532
	    = new Class131(6, "PALETTE4_R5_G6_B5_OES (0x8b92)", 35730);
	aClass131_1533 = new Class131(7, "PALETTE4_RGBA4_OES (0x8b93)", 35731);
	aClass131_1534
	    = new Class131(8, "PALETTE4_RGB5_A1_OES (0x8b94)", 35732);
	aClass131_1526 = new Class131(9, "PALETTE8_RGB8_OES (0x8b95)", 35733);
	aClass131_1573
	    = new Class131(10, "PALETTE8_RGBA8_OES (0x8b96)", 35734);
	aClass131_1537
	    = new Class131(11, "PALETTE8_R5_G6_B5_OES (0x8b97)", 35735);
	aClass131_1535
	    = new Class131(12, "PALETTE8_RGBA4_OES (0x8b98)", 35736);
	aClass131_1539
	    = new Class131(13, "PALETTE8_RGB5_A1_OES (0x8b99)", 35737);
	aClass131_1540
	    = new Class131(14, "COMPRESSED_RGB8_ETC2 (0x9274)", 37492);
	aClass131_1538
	    = new Class131(15, "COMPRESSED_SRGB8_ETC2 (0x9275)", 37493);
	aClass131_1542
	    = new Class131(16,
			   "COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2 (0x9276)",
			   37494);
	aClass131_1543
	    = (new Class131
	       (17, "COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2 (0x9277)",
		37495));
	aClass131_1544
	    = new Class131(18, "COMPRESSED_RGBA8_ETC2_EAC (0x9278)", 37496);
	aClass131_1545
	    = new Class131(19, "COMPRESSED_SRGB8_ALPHA8_ETC2_EAC (0x9279)",
			   37497);
	aClass131_1536
	    = new Class131(20, "COMPRESSED_R11_EAC (0x9270)", 37488);
	aClass131_1555
	    = new Class131(21, "COMPRESSED_SIGNED_R11_EAC (0x9271)", 37489);
	aClass131_1548
	    = new Class131(22, "COMPRESSED_RG11_EAC (0x9272)", 37490);
	aClass131_1549
	    = new Class131(23, "COMPRESSED_SIGNED_RG11_EAC (0x9273)", 37491);
	aClass131_1550
	    = new Class131(24, "COMPRESSED_RGBA_ASTC_4x4_KHR (0x93b0)", 37808);
	aClass131_1551
	    = new Class131(25, "COMPRESSED_RGBA_ASTC_5x4_KHR (0x93b1)", 37809);
	aClass131_1552
	    = new Class131(26, "COMPRESSED_RGBA_ASTC_5x5_KHR (0x93b2)", 37810);
	aClass131_1559
	    = new Class131(27, "COMPRESSED_RGBA_ASTC_6x5_KHR (0x93b3)", 37811);
	aClass131_1554
	    = new Class131(28, "COMPRESSED_RGBA_ASTC_6x6_KHR (0x93b4)", 37812);
	aClass131_1553
	    = new Class131(29, "COMPRESSED_RGBA_ASTC_8x5_KHR (0x93b5)", 37813);
	aClass131_1558
	    = new Class131(30, "COMPRESSED_RGBA_ASTC_8x6_KHR (0x93b6)", 37814);
	aClass131_1557
	    = new Class131(31, "COMPRESSED_RGBA_ASTC_8x8_KHR (0x93b7)", 37815);
	aClass131_1562
	    = new Class131(32, "COMPRESSED_RGBA_ASTC_10x5_KHR (0x93b8)",
			   37816);
	aClass131_1564
	    = new Class131(33, "COMPRESSED_RGBA_ASTC_10x6_KHR (0x93b9)",
			   37817);
	aClass131_1556
	    = new Class131(34, "COMPRESSED_RGBA_ASTC_10x8_KHR (0x93ba)",
			   37818);
	aClass131_1579
	    = new Class131(35, "COMPRESSED_RGBA_ASTC_10x10_KHR (0x93bb)",
			   37819);
	aClass131_1546
	    = new Class131(36, "COMPRESSED_RGBA_ASTC_12x10_KHR (0x93bc)",
			   37820);
	aClass131_1563
	    = new Class131(37, "COMPRESSED_RGBA_ASTC_12x12_KHR (0x93bd)",
			   37821);
	aClass131_1561
	    = new Class131(38, "COMPRESSED_SRGB8_ALPHA8_ASTC_4x4_KHR (0x93d0)",
			   37840);
	aClass131_1565
	    = new Class131(39, "COMPRESSED_SRGB8_ALPHA8_ASTC_5x4_KHR (0x93d1)",
			   37841);
	aClass131_1566
	    = new Class131(40, "COMPRESSED_SRGB8_ALPHA8_ASTC_5x5_KHR (0x93d2)",
			   37842);
	aClass131_1567
	    = new Class131(41, "COMPRESSED_SRGB8_ALPHA8_ASTC_6x5_KHR (0x93d3)",
			   37843);
	aClass131_1568
	    = new Class131(42, "COMPRESSED_SRGB8_ALPHA8_ASTC_6x6_KHR (0x93d4)",
			   37844);
	aClass131_1569
	    = new Class131(43, "COMPRESSED_SRGB8_ALPHA8_ASTC_8x5_KHR (0x93d5)",
			   37845);
	aClass131_1570
	    = new Class131(44, "COMPRESSED_SRGB8_ALPHA8_ASTC_8x6_KHR (0x93d6)",
			   37846);
	aClass131_1571
	    = new Class131(45, "COMPRESSED_SRGB8_ALPHA8_ASTC_8x8_KHR (0x93d7)",
			   37847);
	aClass131_1577
	    = new Class131(46,
			   "COMPRESSED_SRGB8_ALPHA8_ASTC_10x5_KHR (0x93d8)",
			   37848);
	aClass131_1541
	    = new Class131(47,
			   "COMPRESSED_SRGB8_ALPHA8_ASTC_10x6_KHR (0x93d9)",
			   37849);
	aClass131_1574
	    = new Class131(48,
			   "COMPRESSED_SRGB8_ALPHA8_ASTC_10x8_KHR (0x93da)",
			   37850);
	aClass131_1575
	    = new Class131(49,
			   "COMPRESSED_SRGB8_ALPHA8_ASTC_10x10_KHR (0x93db)",
			   37851);
	aClass131_1547
	    = new Class131(50,
			   "COMPRESSED_SRGB8_ALPHA8_ASTC_12x10_KHR (0x93dc)",
			   37852);
	aClass131_1572
	    = new Class131(51,
			   "COMPRESSED_SRGB8_ALPHA8_ASTC_12x12_KHR (0x93dd)",
			   37853);
    }
    
    public int method93() {
	return anInt1578;
    }
    
    public static Class131[] method2307(int i) {
	return new Class131[] { aClass131_1560, aClass131_1527, aClass131_1528,
				aClass131_1529, aClass131_1530, aClass131_1531,
				aClass131_1532, aClass131_1533, aClass131_1534,
				aClass131_1526, aClass131_1573, aClass131_1537,
				aClass131_1535, aClass131_1539, aClass131_1540,
				aClass131_1538, aClass131_1542, aClass131_1543,
				aClass131_1544, aClass131_1545, aClass131_1536,
				aClass131_1555, aClass131_1548, aClass131_1549,
				aClass131_1550, aClass131_1551, aClass131_1552,
				aClass131_1559, aClass131_1554, aClass131_1553,
				aClass131_1558, aClass131_1557, aClass131_1562,
				aClass131_1564, aClass131_1556, aClass131_1579,
				aClass131_1546, aClass131_1563, aClass131_1561,
				aClass131_1565, aClass131_1566, aClass131_1567,
				aClass131_1568, aClass131_1569, aClass131_1570,
				aClass131_1571, aClass131_1577, aClass131_1541,
				aClass131_1574, aClass131_1575, aClass131_1547,
				aClass131_1572 };
    }
    
    public static Class131[] method2308() {
	return new Class131[] { aClass131_1560, aClass131_1527, aClass131_1528,
				aClass131_1529, aClass131_1530, aClass131_1531,
				aClass131_1532, aClass131_1533, aClass131_1534,
				aClass131_1526, aClass131_1573, aClass131_1537,
				aClass131_1535, aClass131_1539, aClass131_1540,
				aClass131_1538, aClass131_1542, aClass131_1543,
				aClass131_1544, aClass131_1545, aClass131_1536,
				aClass131_1555, aClass131_1548, aClass131_1549,
				aClass131_1550, aClass131_1551, aClass131_1552,
				aClass131_1559, aClass131_1554, aClass131_1553,
				aClass131_1558, aClass131_1557, aClass131_1562,
				aClass131_1564, aClass131_1556, aClass131_1579,
				aClass131_1546, aClass131_1563, aClass131_1561,
				aClass131_1565, aClass131_1566, aClass131_1567,
				aClass131_1568, aClass131_1569, aClass131_1570,
				aClass131_1571, aClass131_1577, aClass131_1541,
				aClass131_1574, aClass131_1575, aClass131_1547,
				aClass131_1572 };
    }
    
    public int method22() {
	return anInt1578;
    }
    
    public int method53() {
	return anInt1578;
    }
}
