/* Class40 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.io.IOException;

public class Class40
{
    static Class695 aClass695_308;
    static int[] anIntArray309;
    static int anInt310;
    static final int anInt311 = 3;
    static int anInt312 = 0;
    
    static void method983(String string) {
	Class331.aClass702_3495 = Class702.aClass702_8815;
	Class534_Sub1_Sub2.aString11720 = string;
    }
    
    static void method984() {
	client.aClass100_11094.method1866((byte) -110);
	if (anInt310 * 2039255983 < 2) {
	    Class5.aClass23_49.method818((byte) -28);
	    anInt312 = 0;
	    anInt310 += 625648463;
	    Class592.aClass62_7815 = Class62.aClass62_649;
	} else {
	    Class592.aClass62_7815 = null;
	    aClass695_308 = Class695.aClass695_8778;
	    Class673.method11110(15, 531464174);
	}
    }
    
    public static void method985() {
	if (15 == -1850530127 * client.anInt11039
	    && (!Class202.method3865((byte) 0)
		&& !Class295.method5325(-1556587400)))
	    Class673.method11110(19, -700528637);
    }
    
    static void method986() {
	client.aClass100_11094.method1866((byte) -68);
	if (anInt310 * 2039255983 < 2) {
	    Class5.aClass23_49.method818((byte) -48);
	    anInt312 = 0;
	    anInt310 += 625648463;
	    Class592.aClass62_7815 = Class62.aClass62_649;
	} else {
	    Class592.aClass62_7815 = null;
	    aClass695_308 = Class695.aClass695_8778;
	    Class673.method11110(15, -1939003331);
	}
    }
    
    public static void method987() {
	if (15 == -1850530127 * client.anInt11039
	    && (!Class202.method3865((byte) 0)
		&& !Class295.method5325(-1464339445)))
	    Class673.method11110(19, -168343228);
    }
    
    static void method988() {
	Class592.aClass62_7815 = Class62.aClass62_649;
	aClass695_308 = Class695.aClass695_8776;
	Class44_Sub20.aClass704_11014 = Class704.aClass704_8828;
	Class262.aClass703_2800 = Class703.aClass703_8822;
	Class331.aClass702_3495 = Class702.aClass702_8811;
	Class267.aClass699_2940 = Class699.aClass699_8797;
	Class534_Sub1_Sub2.aString11720 = null;
    }
    
    static void method989() {
	Class592.aClass62_7815 = Class62.aClass62_649;
	aClass695_308 = Class695.aClass695_8776;
	Class44_Sub20.aClass704_11014 = Class704.aClass704_8828;
	Class262.aClass703_2800 = Class703.aClass703_8822;
	Class331.aClass702_3495 = Class702.aClass702_8811;
	Class267.aClass699_2940 = Class699.aClass699_8797;
	Class534_Sub1_Sub2.aString11720 = null;
    }
    
    public static void method990(String string, String string_0_, int i,
				 boolean bool, String string_1_) {
	if (18 == client.anInt11039 * -1850530127) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4200,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16507(0,
								  1791787763);
	    int i_2_
		= (31645619
		   * class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  1472380734);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string_0_,
								  1842158745);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(i,
								  1222479347);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(bool ? 1 : 0,
								  270109324);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string_1_,
								  -1260442613);
	    class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		+= -1122347939;
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16756
		(anIntArray309, i_2_,
		 (31645619
		  * class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811),
		 (byte) 16);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16733
		((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  * 31645619) - i_2_,
		 372220223);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 43);
	    if (i < 13) {
		client.aBool11015 = true;
		Class306.method5607((byte) -103);
	    }
	    Class44_Sub20.aClass704_11014 = Class704.aClass704_8825;
	}
    }
    
    public static void method991(String string) {
	if (client.anInt11039 * -1850530127 == 18) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4155,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16507(0,
								  2123212315);
	    int i = (class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		     * 31645619);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  -1180578574);
	    class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		+= -1122347939;
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16756
		(anIntArray309, i,
		 (class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  * 31645619),
		 (byte) 16);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16733
		((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  * 31645619) - i,
		 111065274);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 25);
	    Class262.aClass703_2800 = Class703.aClass703_8817;
	}
    }
    
    static void method992() {
	if (Class592.aClass62_7815 != null) {
	    try {
		int i;
		if (anInt310 * 2039255983 == 0)
		    i = 250;
		else
		    i = 2000;
		anInt312 += 999247095;
		if (-1232128825 * anInt312 > i)
		    Class690.method14032(270189415);
		if (Class592.aClass62_7815 == Class62.aClass62_649) {
		    client.aClass100_11094.method1880
			(Class106.method1945(Class5.aClass23_49
						 .method817(-753108675),
					     40000, (byte) 89),
			 Class5.aClass23_49.aString223, -1850530127);
		    client.aClass100_11094.method1874(1645107394);
		    Class534_Sub19 class534_sub19
			= Class56.method1225(2128881031);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			(Class413.aClass413_4652.anInt4648 * -100453045,
			 2012022339);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16507(0, 2080975109);
		    int i_3_
			= 31645619 * (class534_sub19.aClass534_Sub40_Sub1_10513
				      .anInt10811);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16507(881, 1438713210);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16507(1, 1172397419);
		    anIntArray309
			= Class175_Sub2_Sub1.method17935(class534_sub19,
							 65815261);
		    int i_4_
			= 31645619 * (class534_sub19.aClass534_Sub40_Sub1_10513
				      .anInt10811);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16713(client.aString11197, 1495956745);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16507
			(1844562269 * client.anInt11020, 935777143);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16510
			(-1608886643 * client.anInt11024, -1178805042);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16510
			(1306630125 * client.anInt11276, -895266801);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16713(Class29.aString267, -1315875427);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			(Class539.aClass672_7171.method93(), 1007756897);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			(-1082924039 * client.aClass675_11016.anInt8642,
			 954191407);
		    Class646.method10690((class534_sub19
					  .aClass534_Sub40_Sub1_10513),
					 (byte) 13);
		    String string = client.aString11029;
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16506(string == null ? 0 : 1, 501834708);
		    if (string != null)
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16713(string, -686266194);
		    Class200_Sub23.aClass534_Sub28_10040.method16312
			(class534_sub19.aClass534_Sub40_Sub1_10513,
			 (byte) -50);
		    class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
			+= -1122347939;
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16756
			(anIntArray309, i_4_,
			 31645619 * (class534_sub19.aClass534_Sub40_Sub1_10513
				     .anInt10811),
			 (byte) 16);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16733
			((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
			  * 31645619) - i_3_,
			 1621390626);
		    client.aClass100_11094.method1863(class534_sub19,
						      (byte) 83);
		    client.aClass100_11094.method1868(1805858475);
		    Class592.aClass62_7815 = Class62.aClass62_648;
		}
		if (Class592.aClass62_7815 == Class62.aClass62_648) {
		    if (client.aClass100_11094.method1867(136751282) == null)
			Class690.method14032(270189415);
		    else if (client.aClass100_11094.method1867(-786832603)
				 .method8977(1, (byte) 1)) {
			client.aClass100_11094.method1867(1384202537)
			    .method8969
			    ((client.aClass100_11094.aClass534_Sub40_Sub1_1179
			      .aByteArray10810),
			     0, 1, (byte) 72);
			aClass695_308
			    = (Class695) (Class448.method7319
					  (Class271.method5040(-1185117485),
					   (client.aClass100_11094
					    .aClass534_Sub40_Sub1_1179
					    .aByteArray10810[0]) & 0xff,
					   2088438307));
			if (aClass695_308 == Class695.aClass695_8779) {
			    client.aClass100_11094.aClass13_1183
				= new IsaacCipher(anIntArray309);
			    int[] is = new int[4];
			    for (int i_5_ = 0; i_5_ < 4; i_5_++)
				is[i_5_] = 50 + anIntArray309[i_5_];
			    client.aClass100_11094.aClass13_1190
				= new IsaacCipher(is);
			    new IsaacCipher(is);
			    client.aClass100_11094
				.aClass534_Sub40_Sub1_1179.method18316
				(client.aClass100_11094.aClass13_1190,
				 163694688);
			    Class673.method11110(18, -2087017690);
			    client.aClass100_11094.method1874(1764654142);
			    client.aClass100_11094
				.aClass534_Sub40_Sub1_1179.anInt10811
				= 0;
			    client.aClass100_11094.aClass409_1199 = null;
			    client.aClass100_11094.aClass409_1196 = null;
			    client.aClass100_11094.aClass409_1195 = null;
			    client.aClass100_11094.anInt1189 = 0;
			    Class78.aClass103_825.aClass612_1294
				.method10098(-1928075602);
			    Class690_Sub21.method17075((short) -8839);
			} else
			    client.aClass100_11094.method1866((byte) -14);
			client.aClass100_11094.aClass409_1186 = null;
			Class592.aClass62_7815 = null;
		    }
		}
	    } catch (IOException ioexception) {
		Class690.method14032(270189415);
	    }
	}
    }
    
    public static void method993(String string) {
	if (client.anInt11039 * -1850530127 == 18) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4155,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16507(0,
								  1475368280);
	    int i = (class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		     * 31645619);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  -1971781457);
	    class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		+= -1122347939;
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16756
		(anIntArray309, i,
		 (class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  * 31645619),
		 (byte) 16);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16733
		((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  * 31645619) - i,
		 739550168);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 37);
	    Class262.aClass703_2800 = Class703.aClass703_8817;
	}
    }
    
    public static void method994(String string) {
	if (18 == -1850530127 * client.anInt11039) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4204,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(0,
								  512050644);
	    int i = (31645619
		     * class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  -2090448729);
	    class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		+= -1122347939;
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16756
		(anIntArray309, i,
		 (class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  * 31645619),
		 (byte) 16);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16528
		((31645619
		  * class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811) - i,
		 1967967891);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 91);
	    Class267.aClass699_2940 = Class699.aClass699_8793;
	}
    }
    
    public static Class702 method995() {
	if (null == Class331.aClass702_3495)
	    return Class702.aClass702_8811;
	return Class331.aClass702_3495;
    }
    
    static void method996(String string) {
	Class331.aClass702_3495 = Class702.aClass702_8815;
	Class534_Sub1_Sub2.aString11720 = string;
    }
    
    public static void method997(String string) {
	if (client.anInt11039 * -1850530127 == 18) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4155,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16507(0,
								  1588221663);
	    int i = (class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		     * 31645619);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  -1781558875);
	    class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		+= -1122347939;
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16756
		(anIntArray309, i,
		 (class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  * 31645619),
		 (byte) 16);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16733
		((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  * 31645619) - i,
		 169110052);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 90);
	    Class262.aClass703_2800 = Class703.aClass703_8817;
	}
    }
    
    public static void method998() {
	if (client.anInt11039 * -1850530127 == 18) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4209,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 55);
	    Class331.aClass702_3495 = Class702.aClass702_8812;
	    Class534_Sub1_Sub2.aString11720 = null;
	}
    }
    
    static boolean method999() {
	return Class592.aClass62_7815 != null;
    }
    
    public static void method1000(int i) {
	if (-1850530127 * client.anInt11039 == 18) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4177,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(i,
								  1368993946);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 9);
	}
    }
    
    public static void method1001(int i) {
	if (-1850530127 * client.anInt11039 == 18) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4177,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(i,
								  280666987);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 75);
	}
    }
    
    public static void method1002() {
	if (client.anInt11039 * -1850530127 == 18) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4209,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 115);
	    Class331.aClass702_3495 = Class702.aClass702_8812;
	    Class534_Sub1_Sub2.aString11720 = null;
	}
    }
    
    public static void method1003() {
	if (15 == -1850530127 * client.anInt11039
	    && (!Class202.method3865((byte) 0)
		&& !Class295.method5325(-2104827903)))
	    Class673.method11110(19, -744109761);
    }
    
    public static Class695 method1004() {
	if (aClass695_308 == null)
	    return Class695.aClass695_8775;
	return aClass695_308;
    }
    
    public static Class704 method1005() {
	if (Class44_Sub20.aClass704_11014 == null)
	    return Class704.aClass704_8828;
	return Class44_Sub20.aClass704_11014;
    }
    
    public static Class703 method1006() {
	if (null == Class262.aClass703_2800)
	    return Class703.aClass703_8822;
	return Class262.aClass703_2800;
    }
    
    public static Class703 method1007() {
	if (null == Class262.aClass703_2800)
	    return Class703.aClass703_8822;
	return Class262.aClass703_2800;
    }
    
    public static Class699 method1008() {
	if (null == Class267.aClass699_2940)
	    return Class699.aClass699_8797;
	return Class267.aClass699_2940;
    }
    
    public static Class702 method1009() {
	if (null == Class331.aClass702_3495)
	    return Class702.aClass702_8811;
	return Class331.aClass702_3495;
    }
    
    public static void method1010() {
	if (client.anInt11039 * -1850530127 == 18) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4209,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 106);
	    Class331.aClass702_3495 = Class702.aClass702_8812;
	    Class534_Sub1_Sub2.aString11720 = null;
	}
    }
    
    public static String method1011() {
	return Class534_Sub1_Sub2.aString11720;
    }
    
    public static String method1012() {
	return Class534_Sub1_Sub2.aString11720;
    }
    
    static void method1013(Class704 class704) {
	Class44_Sub20.aClass704_11014 = class704;
    }
    
    static void method1014(Class704 class704) {
	Class44_Sub20.aClass704_11014 = class704;
    }
    
    static void method1015(Class704 class704) {
	Class44_Sub20.aClass704_11014 = class704;
    }
    
    static void method1016(Class704 class704) {
	Class44_Sub20.aClass704_11014 = class704;
    }
    
    static void method1017(Class703 class703) {
	Class262.aClass703_2800 = class703;
    }
    
    static void method1018(Class703 class703) {
	Class262.aClass703_2800 = class703;
    }
    
    static void method1019(Class699 class699) {
	Class267.aClass699_2940 = class699;
    }
    
    static void method1020(Class702 class702) {
	Class331.aClass702_3495 = class702;
	Class534_Sub1_Sub2.aString11720 = null;
    }
    
    static void method1021(Class702 class702) {
	Class331.aClass702_3495 = class702;
	Class534_Sub1_Sub2.aString11720 = null;
    }
    
    Class40() throws Throwable {
	throw new Error();
    }
    
    static void method1022(String string) {
	Class331.aClass702_3495 = Class702.aClass702_8815;
	Class534_Sub1_Sub2.aString11720 = string;
    }
    
    static void method1023(String string) {
	Class331.aClass702_3495 = Class702.aClass702_8815;
	Class534_Sub1_Sub2.aString11720 = string;
    }
    
    public static Class695 method1024() {
	if (aClass695_308 == null)
	    return Class695.aClass695_8775;
	return aClass695_308;
    }
    
    public static void method1025(String string) {
	if (18 == -1850530127 * client.anInt11039) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4204,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(0,
								  2141542858);
	    int i = (31645619
		     * class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  -404083906);
	    class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		+= -1122347939;
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16756
		(anIntArray309, i,
		 (class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  * 31645619),
		 (byte) 16);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16528
		((31645619
		  * class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811) - i,
		 1993171563);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 36);
	    Class267.aClass699_2940 = Class699.aClass699_8793;
	}
    }
    
    static {
	anInt310 = 0;
    }
    
    static void method1026() {
	if (Class592.aClass62_7815 != null) {
	    try {
		int i;
		if (anInt310 * 2039255983 == 0)
		    i = 250;
		else
		    i = 2000;
		anInt312 += 999247095;
		if (-1232128825 * anInt312 > i)
		    Class690.method14032(270189415);
		if (Class592.aClass62_7815 == Class62.aClass62_649) {
		    client.aClass100_11094.method1880
			(Class106.method1945(Class5.aClass23_49
						 .method817(-837237608),
					     40000, (byte) 94),
			 Class5.aClass23_49.aString223, -1850530127);
		    client.aClass100_11094.method1874(-1055881503);
		    Class534_Sub19 class534_sub19
			= Class56.method1225(1877936543);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			(Class413.aClass413_4652.anInt4648 * -100453045,
			 789912706);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16507(0, 556850863);
		    int i_6_
			= 31645619 * (class534_sub19.aClass534_Sub40_Sub1_10513
				      .anInt10811);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16507(881, 1527501274);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16507(1, 527689488);
		    anIntArray309
			= Class175_Sub2_Sub1.method17935(class534_sub19,
							 2113554396);
		    int i_7_
			= 31645619 * (class534_sub19.aClass534_Sub40_Sub1_10513
				      .anInt10811);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16713(client.aString11197, -305927761);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16507
			(1844562269 * client.anInt11020, 975228292);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16510
			(-1608886643 * client.anInt11024, -1840860259);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16510
			(1306630125 * client.anInt11276, -1276955783);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16713(Class29.aString267, -41337325);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			(Class539.aClass672_7171.method93(), 1382294108);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			(-1082924039 * client.aClass675_11016.anInt8642,
			 1767158506);
		    Class646.method10690((class534_sub19
					  .aClass534_Sub40_Sub1_10513),
					 (byte) 6);
		    String string = client.aString11029;
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16506(string == null ? 0 : 1, 2091832825);
		    if (string != null)
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16713(string, -1555099497);
		    Class200_Sub23.aClass534_Sub28_10040.method16312
			(class534_sub19.aClass534_Sub40_Sub1_10513, (byte) 55);
		    class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
			+= -1122347939;
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16756
			(anIntArray309, i_7_,
			 31645619 * (class534_sub19.aClass534_Sub40_Sub1_10513
				     .anInt10811),
			 (byte) 16);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16733
			((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
			  * 31645619) - i_6_,
			 982050437);
		    client.aClass100_11094.method1863(class534_sub19,
						      (byte) 113);
		    client.aClass100_11094.method1868(1805858475);
		    Class592.aClass62_7815 = Class62.aClass62_648;
		}
		if (Class592.aClass62_7815 == Class62.aClass62_648) {
		    if (client.aClass100_11094.method1867(-1564048827) == null)
			Class690.method14032(270189415);
		    else if (client.aClass100_11094.method1867(1909622506)
				 .method8977(1, (byte) 1)) {
			client.aClass100_11094.method1867(-160182727)
			    .method8969
			    ((client.aClass100_11094.aClass534_Sub40_Sub1_1179
			      .aByteArray10810),
			     0, 1, (byte) 94);
			aClass695_308
			    = (Class695) (Class448.method7319
					  (Class271.method5040(-1185117485),
					   (client.aClass100_11094
					    .aClass534_Sub40_Sub1_1179
					    .aByteArray10810[0]) & 0xff,
					   2088438307));
			if (aClass695_308 == Class695.aClass695_8779) {
			    client.aClass100_11094.aClass13_1183
				= new IsaacCipher(anIntArray309);
			    int[] is = new int[4];
			    for (int i_8_ = 0; i_8_ < 4; i_8_++)
				is[i_8_] = 50 + anIntArray309[i_8_];
			    client.aClass100_11094.aClass13_1190
				= new IsaacCipher(is);
			    new IsaacCipher(is);
			    client.aClass100_11094
				.aClass534_Sub40_Sub1_1179.method18316
				(client.aClass100_11094.aClass13_1190,
				 163694688);
			    Class673.method11110(18, 276537506);
			    client.aClass100_11094.method1874(-1436132542);
			    client.aClass100_11094
				.aClass534_Sub40_Sub1_1179.anInt10811
				= 0;
			    client.aClass100_11094.aClass409_1199 = null;
			    client.aClass100_11094.aClass409_1196 = null;
			    client.aClass100_11094.aClass409_1195 = null;
			    client.aClass100_11094.anInt1189 = 0;
			    Class78.aClass103_825.aClass612_1294
				.method10098(-346581421);
			    Class690_Sub21.method17075((short) 11233);
			} else
			    client.aClass100_11094.method1866((byte) -103);
			client.aClass100_11094.aClass409_1186 = null;
			Class592.aClass62_7815 = null;
		    }
		}
	    } catch (IOException ioexception) {
		Class690.method14032(270189415);
	    }
	}
    }
    
    static int[] method1027(Class534_Sub19 class534_sub19) {
	Class534_Sub40 class534_sub40 = new Class534_Sub40(518);
	int[] is = new int[4];
	for (int i = 0; i < 4; i++)
	    is[i] = (int) (Math.random() * 9.9999999E7);
	class534_sub40.method16506(10, 1990115729);
	class534_sub40.method16510(is[0], -302624008);
	class534_sub40.method16510(is[1], -1618007395);
	class534_sub40.method16510(is[2], -1696788416);
	class534_sub40.method16510(is[3], -1319072299);
	for (int i = 0; i < 10; i++)
	    class534_sub40.method16510((int) (Math.random() * 9.9999999E7),
				       -2121595904);
	class534_sub40.method16507((int) (Math.random() * 9.9999999E7),
				   2063134253);
	class534_sub40.method16556(Class37.RSA_UPDATE_EXPONENT,
				   Class37.LOGIN_MODULUS, 1551841936);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16519
	    (class534_sub40.aByteArray10810, 0,
	     class534_sub40.anInt10811 * 31645619, -229458688);
	return is;
    }
    
    static int[] method1028(Class534_Sub19 class534_sub19) {
	Class534_Sub40 class534_sub40 = new Class534_Sub40(518);
	int[] is = new int[4];
	for (int i = 0; i < 4; i++)
	    is[i] = (int) (Math.random() * 9.9999999E7);
	class534_sub40.method16506(10, 1404933140);
	class534_sub40.method16510(is[0], -193028827);
	class534_sub40.method16510(is[1], -1188065970);
	class534_sub40.method16510(is[2], -682793285);
	class534_sub40.method16510(is[3], -1505587048);
	for (int i = 0; i < 10; i++)
	    class534_sub40.method16510((int) (Math.random() * 9.9999999E7),
				       -192663236);
	class534_sub40.method16507((int) (Math.random() * 9.9999999E7),
				   2006968018);
	class534_sub40.method16556(Class37.RSA_UPDATE_EXPONENT,
				   Class37.LOGIN_MODULUS, -657480535);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16519
	    (class534_sub40.aByteArray10810, 0,
	     class534_sub40.anInt10811 * 31645619, 243216277);
	return is;
    }
    
    static int[] method1029(Class534_Sub19 class534_sub19) {
	Class534_Sub40 class534_sub40 = new Class534_Sub40(518);
	int[] is = new int[4];
	for (int i = 0; i < 4; i++)
	    is[i] = (int) (Math.random() * 9.9999999E7);
	class534_sub40.method16506(10, 1845798532);
	class534_sub40.method16510(is[0], -825428581);
	class534_sub40.method16510(is[1], -491566470);
	class534_sub40.method16510(is[2], -444140104);
	class534_sub40.method16510(is[3], -121881098);
	for (int i = 0; i < 10; i++)
	    class534_sub40.method16510((int) (Math.random() * 9.9999999E7),
				       -489603870);
	class534_sub40.method16507((int) (Math.random() * 9.9999999E7),
				   1015981273);
	class534_sub40.method16556(Class37.RSA_UPDATE_EXPONENT,
				   Class37.LOGIN_MODULUS, 1723489610);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16519
	    (class534_sub40.aByteArray10810, 0,
	     class534_sub40.anInt10811 * 31645619, -623030045);
	return is;
    }
    
    static int[] method1030(Class534_Sub19 class534_sub19) {
	Class534_Sub40 class534_sub40 = new Class534_Sub40(518);
	int[] is = new int[4];
	for (int i = 0; i < 4; i++)
	    is[i] = (int) (Math.random() * 9.9999999E7);
	class534_sub40.method16506(10, 1291964475);
	class534_sub40.method16510(is[0], -1192619685);
	class534_sub40.method16510(is[1], -69398604);
	class534_sub40.method16510(is[2], -2014748249);
	class534_sub40.method16510(is[3], -2013640626);
	for (int i = 0; i < 10; i++)
	    class534_sub40.method16510((int) (Math.random() * 9.9999999E7),
				       -1586906619);
	class534_sub40.method16507((int) (Math.random() * 9.9999999E7),
				   2104448652);
	class534_sub40.method16556(Class37.RSA_UPDATE_EXPONENT,
				   Class37.LOGIN_MODULUS, -330504548);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16519
	    (class534_sub40.aByteArray10810, 0,
	     class534_sub40.anInt10811 * 31645619, 351919206);
	return is;
    }
    
    static void method1031() {
	if (Class592.aClass62_7815 != null) {
	    try {
		int i;
		if (anInt310 * 2039255983 == 0)
		    i = 250;
		else
		    i = 2000;
		anInt312 += 999247095;
		if (-1232128825 * anInt312 > i)
		    Class690.method14032(270189415);
		if (Class592.aClass62_7815 == Class62.aClass62_649) {
		    client.aClass100_11094.method1880
			(Class106.method1945(Class5.aClass23_49
						 .method817(171662554),
					     40000, (byte) 74),
			 Class5.aClass23_49.aString223, -1850530127);
		    client.aClass100_11094.method1874(-174057121);
		    Class534_Sub19 class534_sub19
			= Class56.method1225(1731655275);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			(Class413.aClass413_4652.anInt4648 * -100453045,
			 249631275);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16507(0, 1889554449);
		    int i_9_
			= 31645619 * (class534_sub19.aClass534_Sub40_Sub1_10513
				      .anInt10811);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16507(881, 1388609167);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16507(1, 607528270);
		    anIntArray309
			= Class175_Sub2_Sub1.method17935(class534_sub19,
							 251511077);
		    int i_10_
			= 31645619 * (class534_sub19.aClass534_Sub40_Sub1_10513
				      .anInt10811);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16713(client.aString11197, 1712980104);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16507
			(1844562269 * client.anInt11020, 502871741);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16510
			(-1608886643 * client.anInt11024, -950392091);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16510
			(1306630125 * client.anInt11276, -1917429624);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16713(Class29.aString267, -874826736);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			(Class539.aClass672_7171.method93(), 1698564999);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			(-1082924039 * client.aClass675_11016.anInt8642,
			 2138914610);
		    Class646.method10690((class534_sub19
					  .aClass534_Sub40_Sub1_10513),
					 (byte) 81);
		    String string = client.aString11029;
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16506(string == null ? 0 : 1, 1648053160);
		    if (string != null)
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16713(string, -700240274);
		    Class200_Sub23.aClass534_Sub28_10040.method16312
			(class534_sub19.aClass534_Sub40_Sub1_10513, (byte) 8);
		    class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
			+= -1122347939;
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16756
			(anIntArray309, i_10_,
			 31645619 * (class534_sub19.aClass534_Sub40_Sub1_10513
				     .anInt10811),
			 (byte) 16);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16733
			((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
			  * 31645619) - i_9_,
			 -606490671);
		    client.aClass100_11094.method1863(class534_sub19,
						      (byte) 62);
		    client.aClass100_11094.method1868(1805858475);
		    Class592.aClass62_7815 = Class62.aClass62_648;
		}
		if (Class592.aClass62_7815 == Class62.aClass62_648) {
		    if (client.aClass100_11094.method1867(1758315607) == null)
			Class690.method14032(270189415);
		    else if (client.aClass100_11094.method1867(816703962)
				 .method8977(1, (byte) 1)) {
			client.aClass100_11094.method1867(-1840683485)
			    .method8969
			    ((client.aClass100_11094.aClass534_Sub40_Sub1_1179
			      .aByteArray10810),
			     0, 1, (byte) 117);
			aClass695_308
			    = (Class695) (Class448.method7319
					  (Class271.method5040(-1185117485),
					   (client.aClass100_11094
					    .aClass534_Sub40_Sub1_1179
					    .aByteArray10810[0]) & 0xff,
					   2088438307));
			if (aClass695_308 == Class695.aClass695_8779) {
			    client.aClass100_11094.aClass13_1183
				= new IsaacCipher(anIntArray309);
			    int[] is = new int[4];
			    for (int i_11_ = 0; i_11_ < 4; i_11_++)
				is[i_11_] = 50 + anIntArray309[i_11_];
			    client.aClass100_11094.aClass13_1190
				= new IsaacCipher(is);
			    new IsaacCipher(is);
			    client.aClass100_11094
				.aClass534_Sub40_Sub1_1179.method18316
				(client.aClass100_11094.aClass13_1190,
				 163694688);
			    Class673.method11110(18, 497269466);
			    client.aClass100_11094.method1874(-861730068);
			    client.aClass100_11094
				.aClass534_Sub40_Sub1_1179.anInt10811
				= 0;
			    client.aClass100_11094.aClass409_1199 = null;
			    client.aClass100_11094.aClass409_1196 = null;
			    client.aClass100_11094.aClass409_1195 = null;
			    client.aClass100_11094.anInt1189 = 0;
			    Class78.aClass103_825.aClass612_1294
				.method10098(1866135663);
			    Class690_Sub21.method17075((short) 14900);
			} else
			    client.aClass100_11094.method1866((byte) -44);
			client.aClass100_11094.aClass409_1186 = null;
			Class592.aClass62_7815 = null;
		    }
		}
	    } catch (IOException ioexception) {
		Class690.method14032(270189415);
	    }
	}
    }
    
    public static Class695 method1032() {
	if (aClass695_308 == null)
	    return Class695.aClass695_8775;
	return aClass695_308;
    }
    
    static final void method1033(Class669 class669, int i) {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4272,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	client.aClass100_11264.method1863(class534_sub19, (byte) 8);
    }
    
    static final void method1034
	(Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1, boolean bool,
	 int i) {
	int i_12_ = Class676.aClass676_8646.aByte8648;
	int i_13_ = 0;
	if (class654_sub1_sub5_sub1.anInt11956 * -1626386689
	    > client.anInt11101)
	    Class6.method564(class654_sub1_sub5_sub1, 702611098);
	else if (-1259938159 * class654_sub1_sub5_sub1.anInt11957
		 >= client.anInt11101)
	    Class103.method1918(class654_sub1_sub5_sub1, (byte) 0);
	else {
	    Class328.method5831(class654_sub1_sub5_sub1, bool, 2088438307);
	    i_12_ = Class652.anInt8483 * 431536063;
	    i_13_ = Class179_Sub1.anInt9332 * 808625503;
	}
	Class438 class438
	    = class654_sub1_sub5_sub1.method10807().aClass438_4885;
	if ((int) class438.aFloat4864 < 512 || (int) class438.aFloat4865 < 512
	    || ((int) class438.aFloat4864
		>= (client.aClass512_11100.method8417(401207519) - 1) * 512)
	    || ((int) class438.aFloat4865
		>= ((client.aClass512_11100.method8418(-1533611049) - 1)
		    * 512))) {
	    class654_sub1_sub5_sub1.aClass711_11948.method14330(-1,
								1288034849);
	    for (int i_14_ = 0;
		 i_14_ < class654_sub1_sub5_sub1.aClass529Array11949.length;
		 i_14_++) {
		class654_sub1_sub5_sub1.aClass529Array11949[i_14_].anInt7123
		    = -496843307;
		class654_sub1_sub5_sub1.aClass529Array11949[i_14_]
		    .aClass711_7121.method14330(-1, 1134085174);
	    }
	    class654_sub1_sub5_sub1.anIntArray11946 = null;
	    class654_sub1_sub5_sub1.anInt11956 = 0;
	    class654_sub1_sub5_sub1.anInt11957 = 0;
	    i_12_ = Class676.aClass676_8646.aByte8648;
	    i_13_ = 0;
	    class654_sub1_sub5_sub1.method10815
		((float) (class654_sub1_sub5_sub1.anIntArray11977[0] * 512
			  + (class654_sub1_sub5_sub1.method18545((byte) 1)
			     * 256)),
		 class438.aFloat4863,
		 (float) (512 * class654_sub1_sub5_sub1.anIntArray11978[0]
			  + (class654_sub1_sub5_sub1.method18545((byte) 1)
			     * 256)));
	    class654_sub1_sub5_sub1.method18530(-1556773092);
	}
	if ((Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
	     == class654_sub1_sub5_sub1)
	    && ((int) class438.aFloat4864 < 6144
		|| (int) class438.aFloat4865 < 6144
		|| ((int) class438.aFloat4864
		    >= ((client.aClass512_11100.method8417(256685259) - 12)
			* 512))
		|| ((int) class438.aFloat4865
		    >= ((client.aClass512_11100.method8418(-1533611049) - 12)
			* 512)))) {
	    class654_sub1_sub5_sub1.aClass711_11948.method14330(-1,
								1105798071);
	    for (int i_15_ = 0;
		 i_15_ < class654_sub1_sub5_sub1.aClass529Array11949.length;
		 i_15_++) {
		class654_sub1_sub5_sub1.aClass529Array11949[i_15_].anInt7123
		    = -496843307;
		class654_sub1_sub5_sub1.aClass529Array11949[i_15_]
		    .aClass711_7121.method14330(-1, 1094627757);
	    }
	    class654_sub1_sub5_sub1.anIntArray11946 = null;
	    class654_sub1_sub5_sub1.anInt11956 = 0;
	    class654_sub1_sub5_sub1.anInt11957 = 0;
	    i_12_ = Class676.aClass676_8646.aByte8648;
	    i_13_ = 0;
	    class654_sub1_sub5_sub1.method10815
		((float) (class654_sub1_sub5_sub1.anIntArray11977[0] * 512
			  + (class654_sub1_sub5_sub1.method18545((byte) 1)
			     * 256)),
		 class438.aFloat4863,
		 (float) (class654_sub1_sub5_sub1.anIntArray11978[0] * 512
			  + (class654_sub1_sub5_sub1.method18545((byte) 1)
			     * 256)));
	    class654_sub1_sub5_sub1.method18530(-729339017);
	}
	int i_16_ = Class155.method2579(class654_sub1_sub5_sub1, 1446404112);
	Class304.method5596(class654_sub1_sub5_sub1, (byte) -11);
	Class698.method14123(class654_sub1_sub5_sub1, i_12_, i_13_, i_16_,
			     (byte) 89);
	Class560.method9433(class654_sub1_sub5_sub1, i_12_, -1981807187);
	Class101.method1900(class654_sub1_sub5_sub1, -963140748);
	Class443 class443 = Class443.method7137();
	class443.method7148(Class427.method6799(class654_sub1_sub5_sub1
						    .aClass57_11973
						    .method1231(432154953)),
			    Class427.method6799(class654_sub1_sub5_sub1
						    .aClass57_11975
						    .method1231(467367692)),
			    Class427.method6799(class654_sub1_sub5_sub1
						    .aClass57_11974
						    .method1231(1839699145)));
	class654_sub1_sub5_sub1.method10813(class443);
	class443.method7140();
    }
    
    static final void method1035(boolean bool, Class669 class669, int i) {
	int i_17_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_17_, 285971512);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_17_ >> 16];
	if (bool)
	    Class658.method10913(class243, class247, (byte) 81);
	else
	    Class97.method1828(class243, class247, 229948709);
    }
    
    static final void method1036(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.aClass214_11359.method4035(1327527002).method93();
    }
}
