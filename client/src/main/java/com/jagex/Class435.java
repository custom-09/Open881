/* Class435 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class435
{
    Class435() throws Throwable {
	throw new Error();
    }
    
    public static void method6982(Class438 class438, Class438 class438_0_,
				  Class438 class438_1_, float f,
				  Class438[] class438s) {
	Class438 class438_2_ = Class438.method7055(class438, class438_1_);
	float f_3_ = Class438.method7027(class438_2_, class438_2_) - f * f;
	float f_4_ = Class438.method7027(class438_0_, class438_2_);
	float f_5_ = f_4_ * f_4_ - f_3_;
	if (f_5_ < 0.0F) {
	    Class438[] class438s_6_ = class438s;
	    class438s[1] = null;
	    class438s_6_[0] = null;
	} else if (f_5_ >= 9.765625E-4F) {
	    float f_7_ = (float) Math.sqrt((double) f_5_);
	    class438s[0] = Class438.method7061();
	    class438s[0].method6992(class438);
	    class438s[0].method7026
		(Class438.method7020(Class438.method6994(class438_0_),
				     -f_4_ - f_7_));
	    class438s[1] = Class438.method7061();
	    class438s[1].method6992(class438);
	    class438s[1].method7026
		(Class438.method7020(Class438.method6994(class438_0_),
				     -f_4_ + f_7_));
	} else {
	    class438s[0] = Class438.method7061();
	    class438s[0].method6992(class438);
	    class438s[0].method7026
		(Class438.method7020(Class438.method6994(class438_0_), -f_4_));
	    class438s[1] = null;
	}
    }
    
    public static void method6983(Class438 class438, Class438 class438_8_,
				  Class438 class438_9_, float f,
				  Class438[] class438s) {
	Class438 class438_10_ = Class438.method7055(class438, class438_9_);
	float f_11_ = Class438.method7027(class438_10_, class438_10_) - f * f;
	float f_12_ = Class438.method7027(class438_8_, class438_10_);
	float f_13_ = f_12_ * f_12_ - f_11_;
	if (f_13_ < 0.0F) {
	    Class438[] class438s_14_ = class438s;
	    class438s[1] = null;
	    class438s_14_[0] = null;
	} else if (f_13_ >= 9.765625E-4F) {
	    float f_15_ = (float) Math.sqrt((double) f_13_);
	    class438s[0] = Class438.method7061();
	    class438s[0].method6992(class438);
	    class438s[0].method7026
		(Class438.method7020(Class438.method6994(class438_8_),
				     -f_12_ - f_15_));
	    class438s[1] = Class438.method7061();
	    class438s[1].method6992(class438);
	    class438s[1].method7026
		(Class438.method7020(Class438.method6994(class438_8_),
				     -f_12_ + f_15_));
	} else {
	    class438s[0] = Class438.method7061();
	    class438s[0].method6992(class438);
	    class438s[0].method7026
		(Class438.method7020(Class438.method6994(class438_8_),
				     -f_12_));
	    class438s[1] = null;
	}
    }
    
    public static void method6984(Class438 class438, Class438 class438_16_,
				  Class438 class438_17_, float f,
				  Class438[] class438s) {
	Class438 class438_18_ = Class438.method7055(class438, class438_17_);
	float f_19_ = Class438.method7027(class438_18_, class438_18_) - f * f;
	float f_20_ = Class438.method7027(class438_16_, class438_18_);
	float f_21_ = f_20_ * f_20_ - f_19_;
	if (f_21_ < 0.0F) {
	    Class438[] class438s_22_ = class438s;
	    class438s[1] = null;
	    class438s_22_[0] = null;
	} else if (f_21_ >= 9.765625E-4F) {
	    float f_23_ = (float) Math.sqrt((double) f_21_);
	    class438s[0] = Class438.method7061();
	    class438s[0].method6992(class438);
	    class438s[0].method7026
		(Class438.method7020(Class438.method6994(class438_16_),
				     -f_20_ - f_23_));
	    class438s[1] = Class438.method7061();
	    class438s[1].method6992(class438);
	    class438s[1].method7026
		(Class438.method7020(Class438.method6994(class438_16_),
				     -f_20_ + f_23_));
	} else {
	    class438s[0] = Class438.method7061();
	    class438s[0].method6992(class438);
	    class438s[0].method7026
		(Class438.method7020(Class438.method6994(class438_16_),
				     -f_20_));
	    class438s[1] = null;
	}
    }
    
    public static void method6985(Class438 class438, Class438 class438_24_,
				  Class438 class438_25_, float f,
				  Class438[] class438s) {
	Class438 class438_26_ = Class438.method7055(class438, class438_25_);
	float f_27_ = Class438.method7027(class438_26_, class438_26_) - f * f;
	float f_28_ = Class438.method7027(class438_24_, class438_26_);
	float f_29_ = f_28_ * f_28_ - f_27_;
	if (f_29_ < 0.0F) {
	    Class438[] class438s_30_ = class438s;
	    class438s[1] = null;
	    class438s_30_[0] = null;
	} else if (f_29_ >= 9.765625E-4F) {
	    float f_31_ = (float) Math.sqrt((double) f_29_);
	    class438s[0] = Class438.method7061();
	    class438s[0].method6992(class438);
	    class438s[0].method7026
		(Class438.method7020(Class438.method6994(class438_24_),
				     -f_28_ - f_31_));
	    class438s[1] = Class438.method7061();
	    class438s[1].method6992(class438);
	    class438s[1].method7026
		(Class438.method7020(Class438.method6994(class438_24_),
				     -f_28_ + f_31_));
	} else {
	    class438s[0] = Class438.method7061();
	    class438s[0].method6992(class438);
	    class438s[0].method7026
		(Class438.method7020(Class438.method6994(class438_24_),
				     -f_28_));
	    class438s[1] = null;
	}
    }
}
