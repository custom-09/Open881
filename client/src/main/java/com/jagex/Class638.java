/* Class638 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class638
{
    public int anInt8306;
    public Interface65 anInterface65_8307;
    public Interface65 anInterface65_8308;
    public Class603 aClass603_8309;
    public Class603 aClass603_8310;
    public Class603 aClass603_8311;
    public Interface65 anInterface65_8312;
    public Interface65 anInterface65_8313;
    public int anInt8314;
    public boolean aBool8315;
    
    void method10561(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-1962508200);
	    if (i == 0)
		break;
	    if (1 == i)
		anInterface65_8312
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (2 == i)
		anInterface65_8307
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (i == 3)
		anInterface65_8308
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (i == 4)
		anInterface65_8313
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (5 == i)
		aClass603_8310
		    = Class550.method9030(class534_sub40, 433996202);
	    else if (6 == i)
		aClass603_8309
		    = Class550.method9030(class534_sub40, 300006392);
	    else if (i == 7)
		aClass603_8311
		    = Class550.method9030(class534_sub40, 1028178300);
	    else if (8 == i)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (9 == i)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (i == 10)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (11 == i)
		aBool8315 = true;
	    else if (i == 12)
		anInt8306
		    = class534_sub40.method16533(-258848859) * 1557057133;
	    else if (13 == i)
		anInt8314
		    = class534_sub40.method16533(-258848859) * 1290083805;
	}
    }
    
    void method10562(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(1779872373);
	    if (i == 0)
		break;
	    if (1 == i)
		anInterface65_8312
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (2 == i)
		anInterface65_8307
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (i == 3)
		anInterface65_8308
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (i == 4)
		anInterface65_8313
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (5 == i)
		aClass603_8310
		    = Class550.method9030(class534_sub40, 2031693570);
	    else if (6 == i)
		aClass603_8309
		    = Class550.method9030(class534_sub40, 1430959146);
	    else if (i == 7)
		aClass603_8311
		    = Class550.method9030(class534_sub40, 735218613);
	    else if (8 == i)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (9 == i)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (i == 10)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (11 == i)
		aBool8315 = true;
	    else if (i == 12)
		anInt8306
		    = class534_sub40.method16533(-258848859) * 1557057133;
	    else if (13 == i)
		anInt8314
		    = class534_sub40.method16533(-258848859) * 1290083805;
	}
    }
    
    void method10563(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-230279495);
	    if (i == 0)
		break;
	    if (1 == i)
		anInterface65_8312
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (2 == i)
		anInterface65_8307
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (i == 3)
		anInterface65_8308
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (i == 4)
		anInterface65_8313
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (5 == i)
		aClass603_8310
		    = Class550.method9030(class534_sub40, 1458422937);
	    else if (6 == i)
		aClass603_8309
		    = Class550.method9030(class534_sub40, 1297402954);
	    else if (i == 7)
		aClass603_8311
		    = Class550.method9030(class534_sub40, 878231326);
	    else if (8 == i)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (9 == i)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (i == 10)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (11 == i)
		aBool8315 = true;
	    else if (i == 12)
		anInt8306
		    = class534_sub40.method16533(-258848859) * 1557057133;
	    else if (13 == i)
		anInt8314
		    = class534_sub40.method16533(-258848859) * 1290083805;
	}
    }
    
    void method10564(Class534_Sub40 class534_sub40, byte i) {
	for (;;) {
	    int i_0_ = class534_sub40.method16527(99893490);
	    if (i_0_ == 0)
		break;
	    if (1 == i_0_)
		anInterface65_8312
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (2 == i_0_)
		anInterface65_8307
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (i_0_ == 3)
		anInterface65_8308
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (i_0_ == 4)
		anInterface65_8313
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (5 == i_0_)
		aClass603_8310
		    = Class550.method9030(class534_sub40, 1983090245);
	    else if (6 == i_0_)
		aClass603_8309
		    = Class550.method9030(class534_sub40, 1084172633);
	    else if (i_0_ == 7)
		aClass603_8311
		    = Class550.method9030(class534_sub40, 233581059);
	    else if (8 == i_0_)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (9 == i_0_)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (i_0_ == 10)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (11 == i_0_)
		aBool8315 = true;
	    else if (i_0_ == 12)
		anInt8306
		    = class534_sub40.method16533(-258848859) * 1557057133;
	    else if (13 == i_0_)
		anInt8314
		    = class534_sub40.method16533(-258848859) * 1290083805;
	}
    }
    
    void method10565(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(1037236115);
	    if (i == 0)
		break;
	    if (1 == i)
		anInterface65_8312
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (2 == i)
		anInterface65_8307
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (i == 3)
		anInterface65_8308
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (i == 4)
		anInterface65_8313
		    = Class1.method509(class534_sub40, (byte) 1);
	    else if (5 == i)
		aClass603_8310
		    = Class550.method9030(class534_sub40, 452030787);
	    else if (6 == i)
		aClass603_8309
		    = Class550.method9030(class534_sub40, 502493371);
	    else if (i == 7)
		aClass603_8311
		    = Class550.method9030(class534_sub40, 483321511);
	    else if (8 == i)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (9 == i)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (i == 10)
		Class1.method509(class534_sub40, (byte) 1);
	    else if (11 == i)
		aBool8315 = true;
	    else if (i == 12)
		anInt8306
		    = class534_sub40.method16533(-258848859) * 1557057133;
	    else if (13 == i)
		anInt8314
		    = class534_sub40.method16533(-258848859) * 1290083805;
	}
    }
    
    public Class638(Class472 class472) {
	byte[] is = class472.method7738((Class617.aClass617_8092.anInt8096
					 * -448671533),
					(byte) -94);
	method10564(new Class534_Sub40(is), (byte) 5);
    }
    
    static final void method10566(Class669 class669, int i) {
	int i_1_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class684.method13950(i_1_, 1680867856);
    }
    
    static void method10567(int i, byte i_2_) {
	/* empty */
    }
    
    static void method10568(int i) {
	Class537.method8913(-1476785176);
    }
    
    public static void method10569(int i, byte i_3_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(12, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
}
