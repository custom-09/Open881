/* Class200_Sub15 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class200_Sub15 extends Class200
{
    int anInt9951;
    int anInt9952;
    static int[] anIntArray9953;
    
    public void method3847() {
	Class65.aClass192Array712[174532899 * anInt9951].method3775
	    (2088438307).method18523(-362629391 * anInt9952, true, 886999289);
    }
    
    public void method3846() {
	Class65.aClass192Array712[174532899 * anInt9951].method3775
	    (2088438307)
	    .method18523(-362629391 * anInt9952, true, -1079503930);
    }
    
    Class200_Sub15(Class534_Sub40 class534_sub40) {
	super(class534_sub40);
	anInt9951 = class534_sub40.method16529((byte) 1) * 805064331;
	anInt9952 = class534_sub40.method16529((byte) 1) * -1770817007;
    }
    
    public void method3845(int i) {
	Class65.aClass192Array712[174532899 * anInt9951].method3775
	    (2088438307).method18523(-362629391 * anInt9952, true, -203898286);
    }
    
    static final void method15604(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class575.method9752(class247, class243, class669, 286714898);
    }
    
    static final void method15605(Class669 class669, byte i) {
	class669.anInt8600 -= 617999126;
	if (class669.anIntArray8595[class669.anInt8600 * 2088438307]
	    != class669.anIntArray8595[1 + class669.anInt8600 * 2088438307])
	    class669.anInt8613
		+= (-793595371
		    * class669.anIntArray8591[662605117 * class669.anInt8613]);
    }
    
    static final void method15606(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class534_Sub20.method16195(class247, class243, false, 2, class669,
				   -847614543);
    }
}
