/* Class45 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

final class Class45 implements Interface52
{
    static Class209[] aClass209Array330;
    
    public Object method370(byte[] is, Class16 class16, boolean bool) {
	return Class254.aClass185_2683
		   .method3325(class16, Class178.method2943(is), bool);
    }
    
    public Object method372(byte[] is, Class16 class16, boolean bool) {
	return Class254.aClass185_2683
		   .method3325(class16, Class178.method2943(is), bool);
    }
    
    public Object method371(byte[] is, Class16 class16, boolean bool, int i) {
	return Class254.aClass185_2683
		   .method3325(class16, Class178.method2943(is), bool);
    }
    
    static final void method1101(Class669 class669, byte i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	boolean bool = ((class669.anIntArray8595
			 [(class669.anInt8600 -= 308999563) * 2088438307])
			== 1);
	Class263.method4831(class247, class243, class669,
			    (bool ? Class253.aClass253_2660
			     : Class253.aClass253_2661),
			    (byte) 33);
    }
    
    static final void method1102(Class669 class669, int i) {
	Class534_Sub36 class534_sub36
	    = ((Class534_Sub36)
	       (class669.anObjectArray8593
		[(class669.anInt8594 -= 1460193483) * 1485266147]));
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class534_sub36.anInt10797 * 599803149;
    }
    
    static final void method1103(Class669 class669, int i) {
	long l = Class250.method4604((byte) -113);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class532.method8878(l);
    }
}
