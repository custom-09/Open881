/* Class534_Sub18_Sub9 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub18_Sub9 extends Class534_Sub18
{
    boolean aBool11762;
    public int anInt11763;
    public String aString11764;
    public String aString11765;
    public int anInt11766;
    int anInt11767 = 332354067;
    public int anInt11768 = 682622281;
    public int anInt11769;
    public int anInt11770;
    public int anInt11771;
    public int anInt11772;
    Class700 aClass700_11773;
    
    public boolean method18260(int i, int i_0_, int i_1_, int[] is, int i_2_) {
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     class534_sub34 != null;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(1702807527)) {
	    if (class534_sub34.method16421(i, i_0_, i_1_, 753267043)) {
		class534_sub34.method16420(i_0_, i_1_, is, 1365728515);
		return true;
	    }
	}
	return false;
    }
    
    boolean method18261(int i, int i_3_, int i_4_) {
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     class534_sub34 != null;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(599450419)) {
	    if (class534_sub34.method16431(i, i_3_, 1867151630))
		return true;
	}
	return false;
    }
    
    public boolean method18262(int i, int i_5_, int i_6_, int[] is) {
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     class534_sub34 != null;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(1751111268)) {
	    if (class534_sub34.method16421(i, i_5_, i_6_, 1912547727)) {
		class534_sub34.method16420(i_5_, i_6_, is, -508066625);
		return true;
	    }
	}
	return false;
    }
    
    public boolean method18263(int i, int i_7_, int[] is, int i_8_) {
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     null != class534_sub34;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(1984018275)) {
	    if (class534_sub34.method16431(i, i_7_, 1903315594)) {
		class534_sub34.method16420(i, i_7_, is, -1048286664);
		return true;
	    }
	}
	return false;
    }
    
    public boolean method18264(int i, int i_9_, int[] is, int i_10_) {
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     null != class534_sub34;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(1087430427)) {
	    if (class534_sub34.method16422(i, i_9_, -1255500713)) {
		class534_sub34.method16423(i, i_9_, is, (byte) 55);
		return true;
	    }
	}
	return false;
    }
    
    boolean method18265(int i, int i_11_) {
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     class534_sub34 != null;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(1942873506)) {
	    if (class534_sub34.method16431(i, i_11_, 1911568036))
		return true;
	}
	return false;
    }
    
    boolean method18266(int i, int i_12_) {
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     class534_sub34 != null;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(1666356406)) {
	    if (class534_sub34.method16431(i, i_12_, 1776236093))
		return true;
	}
	return false;
    }
    
    Class534_Sub18_Sub9(int i, String string, String string_13_, int i_14_,
			int i_15_, boolean bool, int i_16_, int i_17_) {
	anInt11763 = -1286750208;
	anInt11770 = 0;
	anInt11771 = -1274920960;
	anInt11772 = 0;
	aBool11762 = true;
	anInt11766 = 1334334759 * i;
	aString11765 = string;
	aString11764 = string_13_;
	anInt11769 = -232489577 * i_14_;
	anInt11767 = -332354067 * i_15_;
	aBool11762 = bool;
	anInt11768 = -682622281 * i_16_;
	if (255 == 646871815 * anInt11768)
	    anInt11768 = 0;
	Class162.method2643(i_17_, -2076097785);
	aClass700_11773 = new Class700();
    }
    
    public boolean method18267(int i, int i_18_, int[] is) {
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     null != class534_sub34;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(798886399)) {
	    if (class534_sub34.method16422(i, i_18_, -1294554761)) {
		class534_sub34.method16423(i, i_18_, is, (byte) 10);
		return true;
	    }
	}
	return false;
    }
    
    void method18268() {
	anInt11763 = -1286750208;
	anInt11770 = 0;
	anInt11771 = -1274920960;
	anInt11772 = 0;
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     class534_sub34 != null;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(588580477)) {
	    if (-1934072337 * class534_sub34.anInt10728
		< anInt11763 * -192382841)
		anInt11763 = -338146983 * class534_sub34.anInt10728;
	    if (class534_sub34.anInt10730 * 105883891 > anInt11770 * 36458189)
		anInt11770 = -2014816577 * class534_sub34.anInt10730;
	    if (class534_sub34.anInt10723 * 1816020231
		< anInt11771 * -1004159431)
		anInt11771 = 1671961919 * class534_sub34.anInt10723;
	    if (class534_sub34.anInt10731 * -1577710271
		> 189160645 * anInt11772)
		anInt11772 = class534_sub34.anInt10731 * -1742082483;
	}
    }
    
    public boolean method18269(int i, int i_19_, int i_20_, int[] is) {
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     class534_sub34 != null;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(1886902210)) {
	    if (class534_sub34.method16421(i, i_19_, i_20_, 818014327)) {
		class534_sub34.method16420(i_19_, i_20_, is, 1430509534);
		return true;
	    }
	}
	return false;
    }
    
    static Class534_Sub18_Sub9 method18270(Class472 class472, int i) {
	Class534_Sub40 class534_sub40
	    = new Class534_Sub40(class472.method7743(0, i, -1167056847));
	return Class415.method6745(class534_sub40, i, (byte) -5);
    }
    
    public boolean method18271(int i, int i_21_, int[] is) {
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     null != class534_sub34;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(1311159450)) {
	    if (class534_sub34.method16431(i, i_21_, 1796057183)) {
		class534_sub34.method16420(i, i_21_, is, -796184325);
		return true;
	    }
	}
	return false;
    }
    
    public boolean method18272(int i, int i_22_, int i_23_, int[] is) {
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     class534_sub34 != null;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(1055517198)) {
	    if (class534_sub34.method16421(i, i_22_, i_23_, 1772275319)) {
		class534_sub34.method16420(i_22_, i_23_, is, 31966475);
		return true;
	    }
	}
	return false;
    }
    
    void method18273() {
	anInt11763 = -1286750208;
	anInt11770 = 0;
	anInt11771 = -1274920960;
	anInt11772 = 0;
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     class534_sub34 != null;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(1582591067)) {
	    if (-1934072337 * class534_sub34.anInt10728
		< anInt11763 * -192382841)
		anInt11763 = -338146983 * class534_sub34.anInt10728;
	    if (class534_sub34.anInt10730 * 105883891 > anInt11770 * 36458189)
		anInt11770 = -2014816577 * class534_sub34.anInt10730;
	    if (class534_sub34.anInt10723 * 1816020231
		< anInt11771 * -1004159431)
		anInt11771 = 1671961919 * class534_sub34.anInt10723;
	    if (class534_sub34.anInt10731 * -1577710271
		> 189160645 * anInt11772)
		anInt11772 = class534_sub34.anInt10731 * -1742082483;
	}
    }
    
    void method18274() {
	anInt11763 = -1286750208;
	anInt11770 = 0;
	anInt11771 = -1274920960;
	anInt11772 = 0;
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     class534_sub34 != null;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(1902170768)) {
	    if (-1934072337 * class534_sub34.anInt10728
		< anInt11763 * -192382841)
		anInt11763 = -338146983 * class534_sub34.anInt10728;
	    if (class534_sub34.anInt10730 * 105883891 > anInt11770 * 36458189)
		anInt11770 = -2014816577 * class534_sub34.anInt10730;
	    if (class534_sub34.anInt10723 * 1816020231
		< anInt11771 * -1004159431)
		anInt11771 = 1671961919 * class534_sub34.anInt10723;
	    if (class534_sub34.anInt10731 * -1577710271
		> 189160645 * anInt11772)
		anInt11772 = class534_sub34.anInt10731 * -1742082483;
	}
    }
    
    void method18275(int i) {
	anInt11763 = -1286750208;
	anInt11770 = 0;
	anInt11771 = -1274920960;
	anInt11772 = 0;
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     class534_sub34 != null;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(719683587)) {
	    if (-1934072337 * class534_sub34.anInt10728
		< anInt11763 * -192382841)
		anInt11763 = -338146983 * class534_sub34.anInt10728;
	    if (class534_sub34.anInt10730 * 105883891 > anInt11770 * 36458189)
		anInt11770 = -2014816577 * class534_sub34.anInt10730;
	    if (class534_sub34.anInt10723 * 1816020231
		< anInt11771 * -1004159431)
		anInt11771 = 1671961919 * class534_sub34.anInt10723;
	    if (class534_sub34.anInt10731 * -1577710271
		> 189160645 * anInt11772)
		anInt11772 = class534_sub34.anInt10731 * -1742082483;
	}
    }
    
    public boolean method18276(int i, int i_24_, int i_25_, int[] is) {
	for (Class534_Sub34 class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14135((byte) -1);
	     class534_sub34 != null;
	     class534_sub34
		 = (Class534_Sub34) aClass700_11773.method14139(1202206837)) {
	    if (class534_sub34.method16421(i, i_24_, i_25_, 2037014240)) {
		class534_sub34.method16420(i_24_, i_25_, is, 394161585);
		return true;
	    }
	}
	return false;
    }
    
    static Class534_Sub18_Sub9 method18277(Class472 class472, int i) {
	Class534_Sub40 class534_sub40
	    = new Class534_Sub40(class472.method7743(0, i, -1996523094));
	return Class415.method6745(class534_sub40, i, (byte) -38);
    }
    
    static Class534_Sub18_Sub9 method18278(Class534_Sub40 class534_sub40,
					   int i) {
	Class534_Sub18_Sub9 class534_sub18_sub9
	    = new Class534_Sub18_Sub9(i,
				      class534_sub40.method16541((byte) -20),
				      class534_sub40.method16541((byte) -50),
				      class534_sub40.method16533(-258848859),
				      class534_sub40.method16533(-258848859),
				      (class534_sub40.method16527(-1847876238)
				       == 1),
				      class534_sub40.method16527(1236744297),
				      class534_sub40.method16527(-1638908457));
	int i_26_ = class534_sub40.method16527(1729027932);
	for (int i_27_ = 0; i_27_ < i_26_; i_27_++)
	    class534_sub18_sub9.aClass700_11773.method14131
		(new Class534_Sub34(class534_sub40.method16527(1713443260),
				    class534_sub40.method16529((byte) 1),
				    class534_sub40.method16529((byte) 1),
				    class534_sub40.method16529((byte) 1),
				    class534_sub40.method16529((byte) 1),
				    class534_sub40.method16529((byte) 1),
				    class534_sub40.method16529((byte) 1),
				    class534_sub40.method16529((byte) 1),
				    class534_sub40.method16529((byte) 1)),
		 (short) 789);
	class534_sub18_sub9.method18275(-1385982431);
	return class534_sub18_sub9;
    }
}
