/* Class236 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class236 implements Interface26
{
    Class212 aClass212_2371;
    Class213 this$0;
    static Class247 aClass247_2372;
    static Class16 aClass16_2373;
    
    public void method173(Class214 class214, int i) {
	class214.method4094(aClass212_2371, 741945062);
    }
    
    public void method174(Class214 class214) {
	class214.method4094(aClass212_2371, 1088964639);
    }
    
    Class236(Class213 class213, Class534_Sub40 class534_sub40) {
	this$0 = class213;
	boolean bool = class534_sub40.method16527(1675734012) != 255;
	if (bool)
	    class534_sub40.anInt10811 -= -1387468933;
	aClass212_2371 = new Class212(class534_sub40, bool, true,
				      class213.anInterface27_2280);
    }
    
    public void method172(Class214 class214) {
	class214.method4094(aClass212_2371, 710580411);
    }
    
    static final void method4409(Class669 class669, byte i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class15 class15
	    = (Class15) Class531.aClass44_Sub7_7135.method91(i_0_, 4568198);
	int i_1_;
	if (class15.aBool181)
	    i_1_ = class15.anInt182 * 510229545;
	else if (class15.aBool106)
	    i_1_ = 1376086885 * Class700.aClass638_8806.anInt8306;
	else
	    i_1_ = Class700.aClass638_8806.anInt8314 * -629529995;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = i_1_;
    }
    
    static final void method4410(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_2_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_3_
	    = class669.anIntArray8595[1 + 2088438307 * class669.anInt8600];
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (Class618.aClass458_8101.method7433(i_2_, -1203343574)
	       .aCharArray11894[i_3_]);
    }
    
    static final void method4411(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10733
		  .method16935(-1807368365);
    }
    
    static final void method4412(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class65.anInt671 * 490572819;
    }
}
