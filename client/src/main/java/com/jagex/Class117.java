/* Class117 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class117
{
    int anInt1402;
    float aFloat1403 = 1.0F;
    float aFloat1404 = 1.0F;
    int anInt1405;
    int anInt1406;
    int anInt1407;
    int anInt1408;
    int anInt1409;
    float aFloat1410;
    int anInt1411;
    int anInt1412;
    
    Class117 method2118() {
	return new Class117(-150900843 * anInt1411, aFloat1403, aFloat1404,
			    -169572407 * anInt1405, anInt1406 * -653522829,
			    anInt1409 * -133325595);
    }
    
    Class117(int i, float f, float f_0_, int i_1_, int i_2_, int i_3_) {
	anInt1411 = i * 874668989;
	aFloat1403 = f;
	aFloat1404 = f_0_;
	anInt1405 = 1831486073 * i_1_;
	anInt1406 = -108929861 * i_2_;
	anInt1409 = 1832042733 * i_3_;
    }
    
    void method2119(Class117 class117_4_, int i) {
	aFloat1403 = class117_4_.aFloat1403;
	aFloat1404 = class117_4_.aFloat1404;
	anInt1405 = 1 * class117_4_.anInt1405;
	anInt1406 = class117_4_.anInt1406 * 1;
	anInt1411 = 1 * class117_4_.anInt1411;
	anInt1409 = 1 * class117_4_.anInt1409;
    }
    
    Class117 method2120(byte i) {
	return new Class117(-150900843 * anInt1411, aFloat1403, aFloat1404,
			    -169572407 * anInt1405, anInt1406 * -653522829,
			    anInt1409 * -133325595);
    }
    
    Class117(int i) {
	anInt1411 = i * 874668989;
    }
    
    Class117 method2121() {
	return new Class117(-150900843 * anInt1411, aFloat1403, aFloat1404,
			    -169572407 * anInt1405, anInt1406 * -653522829,
			    anInt1409 * -133325595);
    }
    
    static final void method2122(Class669 class669, int i) {
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = (class669.aClass534_Sub18_Sub8_8614.anObjectArray11753
	       [class669.anInt8613 * 662605117]);
    }
    
    static final void method2123(boolean bool, Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	if (bool)
	    Class658.method10913(class243, class247, (byte) 114);
	else
	    Class97.method1828(class243, class247, 2039997927);
    }
    
    static final void method2124(Class669 class669, byte i) {
	int i_5_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_5_, 358322111);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_5_ >> 16];
	Class654_Sub1_Sub1_Sub1.method18620(class247, class243, class669,
					    (byte) 107);
    }
    
    static final void method2125(Class669 class669, short i) {
	int i_6_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	int i_7_ = Class11.method612(client.aClass665_11211,
				     Class668.aClass668_8583, i_6_, (byte) 62);
	int i_8_
	    = Class488.method7999(client.aClass665_11211,
				  Class668.aClass668_8583, i_6_, 353134756);
	if (client.anInt11039 * -1850530127 != 9
	    || Class202.method3865((byte) 0))
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
	else
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= Class312_Sub1_Sub1.method18102(i_6_, string, i_7_, i_8_,
						 (byte) -34) ? 1 : 0;
    }
    
    static final void method2126(byte i) {
	Class454.method7416(client.aClass247_11044, -842547161);
	Class621.anInt8126 += 1713249661;
	if (!client.aBool11244 || !client.aBool11103) {
	    if (Class621.anInt8126 * 273482709 > 1) {
		client.aClass247_11044 = null;
		client.aClass247_11342 = null;
	    }
	} else {
	    int i_9_ = Class81.aClass563_834.method9493(-1474749186);
	    int i_10_ = Class81.aClass563_834.method9477(614048422);
	    i_9_ -= client.anInt11131 * -1626933515;
	    i_10_ -= client.anInt11035 * 1953389231;
	    if (i_9_ < client.anInt11236 * -169004095)
		i_9_ = client.anInt11236 * -169004095;
	    if (client.aClass247_11044.anInt2468 * -881188269 + i_9_
		> (client.anInt11236 * -169004095
		   + client.anInt11239 * -1152577469))
		i_9_ = (client.anInt11236 * -169004095
			+ client.anInt11239 * -1152577469
			- client.aClass247_11044.anInt2468 * -881188269);
	    if (i_10_ < -171218513 * client.anInt11158)
		i_10_ = client.anInt11158 * -171218513;
	    if (i_10_ + -1279656873 * client.aClass247_11044.anInt2469
		> (-171218513 * client.anInt11158
		   + client.anInt11243 * 1976194639))
		i_10_ = (-171218513 * client.anInt11158
			 + client.anInt11243 * 1976194639
			 - -1279656873 * client.aClass247_11044.anInt2469);
	    int i_11_;
	    int i_12_;
	    if (Class79.aClass247_830 == client.aClass247_11342) {
		i_11_ = i_9_;
		i_12_ = i_10_;
	    } else {
		i_11_ = (client.aClass247_11342.anInt2478 * -488164841
			 + (i_9_ - client.anInt11236 * -169004095));
		i_12_ = (i_10_ - client.anInt11158 * -171218513
			 + 2142374941 * client.aClass247_11342.anInt2479);
	    }
	    if (!Class81.aClass563_834.method9475((short) -11176)) {
		if (client.aBool11247) {
		    Exception_Sub3.method17943((byte) 1);
		    if (null != client.aClass247_11044.anObjectArray2571) {
			Class534_Sub41 class534_sub41 = new Class534_Sub41();
			class534_sub41.aClass247_10818
			    = client.aClass247_11044;
			class534_sub41.anInt10822 = i_11_ * -1310455037;
			class534_sub41.anInt10816 = i_12_ * 1212820407;
			Class247 class247
			    = client.method17393(client.aClass247_11044);
			Class247 class247_13_ = client.aClass247_11283;
			boolean bool = false;
			for (/**/;
			     (null != class247_13_
			      && class247_13_.anInt2472 * -742015869 != -1
			      && null != class247);
			     class247_13_
				 = (Class44_Sub11.aClass243Array11006
				    [(-1278450723 * class247_13_.anInt2454
				      >> 16)]
				    .aClass247Array2412
				    [(class247_13_.anInt2472 * -742015869
				      & 0xffff)])) {
			    if (class247.anInt2454 * -1278450723
				== class247_13_.anInt2454 * -1278450723) {
				bool = true;
				break;
			    }
			}
			if (null != class247_13_ && class247 != null
			    && class247 != Class79.aClass247_830 && !bool)
			    class534_sub41.aClass247_10824
				= Class79.aClass247_830;
			else
			    class534_sub41.aClass247_10824
				= client.aClass247_11283;
			class534_sub41.anObjectArray10819
			    = client.aClass247_11044.anObjectArray2571;
			Class94.method1764(class534_sub41, -960954150);
		    }
		    if (null != client.aClass247_11283
			&& client.method17393(client.aClass247_11044) != null)
			Class598.method9938(client.aClass247_11044,
					    client.aClass247_11283,
					    -727171052);
		} else if ((client.anInt11213 * 2117088631 == 1
			    || Class331.method5856((byte) 62))
			   && Class72.anInt765 * 324852453 > 2)
		    Class32.method898((-1626933515 * client.anInt11131
				       + 33779563 * client.anInt11245),
				      (client.anInt11035 * 1953389231
				       + 1729150661 * client.anInt11246),
				      894411071);
		else if (Class653.method10801((byte) 5))
		    Class32.method898((client.anInt11245 * 33779563
				       + -1626933515 * client.anInt11131),
				      (1953389231 * client.anInt11035
				       + 1729150661 * client.anInt11246),
				      894411071);
		client.aClass247_11044 = null;
		client.aClass247_11342 = null;
	    } else {
		if (Class621.anInt8126 * 273482709
		    > 73463143 * client.aClass247_11044.anInt2558) {
		    int i_14_ = i_9_ - 33779563 * client.anInt11245;
		    int i_15_ = i_10_ - 1729150661 * client.anInt11246;
		    if (i_14_ > 1344073003 * client.aClass247_11044.anInt2557
			|| i_14_ < -(client.aClass247_11044.anInt2557
				     * 1344073003)
			|| (i_15_
			    > client.aClass247_11044.anInt2557 * 1344073003)
			|| i_15_ < -(1344073003
				     * client.aClass247_11044.anInt2557))
			client.aBool11247 = true;
		}
		if (client.aClass247_11044.anObjectArray2570 != null
		    && client.aBool11247) {
		    Class534_Sub41 class534_sub41 = new Class534_Sub41();
		    class534_sub41.aClass247_10818 = client.aClass247_11044;
		    class534_sub41.anInt10822 = -1310455037 * i_11_;
		    class534_sub41.anInt10816 = 1212820407 * i_12_;
		    class534_sub41.anObjectArray10819
			= client.aClass247_11044.anObjectArray2570;
		    Class94.method1764(class534_sub41, 1916202618);
		}
	    }
	}
    }
    
    static final void method2127(Class669 class669, int i) {
	class669.anInt8596 -= -2111195934;
	if (class669.aLongArray8587[1572578961 * class669.anInt8596]
	    >= class669.aLongArray8587[1572578961 * class669.anInt8596 + 1])
	    class669.anInt8613
		+= (-793595371
		    * class669.anIntArray8591[class669.anInt8613 * 662605117]);
    }
}
