/* Class465 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.io.IOException;

public class Class465
{
    public static Class465 aClass465_5114;
    public static Class465 aClass465_5115 = new Class465(0);
    static Class465 aClass465_5116;
    public static Class465 aClass465_5117;
    public static Class465 aClass465_5118;
    public static Class465 aClass465_5119;
    static Class465 aClass465_5120;
    static Class465 aClass465_5121;
    static Class465 aClass465_5122;
    public int anInt5123;
    
    Class465(int i) {
	anInt5123 = 1500428193 * i;
    }
    
    static {
	aClass465_5114 = new Class465(1);
	aClass465_5119 = new Class465(2);
	aClass465_5117 = new Class465(3);
	aClass465_5118 = new Class465(4);
	aClass465_5116 = new Class465(5);
	aClass465_5120 = new Class465(6);
	aClass465_5121 = new Class465(7);
	aClass465_5122 = new Class465(8);
    }
    
    static final void method7569(Class669 class669, int i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_0_, 1349632572);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_0_ >> 16];
	Class517.method8635(class247, class243, class669, (byte) -3);
    }
    
    public static String method7570(CharSequence charsequence, Class76 class76,
				    byte i) {
	if (null == charsequence)
	    return null;
	int i_1_ = 0;
	int i_2_;
	for (i_2_ = charsequence.length();
	     i_1_ < i_2_ && Class330.method5855(charsequence.charAt(i_1_),
						(short) -9007);
	     i_1_++) {
	    /* empty */
	}
	for (/**/;
	     i_2_ > i_1_ && Class330.method5855(charsequence.charAt(i_2_ - 1),
						(short) -2652);
	     i_2_--) {
	    /* empty */
	}
	int i_3_ = i_2_ - i_1_;
	if (i_3_ < 1 || i_3_ > method7573(class76, 665612131))
	    return null;
	StringBuilder stringbuilder = new StringBuilder(i_3_);
	for (int i_4_ = i_1_; i_4_ < i_2_; i_4_++) {
	    char c = charsequence.charAt(i_4_);
	    if (Class288.method5279(c, (byte) 93)) {
		char c_5_ = Class578.method9798(c, -119520913);
		if (c_5_ != 0)
		    stringbuilder.append(c_5_);
	    }
	}
	if (stringbuilder.length() == 0)
	    return null;
	return stringbuilder.toString();
    }
    
    public static void method7571(int i) {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10758, 2,
	     -88225844);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub17_10759, 2,
	     -564433503);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub10_10751, 1,
	     828232417);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub25_10750, 1,
	     -2057241897);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub24_10756, 1,
	     832372878);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub9_10748),
						       1, 791877895);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub1_10762),
						       1, 1858717238);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763, 1,
	     554414700);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub6_10743),
						       1, -298744654);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753, 1,
	     -666501152);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub3_10767),
						       0, -1203951900);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub11_10749, 1,
	     1129132532);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10781, 0,
	     720312649);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10757, 0,
	     -1728931084);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub5_10737),
						       1, -1344329085);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub22_10745,
	     Class302.aClass302_3246.anInt3244 * 1453209707, 1233283582);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub19_10741, 0,
	     -754326286);
	if (null != Class254.aClass185_2683
	    && Class254.aClass185_2683.method3534()
	    && Class254.aClass185_2683.method3409())
	    Class254.aClass185_2683.method3359();
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub33_10765, 1,
	     1229915923);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub30_10739, 1,
	     227668282);
	Class480.method7924(-731201667);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub20_10742, 1,
	     -35905498);
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub37_10760, 3,
	     -521465379);
	Class635.method10538(-2050267373);
	client.aClass512_11100.method8501((byte) 11).method10157(1270099968);
	client.aBool11059 = true;
    }
    
    static final byte[] method7572(byte[] is, byte i) {
	Class534_Sub40 class534_sub40 = new Class534_Sub40(is);
	Class466 class466 = new Class466(class534_sub40);
	Class473 class473 = class466.method7578(-286691959);
	int i_6_ = class466.method7576((byte) 7);
	if (i_6_ < 0 || (852448745 * Class472.anInt5164 != 0
			 && i_6_ > 852448745 * Class472.anInt5164))
	    throw new RuntimeException();
	if (class473 == Class473.aClass473_5172) {
	    byte[] is_7_ = new byte[i_6_];
	    class534_sub40.method16735(is_7_, 0, i_6_, (short) -15789);
	    return is_7_;
	}
	int i_8_ = class466.method7577(1971173011);
	if (i_8_ < 0 || (0 != 852448745 * Class472.anInt5164
			 && i_8_ > 852448745 * Class472.anInt5164))
	    throw new RuntimeException();
	byte[] is_9_;
	if (class473 == Class473.aClass473_5169) {
	    is_9_ = new byte[i_8_];
	    Class708.method14261(is_9_, i_8_, is, i_6_, 9);
	} else if (class473 == Class473.aClass473_5167) {
	    is_9_ = new byte[i_8_];
	    synchronized (Class472.aClass80_5157) {
		Class472.aClass80_5157.method1625(class534_sub40, is_9_,
						  1854731297);
	    }
	} else if (Class473.aClass473_5168 == class473) {
	    try {
		is_9_
		    = Class666.method11020(class534_sub40, i_8_, -1599107257);
	    } catch (IOException ioexception) {
		throw new RuntimeException(ioexception);
	    }
	} else
	    throw new RuntimeException();
	return is_9_;
    }
    
    static final int method7573(Class76 class76, int i) {
	if (null == class76)
	    return 12;
	switch (-912665199 * class76.anInt813) {
	case 7:
	    return 20;
	default:
	    return 12;
	}
    }
}
