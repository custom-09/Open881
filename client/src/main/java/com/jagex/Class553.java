/* Class553 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class553
{
    static final int anInt7340 = 1;
    static final int anInt7341 = 8;
    static final int anInt7342 = 2;
    static final int anInt7343 = 16;
    static final int anInt7344 = 62;
    static final int anInt7345 = 63;
    
    Class553() throws Throwable {
	throw new Error();
    }
    
    static final void method9102(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_0_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_1_
	    = class669.anIntArray8595[2088438307 * class669.anInt8600 + 1];
	if (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.aClass631_12226
	    != null)
	    Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.aClass631_12226
		.method10438(i_0_, i_1_, (byte) 84);
    }
    
    static final void method9103(Class669 class669, int i) {
	int i_2_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_2_, 407921608);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_2_ >> 16];
	Class181.method2981(class247, class243, class669, (short) 19928);
    }
    
    static final void method9104(Class669 class669, int i) {
	class669.anInt8600 -= 308999563;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.method18603
		  (1862409252).method9921((byte) -60);
    }
    
    public static void method9105(int i, String string, int i_3_) {
	Class272.method5067(i, 0, "", "", "", string, null, (byte) 5);
    }
    
    static final void method9106(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_4_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_5_
	    = class669.anIntArray8595[1 + 2088438307 * class669.anInt8600];
	Class703.method14219(i_4_, new Class534_Sub37(i_5_, 3), null, true,
			     -1659345790);
    }
}
