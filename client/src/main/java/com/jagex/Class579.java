/* Class579 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class579 implements Interface13
{
    public int anInt7745;
    static final int anInt7746 = 70;
    public int anInt7747 = 1558789815;
    Class581 aClass581_7748;
    public int anInt7749;
    public int anInt7750;
    public int anInt7751;
    public int anInt7752;
    public int anInt7753 = 353580339;
    public int anInt7754;
    public int anInt7755;
    public int anInt7756;
    public int anInt7757;
    public static Class163 aClass163_7758;
    
    void method9800(Class534_Sub40 class534_sub40, int i) {
	if (i == 1)
	    class534_sub40.method16529((byte) 1);
	else if (i == 2)
	    anInt7747 = class534_sub40.method16527(1037632292) * -1004467639;
	else if (3 == i)
	    anInt7753 = class534_sub40.method16527(-1045860748) * 1870960589;
	else if (i == 4)
	    anInt7749 = 0;
	else if (i == 5)
	    anInt7751 = class534_sub40.method16529((byte) 1) * -2087205033;
	else if (6 == i)
	    class534_sub40.method16527(1988676477);
	else if (7 == i)
	    anInt7752 = class534_sub40.method16550((byte) -49) * 280918055;
	else if (i == 8)
	    anInt7756 = class534_sub40.method16550((byte) 3) * 1901011869;
	else if (9 == i)
	    anInt7754 = class534_sub40.method16550((byte) 1) * 1251445241;
	else if (i == 10)
	    anInt7755 = class534_sub40.method16550((byte) 35) * -1454241591;
	else if (i == 11)
	    anInt7749 = class534_sub40.method16529((byte) 1) * 322066947;
	else if (i == 12)
	    anInt7745 = class534_sub40.method16550((byte) 30) * -1654398337;
	else if (i == 13)
	    anInt7757 = class534_sub40.method16550((byte) 19) * -1575953609;
    }
    
    public void method79(Class534_Sub40 class534_sub40, byte i) {
	for (;;) {
	    int i_0_ = class534_sub40.method16527(-1269724882);
	    if (i_0_ == 0)
		break;
	    method9801(class534_sub40, i_0_, -1189468530);
	}
    }
    
    void method9801(Class534_Sub40 class534_sub40, int i, int i_1_) {
	if (i == 1)
	    class534_sub40.method16529((byte) 1);
	else if (i == 2)
	    anInt7747 = class534_sub40.method16527(-1722037914) * -1004467639;
	else if (3 == i)
	    anInt7753 = class534_sub40.method16527(-237103265) * 1870960589;
	else if (i == 4)
	    anInt7749 = 0;
	else if (i == 5)
	    anInt7751 = class534_sub40.method16529((byte) 1) * -2087205033;
	else if (6 == i)
	    class534_sub40.method16527(-1075833602);
	else if (7 == i)
	    anInt7752 = class534_sub40.method16550((byte) 47) * 280918055;
	else if (i == 8)
	    anInt7756 = class534_sub40.method16550((byte) 20) * 1901011869;
	else if (9 == i)
	    anInt7754 = class534_sub40.method16550((byte) 11) * 1251445241;
	else if (i == 10)
	    anInt7755 = class534_sub40.method16550((byte) 60) * -1454241591;
	else if (i == 11)
	    anInt7749 = class534_sub40.method16529((byte) 1) * 322066947;
	else if (i == 12)
	    anInt7745 = class534_sub40.method16550((byte) -26) * -1654398337;
	else if (i == 13)
	    anInt7757 = class534_sub40.method16550((byte) 10) * -1575953609;
    }
    
    public Class163 method9802(Class185 class185, int i, byte i_2_) {
	if (i < 0)
	    return null;
	Class163 class163
	    = (Class163) aClass581_7748.aClass203_7763.method3871((long) i);
	if (null == class163) {
	    method9804(class185, (byte) -54);
	    class163 = ((Class163)
			aClass581_7748.aClass203_7763.method3871((long) i));
	}
	return class163;
    }
    
    void method9803(Class534_Sub40 class534_sub40, int i) {
	if (i == 1)
	    class534_sub40.method16529((byte) 1);
	else if (i == 2)
	    anInt7747 = class534_sub40.method16527(179339249) * -1004467639;
	else if (3 == i)
	    anInt7753 = class534_sub40.method16527(-1488915304) * 1870960589;
	else if (i == 4)
	    anInt7749 = 0;
	else if (i == 5)
	    anInt7751 = class534_sub40.method16529((byte) 1) * -2087205033;
	else if (6 == i)
	    class534_sub40.method16527(-882391772);
	else if (7 == i)
	    anInt7752 = class534_sub40.method16550((byte) -11) * 280918055;
	else if (i == 8)
	    anInt7756 = class534_sub40.method16550((byte) -58) * 1901011869;
	else if (9 == i)
	    anInt7754 = class534_sub40.method16550((byte) -28) * 1251445241;
	else if (i == 10)
	    anInt7755 = class534_sub40.method16550((byte) -68) * -1454241591;
	else if (i == 11)
	    anInt7749 = class534_sub40.method16529((byte) 1) * 322066947;
	else if (i == 12)
	    anInt7745 = class534_sub40.method16550((byte) -67) * -1654398337;
	else if (i == 13)
	    anInt7757 = class534_sub40.method16550((byte) -1) * -1575953609;
    }
    
    void method9804(Class185 class185, byte i) {
	method9807(class185, -1494517865 * anInt7752, 402737018);
	method9807(class185, 930052789 * anInt7756, 1375761907);
	method9807(class185, 62974025 * anInt7754, -938416183);
	method9807(class185, 1463849337 * anInt7755, -486552130);
	method9807(class185, anInt7745 * -1550459521, -849051976);
	method9807(class185, anInt7757 * 1417005703, -1652855793);
    }
    
    public void method82(int i) {
	/* empty */
    }
    
    public void method80(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-461977333);
	    if (i == 0)
		break;
	    method9801(class534_sub40, i, -1793899881);
	}
    }
    
    public void method81(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-1654839985);
	    if (i == 0)
		break;
	    method9801(class534_sub40, i, -1943058896);
	}
    }
    
    Class579(int i, Class581 class581) {
	anInt7749 = -322066947;
	anInt7750 = -1942788665;
	anInt7751 = -75464246;
	anInt7752 = -280918055;
	anInt7756 = -1901011869;
	anInt7754 = -1251445241;
	anInt7755 = 1454241591;
	anInt7745 = 1654398337;
	anInt7757 = 1575953609;
	aClass581_7748 = class581;
    }
    
    public void method83() {
	/* empty */
    }
    
    public void method84() {
	/* empty */
    }
    
    void method9805(Class534_Sub40 class534_sub40, int i) {
	if (i == 1)
	    class534_sub40.method16529((byte) 1);
	else if (i == 2)
	    anInt7747 = class534_sub40.method16527(-1391533082) * -1004467639;
	else if (3 == i)
	    anInt7753 = class534_sub40.method16527(844748831) * 1870960589;
	else if (i == 4)
	    anInt7749 = 0;
	else if (i == 5)
	    anInt7751 = class534_sub40.method16529((byte) 1) * -2087205033;
	else if (6 == i)
	    class534_sub40.method16527(476255258);
	else if (7 == i)
	    anInt7752 = class534_sub40.method16550((byte) 54) * 280918055;
	else if (i == 8)
	    anInt7756 = class534_sub40.method16550((byte) -76) * 1901011869;
	else if (9 == i)
	    anInt7754 = class534_sub40.method16550((byte) 83) * 1251445241;
	else if (i == 10)
	    anInt7755 = class534_sub40.method16550((byte) -41) * -1454241591;
	else if (i == 11)
	    anInt7749 = class534_sub40.method16529((byte) 1) * 322066947;
	else if (i == 12)
	    anInt7745 = class534_sub40.method16550((byte) -40) * -1654398337;
	else if (i == 13)
	    anInt7757 = class534_sub40.method16550((byte) 86) * -1575953609;
    }
    
    public Class163 method9806(Class185 class185, int i) {
	if (i < 0)
	    return null;
	Class163 class163
	    = (Class163) aClass581_7748.aClass203_7763.method3871((long) i);
	if (null == class163) {
	    method9804(class185, (byte) -19);
	    class163 = ((Class163)
			aClass581_7748.aClass203_7763.method3871((long) i));
	}
	return class163;
    }
    
    public void method78(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(1676976415);
	    if (i == 0)
		break;
	    method9801(class534_sub40, i, -1002731223);
	}
    }
    
    void method9807(Class185 class185, int i, int i_3_) {
	Class472 class472 = aClass581_7748.aClass472_7764;
	if (i >= 0
	    && aClass581_7748.aClass203_7763.method3871((long) i) == null
	    && class472.method7670(i, (byte) -52)) {
	    Class169 class169 = Class178.method2945(class472, i);
	    aClass581_7748.aClass203_7763
		.method3893(class185.method3279(class169, true), (long) i);
	}
    }
    
    public Class163 method9808(Class185 class185, int i) {
	if (i < 0)
	    return null;
	Class163 class163
	    = (Class163) aClass581_7748.aClass203_7763.method3871((long) i);
	if (null == class163) {
	    method9804(class185, (byte) -42);
	    class163 = ((Class163)
			aClass581_7748.aClass203_7763.method3871((long) i));
	}
	return class163;
    }
    
    void method9809(Class185 class185, int i) {
	Class472 class472 = aClass581_7748.aClass472_7764;
	if (i >= 0
	    && aClass581_7748.aClass203_7763.method3871((long) i) == null
	    && class472.method7670(i, (byte) -42)) {
	    Class169 class169 = Class178.method2945(class472, i);
	    aClass581_7748.aClass203_7763
		.method3893(class185.method3279(class169, true), (long) i);
	}
    }
    
    void method9810(Class185 class185) {
	method9807(class185, -1494517865 * anInt7752, -1565579199);
	method9807(class185, 930052789 * anInt7756, -1648392374);
	method9807(class185, 62974025 * anInt7754, 138478753);
	method9807(class185, 1463849337 * anInt7755, -846608358);
	method9807(class185, anInt7745 * -1550459521, 1900615596);
	method9807(class185, anInt7757 * 1417005703, 1153892687);
    }
    
    public static int method9811(Class443 class443, int i) {
	Class438 class438 = Class438.method6996(0.0F, 0.0F, 1.0F);
	class438.method7021(class443);
	double d = Math.atan2((double) class438.aFloat4864,
			      (double) class438.aFloat4865);
	class438.method7074();
	if (d < 0.0)
	    d = 3.141592653589793 + (3.141592653589793 + d);
	return (int) (d / 6.283185307179586 * 16384.0) & 0x3fff;
    }
}
