/* Class711 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class711
{
    boolean aBool8861;
    public static final int anInt8862 = 1;
    public static final int anInt8863 = 2;
    Class205 aClass205_8864;
    int anInt8865;
    boolean aBool8866;
    int anInt8867;
    public static final int anInt8868 = 0;
    int anInt8869;
    boolean aBool8870 = false;
    int anInt8871;
    int anInt8872;
    int anInt8873;
    Class693 aClass693_8874;
    Class693 aClass693_8875;
    
    public final boolean method14325() {
	return 0 != 1500416377 * anInt8865;
    }
    
    public final void method14326(Class711 class711_0_, byte i) {
	aClass205_8864 = class711_0_.aClass205_8864;
	aBool8870 = class711_0_.aBool8870;
	aBool8866 = class711_0_.aBool8866;
	anInt8865 = class711_0_.anInt8865 * 1;
	anInt8867 = 1 * class711_0_.anInt8867;
	anInt8871 = 1 * class711_0_.anInt8871;
	anInt8873 = class711_0_.anInt8873 * 1;
	anInt8869 = 1 * class711_0_.anInt8869;
	method14351(-1070830455);
    }
    
    public final Class205 method14327() {
	return aClass205_8864;
    }
    
    public final void method14328(int i, int i_1_) {
	method14334(i, i_1_, 0, false, (byte) -98);
    }
    
    public final int method14329(int i) {
	return (aClass205_8864 != null ? aClass205_8864.anInt2208 * -1686227895
		: -1);
    }
    
    public final void method14330(int i, int i_2_) {
	method14334(i, 0, 0, false, (byte) -47);
    }
    
    public final void method14331(int i, int i_3_, int i_4_) {
	method14334(i, i_3_, 0, false, (byte) -53);
    }
    
    public final void method14332(int i, boolean bool, int i_5_) {
	method14334(i, 0, 0, bool, (byte) -98);
    }
    
    public final void method14333(int i, byte i_6_) {
	anInt8865 = i * -1150119735;
    }
    
    public final void method14334(int i, int i_7_, int i_8_, boolean bool,
				  byte i_9_) {
	method14335(i, i_7_, i_8_, bool, false, -2145686133);
    }
    
    final void method14335(int i, int i_10_, int i_11_, boolean bool,
			   boolean bool_12_, int i_13_) {
	if (i != method14329(1349910878)) {
	    if (i != -1) {
		if (aClass205_8864 != null
		    && i == aClass205_8864.anInt2208 * -1686227895) {
		    if (0 == aClass205_8864.anInt2223 * 629077835)
			return;
		} else {
		    aClass205_8864 = (Class205) Class468.aClass44_Sub1_5146
						    .method91(i, 1152495476);
		    if (aClass205_8864 == null
			|| aClass205_8864.anIntArray2209 == null) {
			aClass205_8864 = null;
			return;
		    }
		}
		anInt8867 = 0;
		anInt8865 = -1150119735 * i_10_;
		anInt8872 = i_11_ * 1768349497;
		aBool8861 = bool_12_;
		if (bool) {
		    anInt8871 = ((int) (Math.random()
					* (double) (aClass205_8864
						    .anIntArray2209).length)
				 * 49126185);
		    anInt8869
			= ((int) (Math.random()
				  * (double) (aClass205_8864.anIntArray2211
					      [anInt8871 * -1485754087]))
			   * 1165224707);
		} else {
		    anInt8871 = 0;
		    anInt8869 = 0;
		}
		anInt8873 = -362185365 + 1049213555 * anInt8871;
		if (892520259 * anInt8873 < 0
		    || (anInt8873 * 892520259
			>= aClass205_8864.anIntArray2209.length))
		    anInt8873 = 362185365;
		if (0 == anInt8865 * 1500416377)
		    method14349(aClass205_8864, -1485754087 * anInt8871,
				1978989576);
		aBool8870 = false;
	    } else
		aClass205_8864 = null;
	    method14351(-1070830455);
	}
    }
    
    public final boolean method14336(int i) {
	return 0 != 1500416377 * anInt8865;
    }
    
    public final int method14337(int i) {
	return anInt8865 * 1500416377;
    }
    
    public final boolean method14338(byte i) {
	return null != aClass205_8864;
    }
    
    public final void method14339() {
	method14346(0, -2115582510);
    }
    
    final void method14340(int i, int i_14_, int i_15_, boolean bool,
			   boolean bool_16_) {
	if (i != method14329(1849648322)) {
	    if (i != -1) {
		if (aClass205_8864 != null
		    && i == aClass205_8864.anInt2208 * -1686227895) {
		    if (0 == aClass205_8864.anInt2223 * 629077835)
			return;
		} else {
		    aClass205_8864 = (Class205) Class468.aClass44_Sub1_5146
						    .method91(i, -1553029224);
		    if (aClass205_8864 == null
			|| aClass205_8864.anIntArray2209 == null) {
			aClass205_8864 = null;
			return;
		    }
		}
		anInt8867 = 0;
		anInt8865 = -1150119735 * i_14_;
		anInt8872 = i_15_ * 1768349497;
		aBool8861 = bool_16_;
		if (bool) {
		    anInt8871 = ((int) (Math.random()
					* (double) (aClass205_8864
						    .anIntArray2209).length)
				 * 49126185);
		    anInt8869
			= ((int) (Math.random()
				  * (double) (aClass205_8864.anIntArray2211
					      [anInt8871 * -1485754087]))
			   * 1165224707);
		} else {
		    anInt8871 = 0;
		    anInt8869 = 0;
		}
		anInt8873 = -362185365 + 1049213555 * anInt8871;
		if (892520259 * anInt8873 < 0
		    || (anInt8873 * 892520259
			>= aClass205_8864.anIntArray2209.length))
		    anInt8873 = 362185365;
		if (0 == anInt8865 * 1500416377)
		    method14349(aClass205_8864, -1485754087 * anInt8871,
				2058706834);
		aBool8870 = false;
	    } else
		aClass205_8864 = null;
	    method14351(-1070830455);
	}
    }
    
    public final void method14341(Class183 class183, int i, int i_17_,
				  int i_18_) {
	if (null != aClass205_8864.anIntArray2209 && method14350(-70919275)) {
	    class183.method3196(aClass693_8874.aClass534_Sub18_Sub17_8767,
				aClass693_8874.anInt8769 * 1321112311,
				aClass693_8874.aClass534_Sub18_Sub17_8764,
				1289223255 * aClass693_8874.anInt8766,
				anInt8869 * -1237559381,
				(aClass205_8864.anIntArray2211
				 [-1485754087 * anInt8871]),
				i, i_17_, aClass205_8864.aBool2214, null);
	    if (aBool8866 && null != aClass205_8864.anIntArray2210
		&& aClass693_8875.aBool8768)
		class183.method3196(aClass693_8875.aClass534_Sub18_Sub17_8767,
				    aClass693_8875.anInt8769 * 1321112311,
				    aClass693_8875.aClass534_Sub18_Sub17_8764,
				    1289223255 * aClass693_8875.anInt8766,
				    anInt8869 * -1237559381,
				    (aClass205_8864.anIntArray2211
				     [anInt8871 * -1485754087]),
				    i, i_17_, aClass205_8864.aBool2214, null);
	}
    }
    
    public final void method14342(Class183 class183, int i) {
	if (aClass205_8864.anIntArray2209 != null && method14350(1257607173)) {
	    class183.method3029(aClass693_8874.aClass534_Sub18_Sub17_8767,
				1321112311 * aClass693_8874.anInt8769);
	    if (aBool8866 && aClass205_8864.anIntArray2210 != null
		&& aClass693_8875.aBool8768)
		class183.method3029(aClass693_8875.aClass534_Sub18_Sub17_8767,
				    1321112311 * aClass693_8875.anInt8769);
	}
    }
    
    public final boolean method14343(byte i) {
	return aBool8870;
    }
    
    public final void method14344() {
	anInt8867 = 0;
    }
    
    public static final void method14345(Class183 class183, Class711 class711,
					 Class711 class711_19_) {
	if (class711.method14350(1114980425)
	    && class711_19_.method14350(-1042376376)) {
	    Class205 class205 = class711.aClass205_8864;
	    Class205 class205_20_ = class711_19_.aClass205_8864;
	    class183.method3028
		(class711.aClass693_8874.aClass534_Sub18_Sub17_8767,
		 class711.aClass693_8874.anInt8769 * 1321112311,
		 class711.aClass693_8874.aClass534_Sub18_Sub17_8764,
		 1289223255 * class711.aClass693_8874.anInt8766,
		 -1237559381 * class711.anInt8869,
		 class205.anIntArray2211[class711.anInt8871 * -1485754087],
		 class711_19_.aClass693_8874.aClass534_Sub18_Sub17_8767,
		 class711_19_.aClass693_8874.anInt8769 * 1321112311,
		 class711_19_.aClass693_8874.aClass534_Sub18_Sub17_8764,
		 1289223255 * class711_19_.aClass693_8874.anInt8766,
		 -1237559381 * class711_19_.anInt8869,
		 (class205_20_.anIntArray2211
		  [-1485754087 * class711_19_.anInt8871]),
		 (null != class205.aClass208_2213
		  ? class205.aClass208_2213.aBoolArray2240 : null),
		 class205.aBool2214 | class205_20_.aBool2214);
	}
    }
    
    public final void method14346(int i, int i_21_) {
	anInt8871 = 0;
	anInt8873
	    = -362185365 * (aClass205_8864.anIntArray2209.length > 1 ? 1 : -1);
	anInt8869 = 0;
	aBool8870 = false;
	anInt8865 = i * -1150119735;
	anInt8867 = 0;
	if (null != aClass205_8864 & null != aClass205_8864.anIntArray2209) {
	    method14349(aClass205_8864, anInt8871 * -1485754087, 2026681863);
	    method14351(-1070830455);
	}
    }
    
    public final boolean method14347() {
	return aBool8870;
    }
    
    public final void method14348(int i) {
	method14334(i, 0, 0, false, (byte) -52);
    }
    
    void method14349(Class205 class205, int i, int i_22_) {
	/* empty */
    }
    
    final boolean method14350(int i) {
	if (null != aClass205_8864) {
	    boolean bool
		= aClass693_8874.method14047(Class468.aClass44_Sub1_5146,
					     aClass205_8864,
					     -1485754087 * anInt8871,
					     892520259 * anInt8873,
					     aClass205_8864.anIntArray2209,
					     -2030297700);
	    if (bool && aBool8866 && aClass205_8864.anIntArray2210 != null)
		aClass693_8875.method14047(Class468.aClass44_Sub1_5146,
					   aClass205_8864,
					   -1485754087 * anInt8871,
					   892520259 * anInt8873,
					   aClass205_8864.anIntArray2210,
					   -2030297700);
	    return bool;
	}
	return false;
    }
    
    final void method14351(int i) {
	aClass693_8874.method14049((byte) 0);
	if (aBool8866)
	    aClass693_8875.method14049((byte) 0);
    }
    
    void method14352(Class205 class205, int i) {
	/* empty */
    }
    
    void method14353(Class205 class205, int i) {
	/* empty */
    }
    
    public final void method14354(int i) {
	anInt8865 = i * -1150119735;
    }
    
    public static void method14355(Class44_Sub1 class44_sub1) {
	Class468.aClass44_Sub1_5146 = class44_sub1;
    }
    
    public static void method14356(Class44_Sub1 class44_sub1) {
	Class468.aClass44_Sub1_5146 = class44_sub1;
    }
    
    public final void method14357(Class711 class711_23_) {
	aClass205_8864 = class711_23_.aClass205_8864;
	aBool8870 = class711_23_.aBool8870;
	aBool8866 = class711_23_.aBool8866;
	anInt8865 = class711_23_.anInt8865 * 1;
	anInt8867 = 1 * class711_23_.anInt8867;
	anInt8871 = 1 * class711_23_.anInt8871;
	anInt8873 = class711_23_.anInt8873 * 1;
	anInt8869 = 1 * class711_23_.anInt8869;
	method14351(-1070830455);
    }
    
    public final void method14358(Class711 class711_24_) {
	aClass205_8864 = class711_24_.aClass205_8864;
	aBool8870 = class711_24_.aBool8870;
	aBool8866 = class711_24_.aBool8866;
	anInt8865 = class711_24_.anInt8865 * 1;
	anInt8867 = 1 * class711_24_.anInt8867;
	anInt8871 = 1 * class711_24_.anInt8871;
	anInt8873 = class711_24_.anInt8873 * 1;
	anInt8869 = 1 * class711_24_.anInt8869;
	method14351(-1070830455);
    }
    
    public final void method14359(int i) {
	anInt8865 = i * -1150119735;
    }
    
    public final int method14360(short i) {
	if (method14350(571323994)) {
	    int i_25_ = 0;
	    if (method14350(-28836952)) {
		i_25_ |= aClass693_8874.anInt8765 * 1996767869;
		if (aBool8866 && null != aClass205_8864.anIntArray2210)
		    i_25_ |= 1996767869 * aClass693_8875.anInt8765;
	    }
	    return i_25_;
	}
	return 0;
    }
    
    public final void method14361(int i) {
	method14334(i, 0, 0, false, (byte) -114);
    }
    
    public final int method14362() {
	if (method14350(444362397)) {
	    int i = 0;
	    if (method14350(1524784850)) {
		i |= aClass693_8874.anInt8765 * 1996767869;
		if (aBool8866 && null != aClass205_8864.anIntArray2210)
		    i |= 1996767869 * aClass693_8875.anInt8765;
	    }
	    return i;
	}
	return 0;
    }
    
    public final void method14363() {
	method14346(0, -1810445920);
    }
    
    public final void method14364(int i, int i_26_) {
	method14334(i, i_26_, 0, false, (byte) -92);
    }
    
    void method14365(Class205 class205, int i) {
	/* empty */
    }
    
    public final void method14366(byte i) {
	anInt8867 = 0;
    }
    
    public final void method14367(int i, boolean bool) {
	method14334(i, 0, 0, bool, (byte) -99);
    }
    
    public final void method14368(int i, boolean bool) {
	method14334(i, 0, 0, bool, (byte) -85);
    }
    
    public final void method14369(int i, int i_27_, int i_28_, boolean bool) {
	method14335(i, i_27_, i_28_, bool, false, -2145686133);
    }
    
    public final void method14370(int i, int i_29_, int i_30_, boolean bool) {
	method14335(i, i_29_, i_30_, bool, false, -2145686133);
    }
    
    public final void method14371(int i, int i_31_, int i_32_, boolean bool) {
	method14335(i, i_31_, i_32_, bool, false, -2145686133);
    }
    
    public final boolean method14372() {
	return 0 != 1500416377 * anInt8865;
    }
    
    public final boolean method14373(int i, int i_33_) {
	if (aClass205_8864 == null | (i -= anInt8865 * 1500416377) <= 0)
	    return false;
	return (aClass205_8864.aBool2225
		| (i + -1237559381 * anInt8869
		   > aClass205_8864.anIntArray2211[-1485754087 * anInt8871]));
    }
    
    public final void method14374(int i) {
	method14346(0, -1844476639);
    }
    
    public final boolean method14375() {
	return 0 != 1500416377 * anInt8865;
    }
    
    public final int method14376() {
	return (aClass205_8864 != null ? aClass205_8864.anInt2208 * -1686227895
		: -1);
    }
    
    public final boolean method14377() {
	return 0 != 1500416377 * anInt8865;
    }
    
    public final int method14378() {
	return anInt8865 * 1500416377;
    }
    
    public final void method14379(int i) {
	anInt8865 = i * -1150119735;
    }
    
    public final void method14380() {
	anInt8867 = 0;
    }
    
    public final boolean method14381() {
	return aBool8870;
    }
    
    public final Class205 method14382(int i) {
	return aClass205_8864;
    }
    
    public final int method14383() {
	if (method14350(1190943346)) {
	    int i = 0;
	    if (method14350(718342757)) {
		i |= aClass693_8874.anInt8765 * 1996767869;
		if (aBool8866 && null != aClass205_8864.anIntArray2210)
		    i |= 1996767869 * aClass693_8875.anInt8765;
	    }
	    return i;
	}
	return 0;
    }
    
    public static final void method14384(Class183 class183, Class711 class711,
					 Class711 class711_34_) {
	if (class711.method14350(-991611721)
	    && class711_34_.method14350(-1943820880)) {
	    Class205 class205 = class711.aClass205_8864;
	    Class205 class205_35_ = class711_34_.aClass205_8864;
	    class183.method3028
		(class711.aClass693_8874.aClass534_Sub18_Sub17_8767,
		 class711.aClass693_8874.anInt8769 * 1321112311,
		 class711.aClass693_8874.aClass534_Sub18_Sub17_8764,
		 1289223255 * class711.aClass693_8874.anInt8766,
		 -1237559381 * class711.anInt8869,
		 class205.anIntArray2211[class711.anInt8871 * -1485754087],
		 class711_34_.aClass693_8874.aClass534_Sub18_Sub17_8767,
		 class711_34_.aClass693_8874.anInt8769 * 1321112311,
		 class711_34_.aClass693_8874.aClass534_Sub18_Sub17_8764,
		 1289223255 * class711_34_.aClass693_8874.anInt8766,
		 -1237559381 * class711_34_.anInt8869,
		 (class205_35_.anIntArray2211
		  [-1485754087 * class711_34_.anInt8871]),
		 (null != class205.aClass208_2213
		  ? class205.aClass208_2213.aBoolArray2240 : null),
		 class205.aBool2214 | class205_35_.aBool2214);
	}
    }
    
    public final void method14385(Class183 class183, int i) {
	if (aClass205_8864 != null) {
	    if (null != aClass205_8864.anIntArray2209
		&& method14350(1494001753)) {
		class183.method3061(aClass693_8874.aClass534_Sub18_Sub17_8767,
				    aClass693_8874.anInt8769 * 1321112311,
				    aClass693_8874.aClass534_Sub18_Sub17_8764,
				    1289223255 * aClass693_8874.anInt8766,
				    anInt8869 * -1237559381,
				    (aClass205_8864.anIntArray2211
				     [-1485754087 * anInt8871]),
				    i, aClass205_8864.aBool2214);
		if (aBool8866 && null != aClass205_8864.anIntArray2210
		    && aClass693_8875.aBool8768)
		    class183.method3061((aClass693_8875
					 .aClass534_Sub18_Sub17_8767),
					1321112311 * aClass693_8875.anInt8769,
					(aClass693_8875
					 .aClass534_Sub18_Sub17_8764),
					1289223255 * aClass693_8875.anInt8766,
					anInt8869 * -1237559381,
					(aClass205_8864.anIntArray2211
					 [-1485754087 * anInt8871]),
					i, aClass205_8864.aBool2214);
	    }
	}
    }
    
    public final void method14386(int i, boolean bool, boolean bool_36_,
				  int i_37_) {
	method14335(i, 0, 0, bool, bool_36_, -2145686133);
    }
    
    public final void method14387(Class183 class183) {
	if (aClass205_8864.anIntArray2209 != null
	    && method14350(-1395372068)) {
	    class183.method3029(aClass693_8874.aClass534_Sub18_Sub17_8767,
				1321112311 * aClass693_8874.anInt8769);
	    if (aBool8866 && aClass205_8864.anIntArray2210 != null
		&& aClass693_8875.aBool8768)
		class183.method3029(aClass693_8875.aClass534_Sub18_Sub17_8767,
				    1321112311 * aClass693_8875.anInt8769);
	}
    }
    
    public final void method14388(Class183 class183) {
	if (aClass205_8864.anIntArray2209 != null
	    && method14350(-1433468476)) {
	    class183.method3029(aClass693_8874.aClass534_Sub18_Sub17_8767,
				1321112311 * aClass693_8874.anInt8769);
	    if (aBool8866 && aClass205_8864.anIntArray2210 != null
		&& aClass693_8875.aBool8768)
		class183.method3029(aClass693_8875.aClass534_Sub18_Sub17_8767,
				    1321112311 * aClass693_8875.anInt8769);
	}
    }
    
    Class711(boolean bool) {
	aBool8866 = false;
	anInt8872 = 0;
	aBool8861 = false;
	aBool8866 = bool;
	aClass693_8874 = new Class693();
	if (aBool8866)
	    aClass693_8875 = new Class693();
	else
	    aClass693_8875 = null;
    }
    
    public final void method14389(int i, int i_38_) {
	method14334(i, i_38_, 0, false, (byte) -18);
    }
    
    public final void method14390(Class183 class183, int i) {
	if (aClass205_8864 != null) {
	    if (null != aClass205_8864.anIntArray2209
		&& method14350(232813300)) {
		class183.method3061(aClass693_8874.aClass534_Sub18_Sub17_8767,
				    aClass693_8874.anInt8769 * 1321112311,
				    aClass693_8874.aClass534_Sub18_Sub17_8764,
				    1289223255 * aClass693_8874.anInt8766,
				    anInt8869 * -1237559381,
				    (aClass205_8864.anIntArray2211
				     [-1485754087 * anInt8871]),
				    i, aClass205_8864.aBool2214);
		if (aBool8866 && null != aClass205_8864.anIntArray2210
		    && aClass693_8875.aBool8768)
		    class183.method3061((aClass693_8875
					 .aClass534_Sub18_Sub17_8767),
					1321112311 * aClass693_8875.anInt8769,
					(aClass693_8875
					 .aClass534_Sub18_Sub17_8764),
					1289223255 * aClass693_8875.anInt8766,
					anInt8869 * -1237559381,
					(aClass205_8864.anIntArray2211
					 [-1485754087 * anInt8871]),
					i, aClass205_8864.aBool2214);
	    }
	}
    }
    
    public final boolean method14391() {
	return aBool8870;
    }
    
    public final int method14392() {
	if (method14350(-745238696)) {
	    int i = 0;
	    if (method14350(-516634727)) {
		i |= aClass693_8874.anInt8765 * 1996767869;
		if (aBool8866 && null != aClass205_8864.anIntArray2210)
		    i |= 1996767869 * aClass693_8875.anInt8765;
	    }
	    return i;
	}
	return 0;
    }
    
    public final boolean method14393(int i, byte i_39_) {
	if (null == aClass205_8864 || 0 == i)
	    return false;
	if (anInt8865 * 1500416377 > 0) {
	    if (anInt8865 * 1500416377 > i) {
		anInt8865 -= -1150119735 * i;
		return false;
	    }
	    i -= 1500416377 * anInt8865;
	    anInt8865 = 0;
	    method14349(aClass205_8864, anInt8871 * -1485754087, 2049601996);
	}
	i += -1237559381 * anInt8869;
	boolean bool = aClass205_8864.aBool2225 | Class205.aBool2222;
	if (i > 100 && 1218058879 * aClass205_8864.anInt2206 > 0) {
	    int i_40_;
	    for (i_40_ = (aClass205_8864.anIntArray2209.length
			  - aClass205_8864.anInt2206 * 1218058879);
		 (anInt8871 * -1485754087 < i_40_
		  && i > (aClass205_8864.anIntArray2211
			  [-1485754087 * anInt8871]));
		 anInt8871 += 49126185)
		i -= aClass205_8864.anIntArray2211[-1485754087 * anInt8871];
	    if (-1485754087 * anInt8871 >= i_40_) {
		int i_41_ = 0;
		for (int i_42_ = i_40_;
		     i_42_ < aClass205_8864.anIntArray2209.length; i_42_++)
		    i_41_ += aClass205_8864.anIntArray2211[i_42_];
		if (0 == anInt8872 * 116154121)
		    anInt8867 += -303458915 * (i / i_41_);
		i %= i_41_;
	    }
	    anInt8873 = 1049213555 * anInt8871 + -362185365;
	    if (anInt8873 * 892520259
		>= aClass205_8864.anIntArray2209.length) {
		if (-1 == 1218058879 * aClass205_8864.anInt2206 && aBool8861)
		    anInt8873 = 0;
		else
		    anInt8873 -= -1955588587 * aClass205_8864.anInt2206;
		if (anInt8873 * 892520259 < 0
		    || (anInt8873 * 892520259
			>= aClass205_8864.anIntArray2209.length))
		    anInt8873 = 362185365;
	    }
	    bool = true;
	}
	while (i > aClass205_8864.anIntArray2211[anInt8871 * -1485754087]) {
	    bool = true;
	    i -= (aClass205_8864.anIntArray2211
		  [(anInt8871 += 49126185) * -1485754087 - 1]);
	    if (-1485754087 * anInt8871
		>= aClass205_8864.anIntArray2209.length) {
		if (-1 != aClass205_8864.anInt2206 * 1218058879
		    && anInt8872 * 116154121 != 2) {
		    anInt8871 -= 1951146839 * aClass205_8864.anInt2206;
		    if (116154121 * anInt8872 == 0)
			anInt8867 += -303458915;
		}
		if ((265590965 * anInt8867
		     >= aClass205_8864.anInt2220 * 1211421917)
		    || anInt8871 * -1485754087 < 0
		    || (-1485754087 * anInt8871
			>= aClass205_8864.anIntArray2209.length)) {
		    aBool8870 = true;
		    break;
		}
	    }
	    method14349(aClass205_8864, anInt8871 * -1485754087, 1923563123);
	    anInt8873 = 1049213555 * anInt8871 + -362185365;
	    if (892520259 * anInt8873
		>= aClass205_8864.anIntArray2209.length) {
		if (aClass205_8864.anInt2206 * 1218058879 == -1 && aBool8861)
		    anInt8873 = 0;
		else
		    anInt8873 -= aClass205_8864.anInt2206 * -1955588587;
		if (892520259 * anInt8873 < 0
		    || (892520259 * anInt8873
			>= aClass205_8864.anIntArray2209.length))
		    anInt8873 = 362185365;
	    }
	}
	anInt8869 = 1165224707 * i;
	if (bool)
	    method14351(-1070830455);
	return bool;
    }
    
    public final void method14394() {
	anInt8867 = 0;
    }
    
    final void method14395(int i, int i_43_, int i_44_, boolean bool,
			   boolean bool_45_) {
	if (i != method14329(670230300)) {
	    if (i != -1) {
		if (aClass205_8864 != null
		    && i == aClass205_8864.anInt2208 * -1686227895) {
		    if (0 == aClass205_8864.anInt2223 * 629077835)
			return;
		} else {
		    aClass205_8864 = (Class205) Class468.aClass44_Sub1_5146
						    .method91(i, -1568407900);
		    if (aClass205_8864 == null
			|| aClass205_8864.anIntArray2209 == null) {
			aClass205_8864 = null;
			return;
		    }
		}
		anInt8867 = 0;
		anInt8865 = -1150119735 * i_43_;
		anInt8872 = i_44_ * 1768349497;
		aBool8861 = bool_45_;
		if (bool) {
		    anInt8871 = ((int) (Math.random()
					* (double) (aClass205_8864
						    .anIntArray2209).length)
				 * 49126185);
		    anInt8869
			= ((int) (Math.random()
				  * (double) (aClass205_8864.anIntArray2211
					      [anInt8871 * -1485754087]))
			   * 1165224707);
		} else {
		    anInt8871 = 0;
		    anInt8869 = 0;
		}
		anInt8873 = -362185365 + 1049213555 * anInt8871;
		if (892520259 * anInt8873 < 0
		    || (anInt8873 * 892520259
			>= aClass205_8864.anIntArray2209.length))
		    anInt8873 = 362185365;
		if (0 == anInt8865 * 1500416377)
		    method14349(aClass205_8864, -1485754087 * anInt8871,
				1927193402);
		aBool8870 = false;
	    } else
		aClass205_8864 = null;
	    method14351(-1070830455);
	}
    }
    
    public final void method14396() {
	anInt8867 = 0;
    }
    
    public final void method14397() {
	anInt8867 = 0;
    }
    
    public final void method14398(int i, boolean bool) {
	method14334(i, 0, 0, bool, (byte) -81);
    }
    
    public final void method14399(Class183 class183, int i, int i_46_) {
	if (aClass205_8864 != null) {
	    if (null != aClass205_8864.anIntArray2209
		&& method14350(849210782)) {
		class183.method3061(aClass693_8874.aClass534_Sub18_Sub17_8767,
				    aClass693_8874.anInt8769 * 1321112311,
				    aClass693_8874.aClass534_Sub18_Sub17_8764,
				    1289223255 * aClass693_8874.anInt8766,
				    anInt8869 * -1237559381,
				    (aClass205_8864.anIntArray2211
				     [-1485754087 * anInt8871]),
				    i, aClass205_8864.aBool2214);
		if (aBool8866 && null != aClass205_8864.anIntArray2210
		    && aClass693_8875.aBool8768)
		    class183.method3061((aClass693_8875
					 .aClass534_Sub18_Sub17_8767),
					1321112311 * aClass693_8875.anInt8769,
					(aClass693_8875
					 .aClass534_Sub18_Sub17_8764),
					1289223255 * aClass693_8875.anInt8766,
					anInt8869 * -1237559381,
					(aClass205_8864.anIntArray2211
					 [-1485754087 * anInt8871]),
					i, aClass205_8864.aBool2214);
	    }
	}
    }
    
    public final void method14400() {
	method14346(0, -2109169984);
    }
    
    public final void method14401() {
	method14346(0, -1886890372);
    }
    
    public final void method14402(int i) {
	anInt8871 = 0;
	anInt8873
	    = -362185365 * (aClass205_8864.anIntArray2209.length > 1 ? 1 : -1);
	anInt8869 = 0;
	aBool8870 = false;
	anInt8865 = i * -1150119735;
	anInt8867 = 0;
	if (null != aClass205_8864 & null != aClass205_8864.anIntArray2209) {
	    method14349(aClass205_8864, anInt8871 * -1485754087, 1919944706);
	    method14351(-1070830455);
	}
    }
    
    public final void method14403(int i) {
	anInt8871 = 0;
	anInt8873
	    = -362185365 * (aClass205_8864.anIntArray2209.length > 1 ? 1 : -1);
	anInt8869 = 0;
	aBool8870 = false;
	anInt8865 = i * -1150119735;
	anInt8867 = 0;
	if (null != aClass205_8864 & null != aClass205_8864.anIntArray2209) {
	    method14349(aClass205_8864, anInt8871 * -1485754087, 2082694606);
	    method14351(-1070830455);
	}
    }
    
    public final boolean method14404(int i) {
	if (null == aClass205_8864 || 0 == i)
	    return false;
	if (anInt8865 * 1500416377 > 0) {
	    if (anInt8865 * 1500416377 > i) {
		anInt8865 -= -1150119735 * i;
		return false;
	    }
	    i -= 1500416377 * anInt8865;
	    anInt8865 = 0;
	    method14349(aClass205_8864, anInt8871 * -1485754087, 1862305039);
	}
	i += -1237559381 * anInt8869;
	boolean bool = aClass205_8864.aBool2225 | Class205.aBool2222;
	if (i > 100 && 1218058879 * aClass205_8864.anInt2206 > 0) {
	    int i_47_;
	    for (i_47_ = (aClass205_8864.anIntArray2209.length
			  - aClass205_8864.anInt2206 * 1218058879);
		 (anInt8871 * -1485754087 < i_47_
		  && i > (aClass205_8864.anIntArray2211
			  [-1485754087 * anInt8871]));
		 anInt8871 += 49126185)
		i -= aClass205_8864.anIntArray2211[-1485754087 * anInt8871];
	    if (-1485754087 * anInt8871 >= i_47_) {
		int i_48_ = 0;
		for (int i_49_ = i_47_;
		     i_49_ < aClass205_8864.anIntArray2209.length; i_49_++)
		    i_48_ += aClass205_8864.anIntArray2211[i_49_];
		if (0 == anInt8872 * 116154121)
		    anInt8867 += -303458915 * (i / i_48_);
		i %= i_48_;
	    }
	    anInt8873 = 1049213555 * anInt8871 + -362185365;
	    if (anInt8873 * 892520259
		>= aClass205_8864.anIntArray2209.length) {
		if (-1 == 1218058879 * aClass205_8864.anInt2206 && aBool8861)
		    anInt8873 = 0;
		else
		    anInt8873 -= -1955588587 * aClass205_8864.anInt2206;
		if (anInt8873 * 892520259 < 0
		    || (anInt8873 * 892520259
			>= aClass205_8864.anIntArray2209.length))
		    anInt8873 = 362185365;
	    }
	    bool = true;
	}
	while (i > aClass205_8864.anIntArray2211[anInt8871 * -1485754087]) {
	    bool = true;
	    i -= (aClass205_8864.anIntArray2211
		  [(anInt8871 += 49126185) * -1485754087 - 1]);
	    if (-1485754087 * anInt8871
		>= aClass205_8864.anIntArray2209.length) {
		if (-1 != aClass205_8864.anInt2206 * 1218058879
		    && anInt8872 * 116154121 != 2) {
		    anInt8871 -= 1951146839 * aClass205_8864.anInt2206;
		    if (116154121 * anInt8872 == 0)
			anInt8867 += -303458915;
		}
		if ((265590965 * anInt8867
		     >= aClass205_8864.anInt2220 * 1211421917)
		    || anInt8871 * -1485754087 < 0
		    || (-1485754087 * anInt8871
			>= aClass205_8864.anIntArray2209.length)) {
		    aBool8870 = true;
		    break;
		}
	    }
	    method14349(aClass205_8864, anInt8871 * -1485754087, 1898184577);
	    anInt8873 = 1049213555 * anInt8871 + -362185365;
	    if (892520259 * anInt8873
		>= aClass205_8864.anIntArray2209.length) {
		if (aClass205_8864.anInt2206 * 1218058879 == -1 && aBool8861)
		    anInt8873 = 0;
		else
		    anInt8873 -= aClass205_8864.anInt2206 * -1955588587;
		if (892520259 * anInt8873 < 0
		    || (892520259 * anInt8873
			>= aClass205_8864.anIntArray2209.length))
		    anInt8873 = 362185365;
	    }
	}
	anInt8869 = 1165224707 * i;
	if (bool)
	    method14351(-1070830455);
	return bool;
    }
    
    public final boolean method14405(int i) {
	if (null == aClass205_8864 || 0 == i)
	    return false;
	if (anInt8865 * 1500416377 > 0) {
	    if (anInt8865 * 1500416377 > i) {
		anInt8865 -= -1150119735 * i;
		return false;
	    }
	    i -= 1500416377 * anInt8865;
	    anInt8865 = 0;
	    method14349(aClass205_8864, anInt8871 * -1485754087, 1938171309);
	}
	i += -1237559381 * anInt8869;
	boolean bool = aClass205_8864.aBool2225 | Class205.aBool2222;
	if (i > 100 && 1218058879 * aClass205_8864.anInt2206 > 0) {
	    int i_50_;
	    for (i_50_ = (aClass205_8864.anIntArray2209.length
			  - aClass205_8864.anInt2206 * 1218058879);
		 (anInt8871 * -1485754087 < i_50_
		  && i > (aClass205_8864.anIntArray2211
			  [-1485754087 * anInt8871]));
		 anInt8871 += 49126185)
		i -= aClass205_8864.anIntArray2211[-1485754087 * anInt8871];
	    if (-1485754087 * anInt8871 >= i_50_) {
		int i_51_ = 0;
		for (int i_52_ = i_50_;
		     i_52_ < aClass205_8864.anIntArray2209.length; i_52_++)
		    i_51_ += aClass205_8864.anIntArray2211[i_52_];
		if (0 == anInt8872 * 116154121)
		    anInt8867 += -303458915 * (i / i_51_);
		i %= i_51_;
	    }
	    anInt8873 = 1049213555 * anInt8871 + -362185365;
	    if (anInt8873 * 892520259
		>= aClass205_8864.anIntArray2209.length) {
		if (-1 == 1218058879 * aClass205_8864.anInt2206 && aBool8861)
		    anInt8873 = 0;
		else
		    anInt8873 -= -1955588587 * aClass205_8864.anInt2206;
		if (anInt8873 * 892520259 < 0
		    || (anInt8873 * 892520259
			>= aClass205_8864.anIntArray2209.length))
		    anInt8873 = 362185365;
	    }
	    bool = true;
	}
	while (i > aClass205_8864.anIntArray2211[anInt8871 * -1485754087]) {
	    bool = true;
	    i -= (aClass205_8864.anIntArray2211
		  [(anInt8871 += 49126185) * -1485754087 - 1]);
	    if (-1485754087 * anInt8871
		>= aClass205_8864.anIntArray2209.length) {
		if (-1 != aClass205_8864.anInt2206 * 1218058879
		    && anInt8872 * 116154121 != 2) {
		    anInt8871 -= 1951146839 * aClass205_8864.anInt2206;
		    if (116154121 * anInt8872 == 0)
			anInt8867 += -303458915;
		}
		if ((265590965 * anInt8867
		     >= aClass205_8864.anInt2220 * 1211421917)
		    || anInt8871 * -1485754087 < 0
		    || (-1485754087 * anInt8871
			>= aClass205_8864.anIntArray2209.length)) {
		    aBool8870 = true;
		    break;
		}
	    }
	    method14349(aClass205_8864, anInt8871 * -1485754087, 1961295898);
	    anInt8873 = 1049213555 * anInt8871 + -362185365;
	    if (892520259 * anInt8873
		>= aClass205_8864.anIntArray2209.length) {
		if (aClass205_8864.anInt2206 * 1218058879 == -1 && aBool8861)
		    anInt8873 = 0;
		else
		    anInt8873 -= aClass205_8864.anInt2206 * -1955588587;
		if (892520259 * anInt8873 < 0
		    || (892520259 * anInt8873
			>= aClass205_8864.anIntArray2209.length))
		    anInt8873 = 362185365;
	    }
	}
	anInt8869 = 1165224707 * i;
	if (bool)
	    method14351(-1070830455);
	return bool;
    }
    
    public final boolean method14406(int i) {
	if (null == aClass205_8864 || 0 == i)
	    return false;
	if (anInt8865 * 1500416377 > 0) {
	    if (anInt8865 * 1500416377 > i) {
		anInt8865 -= -1150119735 * i;
		return false;
	    }
	    i -= 1500416377 * anInt8865;
	    anInt8865 = 0;
	    method14349(aClass205_8864, anInt8871 * -1485754087, 1962037155);
	}
	i += -1237559381 * anInt8869;
	boolean bool = aClass205_8864.aBool2225 | Class205.aBool2222;
	if (i > 100 && 1218058879 * aClass205_8864.anInt2206 > 0) {
	    int i_53_;
	    for (i_53_ = (aClass205_8864.anIntArray2209.length
			  - aClass205_8864.anInt2206 * 1218058879);
		 (anInt8871 * -1485754087 < i_53_
		  && i > (aClass205_8864.anIntArray2211
			  [-1485754087 * anInt8871]));
		 anInt8871 += 49126185)
		i -= aClass205_8864.anIntArray2211[-1485754087 * anInt8871];
	    if (-1485754087 * anInt8871 >= i_53_) {
		int i_54_ = 0;
		for (int i_55_ = i_53_;
		     i_55_ < aClass205_8864.anIntArray2209.length; i_55_++)
		    i_54_ += aClass205_8864.anIntArray2211[i_55_];
		if (0 == anInt8872 * 116154121)
		    anInt8867 += -303458915 * (i / i_54_);
		i %= i_54_;
	    }
	    anInt8873 = 1049213555 * anInt8871 + -362185365;
	    if (anInt8873 * 892520259
		>= aClass205_8864.anIntArray2209.length) {
		if (-1 == 1218058879 * aClass205_8864.anInt2206 && aBool8861)
		    anInt8873 = 0;
		else
		    anInt8873 -= -1955588587 * aClass205_8864.anInt2206;
		if (anInt8873 * 892520259 < 0
		    || (anInt8873 * 892520259
			>= aClass205_8864.anIntArray2209.length))
		    anInt8873 = 362185365;
	    }
	    bool = true;
	}
	while (i > aClass205_8864.anIntArray2211[anInt8871 * -1485754087]) {
	    bool = true;
	    i -= (aClass205_8864.anIntArray2211
		  [(anInt8871 += 49126185) * -1485754087 - 1]);
	    if (-1485754087 * anInt8871
		>= aClass205_8864.anIntArray2209.length) {
		if (-1 != aClass205_8864.anInt2206 * 1218058879
		    && anInt8872 * 116154121 != 2) {
		    anInt8871 -= 1951146839 * aClass205_8864.anInt2206;
		    if (116154121 * anInt8872 == 0)
			anInt8867 += -303458915;
		}
		if ((265590965 * anInt8867
		     >= aClass205_8864.anInt2220 * 1211421917)
		    || anInt8871 * -1485754087 < 0
		    || (-1485754087 * anInt8871
			>= aClass205_8864.anIntArray2209.length)) {
		    aBool8870 = true;
		    break;
		}
	    }
	    method14349(aClass205_8864, anInt8871 * -1485754087, 1761569095);
	    anInt8873 = 1049213555 * anInt8871 + -362185365;
	    if (892520259 * anInt8873
		>= aClass205_8864.anIntArray2209.length) {
		if (aClass205_8864.anInt2206 * 1218058879 == -1 && aBool8861)
		    anInt8873 = 0;
		else
		    anInt8873 -= aClass205_8864.anInt2206 * -1955588587;
		if (892520259 * anInt8873 < 0
		    || (892520259 * anInt8873
			>= aClass205_8864.anIntArray2209.length))
		    anInt8873 = 362185365;
	    }
	}
	anInt8869 = 1165224707 * i;
	if (bool)
	    method14351(-1070830455);
	return bool;
    }
    
    public final boolean method14407(int i) {
	if (null == aClass205_8864 || 0 == i)
	    return false;
	if (anInt8865 * 1500416377 > 0) {
	    if (anInt8865 * 1500416377 > i) {
		anInt8865 -= -1150119735 * i;
		return false;
	    }
	    i -= 1500416377 * anInt8865;
	    anInt8865 = 0;
	    method14349(aClass205_8864, anInt8871 * -1485754087, 2113990855);
	}
	i += -1237559381 * anInt8869;
	boolean bool = aClass205_8864.aBool2225 | Class205.aBool2222;
	if (i > 100 && 1218058879 * aClass205_8864.anInt2206 > 0) {
	    int i_56_;
	    for (i_56_ = (aClass205_8864.anIntArray2209.length
			  - aClass205_8864.anInt2206 * 1218058879);
		 (anInt8871 * -1485754087 < i_56_
		  && i > (aClass205_8864.anIntArray2211
			  [-1485754087 * anInt8871]));
		 anInt8871 += 49126185)
		i -= aClass205_8864.anIntArray2211[-1485754087 * anInt8871];
	    if (-1485754087 * anInt8871 >= i_56_) {
		int i_57_ = 0;
		for (int i_58_ = i_56_;
		     i_58_ < aClass205_8864.anIntArray2209.length; i_58_++)
		    i_57_ += aClass205_8864.anIntArray2211[i_58_];
		if (0 == anInt8872 * 116154121)
		    anInt8867 += -303458915 * (i / i_57_);
		i %= i_57_;
	    }
	    anInt8873 = 1049213555 * anInt8871 + -362185365;
	    if (anInt8873 * 892520259
		>= aClass205_8864.anIntArray2209.length) {
		if (-1 == 1218058879 * aClass205_8864.anInt2206 && aBool8861)
		    anInt8873 = 0;
		else
		    anInt8873 -= -1955588587 * aClass205_8864.anInt2206;
		if (anInt8873 * 892520259 < 0
		    || (anInt8873 * 892520259
			>= aClass205_8864.anIntArray2209.length))
		    anInt8873 = 362185365;
	    }
	    bool = true;
	}
	while (i > aClass205_8864.anIntArray2211[anInt8871 * -1485754087]) {
	    bool = true;
	    i -= (aClass205_8864.anIntArray2211
		  [(anInt8871 += 49126185) * -1485754087 - 1]);
	    if (-1485754087 * anInt8871
		>= aClass205_8864.anIntArray2209.length) {
		if (-1 != aClass205_8864.anInt2206 * 1218058879
		    && anInt8872 * 116154121 != 2) {
		    anInt8871 -= 1951146839 * aClass205_8864.anInt2206;
		    if (116154121 * anInt8872 == 0)
			anInt8867 += -303458915;
		}
		if ((265590965 * anInt8867
		     >= aClass205_8864.anInt2220 * 1211421917)
		    || anInt8871 * -1485754087 < 0
		    || (-1485754087 * anInt8871
			>= aClass205_8864.anIntArray2209.length)) {
		    aBool8870 = true;
		    break;
		}
	    }
	    method14349(aClass205_8864, anInt8871 * -1485754087, 2058764705);
	    anInt8873 = 1049213555 * anInt8871 + -362185365;
	    if (892520259 * anInt8873
		>= aClass205_8864.anIntArray2209.length) {
		if (aClass205_8864.anInt2206 * 1218058879 == -1 && aBool8861)
		    anInt8873 = 0;
		else
		    anInt8873 -= aClass205_8864.anInt2206 * -1955588587;
		if (892520259 * anInt8873 < 0
		    || (892520259 * anInt8873
			>= aClass205_8864.anIntArray2209.length))
		    anInt8873 = 362185365;
	    }
	}
	anInt8869 = 1165224707 * i;
	if (bool)
	    method14351(-1070830455);
	return bool;
    }
    
    public final boolean method14408(int i) {
	if (aClass205_8864 == null | (i -= anInt8865 * 1500416377) <= 0)
	    return false;
	return (aClass205_8864.aBool2225
		| (i + -1237559381 * anInt8869
		   > aClass205_8864.anIntArray2211[-1485754087 * anInt8871]));
    }
    
    public final boolean method14409(int i) {
	if (aClass205_8864 == null | (i -= anInt8865 * 1500416377) <= 0)
	    return false;
	return (aClass205_8864.aBool2225
		| (i + -1237559381 * anInt8869
		   > aClass205_8864.anIntArray2211[-1485754087 * anInt8871]));
    }
    
    final boolean method14410() {
	if (null != aClass205_8864) {
	    boolean bool
		= aClass693_8874.method14047(Class468.aClass44_Sub1_5146,
					     aClass205_8864,
					     -1485754087 * anInt8871,
					     892520259 * anInt8873,
					     aClass205_8864.anIntArray2209,
					     -2030297700);
	    if (bool && aBool8866 && aClass205_8864.anIntArray2210 != null)
		aClass693_8875.method14047(Class468.aClass44_Sub1_5146,
					   aClass205_8864,
					   -1485754087 * anInt8871,
					   892520259 * anInt8873,
					   aClass205_8864.anIntArray2210,
					   -2030297700);
	    return bool;
	}
	return false;
    }
    
    final boolean method14411() {
	if (null != aClass205_8864) {
	    boolean bool
		= aClass693_8874.method14047(Class468.aClass44_Sub1_5146,
					     aClass205_8864,
					     -1485754087 * anInt8871,
					     892520259 * anInt8873,
					     aClass205_8864.anIntArray2209,
					     -2030297700);
	    if (bool && aBool8866 && aClass205_8864.anIntArray2210 != null)
		aClass693_8875.method14047(Class468.aClass44_Sub1_5146,
					   aClass205_8864,
					   -1485754087 * anInt8871,
					   892520259 * anInt8873,
					   aClass205_8864.anIntArray2210,
					   -2030297700);
	    return bool;
	}
	return false;
    }
    
    final void method14412() {
	aClass693_8874.method14049((byte) 0);
	if (aBool8866)
	    aClass693_8875.method14049((byte) 0);
    }
    
    final void method14413() {
	aClass693_8874.method14049((byte) 0);
	if (aBool8866)
	    aClass693_8875.method14049((byte) 0);
    }
    
    public static Class16 method14414(Class472 class472, int i, int i_59_,
				      int i_60_) {
	return Class288.method5276(class472, i, i_59_, null, 1140369498);
    }
    
    static Class534_Sub19 method14415(int i, int i_61_, int i_62_, int i_63_) {
	Class534_Sub19 class534_sub19 = null;
	if (i_62_ == 0)
	    class534_sub19
		= Class346.method6128(Class404.aClass404_4153,
				      client.aClass100_11264.aClass13_1183,
				      1341391005);
	if (i_62_ == 1)
	    class534_sub19
		= Class346.method6128(Class404.aClass404_4195,
				      client.aClass100_11264.aClass13_1183,
				      1341391005);
	Class597 class597 = client.aClass512_11100.method8416((byte) 63);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16569(-424199969 * class597.anInt7859 + i, -494036761);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16687(i_61_ + -1166289421 * class597.anInt7861, 2134011012);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16559(Class479.method7916(-877455222) ? 1 : 0, 836366524);
	Class113.anInt1375 = -1660827629 * i;
	Class113.anInt1376 = 517634255 * i_61_;
	Class113.aBool1373 = false;
	Class453_Sub3.method15985(-1097145401);
	return class534_sub19;
    }
    
    public static int method14416(Class185 class185, int i) {
	Class535.method8896(99481101);
	return Class523.aTwitchTV7088.StopStreaming();
    }
    
    static void method14417(Class669 class669, int i) {
	Class273 class273
	    = ((Class273)
	       Class223.aClass53_Sub2_2316.method91((class669.anIntArray8595
						     [((class669.anInt8600
							-= 308999563)
						       * 2088438307)]),
						    879931456));
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (class273.anIntArray3029 == null ? 0
	       : class273.anIntArray3029.length);
    }
    
    public static void method14418(int i, byte i_64_) {
	Class25 class25
	    = (Class25) Class554_Sub1.aHashMap10682.get(Integer.valueOf(i));
	if (class25 == null)
	    class25 = new Class25();
	class25.anInt232 = -1551336507 * Class554_Sub1.anInt10665;
	class25.anInt231 = -1184677759 * Class554_Sub1.anInt10691;
	Class554_Sub1.aHashMap10682.put(Integer.valueOf(i), class25);
    }
}
