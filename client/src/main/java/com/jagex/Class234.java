/* Class234 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class234 implements Interface28
{
    int anInt2360;
    int anInt2361;
    Class213 this$0;
    
    Class234(Class213 class213, Class534_Sub40 class534_sub40) {
	this$0 = class213;
	anInt2360 = class534_sub40.method16529((byte) 1) * 1051402597;
	anInt2361 = class534_sub40.method16527(1669668511) * 334928121;
    }
    
    public void method173(Class214 class214, int i) {
	class214.method4076(-1290852243 * anInt2360, 264415561 * anInt2361,
			    596502257);
	class214.method4044(anInt2360 * -1290852243, 1481307617)
	    .method3998((byte) -9);
    }
    
    public void method172(Class214 class214) {
	class214.method4076(-1290852243 * anInt2360, 264415561 * anInt2361,
			    1022893504);
	class214.method4044(anInt2360 * -1290852243, 1481307617)
	    .method3998((byte) -39);
    }
    
    public void method174(Class214 class214) {
	class214.method4076(-1290852243 * anInt2360, 264415561 * anInt2361,
			    -1273920352);
	class214.method4044(anInt2360 * -1290852243, 1481307617)
	    .method3998((byte) -119);
    }
    
    static final void method4342(Class669 class669, byte i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class637.method10557(class247, class243, class669, (byte) 126);
    }
    
    static final void method4343(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class504.method8321(class247, class669, 970115535);
    }
    
    static final void method4344(Class669 class669, byte i) {
	Class640.method10597(2106615580);
    }
}
