/* Class657 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class657
{
    public static final int anInt8525 = 16777215;
    public static final int anInt8526 = 13156520;
    public static final float aFloat8527 = 1.1523438F;
    static final int anInt8528 = 5000;
    public static final float aFloat8529 = 1.2F;
    public static final int anInt8530 = -50;
    public static final int anInt8531 = -60;
    public static final int anInt8532 = -50;
    public static final int anInt8533 = 0;
    public static final int anInt8534 = 2816;
    public static final int anInt8535 = 2816;
    public static final float aFloat8536 = 0.69921875F;
    public static final int anInt8537 = 2816;
    static Class163 aClass163_8538;
    
    static {
	new Class438(0.6F, 0.6F, 0.6F);
	new Class438(0.3F, 0.3F, 0.3F);
	new Class438(0.3F, 0.3F, 0.5F);
    }
    
    Class657() throws Throwable {
	throw new Error();
    }
    
    static final void method10895(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class193.method3793(class247, class243, class669, (byte) 54);
    }
    
    public static void method10896(String string, String string_0_, int i,
				   boolean bool, String string_1_, int i_2_) {
	if (18 == client.anInt11039 * -1850530127) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4200,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16507(0,
								  1524269217);
	    int i_3_
		= (31645619
		   * class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  1745813546);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string_0_,
								  31116204);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(i,
								  745460464);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(bool ? 1 : 0,
								  1864902097);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string_1_,
								  -1796457218);
	    class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		+= -1122347939;
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16756
		(Class40.anIntArray309, i_3_,
		 (31645619
		  * class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811),
		 (byte) 16);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16733
		((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  * 31645619) - i_3_,
		 1552799821);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 81);
	    if (i < 13) {
		client.aBool11015 = true;
		Class306.method5607((byte) -117);
	    }
	    Class44_Sub20.aClass704_11014 = Class704.aClass704_8825;
	}
    }
}
