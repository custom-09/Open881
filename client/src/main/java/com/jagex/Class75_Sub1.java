/* Class75_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class75_Sub1 extends Class75
{
    public Interface13 method63(int i, Interface14 interface14) {
	return new Class85(i, this);
    }
    
    Class75_Sub1(Class472 class472, int i) {
	super(class472, i);
    }
    
    public Interface13 method58(int i, Interface14 interface14, byte i_0_) {
	return new Class85(i, this);
    }
    
    public Interface13 method64(int i, Interface14 interface14) {
	return new Class85(i, this);
    }
    
    public Interface13 method60(int i, Interface14 interface14) {
	return new Class85(i, this);
    }
    
    public Interface13 method62(int i, Interface14 interface14) {
	return new Class85(i, this);
    }
    
    public Class method59(short i) {
	return com.jagex.Class85.class;
    }
    
    public Class method61() {
	return com.jagex.Class85.class;
    }
    
    static final void method17365(Class669 class669, byte i) {
	int i_1_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class41 class41
	    = ((Class41)
	       Class667.aClass44_Sub21_8582.method91(i_1_, -318255930));
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class41.method1043(-1928743265);
    }
    
    public static void method17366(Class247[] class247s, int i, int i_2_,
				   int i_3_, boolean bool, int i_4_) {
	for (int i_5_ = 0; i_5_ < class247s.length; i_5_++) {
	    Class247 class247 = class247s[i_5_];
	    if (class247 != null && i == class247.anInt2472 * -742015869) {
		Class165.method2743(class247, i_2_, i_3_, bool, 274877095);
		Class530.method8849(class247, i_2_, i_3_, -1288423629);
		if (-488164841 * class247.anInt2478
		    > (1500908359 * class247.anInt2480
		       - class247.anInt2468 * -881188269))
		    class247.anInt2478 = (class247.anInt2480 * 98803281
					  - -1988664539 * class247.anInt2468);
		if (class247.anInt2478 * -488164841 < 0)
		    class247.anInt2478 = 0;
		if (class247.anInt2479 * 2142374941
		    > (class247.anInt2481 * -166726847
		       - class247.anInt2469 * -1279656873))
		    class247.anInt2479 = (-1667108235 * class247.anInt2481
					  - -226772989 * class247.anInt2469);
		if (class247.anInt2479 * 2142374941 < 0)
		    class247.anInt2479 = 0;
		if (class247.anInt2438 * -1960530827 == 0)
		    Class662.method10991(class247s, class247, bool,
					 1428628081);
	    }
	}
    }
}
