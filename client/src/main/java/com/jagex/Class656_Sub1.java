/* Class656_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.LinkedList;
import java.util.Queue;

public class Class656_Sub1 extends Class656
{
    Queue aQueue10901 = new LinkedList();
    
    void method17039(Class534_Sub42_Sub1 class534_sub42_sub1) {
	aQueue10901.add(class534_sub42_sub1);
	if (aQueue10901.size() > 10)
	    aQueue10901.poll();
    }
    
    void method17040(Class534_Sub42_Sub1 class534_sub42_sub1, byte i) {
	aQueue10901.add(class534_sub42_sub1);
	if (aQueue10901.size() > 10)
	    aQueue10901.poll();
    }
    
    void method10883(byte i) {
	Class534_Sub42_Sub1 class534_sub42_sub1
	    = (Class534_Sub42_Sub1) aQueue10901.poll();
	if (null != class534_sub42_sub1) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4269,
				      client.aClass100_11264.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16568
		(method10862(class534_sub42_sub1, 65535, (byte) -54),
		 96738546);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16579
		((class534_sub42_sub1.method16799((byte) 78)
		  | class534_sub42_sub1.method16800(-52508810) << 16),
		 855044877);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16559
		((class534_sub42_sub1.method18408(726805513) << 1
		  | class534_sub42_sub1.method18417(-1658726153) & 0x1),
		 64504088);
	    client.aClass100_11264.method1863(class534_sub19, (byte) 107);
	    class534_sub42_sub1.method16803((byte) -51);
	}
    }
    
    boolean method10866(byte i) {
	return (!aQueue10901.isEmpty()
		|| (aLong8522 * 1164426520149525653L
		    < Class250.method4604((byte) -5) - 2000L));
    }
    
    int method10867() {
	return 1;
    }
    
    void method17041(Class534_Sub40 class534_sub40,
		     Class534_Sub42_Sub1 class534_sub42_sub1, short i) {
	class534_sub40.method16506(class534_sub42_sub1.method18417(-433696191),
				   847084713);
    }
    
    int method10882(int i) {
	return 1;
    }
    
    void method17042(Class534_Sub42_Sub1 class534_sub42_sub1) {
	aQueue10901.add(class534_sub42_sub1);
	if (aQueue10901.size() > 10)
	    aQueue10901.poll();
    }
    
    void method17043(Class534_Sub40 class534_sub40,
		     Class534_Sub42_Sub1 class534_sub42_sub1) {
	class534_sub40.method16506(class534_sub42_sub1
				       .method18417(-1516953025),
				   1403298659);
    }
    
    boolean method10871() {
	return (!aQueue10901.isEmpty()
		|| (aLong8522 * 1164426520149525653L
		    < Class250.method4604((byte) -54) - 2000L));
    }
    
    void method10870() {
	Class534_Sub42_Sub1 class534_sub42_sub1
	    = (Class534_Sub42_Sub1) aQueue10901.poll();
	if (null != class534_sub42_sub1) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4269,
				      client.aClass100_11264.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16568
		(method10862(class534_sub42_sub1, 65535, (byte) -108),
		 -1296925270);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16579
		((class534_sub42_sub1.method16799((byte) 102)
		  | class534_sub42_sub1.method16800(-992268631) << 16),
		 -654990782);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16559
		((class534_sub42_sub1.method18408(1330061636) << 1
		  | class534_sub42_sub1.method18417(-1704702571) & 0x1),
		 2147185984);
	    client.aClass100_11264.method1863(class534_sub19, (byte) 27);
	    class534_sub42_sub1.method16803((byte) -59);
	}
    }
    
    void method10868() {
	Class534_Sub42_Sub1 class534_sub42_sub1
	    = (Class534_Sub42_Sub1) aQueue10901.poll();
	if (null != class534_sub42_sub1) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4269,
				      client.aClass100_11264.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16568
		(method10862(class534_sub42_sub1, 65535, (byte) -112),
		 -799181892);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16579
		((class534_sub42_sub1.method16799((byte) 113)
		  | class534_sub42_sub1.method16800(-604661827) << 16),
		 2129953159);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16559
		((class534_sub42_sub1.method18408(669163056) << 1
		  | class534_sub42_sub1.method18417(-895223415) & 0x1),
		 1064847628);
	    client.aClass100_11264.method1863(class534_sub19, (byte) 118);
	    class534_sub42_sub1.method16803((byte) -58);
	}
    }
    
    void method10861() {
	Class534_Sub42_Sub1 class534_sub42_sub1
	    = (Class534_Sub42_Sub1) aQueue10901.poll();
	if (null != class534_sub42_sub1) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4269,
				      client.aClass100_11264.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16568
		(method10862(class534_sub42_sub1, 65535, (byte) -79),
		 -1372222765);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16579
		((class534_sub42_sub1.method16799((byte) 13)
		  | class534_sub42_sub1.method16800(-585119876) << 16),
		 687146802);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16559
		((class534_sub42_sub1.method18408(-584210687) << 1
		  | class534_sub42_sub1.method18417(-2063438550) & 0x1),
		 -8422102);
	    client.aClass100_11264.method1863(class534_sub19, (byte) 22);
	    class534_sub42_sub1.method16803((byte) -116);
	}
    }
    
    Class534_Sub19 method10873() {
	return Class346.method6128(Class404.aClass404_4222,
				   client.aClass100_11264.aClass13_1183,
				   1341391005);
    }
    
    void method17044(Class534_Sub42_Sub1 class534_sub42_sub1) {
	aQueue10901.add(class534_sub42_sub1);
	if (aQueue10901.size() > 10)
	    aQueue10901.poll();
    }
    
    Class534_Sub19 method10872() {
	return Class346.method6128(Class404.aClass404_4222,
				   client.aClass100_11264.aClass13_1183,
				   1341391005);
    }
    
    void method10864(Class534_Sub40 class534_sub40,
		     Class534_Sub42 class534_sub42, byte i) {
	method17041(class534_sub40, (Class534_Sub42_Sub1) class534_sub42,
		    (short) 14328);
    }
    
    Class534_Sub19 method10874() {
	return Class346.method6128(Class404.aClass404_4222,
				   client.aClass100_11264.aClass13_1183,
				   1341391005);
    }
    
    Class534_Sub19 method10887(int i) {
	return Class346.method6128(Class404.aClass404_4222,
				   client.aClass100_11264.aClass13_1183,
				   1341391005);
    }
    
    Class656_Sub1() {
	/* empty */
    }
    
    int method10876() {
	return 1;
    }
    
    int method10877() {
	return 1;
    }
    
    void method10878(Class534_Sub40 class534_sub40,
		     Class534_Sub42 class534_sub42) {
	method17041(class534_sub40, (Class534_Sub42_Sub1) class534_sub42,
		    (short) 25204);
    }
    
    void method10879(Class534_Sub40 class534_sub40,
		     Class534_Sub42 class534_sub42) {
	method17041(class534_sub40, (Class534_Sub42_Sub1) class534_sub42,
		    (short) 5148);
    }
}
