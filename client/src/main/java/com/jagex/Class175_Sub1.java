/* Class175_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class175_Sub1 extends Class175
{
    protected static final int anInt9426 = 4;
    public static Class44_Sub13 aClass44_Sub13_9427;
    
    public abstract void method15073(Interface21 interface21);
    
    public abstract void method15074(Interface21 interface21);
    
    public abstract void method15075(int i, Interface22 interface22);
    
    public abstract boolean method15076();
    
    public abstract void method15077(int i, int i_0_, int i_1_, int i_2_,
				     int i_3_, int i_4_, boolean bool,
				     boolean bool_5_);
    
    public abstract void method15078(int i, Interface22 interface22);
    
    public abstract void method15079(int i, Interface22 interface22);
    
    Class175_Sub1() {
	/* empty */
    }
    
    public abstract void method15080(int i, Interface22 interface22);
    
    public abstract void method15081(Interface21 interface21);
    
    public abstract void method15082(Interface21 interface21);
    
    public abstract void method15083(Interface21 interface21);
    
    public abstract boolean method15084();
    
    public abstract boolean method15085();
    
    public abstract void method15086(int i, int i_6_, int i_7_, int i_8_,
				     int i_9_, int i_10_, boolean bool,
				     boolean bool_11_);
    
    public abstract void method15087(int i, int i_12_, int i_13_, int i_14_,
				     int i_15_, int i_16_, boolean bool,
				     boolean bool_17_);
    
    static final void method15088(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
    
    static final void method15089(Class669 class669, int i) {
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = class669.aClass534_Sub26_8606.aString10578;
    }
}
