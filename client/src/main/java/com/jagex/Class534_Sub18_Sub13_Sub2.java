/* Class534_Sub18_Sub13_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub18_Sub13_Sub2 extends Class534_Sub18_Sub13
{
    float aFloat12158;
    float aFloat12159;
    float aFloat12160;
    Class285 aClass285_12161;
    
    public void method18386(Class534_Sub40 class534_sub40) {
	aClass285_12161
	    = Class54.method1209(class534_sub40.method16527(931730464),
				 1067003437);
	aFloat12159 = class534_sub40.method16539(-1446686828);
	aFloat12160 = class534_sub40.method16539(-1955362068);
    }
    
    Class534_Sub18_Sub13_Sub2(int i, Class534_Sub40 class534_sub40) {
	super(i);
	aClass285_12161
	    = Class54.method1209(class534_sub40.method16527(-549068416),
				 1639520815);
	aFloat12159 = class534_sub40.method16539(-1447801353);
	aFloat12160 = class534_sub40.method16539(-2020978174);
    }
    
    public void method18388(Class287 class287, Class446 class446,
			    Class433 class433) {
	float f = (float) Math.sin((double) aFloat12158);
	float f_0_ = f * aFloat12159;
	if (Class285.aClass285_3074 == aClass285_12161) {
	    Class287 class287_1_;
	    (class287_1_ = class287).anInt3082
		= ((int) (f_0_ + (float) (1135682509 * class287_1_.anInt3082))
		   * -1803517691);
	    class446.method7287(f_0_, 0.0F, 0.0F);
	} else if (Class285.aClass285_3075 == aClass285_12161) {
	    Class287 class287_2_;
	    (class287_2_ = class287).anInt3083
		= ((int) (f_0_ + (float) (-1018779427 * class287_2_.anInt3083))
		   * -1390317707);
	    class446.method7287(0.0F, f_0_, 0.0F);
	} else if (Class285.aClass285_3076 == aClass285_12161) {
	    Class287 class287_3_;
	    (class287_3_ = class287).anInt3081
		= ((int) ((float) (class287_3_.anInt3081 * 975339373) + f_0_)
		   * 1552704101);
	    class446.method7287(0.0F, 0.0F, f_0_);
	} else if (Class285.aClass285_3077 == aClass285_12161)
	    class446.method7245(1.0F, 0.0F, 0.0F, f_0_);
	else if (aClass285_12161 == Class285.aClass285_3078)
	    class446.method7245(0.0F, 1.0F, 0.0F, f_0_);
	else if (Class285.aClass285_3079 == aClass285_12161)
	    class446.method7245(0.0F, 0.0F, 1.0F, f_0_);
    }
    
    public Class534_Sub18_Sub13_Sub2(int i, Class285 class285, float f,
				     float f_4_) {
	super(i);
	aClass285_12161 = class285;
	aFloat12159 = f;
	aFloat12160 = f_4_;
    }
    
    public void method18383(Class534_Sub40 class534_sub40, int i) {
	aClass285_12161
	    = Class54.method1209(class534_sub40.method16527(750910456),
				 952188427);
	aFloat12159 = class534_sub40.method16539(-1607918081);
	aFloat12160 = class534_sub40.method16539(-1321908529);
    }
    
    public void method18390(Class534_Sub40 class534_sub40) {
	aClass285_12161
	    = Class54.method1209(class534_sub40.method16527(-976159014),
				 282254445);
	aFloat12159 = class534_sub40.method16539(-2077955945);
	aFloat12160 = class534_sub40.method16539(-1822401159);
    }
    
    public void method18385(float f) {
	aFloat12158 += f * aFloat12160;
    }
    
    public void method18384(float f) {
	aFloat12158 += f * aFloat12160;
    }
    
    public void method18387(Class534_Sub40 class534_sub40) {
	aClass285_12161
	    = Class54.method1209(class534_sub40.method16527(237934559),
				 1441588068);
	aFloat12159 = class534_sub40.method16539(-1490285506);
	aFloat12160 = class534_sub40.method16539(-1212052676);
    }
    
    public void method18382(Class287 class287, Class446 class446,
			    Class433 class433, int i) {
	float f = (float) Math.sin((double) aFloat12158);
	float f_5_ = f * aFloat12159;
	if (Class285.aClass285_3074 == aClass285_12161) {
	    Class287 class287_6_;
	    (class287_6_ = class287).anInt3082
		= ((int) (f_5_ + (float) (1135682509 * class287_6_.anInt3082))
		   * -1803517691);
	    class446.method7287(f_5_, 0.0F, 0.0F);
	} else if (Class285.aClass285_3075 == aClass285_12161) {
	    Class287 class287_7_;
	    (class287_7_ = class287).anInt3083
		= ((int) (f_5_ + (float) (-1018779427 * class287_7_.anInt3083))
		   * -1390317707);
	    class446.method7287(0.0F, f_5_, 0.0F);
	} else if (Class285.aClass285_3076 == aClass285_12161) {
	    Class287 class287_8_;
	    (class287_8_ = class287).anInt3081
		= ((int) ((float) (class287_8_.anInt3081 * 975339373) + f_5_)
		   * 1552704101);
	    class446.method7287(0.0F, 0.0F, f_5_);
	} else if (Class285.aClass285_3077 == aClass285_12161)
	    class446.method7245(1.0F, 0.0F, 0.0F, f_5_);
	else if (aClass285_12161 == Class285.aClass285_3078)
	    class446.method7245(0.0F, 1.0F, 0.0F, f_5_);
	else if (Class285.aClass285_3079 == aClass285_12161)
	    class446.method7245(0.0F, 0.0F, 1.0F, f_5_);
    }
    
    public void method18389(Class534_Sub40 class534_sub40) {
	aClass285_12161
	    = Class54.method1209(class534_sub40.method16527(1239632535),
				 2053703329);
	aFloat12159 = class534_sub40.method16539(-998835094);
	aFloat12160 = class534_sub40.method16539(-1418181111);
    }
    
    public void method18381(float f, byte i) {
	aFloat12158 += f * aFloat12160;
    }
}
