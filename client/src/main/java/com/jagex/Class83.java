/* Class83 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import jaclib.ping.Ping;

public class Class83
{
    static final int anInt836 = 3;
    
    Class83() throws Throwable {
	throw new Error();
    }
    
    public static int method1651(byte i) {
	if (Class449.anInt4912 * 41966215 == 0) {
	    Class47.aClass47_333.method1121(new Class448("jaclib"),
					    (short) 128);
	    if (Class47.aClass47_333.method1118(1828410049)
		    .method56(-1674657878)
		!= 100)
		return 1;
	    if (!((Class448) Class47.aClass47_333.method1118(1828410049))
		     .method7310(2021454680)
		&& !Class331.method5859(-608656088)) {
		try {
		    Class700.aclient8805.method8170(-1004977104);
		    Class313.method5689(2045641570);
		    Ping.init();
		} catch (Exception_Sub3 exception_sub3) {
		    Class530.method8852(Class68.aClass68_725,
					exception_sub3.aString11405,
					exception_sub3.anInt11406 * 1583361521,
					exception_sub3.getCause(), (byte) 53);
		} catch (Throwable throwable) {
		    /* empty */
		}
	    }
	    Class449.anInt4912 = -341750985;
	}
	if (41966215 * Class449.anInt4912 == 1) {
	    Class449.aClass47Array4911 = Class47.method1126(496049595);
	    Class47.aClass47_351.method1121(new Class439(Class588
							 .aClass472_7807),
					    (short) 128);
	    Class47.aClass47_334.method1121(new Class448("jaggl"),
					    (short) 128);
	    Class448 class448 = new Class448("jagdx");
	    if (!Class262.aString2801.startsWith("win"))
		class448.method7309(1031447805);
	    Class47.aClass47_335.method1121(class448, (short) 128);
	    Class47.aClass47_336.method1121(new Class448("sw3d"), (short) 128);
	    Class47.aClass47_343.method1121(new Class448("hw3d"), (short) 128);
	    Class47.aClass47_352.method1121(new Class448("RuneScape-Setup.exe",
							 true),
					    (short) 128);
	    Class47.aClass47_339.method1121(new Class439(Class295
							 .aClass472_3161),
					    (short) 128);
	    Class47.aClass47_340.method1121(new Class439(Class699
							 .aClass472_8802),
					    (short) 128);
	    Class47.aClass47_349.method1121(new Class439(Class233
							 .aClass472_2358),
					    (short) 128);
	    Class47.aClass47_342.method1121(new Class439(Class190
							 .aClass472_2137),
					    (short) 128);
	    Class47.aClass47_337.method1121(new Class439(Class242
							 .aClass472_2410),
					    (short) 128);
	    Class47.aClass47_344.method1121(new Class439(Class200_Sub6
							 .aClass472_9906),
					    (short) 128);
	    Class47.aClass47_345.method1121(new Class439(Class711_Sub3_Sub1
							 .aClass472_11919),
					    (short) 128);
	    Class47.aClass47_341.method1121(new Class439(Class347_Sub3
							 .aClass472_10254),
					    (short) 128);
	    Class47.aClass47_347.method1121(new Class439(Class311
							 .aClass472_3364),
					    (short) 128);
	    Class47.aClass47_348
		.method1121(new Class439(Class52.aClass472_435), (short) 128);
	    Class47.aClass47_356.method1121(new Class439(Class6.aClass472_57),
					    (short) 128);
	    Class47.aClass47_350.method1121(new Class439(Class494
							 .aClass472_5535),
					    (short) 128);
	    Class47.aClass47_346.method1121(new Class439(Class369
							 .aClass472_3868),
					    (short) 128);
	    Class47.aClass47_355.method1121(new Class439(Class680
							 .aClass472_8669),
					    (short) 128);
	    Class47.aClass47_353.method1121(new Class439(Class397
							 .aClass472_4120),
					    (short) 128);
	    Class47.aClass47_362.method1121(new Class447((Class631
							  .aClass472_8222),
							 "huffman"),
					    (short) 128);
	    Class47.aClass47_338.method1121(new Class439(Class289
							 .aClass472_3106),
					    (short) 128);
	    Class47.aClass47_332.method1121(new Class439(Class294
							 .aClass472_3158),
					    (short) 128);
	    Class47.aClass47_357.method1121(new Class439(Class606
							 .aClass472_7988),
					    (short) 128);
	    Class47.aClass47_358.method1121(new Class437((Class693
							  .aClass472_8770),
							 0),
					    (short) 128);
	    for (int i_0_ = 0; i_0_ < Class449.aClass47Array4911.length;
		 i_0_++) {
		if (Class449.aClass47Array4911[i_0_].method1118(1828410049)
		    == null)
		    throw new RuntimeException();
	    }
	    int i_1_ = 0;
	    Class47[] class47s = Class449.aClass47Array4911;
	    for (int i_2_ = 0; i_2_ < class47s.length; i_2_++) {
		Class47 class47 = class47s[i_2_];
		int i_3_ = class47.method1122(1716080348);
		int i_4_ = class47.method1118(1828410049).method56(-625239595);
		i_1_ += i_3_ * i_4_ / 100;
	    }
	    Class449.anInt4913 = i_1_ * -285262333;
	    Class449.anInt4912 = -683501970;
	}
	if (null == Class449.aClass47Array4911)
	    return 100;
	int i_5_ = 0;
	int i_6_ = 0;
	boolean bool = true;
	Class47[] class47s = Class449.aClass47Array4911;
	for (int i_7_ = 0; i_7_ < class47s.length; i_7_++) {
	    Class47 class47 = class47s[i_7_];
	    int i_8_ = class47.method1122(1749144940);
	    int i_9_ = class47.method1118(1828410049).method56(-1495956647);
	    if (i_9_ < 100)
		bool = false;
	    i_5_ += i_8_;
	    i_6_ += i_9_ * i_8_ / 100;
	}
	if (bool)
	    Class449.aClass47Array4911 = null;
	i_6_ -= Class449.anInt4913 * -1094853461;
	i_5_ -= Class449.anInt4913 * -1094853461;
	int i_10_ = i_5_ > 0 ? i_6_ * 100 / i_5_ : 100;
	if (!bool && i_10_ > 99)
	    i_10_ = 99;
	return i_10_;
    }
    
    static final void method1652(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class171.method2882(class247, class243, class669, -271914077);
    }
}
