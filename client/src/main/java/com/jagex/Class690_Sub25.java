/* Class690_Sub25 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub25 extends Class690
{
    public static final int anInt10923 = 0;
    public static final int anInt10924 = 1;
    
    void method14023(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    public Class690_Sub25(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public Class690_Sub25(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    int method14017(int i) {
	return 1;
    }
    
    public boolean method17101(int i) {
	if (aClass534_Sub35_8752.method16441(-1974233767)
	    == Class675.aClass675_8634)
	    return true;
	return false;
    }
    
    public int method14026(int i, int i_0_) {
	if (aClass534_Sub35_8752.method16441(756880633)
	    == Class675.aClass675_8634) {
	    if (i == 0) {
		if (aClass534_Sub35_8752.aClass690_Sub11_10749
			.method16976(2088438307)
		    == 1)
		    return 2;
		if (aClass534_Sub35_8752.aClass690_Sub16_10763
			.method17030((byte) -27)
		    == 1)
		    return 2;
		if (aClass534_Sub35_8752.aClass690_Sub3_10767
			.method16839(-1182835781)
		    > 0)
		    return 2;
	    }
	    return 1;
	}
	return 3;
    }
    
    void method14025(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    public int method17102(byte i) {
	return 189295939 * anInt8753;
    }
    
    public void method17103() {
	if (aClass534_Sub35_8752.method16441(-1776655393)
	    != Class675.aClass675_8634)
	    anInt8753 = 1823770475;
	if (0 != 189295939 * anInt8753 && 1 != 189295939 * anInt8753)
	    anInt8753 = method14017(2144688171) * 1823770475;
    }
    
    int method14022() {
	return 1;
    }
    
    int method14018() {
	return 1;
    }
    
    void method14024(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    void method14020(int i, int i_1_) {
	anInt8753 = 1823770475 * i;
    }
    
    public void method17104(int i) {
	if (aClass534_Sub35_8752.method16441(1333269807)
	    != Class675.aClass675_8634)
	    anInt8753 = 1823770475;
	if (0 != 189295939 * anInt8753 && 1 != 189295939 * anInt8753)
	    anInt8753 = method14017(2115259273) * 1823770475;
    }
    
    public int method14027(int i) {
	if (aClass534_Sub35_8752.method16441(635354779)
	    == Class675.aClass675_8634) {
	    if (i == 0) {
		if (aClass534_Sub35_8752.aClass690_Sub11_10749
			.method16976(2088438307)
		    == 1)
		    return 2;
		if (aClass534_Sub35_8752.aClass690_Sub16_10763
			.method17030((byte) -4)
		    == 1)
		    return 2;
		if (aClass534_Sub35_8752.aClass690_Sub3_10767
			.method16839(82695998)
		    > 0)
		    return 2;
	    }
	    return 1;
	}
	return 3;
    }
    
    public boolean method17105() {
	if (aClass534_Sub35_8752.method16441(1538356836)
	    == Class675.aClass675_8634)
	    return true;
	return false;
    }
    
    public int method14029(int i) {
	if (aClass534_Sub35_8752.method16441(-1170133732)
	    == Class675.aClass675_8634) {
	    if (i == 0) {
		if (aClass534_Sub35_8752.aClass690_Sub11_10749
			.method16976(2088438307)
		    == 1)
		    return 2;
		if (aClass534_Sub35_8752.aClass690_Sub16_10763
			.method17030((byte) -34)
		    == 1)
		    return 2;
		if (aClass534_Sub35_8752.aClass690_Sub3_10767
			.method16839(232993668)
		    > 0)
		    return 2;
	    }
	    return 1;
	}
	return 3;
    }
    
    public int method14030(int i) {
	if (aClass534_Sub35_8752.method16441(46002304)
	    == Class675.aClass675_8634) {
	    if (i == 0) {
		if (aClass534_Sub35_8752.aClass690_Sub11_10749
			.method16976(2088438307)
		    == 1)
		    return 2;
		if (aClass534_Sub35_8752.aClass690_Sub16_10763
			.method17030((byte) -2)
		    == 1)
		    return 2;
		if (aClass534_Sub35_8752.aClass690_Sub3_10767
			.method16839(69163636)
		    > 0)
		    return 2;
	    }
	    return 1;
	}
	return 3;
    }
    
    public void method17106() {
	if (aClass534_Sub35_8752.method16441(344091216)
	    != Class675.aClass675_8634)
	    anInt8753 = 1823770475;
	if (0 != 189295939 * anInt8753 && 1 != 189295939 * anInt8753)
	    anInt8753 = method14017(2128554055) * 1823770475;
    }
    
    public int method14028(int i) {
	if (aClass534_Sub35_8752.method16441(142617980)
	    == Class675.aClass675_8634) {
	    if (i == 0) {
		if (aClass534_Sub35_8752.aClass690_Sub11_10749
			.method16976(2088438307)
		    == 1)
		    return 2;
		if (aClass534_Sub35_8752.aClass690_Sub16_10763
			.method17030((byte) -77)
		    == 1)
		    return 2;
		if (aClass534_Sub35_8752.aClass690_Sub3_10767
			.method16839(-1142289184)
		    > 0)
		    return 2;
	    }
	    return 1;
	}
	return 3;
    }
    
    public boolean method17107() {
	if (aClass534_Sub35_8752.method16441(1765895756)
	    == Class675.aClass675_8634)
	    return true;
	return false;
    }
    
    public boolean method17108() {
	if (aClass534_Sub35_8752.method16441(-2094527015)
	    == Class675.aClass675_8634)
	    return true;
	return false;
    }
    
    public boolean method17109() {
	if (aClass534_Sub35_8752.method16441(-30159728)
	    == Class675.aClass675_8634)
	    return true;
	return false;
    }
    
    public int method17110() {
	return 189295939 * anInt8753;
    }
    
    int method14021() {
	return 1;
    }
    
    static final void method17111(Class247 class247, Class243 class243,
				  Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_2_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_3_
	    = class669.anIntArray8595[1 + 2088438307 * class669.anInt8600];
	if (-1 == i_2_ && i_3_ == -1)
	    class247.aClass247_2556 = null;
	else
	    class247.aClass247_2556
		= Class81.method1637(i_2_, i_3_, 1857943806);
    }
}
