/* Class259 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class259 implements Interface28
{
    int anInt2782;
    int anInt2783;
    Class213 this$0;
    
    public void method173(Class214 class214, int i) {
	class214.method4048(676628665 * anInt2782, -365211249 * anInt2783,
			    (byte) -31);
	class214.method4044(676628665 * anInt2782, 1481307617)
	    .method3998((byte) -101);
    }
    
    public void method172(Class214 class214) {
	class214.method4048(676628665 * anInt2782, -365211249 * anInt2783,
			    (byte) -80);
	class214.method4044(676628665 * anInt2782, 1481307617)
	    .method3998((byte) -31);
    }
    
    Class259(Class213 class213, Class534_Sub40 class534_sub40) {
	this$0 = class213;
	anInt2782 = class534_sub40.method16529((byte) 1) * -1027277431;
	anInt2783 = class534_sub40.method16527(1967854157) * 2050403695;
    }
    
    public void method174(Class214 class214) {
	class214.method4048(676628665 * anInt2782, -365211249 * anInt2783,
			    (byte) -99);
	class214.method4044(676628665 * anInt2782, 1481307617)
	    .method3998((byte) -41);
    }
    
    static void method4794(String string, int i) {
	Class331.aClass702_3495 = Class702.aClass702_8815;
	Class534_Sub1_Sub2.aString11720 = string;
    }
    
    static final void method4795(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class453.method7403(class247, class243, class669, -1196995294);
    }
    
    static final void method4796(Class669 class669, byte i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_0_, 1233905264);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_0_ >> 16];
	Class332.method5861(class247, class243, class669, (byte) -52);
    }
    
    static final void method4797(Class669 class669, byte i) {
	int i_1_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_1_, 1959152507);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 290091559 * class247.anInt2482;
    }
    
    static final void method4798(Class669 class669, byte i) {
	int i_2_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class100 class100 = Class201.method3864(2095398292);
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4167,
				  class100.aClass13_1183, 1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16506(0, 1114858935);
	int i_3_
	    = class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811 * 31645619;
	class534_sub19.aClass534_Sub40_Sub1_10513.method16506(i_2_,
							      1802063225);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16507
	    (-1791556207 * class669.aClass395_8601.anInt4103, 1848723882);
	class669.aClass395_8601.aClass534_Sub18_Sub12_4104.method18357
	    (class534_sub19.aClass534_Sub40_Sub1_10513,
	     class669.aClass395_8601.anIntArray4102, -523037856);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16528
	    ((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811 * 31645619
	      - i_3_),
	     2110128355);
	class100.method1863(class534_sub19, (byte) 39);
    }
    
    static final void method4799(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
    
    static final void method4800(Class669 class669, byte i) {
	class669.anInt8600 -= 926998689;
	Class351.method6187((class669.anIntArray8595
			     [class669.anInt8600 * 2088438307 + 1]),
			    489581164);
    }
    
    static final void method4801(int i) {
	Class555.aClass44_Sub16_7417.method1081(5, (short) 25753);
	Class88.aClass44_Sub12_884.method1081(5, (short) 13937);
	Class307.aClass44_Sub15_3349.method1081(5, (short) 24232);
	client.aClass512_11100.method8428(-1486655428)
	    .method1081(5, (short) 26107);
	Class578.aClass44_Sub3_7743.method1081(5, (short) 7301);
	Class531.aClass44_Sub7_7135.method1081(5, (short) 4033);
	Class200_Sub12.aClass44_Sub1_9934.method1081(5, (short) 28163);
	Class55.aClass44_Sub4_447.method1081(5, (short) 21104);
	Class84.aClass44_Sub11_840.method1081(5, (short) 29903);
	Class562.aClass110_Sub1_Sub1_7560.method17854(5, 1610637480);
	Class535.aClass110_Sub1_Sub2_7162.method17880(5, 620498166);
	Class78.aClass110_Sub1_Sub2_826.method17880(5, 870622525);
	Class279.aClass110_Sub1_Sub2_3053.method17880(5, 752938699);
	Class522.aClass110_Sub1_Sub2_7083.method17880(5, -1254829506);
	Class534_Sub24.aClass110_Sub1_Sub2_10565.method17880(5, 141546490);
	Class200_Sub23.aClass44_Sub14_10041.method1081(5, (short) 18695);
	Class222.aClass44_Sub9_2313.method1081(5, (short) 21482);
	Class394_Sub1.aClass44_Sub18_10148.method1081(5, (short) 13404);
	Class534_Sub11_Sub13.aClass44_Sub22_11730.method1081(5, (short) 28201);
	Class184.aClass44_Sub6_1988.method1081(5, (short) 1233);
	Class492.aClass44_Sub10_5341.method1081(5, (short) 18548);
	Class200_Sub10.aClass44_Sub20_9926.method1081(5, (short) 9488);
	Class650.aClass44_Sub5_8464.method1081(5, (short) 746);
	Class632.aClass44_Sub2_8270.method1081(5, (short) 15757);
	Class470.aClass44_Sub17_5153.method1081(5, (short) 24102);
	Class302.method5566(5, (byte) 2);
	Class459.method7444(50, -542081924);
	Class351.aClass406_3620.method6670(50, 1225863589);
	Class483.method7943(5, 502795905);
	Class388.method6529(5, (byte) 92);
	client.aClass203_11078.method3876(5, (byte) 0);
	client.aClass203_11079.method3876(5, (byte) 0);
	Class679.aClass203_8660.method3876(5, (byte) 0);
	client.aClass203_11336.method3876(5, (byte) 0);
	Class150_Sub1.aClass44_8902.method1081(5, (short) 3482);
	Class706.aClass44_8845.method1081(5, (short) 29229);
	client.aClass203_11082.method3876(5, (byte) 0);
    }
}
