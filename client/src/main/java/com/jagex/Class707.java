/* Class707 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class707
{
    public static byte[] method14255(byte[] is) {
	if (is == null)
	    return null;
	byte[] is_0_ = new byte[is.length];
	System.arraycopy(is, 0, is_0_, 0, is.length);
	return is_0_;
    }
    
    Class707() throws Throwable {
	throw new Error();
    }
    
    public static byte[] method14256(byte[] is) {
	if (is == null)
	    return null;
	byte[] is_1_ = new byte[is.length];
	System.arraycopy(is, 0, is_1_, 0, is.length);
	return is_1_;
    }
    
    public static byte[] method14257(byte[] is) {
	if (is == null)
	    return null;
	byte[] is_2_ = new byte[is.length];
	System.arraycopy(is, 0, is_2_, 0, is.length);
	return is_2_;
    }
    
    public static Class398 method14258(Class534_Sub40 class534_sub40, byte i) {
	int i_3_ = class534_sub40.method16533(-258848859);
	return new Class398(i_3_);
    }
    
    static final void method14259(Class669 class669, int i) {
	class669.anInt8600 -= 926998689;
	int i_4_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_5_
	    = class669.anIntArray8595[2088438307 * class669.anInt8600 + 1];
	int i_6_
	    = class669.anIntArray8595[2 + 2088438307 * class669.anInt8600];
	int i_7_ = 255;
	int i_8_ = 256;
	Class171_Sub4.aClass232_9944.method4237(Class211.aClass211_2255, i_4_,
						i_5_, i_7_,
						Class190.aClass190_2134
						    .method3763(-975813861),
						Class207.aClass207_2235, 0.0F,
						0.0F, null, 0, i_8_, i_6_,
						(byte) 104);
    }
}
