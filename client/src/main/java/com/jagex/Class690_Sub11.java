/* Class690_Sub11 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub11 extends Class690
{
    public static final int anInt10880 = 0;
    public static final int anInt10881 = 1;
    
    int method14017(int i) {
	return 1;
    }
    
    public boolean method16974(byte i) {
	return true;
    }
    
    int method14022() {
	return 1;
    }
    
    public Class690_Sub11(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public void method16975(int i) {
	if (189295939 * anInt8753 != 0
	    && aClass534_Sub35_8752.aClass690_Sub25_10750
		   .method17102((byte) 1) != 1)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 != 0
	    && aClass534_Sub35_8752.aClass690_Sub13_10761
		   .method16993((byte) 64) == 2)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 < 0 || 189295939 * anInt8753 > 1)
	    anInt8753 = method14017(2106016500) * 1823770475;
    }
    
    public int method14026(int i, int i_0_) {
	if (0 != i && aClass534_Sub35_8752.aClass690_Sub13_10761
			  .method16993((byte) 56) == 2)
	    return 3;
	if (0 == i || aClass534_Sub35_8752.aClass690_Sub25_10750
			  .method17102((byte) -58) == 1)
	    return 1;
	return 2;
    }
    
    void method14020(int i, int i_1_) {
	anInt8753 = 1823770475 * i;
    }
    
    public int method16976(int i) {
	return anInt8753 * 189295939;
    }
    
    public void method16977() {
	if (189295939 * anInt8753 != 0
	    && aClass534_Sub35_8752.aClass690_Sub25_10750
		   .method17102((byte) -35) != 1)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 != 0
	    && aClass534_Sub35_8752.aClass690_Sub13_10761
		   .method16993((byte) 18) == 2)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 < 0 || 189295939 * anInt8753 > 1)
	    anInt8753 = method14017(2098716905) * 1823770475;
    }
    
    public Class690_Sub11(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    int method14018() {
	return 1;
    }
    
    void method14024(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    void method14025(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    void method14023(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    public int method14027(int i) {
	if (0 != i && aClass534_Sub35_8752.aClass690_Sub13_10761
			  .method16993((byte) -12) == 2)
	    return 3;
	if (0 == i || aClass534_Sub35_8752.aClass690_Sub25_10750
			  .method17102((byte) -124) == 1)
	    return 1;
	return 2;
    }
    
    int method14021() {
	return 1;
    }
    
    public int method14029(int i) {
	if (0 != i && aClass534_Sub35_8752.aClass690_Sub13_10761
			  .method16993((byte) -108) == 2)
	    return 3;
	if (0 == i || aClass534_Sub35_8752.aClass690_Sub25_10750
			  .method17102((byte) -110) == 1)
	    return 1;
	return 2;
    }
    
    public int method14030(int i) {
	if (0 != i && aClass534_Sub35_8752.aClass690_Sub13_10761
			  .method16993((byte) 1) == 2)
	    return 3;
	if (0 == i || aClass534_Sub35_8752.aClass690_Sub25_10750
			  .method17102((byte) -126) == 1)
	    return 1;
	return 2;
    }
    
    public int method14028(int i) {
	if (0 != i && aClass534_Sub35_8752.aClass690_Sub13_10761
			  .method16993((byte) 10) == 2)
	    return 3;
	if (0 == i || aClass534_Sub35_8752.aClass690_Sub25_10750
			  .method17102((byte) -14) == 1)
	    return 1;
	return 2;
    }
    
    public int method16978() {
	return anInt8753 * 189295939;
    }
    
    public void method16979() {
	if (189295939 * anInt8753 != 0
	    && aClass534_Sub35_8752.aClass690_Sub25_10750
		   .method17102((byte) -121) != 1)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 != 0
	    && aClass534_Sub35_8752.aClass690_Sub13_10761
		   .method16993((byte) 17) == 2)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 < 0 || 189295939 * anInt8753 > 1)
	    anInt8753 = method14017(2115413590) * 1823770475;
    }
    
    public boolean method16980() {
	return true;
    }
    
    public boolean method16981() {
	return true;
    }
    
    public void method16982() {
	if (189295939 * anInt8753 != 0
	    && aClass534_Sub35_8752.aClass690_Sub25_10750
		   .method17102((byte) -54) != 1)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 != 0
	    && aClass534_Sub35_8752.aClass690_Sub13_10761
		   .method16993((byte) -11) == 2)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 < 0 || 189295939 * anInt8753 > 1)
	    anInt8753 = method14017(2092419409) * 1823770475;
    }
    
    public int method16983() {
	return anInt8753 * 189295939;
    }
    
    static final void method16984(Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = Class47.method1127(string, (byte) 117);
    }
}
