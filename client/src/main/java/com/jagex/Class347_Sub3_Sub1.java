/* Class347_Sub3_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class347_Sub3_Sub1 extends Class347_Sub3
{
    int anInt11455 = 0;
    float[] aFloatArray11456;
    float[] aFloatArray11457;
    
    public float method6134(int i) {
	return aClass434Array10250[-1976881567 * anInt11455]
		   .method6963(aFloat10251);
    }
    
    void method15854(byte i) {
	anInt11455 += 1771435425;
    }
    
    float method15853(float f, float f_0_, float f_1_, int i) {
	float f_2_ = (aFloat10251
		      / (float) aClass434Array10250
				    [-1976881567 * anInt11455].method6957());
	float f_3_ = ((aFloatArray11456[anInt11455 * -1976881567]
		       - aFloatArray11457[anInt11455 * -1976881567]) * f_2_
		      + aFloatArray11457[-1976881567 * anInt11455]);
	return f_3_;
    }
    
    void method15852(float f, float f_4_, int i) {
	aFloat10251 += f;
	if (aFloat10251 > (float) aClass434Array10250
				      [-1976881567 * anInt11455].method6957())
	    aFloat10251 = (float) aClass434Array10250
				      [anInt11455 * -1976881567].method6957();
    }
    
    public Class438 method6137(int i) {
	Class438 class438 = Class438.method7061();
	double[] ds = aClass434Array10250[-1976881567 * anInt11455]
			  .method6971(aFloat10251);
	class438.aFloat4864 = (float) ds[0];
	class438.aFloat4863 = (float) ds[1];
	class438.aFloat4865 = (float) ds[2];
	return class438;
    }
    
    public double[] method6130(int i) {
	return aClass434Array10250[anInt11455 * -1976881567]
		   .method6971(aFloat10251);
    }
    
    public float method15861(byte i) {
	return aFloat10251;
    }
    
    public Class438 method6143() {
	Class438 class438 = Class438.method7061();
	double[] ds = aClass434Array10250[-1976881567 * anInt11455]
			  .method6971(aFloat10251);
	class438.aFloat4864 = (float) ds[0];
	class438.aFloat4863 = (float) ds[1];
	class438.aFloat4865 = (float) ds[2];
	return class438;
    }
    
    void method15860(Class534_Sub40 class534_sub40, int i, int i_5_) {
	aFloatArray11457 = new float[i];
	aFloatArray11456 = new float[i];
	anInt11455 = 0;
	for (int i_6_ = 0; i_6_ < i; i_6_++) {
	    aFloatArray11457[i_6_] = class534_sub40.method16539(-2137289442);
	    aFloatArray11456[i_6_] = class534_sub40.method16539(-1331921345);
	}
    }
    
    void method15855() {
	anInt11455 += 1771435425;
    }
    
    void method15856() {
	anInt11455 += 1771435425;
    }
    
    float method15862(float f, float f_7_, float f_8_) {
	float f_9_ = (aFloat10251
		      / (float) aClass434Array10250
				    [-1976881567 * anInt11455].method6957());
	float f_10_ = ((aFloatArray11456[anInt11455 * -1976881567]
			- aFloatArray11457[anInt11455 * -1976881567]) * f_9_
		       + aFloatArray11457[-1976881567 * anInt11455]);
	return f_10_;
    }
    
    void method15857(Class534_Sub40 class534_sub40, int i) {
	aFloatArray11457 = new float[i];
	aFloatArray11456 = new float[i];
	anInt11455 = 0;
	for (int i_11_ = 0; i_11_ < i; i_11_++) {
	    aFloatArray11457[i_11_] = class534_sub40.method16539(-1440254594);
	    aFloatArray11456[i_11_] = class534_sub40.method16539(-1372285624);
	}
    }
    
    void method15858(Class534_Sub40 class534_sub40, int i) {
	aFloatArray11457 = new float[i];
	aFloatArray11456 = new float[i];
	anInt11455 = 0;
	for (int i_12_ = 0; i_12_ < i; i_12_++) {
	    aFloatArray11457[i_12_] = class534_sub40.method16539(-1887882041);
	    aFloatArray11456[i_12_] = class534_sub40.method16539(-1702914818);
	}
    }
    
    public float method15865() {
	return aFloat10251;
    }
    
    public Class438 method6129() {
	Class438 class438 = Class438.method7061();
	double[] ds = aClass434Array10250[-1976881567 * anInt11455]
			  .method6971(aFloat10251);
	class438.aFloat4864 = (float) ds[0];
	class438.aFloat4863 = (float) ds[1];
	class438.aFloat4865 = (float) ds[2];
	return class438;
    }
    
    void method15866(Class534_Sub40 class534_sub40, int i) {
	aFloatArray11457 = new float[i];
	aFloatArray11456 = new float[i];
	anInt11455 = 0;
	for (int i_13_ = 0; i_13_ < i; i_13_++) {
	    aFloatArray11457[i_13_] = class534_sub40.method16539(-1596792655);
	    aFloatArray11456[i_13_] = class534_sub40.method16539(-1519550202);
	}
    }
    
    public float method15859() {
	return aFloat10251;
    }
    
    public double[] method6144() {
	return aClass434Array10250[anInt11455 * -1976881567]
		   .method6971(aFloat10251);
    }
    
    public float method6153() {
	return aClass434Array10250[-1976881567 * anInt11455]
		   .method6963(aFloat10251);
    }
    
    public float method6154() {
	return aClass434Array10250[-1976881567 * anInt11455]
		   .method6963(aFloat10251);
    }
    
    public Class438 method6146() {
	Class438 class438 = Class438.method7061();
	double[] ds = aClass434Array10250[-1976881567 * anInt11455]
			  .method6971(aFloat10251);
	class438.aFloat4864 = (float) ds[0];
	class438.aFloat4863 = (float) ds[1];
	class438.aFloat4865 = (float) ds[2];
	return class438;
    }
    
    float method15863(float f, float f_14_, float f_15_) {
	float f_16_ = (aFloat10251
		       / (float) aClass434Array10250
				     [-1976881567 * anInt11455].method6957());
	float f_17_ = ((aFloatArray11456[anInt11455 * -1976881567]
			- aFloatArray11457[anInt11455 * -1976881567]) * f_16_
		       + aFloatArray11457[-1976881567 * anInt11455]);
	return f_17_;
    }
    
    void method15864(float f, float f_18_) {
	aFloat10251 += f;
	if (aFloat10251 > (float) aClass434Array10250
				      [-1976881567 * anInt11455].method6957())
	    aFloat10251 = (float) aClass434Array10250
				      [anInt11455 * -1976881567].method6957();
    }
    
    public Class347_Sub3_Sub1(Class298 class298) {
	super(class298);
    }
    
    public Class438 method6145() {
	Class438 class438 = Class438.method7061();
	double[] ds = aClass434Array10250[-1976881567 * anInt11455]
			  .method6971(aFloat10251);
	class438.aFloat4864 = (float) ds[0];
	class438.aFloat4863 = (float) ds[1];
	class438.aFloat4865 = (float) ds[2];
	return class438;
    }
}
