/* Class62 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class62
{
    static Class62 aClass62_648;
    static Class62 aClass62_649 = new Class62();
    static Class534_Sub18_Sub7 aClass534_Sub18_Sub7_650;
    
    Class62() {
	/* empty */
    }
    
    static {
	aClass62_648 = new Class62();
    }
    
    public static Class605[] method1262(byte i) {
	return (new Class605[]
		{ Class605.aClass605_7979, Class605.aClass605_7977,
		  Class605.aClass605_7978 });
    }
    
    static final void method1263(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub28_10754
		  .method17131(892704307);
    }
    
    static final void method1264(Class654_Sub1 class654_sub1, int i,
				 boolean bool, short i_0_) {
	Class17.method774(class654_sub1, i, false, bool, 16711935);
    }
    
    static final void method1265(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class247 class247_1_
	    = Class578.method9799(class243, class247, -1495101509);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class247_1_ == null ? -1 : class247_1_.anInt2454 * -1278450723;
    }
}
