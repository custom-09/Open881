/* Class534_Sub37_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub37_Sub2 extends Class534_Sub37
{
    int anInt11884;
    
    public Class534_Sub37_Sub2(int i, int i_0_, int i_1_) {
	super(i, i_0_);
	anInt11884 = i_1_ * -904677305;
    }
    
    public boolean method16499(byte i) {
	Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2
	    = (client.aClass654_Sub1_Sub5_Sub1_Sub2Array11279
	       [anInt11884 * -1668124297]);
	if (class654_sub1_sub5_sub1_sub2 != null) {
	    Class184.method3228(Class583.aClass583_7776,
				anInt10803 * 1225863589, -1,
				class654_sub1_sub5_sub1_sub2,
				anInt11884 * -1668124297, 1781521033);
	    return true;
	}
	return false;
    }
    
    public boolean method16500() {
	Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2
	    = (client.aClass654_Sub1_Sub5_Sub1_Sub2Array11279
	       [anInt11884 * -1668124297]);
	if (class654_sub1_sub5_sub1_sub2 != null) {
	    Class184.method3228(Class583.aClass583_7776,
				anInt10803 * 1225863589, -1,
				class654_sub1_sub5_sub1_sub2,
				anInt11884 * -1668124297, 1781521033);
	    return true;
	}
	return false;
    }
}
