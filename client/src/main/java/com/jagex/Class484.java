/* Class484 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

class Class484 implements Interface56
{
    Class491 this$0;
    
    public byte[] method378(int i, int i_0_) {
	return this$0.anInterface70_5317.method447(i, (byte) -94);
    }
    
    Class484(Class491 class491) {
	this$0 = class491;
    }
    
    public byte[] method377(int i) {
	return this$0.anInterface70_5317.method447(i, (byte) -84);
    }
    
    public byte[] method376(int i) {
	return this$0.anInterface70_5317.method447(i, (byte) -1);
    }
    
    static final void method7953(Class669 class669, int i) {
	int i_1_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class669.aClass352_8602.aBoolArray3632[i_1_] ? 1 : 0;
    }
    
    static final void method7954(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
    
    public static final void method7955(String string, String string_2_,
					int i) {
	Class100 class100 = Class201.method3864(2095398292);
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4190,
				  class100.aClass13_1183, 1341391005);
	if (string_2_.length() > 30)
	    string_2_ = string_2_.substring(0, 30);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16506
	    ((Class668.method11029(string, (byte) 0)
	      + Class668.method11029(string_2_, (byte) 0)),
	     1238758757);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string_2_,
							      -2046596048);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
							      2005170310);
	class100.method1863(class534_sub19, (byte) 28);
    }
    
    static final void method7956(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
    
    static final void method7957(Class669 class669, byte i) {
	Class171_Sub4.aClass232_9944.method4247(1510944436);
    }
    
    static final void method7958(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub27_10781
		  .method17119((byte) 113);
    }
    
    static void method7959(int i, int i_3_) {
	Class551.anInt7308 = i * 928182963;
	Class99.aClass203_1168.method3877(1136296827);
    }
}
