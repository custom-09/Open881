/* Class254 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class254 implements Interface76
{
    int anInt2667;
    static Class254 aClass254_2668;
    static Class254 aClass254_2669;
    static Class254 aClass254_2670;
    int anInt2671;
    static Class254 aClass254_2672;
    static Class254 aClass254_2673;
    static Class254 aClass254_2674;
    static Class254 aClass254_2675;
    static Class254 aClass254_2676;
    static Class254 aClass254_2677;
    static Class254 aClass254_2678;
    static Class254 aClass254_2679;
    static Class254 aClass254_2680;
    static Class254 aClass254_2681;
    static Class254 aClass254_2682 = new Class254(13, 1);
    public static Class185 aClass185_2683;
    
    public int method93() {
	return -1127889311 * anInt2671;
    }
    
    static Class254[] method4638(byte i) {
	return new Class254[] { aClass254_2675, aClass254_2677, aClass254_2672,
				aClass254_2678, aClass254_2669, aClass254_2674,
				aClass254_2673, aClass254_2681, aClass254_2680,
				aClass254_2668, aClass254_2676, aClass254_2670,
				aClass254_2679, aClass254_2682 };
    }
    
    Class254(int i, int i_0_) {
	anInt2667 = -942861193 * i;
	anInt2671 = -1079157343 * i_0_;
    }
    
    static {
	aClass254_2668 = new Class254(9, 2);
	aClass254_2669 = new Class254(4, 3);
	aClass254_2675 = new Class254(0, 4);
	aClass254_2674 = new Class254(5, 5);
	aClass254_2672 = new Class254(2, 6);
	aClass254_2673 = new Class254(6, 7);
	aClass254_2681 = new Class254(7, 8);
	aClass254_2670 = new Class254(11, 9);
	aClass254_2676 = new Class254(10, 10);
	aClass254_2677 = new Class254(1, 11);
	aClass254_2678 = new Class254(3, 12);
	aClass254_2679 = new Class254(12, 13);
	aClass254_2680 = new Class254(8, 14);
    }
    
    public int method22() {
	return -1127889311 * anInt2671;
    }
    
    public int method53() {
	return -1127889311 * anInt2671;
    }
    
    static Class254[] method4639() {
	return new Class254[] { aClass254_2675, aClass254_2677, aClass254_2672,
				aClass254_2678, aClass254_2669, aClass254_2674,
				aClass254_2673, aClass254_2681, aClass254_2680,
				aClass254_2668, aClass254_2676, aClass254_2670,
				aClass254_2679, aClass254_2682 };
    }
    
    static Class254[] method4640() {
	return new Class254[] { aClass254_2675, aClass254_2677, aClass254_2672,
				aClass254_2678, aClass254_2669, aClass254_2674,
				aClass254_2673, aClass254_2681, aClass254_2680,
				aClass254_2668, aClass254_2676, aClass254_2670,
				aClass254_2679, aClass254_2682 };
    }
    
    static final void method4641(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	if (-1 == class666.aClass247_8574.anInt2580 * 1365669833) {
	    if (class669.aBool8615)
		throw new RuntimeException("");
	    throw new RuntimeException("");
	}
	Class247 class247 = class666.method11014((byte) 73);
	class247.aClass247Array2620[(1365669833
				     * class666.aClass247_8574.anInt2580)]
	    = null;
	Class454.method7416(class247, -1625835754);
    }
    
    static final void method4642(Class669 class669, int i) {
	class669.anInt8600 -= 308999563;
    }
}
