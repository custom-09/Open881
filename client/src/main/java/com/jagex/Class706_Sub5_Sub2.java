/* Class706_Sub5_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class706_Sub5_Sub2 extends Class706_Sub5
{
    float[] aFloatArray11903;
    float[] aFloatArray11904;
    int anInt11905 = 0;
    
    public Class438 method14236(int i) {
	Class438 class438 = Class438.method7061();
	double[] ds = aClass434Array11004[1630917157 * anInt11905]
			  .method6971(aFloat11003);
	class438.aFloat4864 = (float) ds[0];
	class438.aFloat4863 = (float) ds[1];
	class438.aFloat4865 = (float) ds[2];
	return class438;
    }
    
    void method17342(int i) {
	anInt11905 += 467663277;
    }
    
    float method17328(float f, float f_0_, float f_1_, int i) {
	float f_2_
	    = aFloat11003 / (float) aClass434Array11004
					[anInt11905 * 1630917157].method6957();
	float f_3_ = ((aFloatArray11904[1630917157 * anInt11905]
		       - aFloatArray11903[anInt11905 * 1630917157]) * f_2_
		      + aFloatArray11903[anInt11905 * 1630917157]);
	return f_3_;
    }
    
    void method17345(float f, float f_4_) {
	aFloat11003 += f;
	if (aFloat11003 > (float) aClass434Array11004
				      [1630917157 * anInt11905].method6957())
	    aFloat11003 = (float) aClass434Array11004
				      [anInt11905 * 1630917157].method6957();
    }
    
    float method17332(float f, float f_5_, float f_6_) {
	float f_7_
	    = aFloat11003 / (float) aClass434Array11004
					[anInt11905 * 1630917157].method6957();
	float f_8_ = ((aFloatArray11904[1630917157 * anInt11905]
		       - aFloatArray11903[anInt11905 * 1630917157]) * f_7_
		      + aFloatArray11903[anInt11905 * 1630917157]);
	return f_8_;
    }
    
    public Class706_Sub5_Sub2(Class298 class298) {
	super(class298);
    }
    
    public Class438 method14248(int i) {
	return method14236(308999563);
    }
    
    double[] method17330(int i) {
	return aClass434Array11004[1630917157 * anInt11905]
		   .method6971(aFloat11003);
    }
    
    void method17344(float f, float f_9_) {
	aFloat11003 += f;
	if (aFloat11003 > (float) aClass434Array11004
				      [1630917157 * anInt11905].method6957())
	    aFloat11003 = (float) aClass434Array11004
				      [anInt11905 * 1630917157].method6957();
    }
    
    void method17327(float f, float f_10_, int i) {
	aFloat11003 += f;
	if (aFloat11003 > (float) aClass434Array11004
				      [1630917157 * anInt11905].method6957())
	    aFloat11003 = (float) aClass434Array11004
				      [anInt11905 * 1630917157].method6957();
    }
    
    float method17334(float f, float f_11_, float f_12_) {
	float f_13_
	    = aFloat11003 / (float) aClass434Array11004
					[anInt11905 * 1630917157].method6957();
	float f_14_ = ((aFloatArray11904[1630917157 * anInt11905]
			- aFloatArray11903[anInt11905 * 1630917157]) * f_13_
		       + aFloatArray11903[anInt11905 * 1630917157]);
	return f_14_;
    }
    
    void method17335() {
	anInt11905 += 467663277;
    }
    
    void method17339(Class534_Sub40 class534_sub40, int i) {
	aFloatArray11903 = new float[i];
	aFloatArray11904 = new float[i];
	anInt11905 = 0;
	for (int i_15_ = 0; i_15_ < i; i_15_++) {
	    aFloatArray11903[i_15_] = class534_sub40.method16539(-1999590142);
	    aFloatArray11904[i_15_] = class534_sub40.method16539(-1020285901);
	}
    }
    
    void method17336(Class534_Sub40 class534_sub40, int i) {
	aFloatArray11903 = new float[i];
	aFloatArray11904 = new float[i];
	anInt11905 = 0;
	for (int i_16_ = 0; i_16_ < i; i_16_++) {
	    aFloatArray11903[i_16_] = class534_sub40.method16539(-1792513166);
	    aFloatArray11904[i_16_] = class534_sub40.method16539(-1411860547);
	}
    }
    
    void method17337(Class534_Sub40 class534_sub40, int i) {
	aFloatArray11903 = new float[i];
	aFloatArray11904 = new float[i];
	anInt11905 = 0;
	for (int i_17_ = 0; i_17_ < i; i_17_++) {
	    aFloatArray11903[i_17_] = class534_sub40.method16539(-1318823140);
	    aFloatArray11904[i_17_] = class534_sub40.method16539(-1361310435);
	}
    }
    
    float method17333(float f, float f_18_, float f_19_) {
	float f_20_
	    = aFloat11003 / (float) aClass434Array11004
					[anInt11905 * 1630917157].method6957();
	float f_21_ = ((aFloatArray11904[1630917157 * anInt11905]
			- aFloatArray11903[anInt11905 * 1630917157]) * f_20_
		       + aFloatArray11903[anInt11905 * 1630917157]);
	return f_21_;
    }
    
    public Class438 method14243() {
	Class438 class438 = Class438.method7061();
	double[] ds = aClass434Array11004[1630917157 * anInt11905]
			  .method6971(aFloat11003);
	class438.aFloat4864 = (float) ds[0];
	class438.aFloat4863 = (float) ds[1];
	class438.aFloat4865 = (float) ds[2];
	return class438;
    }
    
    public Class438 method14241() {
	Class438 class438 = Class438.method7061();
	double[] ds = aClass434Array11004[1630917157 * anInt11905]
			  .method6971(aFloat11003);
	class438.aFloat4864 = (float) ds[0];
	class438.aFloat4863 = (float) ds[1];
	class438.aFloat4865 = (float) ds[2];
	return class438;
    }
    
    public Class438 method14249() {
	return method14236(308999563);
    }
    
    public Class438 method14247() {
	Class438 class438 = Class438.method7061();
	double[] ds = aClass434Array11004[1630917157 * anInt11905]
			  .method6971(aFloat11003);
	class438.aFloat4864 = (float) ds[0];
	class438.aFloat4863 = (float) ds[1];
	class438.aFloat4865 = (float) ds[2];
	return class438;
    }
    
    void method17340(Class534_Sub40 class534_sub40, int i, byte i_22_) {
	aFloatArray11903 = new float[i];
	aFloatArray11904 = new float[i];
	anInt11905 = 0;
	for (int i_23_ = 0; i_23_ < i; i_23_++) {
	    aFloatArray11903[i_23_] = class534_sub40.method16539(-1561883670);
	    aFloatArray11904[i_23_] = class534_sub40.method16539(-1397556347);
	}
    }
    
    public Class438 method14250() {
	return method14236(308999563);
    }
    
    double[] method17331() {
	return aClass434Array11004[1630917157 * anInt11905]
		   .method6971(aFloat11003);
    }
    
    double[] method17346() {
	return aClass434Array11004[1630917157 * anInt11905]
		   .method6971(aFloat11003);
    }
}
