/* Class383 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class383
{
    public int anInt3935;
    public int anInt3936 = 1748712647;
    public int anInt3937 = -422558144;
    public int anInt3938 = -1751998400;
    static final int anInt3939 = 0;
    public int anInt3940;
    public boolean aBool3941;
    public boolean aBool3942;
    public static int anInt3943;
    
    void method6451(Class534_Sub40 class534_sub40, int i, int i_0_) {
	for (;;) {
	    int i_1_ = class534_sub40.method16527(563605515);
	    if (0 == i_1_)
		break;
	    method6453(class534_sub40, i_1_, i, (byte) 80);
	}
    }
    
    void method6452(Class534_Sub40 class534_sub40, int i) {
	for (;;) {
	    int i_2_ = class534_sub40.method16527(-1243532996);
	    if (0 == i_2_)
		break;
	    method6453(class534_sub40, i_2_, i, (byte) 22);
	}
    }
    
    void method6453(Class534_Sub40 class534_sub40, int i, int i_3_,
		    byte i_4_) {
	if (i == 1) {
	    anInt3936 = class534_sub40.method16529((byte) 1) * -1748712647;
	    if (651115273 * anInt3936 == 65535)
		anInt3936 = 1748712647;
	} else if (i == 2) {
	    anInt3937
		= (class534_sub40.method16529((byte) 1) + 1) * 1536901401;
	    anInt3938
		= (class534_sub40.method16529((byte) 1) + 1) * 1046366849;
	} else if (3 == i)
	    class534_sub40.method16586((byte) 1);
	else if (i == 4)
	    anInt3935 = class534_sub40.method16527(-269437856) * -220593993;
	else if (5 == i)
	    anInt3940 = class534_sub40.method16527(-750117604) * -1426481801;
	else if (6 == i)
	    aBool3941 = true;
	else if (7 == i)
	    aBool3942 = true;
    }
    
    void method6454(Class534_Sub40 class534_sub40, int i) {
	for (;;) {
	    int i_5_ = class534_sub40.method16527(-167678819);
	    if (0 == i_5_)
		break;
	    method6453(class534_sub40, i_5_, i, (byte) 29);
	}
    }
    
    void method6455(Class534_Sub40 class534_sub40, int i) {
	for (;;) {
	    int i_6_ = class534_sub40.method16527(1820280041);
	    if (0 == i_6_)
		break;
	    method6453(class534_sub40, i_6_, i, (byte) -82);
	}
    }
    
    void method6456(Class534_Sub40 class534_sub40, int i, int i_7_) {
	if (i == 1) {
	    anInt3936 = class534_sub40.method16529((byte) 1) * -1748712647;
	    if (651115273 * anInt3936 == 65535)
		anInt3936 = 1748712647;
	} else if (i == 2) {
	    anInt3937
		= (class534_sub40.method16529((byte) 1) + 1) * 1536901401;
	    anInt3938
		= (class534_sub40.method16529((byte) 1) + 1) * 1046366849;
	} else if (3 == i)
	    class534_sub40.method16586((byte) 1);
	else if (i == 4)
	    anInt3935 = class534_sub40.method16527(-594396590) * -220593993;
	else if (5 == i)
	    anInt3940 = class534_sub40.method16527(-1512361024) * -1426481801;
	else if (6 == i)
	    aBool3941 = true;
	else if (7 == i)
	    aBool3942 = true;
    }
    
    void method6457(Class534_Sub40 class534_sub40, int i, int i_8_) {
	if (i == 1) {
	    anInt3936 = class534_sub40.method16529((byte) 1) * -1748712647;
	    if (651115273 * anInt3936 == 65535)
		anInt3936 = 1748712647;
	} else if (i == 2) {
	    anInt3937
		= (class534_sub40.method16529((byte) 1) + 1) * 1536901401;
	    anInt3938
		= (class534_sub40.method16529((byte) 1) + 1) * 1046366849;
	} else if (3 == i)
	    class534_sub40.method16586((byte) 1);
	else if (i == 4)
	    anInt3935 = class534_sub40.method16527(2110221647) * -220593993;
	else if (5 == i)
	    anInt3940 = class534_sub40.method16527(-2062600046) * -1426481801;
	else if (6 == i)
	    aBool3941 = true;
	else if (7 == i)
	    aBool3942 = true;
    }
    
    Class383() {
	anInt3935 = -441187986;
	anInt3940 = -1426481801;
	aBool3941 = false;
	aBool3942 = false;
    }
    
    static final void method6458(Class669 class669, byte i) {
	int i_9_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_9_, 2037164992);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_9_ >> 16];
	Class193.method3793(class247, class243, class669, (byte) 110);
    }
    
    static final void method6459(Class669 class669, short i) {
	int i_10_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_10_, 1227461773);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_10_ >> 16];
	Class283.method5248(class247, class243, class669, (byte) 0);
    }
    
    static final void method6460(Class669 class669, byte i) {
	int i_11_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_11_, -283078799);
	int i_12_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	i_12_--;
	if (null == class247.aStringArray2487
	    || i_12_ >= class247.aStringArray2487.length
	    || class247.aStringArray2487[i_12_] == null)
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= "";
	else
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= class247.aStringArray2487[i_12_];
    }
    
    static final void method6461(Class669 class669, short i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class80.method1630(class247, class243, class669, (byte) -75);
    }
}
