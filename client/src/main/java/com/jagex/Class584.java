/* Class584 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class584 implements Interface68
{
    int anInt7795;
    String aString7796;
    Class68 aClass68_7797;
    String aString7798;
    
    public void method286() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4154,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	int i = ((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  += -1387468933)
		 * 31645619);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16506(aClass68_7797.method93(), 1896019073);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16713(aString7796,
							      1327089783);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16507(-1993065431 * anInt7795, 1663344240);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16713(Class503.aString5625, -1105948641);
	if (null != aString7798) {
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(1,
								  1240286897);
	    String string = aString7798;
	    if (string.length() > 100)
		string = string.substring(0, 100);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  -948622279);
	} else
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(0,
								  1968597998);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16528
	    ((31645619 * class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
	      - i),
	     2051201527);
	client.aClass100_11264.method1863(class534_sub19, (byte) 92);
    }
    
    Class584(Class68 class68, String string, int i, Throwable throwable) {
	aClass68_7797 = class68;
	aString7796 = string;
	anInt7795 = i * -344383463;
	aString7798 = throwable != null ? throwable.getMessage() : null;
    }
    
    public void method439() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4154,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	int i = ((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  += -1387468933)
		 * 31645619);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16506(aClass68_7797.method93(), 1908726746);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16713(aString7796,
							      524161743);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16507(-1993065431 * anInt7795, 1323160404);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16713(Class503.aString5625, -1509158751);
	if (null != aString7798) {
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(1,
								  1368981777);
	    String string = aString7798;
	    if (string.length() > 100)
		string = string.substring(0, 100);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  2137184282);
	} else
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(0,
								  1706654884);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16528
	    ((31645619 * class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
	      - i),
	     2005502825);
	client.aClass100_11264.method1863(class534_sub19, (byte) 117);
    }
    
    public void method142() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4154,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	int i = ((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  += -1387468933)
		 * 31645619);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16506(aClass68_7797.method93(), 483318230);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16713(aString7796,
							      -79939795);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16507(-1993065431 * anInt7795, 1055641363);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16713(Class503.aString5625, 1291898760);
	if (null != aString7798) {
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(1,
								  1451911912);
	    String string = aString7798;
	    if (string.length() > 100)
		string = string.substring(0, 100);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  1881525887);
	} else
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(0,
								  1709350082);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16528
	    ((31645619 * class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
	      - i),
	     2064917090);
	client.aClass100_11264.method1863(class534_sub19, (byte) 78);
    }
    
    public void method356(int i) {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4154,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	int i_0_ = ((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		     += -1387468933)
		    * 31645619);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16506(aClass68_7797.method93(), 1446968307);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16713(aString7796,
							      -459775110);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16507(-1993065431 * anInt7795, 855488055);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16713(Class503.aString5625, 1531240);
	if (null != aString7798) {
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(1,
								  2046712937);
	    String string = aString7798;
	    if (string.length() > 100)
		string = string.substring(0, 100);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  -141015738);
	} else
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(0,
								  843923644);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16528
	    ((31645619 * class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
	      - i_0_),
	     2138081760);
	client.aClass100_11264.method1863(class534_sub19, (byte) 66);
    }
    
    public void method203() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4154,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	int i = ((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  += -1387468933)
		 * 31645619);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16506(aClass68_7797.method93(), 1888159239);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16713(aString7796,
							      -1230820060);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16507(-1993065431 * anInt7795, 1962098533);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16713(Class503.aString5625, -429368283);
	if (null != aString7798) {
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(1,
								  317679037);
	    String string = aString7798;
	    if (string.length() > 100)
		string = string.substring(0, 100);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  -1297453004);
	} else
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16506(0,
								  501854418);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16528
	    ((31645619 * class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
	      - i),
	     2085385667);
	client.aClass100_11264.method1863(class534_sub19, (byte) 115);
    }
    
    static final void method9839(Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	StringBuilder stringbuilder = new StringBuilder(string.length());
	boolean bool = false;
	for (int i_1_ = 0; i_1_ < string.length(); i_1_++) {
	    char c = string.charAt(i_1_);
	    if (c == '<')
		bool = true;
	    else if ('>' == c)
		bool = false;
	    else if (!bool)
		stringbuilder.append(c);
	}
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = stringbuilder.toString();
    }
    
    static final void method9840(Class669 class669, byte i) {
	if (Class112.anInt1353 * -710165623 == 1)
	    Class112.aBool1354 = true;
	else if (3 == Class112.anInt1353 * -710165623)
	    Class112.aBool1361 = true;
    }
    
    public static void method9841(Interface74 interface74, byte i) {
	if (null == IcmpService_Sub1.anIcmpService_Sub1_10890)
	    throw new IllegalStateException("");
	IcmpService_Sub1.anIcmpService_Sub1_10890.aList10891.add(interface74);
    }
    
    static void method9842(Class669 class669, int i) {
	boolean bool = ((class669.anIntArray8595
			 [(class669.anInt8600 -= 308999563) * 2088438307])
			!= 0);
	Class635.method10536(bool, 65535);
    }
}
