/* Class534_Sub16 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub16 extends Class534
{
    int anInt10467;
    static Class700 aClass700_10468 = new Class700();
    static Class700 aClass700_10469 = new Class700();
    static Class9 aClass9_10470 = new Class9(16);
    int anInt10471;
    int anInt10472;
    int anInt10473;
    int anInt10474;
    int anInt10475;
    Class438 aClass438_10476 = new Class438(0.0F, 0.0F, 0.0F);
    static final int anInt10477 = 512;
    Class438 aClass438_10478 = new Class438(0.0F, 0.0F, 0.0F);
    int[] anIntArray10479;
    int anInt10480;
    Class602 aClass602_10481;
    Class654_Sub1_Sub5_Sub1_Sub1 aClass654_Sub1_Sub5_Sub1_Sub1_10482;
    int anInt10483;
    static final int anInt10484 = 0;
    static final int anInt10485 = 1;
    static final int anInt10486 = 2;
    static final int anInt10487 = 3;
    int anInt10488;
    int anInt10489;
    Class654_Sub1_Sub5_Sub1_Sub2 aClass654_Sub1_Sub5_Sub1_Sub2_10490;
    int anInt10491;
    Class491 aClass491_10492;
    int anInt10493;
    Class491 aClass491_10494;
    int anInt10495;
    int anInt10496;
    int anInt10497 = 0;
    boolean aBool10498;
    
    public static void method16150
	(Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2) {
	Class534_Sub16 class534_sub16
	    = ((Class534_Sub16)
	       aClass9_10470.method579((long) ((class654_sub1_sub5_sub1_sub2
						.anInt11922)
					       * 1126388985)));
	if (class534_sub16 == null)
	    Class449.method7330(class654_sub1_sub5_sub1_sub2.aByte10854,
				(class654_sub1_sub5_sub1_sub2.anIntArray11977
				 [0]),
				(class654_sub1_sub5_sub1_sub2.anIntArray11978
				 [0]),
				0, null, null, class654_sub1_sub5_sub1_sub2,
				1608633510);
	else
	    class534_sub16.method16151(-319387405);
    }
    
    void method16151(int i) {
	int i_0_ = -241175639 * anInt10489;
	if (aClass602_10481 != null) {
	    Class602 class602 = (aClass602_10481.method9988
				 (Class78.aClass103_825,
				  (-1468443459 * client.anInt11155 == 3
				   ? (Interface20) Class201.anInterface20_2188
				   : Class78.aClass103_825),
				  1213388223));
	    if (null != class602) {
		anInt10489 = class602.anInt7945 * 1128583165;
		anInt10467
		    = (372226515 * class602.anInt7946 << 9) * -612932795;
		anInt10480 = class602.anInt7948 * 1993106295;
		anInt10495 = 617813239 * class602.anInt7915;
		anInt10496 = class602.anInt7950 * 1071004077;
		anIntArray10479 = class602.anIntArray7951;
		anInt10493 = -1005766001 * class602.anInt7960;
		anInt10483 = -990708005 * class602.anInt7901;
	    } else {
		anInt10489 = -1086873753;
		anInt10467 = 0;
		anInt10480 = 0;
		anInt10495 = 0;
		anInt10496 = 0;
		anIntArray10479 = null;
		anInt10493 = 1916307200;
		anInt10483 = 665776896;
		anInt10491 = 0;
	    }
	} else if (aClass654_Sub1_Sub5_Sub1_Sub1_10482 != null) {
	    int i_1_ = Class308.method5656(aClass654_Sub1_Sub5_Sub1_Sub1_10482,
					   -758062783);
	    if (i_0_ != i_1_) {
		anInt10489 = i_1_ * 1086873753;
		Class307 class307
		    = aClass654_Sub1_Sub5_Sub1_Sub1_10482.aClass307_12204;
		if (class307.anIntArray3284 != null)
		    class307 = class307.method5615(Class78.aClass103_825,
						   Class78.aClass103_825,
						   -1466068515);
		if (null != class307) {
		    anInt10467
			= -612932795 * (class307.anInt3332 * -869231065 << 9);
		    anInt10491 = ((class307.anInt3295 * -1156607845 << 9)
				  * -2034082261);
		    anInt10480 = 1689126761 * class307.anInt3334;
		    anInt10493 = class307.anInt3344 * 932822485;
		    anInt10483 = class307.anInt3343 * -32158863;
		} else {
		    anInt10491 = 0;
		    anInt10467 = 0;
		    anInt10480 = 0;
		    anInt10493 = 1916307200;
		    anInt10483 = 665776896;
		}
	    }
	} else if (aClass654_Sub1_Sub5_Sub1_Sub2_10490 != null) {
	    anInt10489
		= Class206.method3937(aClass654_Sub1_Sub5_Sub1_Sub2_10490,
				      962857524) * 1086873753;
	    anInt10467
		= (aClass654_Sub1_Sub5_Sub1_Sub2_10490.anInt12229 * -785793877
		   << 9) * -612932795;
	    anInt10491 = 0;
	    anInt10480
		= aClass654_Sub1_Sub5_Sub1_Sub2_10490.anInt12215 * -908420807;
	    anInt10493 = 1916307200;
	    anInt10483 = 665776896;
	}
	if (anInt10489 * -241175639 != i_0_ && null != aClass491_10492
	    && aClass491_10492 != null) {
	    aClass491_10492.method8014(100, 1962988118);
	    Class171_Sub4.aClass232_9944.method4234(aClass491_10492,
						    -275571947);
	    aClass491_10492 = null;
	}
    }
    
    public static void method16152
	(int i, int i_2_, int i_3_, int i_4_, Class602 class602,
	 Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1,
	 Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2) {
	Class534_Sub16 class534_sub16 = new Class534_Sub16();
	class534_sub16.anInt10471 = -1178810913 * i;
	class534_sub16.anInt10472 = -973592293 * (i_2_ << 9);
	class534_sub16.anInt10473 = -1604946927 * (i_3_ << 9);
	if (class602 != null) {
	    class534_sub16.aClass602_10481 = class602;
	    int i_5_ = class602.anInt7904 * -1082258489;
	    int i_6_ = -1990374967 * class602.anInt7928;
	    if (1 == i_4_ || i_4_ == 3) {
		i_5_ = class602.anInt7928 * -1990374967;
		i_6_ = class602.anInt7904 * -1082258489;
	    }
	    class534_sub16.anInt10474 = (i_5_ + i_2_ << 9) * -800010841;
	    class534_sub16.anInt10475 = 1156683381 * (i_6_ + i_3_ << 9);
	    class534_sub16.anInt10489 = class602.anInt7945 * 1128583165;
	    class534_sub16.anInt10467
		= (372226515 * class602.anInt7946 << 9) * -612932795;
	    class534_sub16.anInt10480 = class602.anInt7948 * 1993106295;
	    class534_sub16.anInt10495 = class602.anInt7915 * 617813239;
	    class534_sub16.anInt10496 = 1071004077 * class602.anInt7950;
	    class534_sub16.anIntArray10479 = class602.anIntArray7951;
	    class534_sub16.anInt10493 = -1005766001 * class602.anInt7960;
	    class534_sub16.anInt10483 = class602.anInt7901 * -990708005;
	    class534_sub16.anInt10491
		= -2034082261 * (925162477 * class602.anInt7947 << 9);
	    if (null != class602.anIntArray7943) {
		class534_sub16.aBool10498 = true;
		class534_sub16.method16151(-2057364644);
	    }
	    if (null != class534_sub16.anIntArray10479)
		class534_sub16.anInt10488
		    = ((class534_sub16.anInt10495 * -1089168257
			+ (int) (Math.random()
				 * (double) ((1512108791
					      * class534_sub16.anInt10496)
					     - (-1089168257
						* class534_sub16.anInt10495))))
		       * 835810559);
	    aClass700_10468.method14131(class534_sub16, (short) 789);
	    Class171_Sub4.aClass232_9944.method4230((-241175639
						     * (class534_sub16
							.anInt10489)),
						    -1750237937);
	    Class171_Sub4.aClass232_9944
		.method4310(class534_sub16.anIntArray10479, -424341433);
	} else if (class654_sub1_sub5_sub1_sub1 != null) {
	    class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		= class654_sub1_sub5_sub1_sub1;
	    Class307 class307 = class654_sub1_sub5_sub1_sub1.aClass307_12204;
	    if (class307.anIntArray3284 != null) {
		class534_sub16.aBool10498 = true;
		class307
		    = class307.method5615(Class78.aClass103_825,
					  Class78.aClass103_825, -1466068515);
	    }
	    if (null != class307) {
		class534_sub16.anInt10474
		    = -800010841 * (class307.anInt3328 * -1821838479 + i_2_
				    << 9);
		class534_sub16.anInt10475
		    = ((-1821838479 * class307.anInt3328 + i_3_ << 9)
		       * 1156683381);
		class534_sub16.anInt10489
		    = Class308.method5656(class654_sub1_sub5_sub1_sub1,
					  593960231) * 1086873753;
		class534_sub16.anInt10467
		    = (class307.anInt3332 * -869231065 << 9) * -612932795;
		class534_sub16.anInt10480 = 1689126761 * class307.anInt3334;
		class534_sub16.anInt10493 = class307.anInt3344 * 932822485;
		class534_sub16.anInt10483 = -32158863 * class307.anInt3343;
		class534_sub16.anInt10491
		    = (class307.anInt3295 * -1156607845 << 9) * -2034082261;
		Class171_Sub4.aClass232_9944
		    .method4230(1315529581 * class307.anInt3304, -1633034458);
		Class171_Sub4.aClass232_9944
		    .method4230(1912925203 * class307.anInt3329, -1476640985);
		Class171_Sub4.aClass232_9944
		    .method4230(1144816973 * class307.anInt3330, -1372079942);
		Class171_Sub4.aClass232_9944
		    .method4230(-306882215 * class307.anInt3331, -1879236718);
	    }
	    aClass700_10469.method14131(class534_sub16, (short) 789);
	} else if (class654_sub1_sub5_sub1_sub2 != null) {
	    class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
		= class654_sub1_sub5_sub1_sub2;
	    class534_sub16.anInt10474
		= (i_2_ + class654_sub1_sub5_sub1_sub2.method18545((byte) 1)
		   << 9) * -800010841;
	    class534_sub16.anInt10475
		= (i_3_ + class654_sub1_sub5_sub1_sub2.method18545((byte) 1)
		   << 9) * 1156683381;
	    class534_sub16.anInt10489
		= Class206.method3937(class654_sub1_sub5_sub1_sub2,
				      1882279070) * 1086873753;
	    class534_sub16.anInt10467
		= ((class654_sub1_sub5_sub1_sub2.anInt12229 * -785793877 << 9)
		   * -612932795);
	    class534_sub16.anInt10480
		= -908420807 * class654_sub1_sub5_sub1_sub2.anInt12215;
	    class534_sub16.anInt10493 = 1916307200;
	    class534_sub16.anInt10483 = 665776896;
	    class534_sub16.anInt10491 = 0;
	    aClass9_10470.method581(class534_sub16,
				    (long) (1126388985
					    * (class654_sub1_sub5_sub1_sub2
					       .anInt11922)));
	    Class171_Sub4.aClass232_9944.method4230
		(class654_sub1_sub5_sub1_sub2.anInt12230 * -729876395,
		 -1910074976);
	    Class171_Sub4.aClass232_9944.method4230
		(-2046269725 * class654_sub1_sub5_sub1_sub2.anInt12222,
		 -1935483467);
	    Class171_Sub4.aClass232_9944.method4230
		(1734964233 * class654_sub1_sub5_sub1_sub2.anInt12227,
		 -1614236676);
	    Class171_Sub4.aClass232_9944.method4230
		(-510526957 * class654_sub1_sub5_sub1_sub2.anInt12231,
		 -1440995779);
	}
    }
    
    static void method16153(Class534_Sub16 class534_sub16, int i, int i_7_,
			    int i_8_, int i_9_) {
	if (-1 != -241175639 * class534_sub16.anInt10489
	    || class534_sub16.anIntArray10479 != null) {
	    int i_10_ = class534_sub16.anInt10480 * 63994171;
	    if (0 == 984135565 * class534_sub16.anInt10467
		|| Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub28_10754
		       .method17131(-1425833450) == 0
		|| 377042463 * class534_sub16.anInt10471 != i) {
		if (class534_sub16.aClass491_10492 != null) {
		    class534_sub16.aClass491_10492.method8014(100, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    1498518479);
		    class534_sub16.aClass491_10492 = null;
		}
	    } else {
		if (null != class534_sub16.aClass491_10492
		    && ((class534_sub16.aClass491_10492.method8027((byte) 2)
			 == Class480.aClass480_5253)
			|| (class534_sub16.aClass491_10492.method8027((byte) 2)
			    == Class480.aClass480_5257))) {
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    1681415218);
		    class534_sub16.aClass491_10492 = null;
		}
		if (class534_sub16.aClass491_10492 == null) {
		    if (-241175639 * class534_sub16.anInt10489 >= 0) {
			int i_11_ = 256;
			int i_12_
			    = (int) ((0.5F
				      * (float) ((37947927
						  * class534_sub16.anInt10474)
						 - (class534_sub16.anInt10472
						    * 1285097747)))
				     + (float) (1285097747
						* class534_sub16.anInt10472));
			int i_13_
			    = (int) ((float) (-1700987663
					      * class534_sub16.anInt10473)
				     + (float) ((class534_sub16.anInt10475
						 * -374666787)
						- (class534_sub16.anInt10473
						   * -1700987663)) * 0.5F);
			class534_sub16.aClass438_10476.aFloat4864
			    = (float) i_12_;
			class534_sub16.aClass438_10476.aFloat4865
			    = (float) i_13_;
			class534_sub16.aClass491_10492
			    = (Class171_Sub4.aClass232_9944.method4229
			       (Class211.aClass211_2248, class534_sub16,
				-241175639 * class534_sub16.anInt10489, -1, 0,
				Class190.aClass190_2136.method3763(-223700448),
				Class207.aClass207_2236,
				(float) (-1081773949
					 * class534_sub16.anInt10491),
				(float) (984135565
					 * class534_sub16.anInt10467),
				class534_sub16.aClass438_10476, 0, i_11_,
				false, -1245755770));
			if (class534_sub16.aClass491_10492 != null) {
			    float f = (float) i_10_ / 255.0F;
			    class534_sub16.aClass491_10492
				.method8037(f, 150, 1426296472);
			    class534_sub16.aClass491_10492
				.method8069(1952394526);
			}
		    }
		} else {
		    int i_14_
			= (int) ((float) (1285097747
					  * class534_sub16.anInt10472)
				 + ((float) ((class534_sub16.anInt10474
					      * 37947927)
					     - (1285097747
						* class534_sub16.anInt10472))
				    * 0.5F));
		    int i_15_
			= (int) (0.5F * (float) ((-374666787
						  * class534_sub16.anInt10475)
						 - (class534_sub16.anInt10473
						    * -1700987663))
				 + (float) (class534_sub16.anInt10473
					    * -1700987663));
		    class534_sub16.aClass438_10476.aFloat4864 = (float) i_14_;
		    class534_sub16.aClass438_10476.aFloat4865 = (float) i_15_;
		}
		if (class534_sub16.aClass491_10494 == null) {
		    if (class534_sub16.anIntArray10479 != null
			&& ((class534_sub16.anInt10488 -= 835810559 * i_9_)
			    * 1102416639) <= 0) {
			int i_16_
			    = ((256 == class534_sub16.anInt10493 * -1536094921
				&& (class534_sub16.anInt10483 * 1762794043
				    == 256))
			       ? 256
			       : ((int) (Math.random()
					 * (double) ((-1536094921
						      * (class534_sub16
							 .anInt10493))
						     - ((class534_sub16
							 .anInt10483)
							* 1762794043)))
				  + 1762794043 * class534_sub16.anInt10483));
			int i_17_
			    = (int) (Math.random()
				     * (double) (class534_sub16
						 .anIntArray10479).length);
			int i_18_
			    = (int) ((float) (class534_sub16.anInt10472
					      * 1285097747)
				     + (float) ((class534_sub16.anInt10474
						 * 37947927)
						- (class534_sub16.anInt10472
						   * 1285097747)) * 0.5F);
			int i_19_
			    = (int) ((float) (class534_sub16.anInt10473
					      * -1700987663)
				     + (0.5F
					* (float) (-374666787 * (class534_sub16
								 .anInt10475)
						   - (-1700987663
						      * (class534_sub16
							 .anInt10473)))));
			class534_sub16.aClass438_10478.aFloat4864
			    = (float) i_18_;
			class534_sub16.aClass438_10478.aFloat4865
			    = (float) i_19_;
			class534_sub16.aClass491_10494
			    = (Class171_Sub4.aClass232_9944.method4229
			       (Class211.aClass211_2249, class534_sub16,
				class534_sub16.anIntArray10479[i_17_], 0,
				i_10_,
				Class190.aClass190_2127.method3763(697725947),
				Class207.aClass207_2236,
				(float) (-1081773949
					 * class534_sub16.anInt10491),
				(float) (class534_sub16.anInt10467 * 984135565
					 + (class534_sub16.anInt10491
					    * -1081773949)),
				class534_sub16.aClass438_10478, 0, i_16_,
				false, -700661274));
			if (class534_sub16.aClass491_10494 != null)
			    class534_sub16.aClass491_10494
				.method8069(1976334426);
			class534_sub16.anInt10488
			    = ((-1089168257 * class534_sub16.anInt10495
				+ (int) (Math.random()
					 * (double) ((class534_sub16.anInt10496
						      * 1512108791)
						     - (-1089168257
							* (class534_sub16
							   .anInt10495)))))
			       * 835810559);
		    }
		} else {
		    int i_20_
			= (int) (0.5F * (float) ((37947927
						  * class534_sub16.anInt10474)
						 - (class534_sub16.anInt10472
						    * 1285097747))
				 + (float) (1285097747
					    * class534_sub16.anInt10472));
		    int i_21_
			= (int) ((float) (-1700987663
					  * class534_sub16.anInt10473)
				 + 0.5F * (float) ((class534_sub16.anInt10475
						    * -374666787)
						   - (class534_sub16.anInt10473
						      * -1700987663)));
		    class534_sub16.aClass438_10478.aFloat4864 = (float) i_20_;
		    class534_sub16.aClass438_10478.aFloat4865 = (float) i_21_;
		    if ((class534_sub16.aClass491_10494.method8027((byte) 2)
			 == Class480.aClass480_5253)
			|| (class534_sub16.aClass491_10494.method8027((byte) 2)
			    == Class480.aClass480_5257)) {
			Class171_Sub4.aClass232_9944.method4234
			    (class534_sub16.aClass491_10494, -1320808306);
			class534_sub16.aClass491_10494 = null;
		    }
		}
	    }
	}
    }
    
    public static void method16154(boolean bool) {
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14135((byte) -1);
	     null != class534_sub16;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14139(2124056433)) {
	    if (class534_sub16.aClass491_10492 != null) {
		class534_sub16.aClass491_10492.method8014(150, 1962988118);
		Class171_Sub4.aClass232_9944
		    .method4234(class534_sub16.aClass491_10492, 1538498488);
		class534_sub16.aClass491_10492 = null;
	    }
	    if (null != class534_sub16.aClass491_10494) {
		class534_sub16.aClass491_10494.method8014(150, 1962988118);
		Class171_Sub4.aClass232_9944
		    .method4234(class534_sub16.aClass491_10494, -190594975);
		class534_sub16.aClass491_10494 = null;
	    }
	    class534_sub16.method8892((byte) 1);
	}
	if (bool) {
	    for (Class534_Sub16 class534_sub16
		     = (Class534_Sub16) aClass700_10469.method14135((byte) -1);
		 null != class534_sub16;
		 class534_sub16 = ((Class534_Sub16)
				   aClass700_10469.method14139(1643346990))) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(150, 1962988118);
		    Class171_Sub4.aClass232_9944
			.method4234(class534_sub16.aClass491_10492, 471522689);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
	    }
	    for (Class534_Sub16 class534_sub16
		     = (Class534_Sub16) aClass9_10470.method583(-1857845058);
		 null != class534_sub16;
		 class534_sub16
		     = (Class534_Sub16) aClass9_10470.method584((byte) -41)) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(150, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    1590084020);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
	    }
	}
    }
    
    public static void method16155(boolean bool) {
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14135((byte) -1);
	     null != class534_sub16;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14139(1550343473)) {
	    if (class534_sub16.aClass491_10492 != null) {
		class534_sub16.aClass491_10492.method8014(150, 1962988118);
		Class171_Sub4.aClass232_9944
		    .method4234(class534_sub16.aClass491_10492, 26700732);
		class534_sub16.aClass491_10492 = null;
	    }
	    if (null != class534_sub16.aClass491_10494) {
		class534_sub16.aClass491_10494.method8014(150, 1962988118);
		Class171_Sub4.aClass232_9944
		    .method4234(class534_sub16.aClass491_10494, 21209231);
		class534_sub16.aClass491_10494 = null;
	    }
	    class534_sub16.method8892((byte) 1);
	}
	if (bool) {
	    for (Class534_Sub16 class534_sub16
		     = (Class534_Sub16) aClass700_10469.method14135((byte) -1);
		 null != class534_sub16;
		 class534_sub16 = ((Class534_Sub16)
				   aClass700_10469.method14139(924541644))) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(150, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    -699560462);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
	    }
	    for (Class534_Sub16 class534_sub16
		     = (Class534_Sub16) aClass9_10470.method583(-1898196119);
		 null != class534_sub16;
		 class534_sub16
		     = (Class534_Sub16) aClass9_10470.method584((byte) -121)) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(150, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    -743220434);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
	    }
	}
    }
    
    public static void method16156() {
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14135((byte) -1);
	     class534_sub16 != null;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14139(1416551345)) {
	    if (class534_sub16.aBool10498)
		class534_sub16.method16151(1071576436);
	}
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14135((byte) -1);
	     class534_sub16 != null;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14139(865292565)) {
	    if (class534_sub16.aBool10498)
		class534_sub16.method16151(-1884890631);
	}
    }
    
    public static void method16157() {
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14135((byte) -1);
	     class534_sub16 != null;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14139(1660033027)) {
	    if (class534_sub16.aBool10498)
		class534_sub16.method16151(-1147192503);
	}
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14135((byte) -1);
	     class534_sub16 != null;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14139(665620018)) {
	    if (class534_sub16.aBool10498)
		class534_sub16.method16151(-220131704);
	}
    }
    
    public static void method16158() {
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14135((byte) -1);
	     class534_sub16 != null;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14139(1539956289)) {
	    if (class534_sub16.aBool10498)
		class534_sub16.method16151(-432007962);
	}
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14135((byte) -1);
	     class534_sub16 != null;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14139(1825756377)) {
	    if (class534_sub16.aBool10498)
		class534_sub16.method16151(1596728404);
	}
    }
    
    void method16159() {
	int i = -241175639 * anInt10489;
	if (aClass602_10481 != null) {
	    Class602 class602 = (aClass602_10481.method9988
				 (Class78.aClass103_825,
				  (-1468443459 * client.anInt11155 == 3
				   ? (Interface20) Class201.anInterface20_2188
				   : Class78.aClass103_825),
				  -2110288546));
	    if (null != class602) {
		anInt10489 = class602.anInt7945 * 1128583165;
		anInt10467
		    = (372226515 * class602.anInt7946 << 9) * -612932795;
		anInt10480 = class602.anInt7948 * 1993106295;
		anInt10495 = 617813239 * class602.anInt7915;
		anInt10496 = class602.anInt7950 * 1071004077;
		anIntArray10479 = class602.anIntArray7951;
		anInt10493 = -1005766001 * class602.anInt7960;
		anInt10483 = -990708005 * class602.anInt7901;
	    } else {
		anInt10489 = -1086873753;
		anInt10467 = 0;
		anInt10480 = 0;
		anInt10495 = 0;
		anInt10496 = 0;
		anIntArray10479 = null;
		anInt10493 = 1916307200;
		anInt10483 = 665776896;
		anInt10491 = 0;
	    }
	} else if (aClass654_Sub1_Sub5_Sub1_Sub1_10482 != null) {
	    int i_22_
		= Class308.method5656(aClass654_Sub1_Sub5_Sub1_Sub1_10482,
				      498751196);
	    if (i != i_22_) {
		anInt10489 = i_22_ * 1086873753;
		Class307 class307
		    = aClass654_Sub1_Sub5_Sub1_Sub1_10482.aClass307_12204;
		if (class307.anIntArray3284 != null)
		    class307 = class307.method5615(Class78.aClass103_825,
						   Class78.aClass103_825,
						   -1466068515);
		if (null != class307) {
		    anInt10467
			= -612932795 * (class307.anInt3332 * -869231065 << 9);
		    anInt10491 = ((class307.anInt3295 * -1156607845 << 9)
				  * -2034082261);
		    anInt10480 = 1689126761 * class307.anInt3334;
		    anInt10493 = class307.anInt3344 * 932822485;
		    anInt10483 = class307.anInt3343 * -32158863;
		} else {
		    anInt10491 = 0;
		    anInt10467 = 0;
		    anInt10480 = 0;
		    anInt10493 = 1916307200;
		    anInt10483 = 665776896;
		}
	    }
	} else if (aClass654_Sub1_Sub5_Sub1_Sub2_10490 != null) {
	    anInt10489
		= Class206.method3937(aClass654_Sub1_Sub5_Sub1_Sub2_10490,
				      1678685691) * 1086873753;
	    anInt10467
		= (aClass654_Sub1_Sub5_Sub1_Sub2_10490.anInt12229 * -785793877
		   << 9) * -612932795;
	    anInt10491 = 0;
	    anInt10480
		= aClass654_Sub1_Sub5_Sub1_Sub2_10490.anInt12215 * -908420807;
	    anInt10493 = 1916307200;
	    anInt10483 = 665776896;
	}
	if (anInt10489 * -241175639 != i && null != aClass491_10492
	    && aClass491_10492 != null) {
	    aClass491_10492.method8014(100, 1962988118);
	    Class171_Sub4.aClass232_9944.method4234(aClass491_10492,
						    1257361043);
	    aClass491_10492 = null;
	}
    }
    
    public static void method16160(int i, int i_23_, int i_24_,
				   Class602 class602) {
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14135((byte) -1);
	     null != class534_sub16;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14139(668408965)) {
	    if (class534_sub16.anInt10471 * 377042463 == i
		&& i_23_ << 9 == class534_sub16.anInt10472 * 1285097747
		&& i_24_ << 9 == -1700987663 * class534_sub16.anInt10473
		&& (-1562722583 * class602.anInt7887
		    == (class534_sub16.aClass602_10481.anInt7887
			* -1562722583))) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(100, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    1828790106);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
		break;
	    }
	}
    }
    
    public static void method16161
	(Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1) {
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14135((byte) -1);
	     class534_sub16 != null;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14139(1004202371)) {
	    if (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		== class654_sub1_sub5_sub1_sub1) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(100, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    -865944873);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
		break;
	    }
	}
    }
    
    public static void method16162
	(int i, int i_25_, int i_26_, int i_27_, Class602 class602,
	 Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1,
	 Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2) {
	Class534_Sub16 class534_sub16 = new Class534_Sub16();
	class534_sub16.anInt10471 = -1178810913 * i;
	class534_sub16.anInt10472 = -973592293 * (i_25_ << 9);
	class534_sub16.anInt10473 = -1604946927 * (i_26_ << 9);
	if (class602 != null) {
	    class534_sub16.aClass602_10481 = class602;
	    int i_28_ = class602.anInt7904 * -1082258489;
	    int i_29_ = -1990374967 * class602.anInt7928;
	    if (1 == i_27_ || i_27_ == 3) {
		i_28_ = class602.anInt7928 * -1990374967;
		i_29_ = class602.anInt7904 * -1082258489;
	    }
	    class534_sub16.anInt10474 = (i_28_ + i_25_ << 9) * -800010841;
	    class534_sub16.anInt10475 = 1156683381 * (i_29_ + i_26_ << 9);
	    class534_sub16.anInt10489 = class602.anInt7945 * 1128583165;
	    class534_sub16.anInt10467
		= (372226515 * class602.anInt7946 << 9) * -612932795;
	    class534_sub16.anInt10480 = class602.anInt7948 * 1993106295;
	    class534_sub16.anInt10495 = class602.anInt7915 * 617813239;
	    class534_sub16.anInt10496 = 1071004077 * class602.anInt7950;
	    class534_sub16.anIntArray10479 = class602.anIntArray7951;
	    class534_sub16.anInt10493 = -1005766001 * class602.anInt7960;
	    class534_sub16.anInt10483 = class602.anInt7901 * -990708005;
	    class534_sub16.anInt10491
		= -2034082261 * (925162477 * class602.anInt7947 << 9);
	    if (null != class602.anIntArray7943) {
		class534_sub16.aBool10498 = true;
		class534_sub16.method16151(-1139117942);
	    }
	    if (null != class534_sub16.anIntArray10479)
		class534_sub16.anInt10488
		    = ((class534_sub16.anInt10495 * -1089168257
			+ (int) (Math.random()
				 * (double) ((1512108791
					      * class534_sub16.anInt10496)
					     - (-1089168257
						* class534_sub16.anInt10495))))
		       * 835810559);
	    aClass700_10468.method14131(class534_sub16, (short) 789);
	    Class171_Sub4.aClass232_9944.method4230((-241175639
						     * (class534_sub16
							.anInt10489)),
						    -1414890769);
	    Class171_Sub4.aClass232_9944
		.method4310(class534_sub16.anIntArray10479, 905878110);
	} else if (class654_sub1_sub5_sub1_sub1 != null) {
	    class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		= class654_sub1_sub5_sub1_sub1;
	    Class307 class307 = class654_sub1_sub5_sub1_sub1.aClass307_12204;
	    if (class307.anIntArray3284 != null) {
		class534_sub16.aBool10498 = true;
		class307
		    = class307.method5615(Class78.aClass103_825,
					  Class78.aClass103_825, -1466068515);
	    }
	    if (null != class307) {
		class534_sub16.anInt10474
		    = -800010841 * (class307.anInt3328 * -1821838479 + i_25_
				    << 9);
		class534_sub16.anInt10475
		    = ((-1821838479 * class307.anInt3328 + i_26_ << 9)
		       * 1156683381);
		class534_sub16.anInt10489
		    = Class308.method5656(class654_sub1_sub5_sub1_sub1,
					  -949101202) * 1086873753;
		class534_sub16.anInt10467
		    = (class307.anInt3332 * -869231065 << 9) * -612932795;
		class534_sub16.anInt10480 = 1689126761 * class307.anInt3334;
		class534_sub16.anInt10493 = class307.anInt3344 * 932822485;
		class534_sub16.anInt10483 = -32158863 * class307.anInt3343;
		class534_sub16.anInt10491
		    = (class307.anInt3295 * -1156607845 << 9) * -2034082261;
		Class171_Sub4.aClass232_9944
		    .method4230(1315529581 * class307.anInt3304, -2044436971);
		Class171_Sub4.aClass232_9944
		    .method4230(1912925203 * class307.anInt3329, -1723198732);
		Class171_Sub4.aClass232_9944
		    .method4230(1144816973 * class307.anInt3330, -1707870902);
		Class171_Sub4.aClass232_9944
		    .method4230(-306882215 * class307.anInt3331, -1504591121);
	    }
	    aClass700_10469.method14131(class534_sub16, (short) 789);
	} else if (class654_sub1_sub5_sub1_sub2 != null) {
	    class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
		= class654_sub1_sub5_sub1_sub2;
	    class534_sub16.anInt10474
		= (i_25_ + class654_sub1_sub5_sub1_sub2.method18545((byte) 1)
		   << 9) * -800010841;
	    class534_sub16.anInt10475
		= (i_26_ + class654_sub1_sub5_sub1_sub2.method18545((byte) 1)
		   << 9) * 1156683381;
	    class534_sub16.anInt10489
		= (Class206.method3937(class654_sub1_sub5_sub1_sub2, 947225566)
		   * 1086873753);
	    class534_sub16.anInt10467
		= ((class654_sub1_sub5_sub1_sub2.anInt12229 * -785793877 << 9)
		   * -612932795);
	    class534_sub16.anInt10480
		= -908420807 * class654_sub1_sub5_sub1_sub2.anInt12215;
	    class534_sub16.anInt10493 = 1916307200;
	    class534_sub16.anInt10483 = 665776896;
	    class534_sub16.anInt10491 = 0;
	    aClass9_10470.method581(class534_sub16,
				    (long) (1126388985
					    * (class654_sub1_sub5_sub1_sub2
					       .anInt11922)));
	    Class171_Sub4.aClass232_9944.method4230
		(class654_sub1_sub5_sub1_sub2.anInt12230 * -729876395,
		 -1721383358);
	    Class171_Sub4.aClass232_9944.method4230
		(-2046269725 * class654_sub1_sub5_sub1_sub2.anInt12222,
		 -1813671589);
	    Class171_Sub4.aClass232_9944.method4230
		(1734964233 * class654_sub1_sub5_sub1_sub2.anInt12227,
		 -1404397088);
	    Class171_Sub4.aClass232_9944.method4230
		(-510526957 * class654_sub1_sub5_sub1_sub2.anInt12231,
		 -2105648157);
	}
    }
    
    public static void method16163(int i, int i_30_, int i_31_,
				   Class602 class602) {
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14135((byte) -1);
	     null != class534_sub16;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14139(639891859)) {
	    if (class534_sub16.anInt10471 * 377042463 == i
		&& i_30_ << 9 == class534_sub16.anInt10472 * 1285097747
		&& i_31_ << 9 == -1700987663 * class534_sub16.anInt10473
		&& (-1562722583 * class602.anInt7887
		    == (class534_sub16.aClass602_10481.anInt7887
			* -1562722583))) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(100, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    1764353161);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
		break;
	    }
	}
    }
    
    public static void method16164(int i, int i_32_, int i_33_,
				   Class602 class602) {
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14135((byte) -1);
	     null != class534_sub16;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14139(1899567351)) {
	    if (class534_sub16.anInt10471 * 377042463 == i
		&& i_32_ << 9 == class534_sub16.anInt10472 * 1285097747
		&& i_33_ << 9 == -1700987663 * class534_sub16.anInt10473
		&& (-1562722583 * class602.anInt7887
		    == (class534_sub16.aClass602_10481.anInt7887
			* -1562722583))) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(100, 1962988118);
		    Class171_Sub4.aClass232_9944
			.method4234(class534_sub16.aClass491_10492, 203428480);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
		break;
	    }
	}
    }
    
    public static void method16165
	(Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1) {
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14135((byte) -1);
	     class534_sub16 != null;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14139(1020294008)) {
	    if (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		== class654_sub1_sub5_sub1_sub1) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(100, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    -2048106042);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
		break;
	    }
	}
    }
    
    public static void method16166
	(int i, int i_34_, int i_35_, int i_36_, Class602 class602,
	 Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1,
	 Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2) {
	Class534_Sub16 class534_sub16 = new Class534_Sub16();
	class534_sub16.anInt10471 = -1178810913 * i;
	class534_sub16.anInt10472 = -973592293 * (i_34_ << 9);
	class534_sub16.anInt10473 = -1604946927 * (i_35_ << 9);
	if (class602 != null) {
	    class534_sub16.aClass602_10481 = class602;
	    int i_37_ = class602.anInt7904 * -1082258489;
	    int i_38_ = -1990374967 * class602.anInt7928;
	    if (1 == i_36_ || i_36_ == 3) {
		i_37_ = class602.anInt7928 * -1990374967;
		i_38_ = class602.anInt7904 * -1082258489;
	    }
	    class534_sub16.anInt10474 = (i_37_ + i_34_ << 9) * -800010841;
	    class534_sub16.anInt10475 = 1156683381 * (i_38_ + i_35_ << 9);
	    class534_sub16.anInt10489 = class602.anInt7945 * 1128583165;
	    class534_sub16.anInt10467
		= (372226515 * class602.anInt7946 << 9) * -612932795;
	    class534_sub16.anInt10480 = class602.anInt7948 * 1993106295;
	    class534_sub16.anInt10495 = class602.anInt7915 * 617813239;
	    class534_sub16.anInt10496 = 1071004077 * class602.anInt7950;
	    class534_sub16.anIntArray10479 = class602.anIntArray7951;
	    class534_sub16.anInt10493 = -1005766001 * class602.anInt7960;
	    class534_sub16.anInt10483 = class602.anInt7901 * -990708005;
	    class534_sub16.anInt10491
		= -2034082261 * (925162477 * class602.anInt7947 << 9);
	    if (null != class602.anIntArray7943) {
		class534_sub16.aBool10498 = true;
		class534_sub16.method16151(1387032259);
	    }
	    if (null != class534_sub16.anIntArray10479)
		class534_sub16.anInt10488
		    = ((class534_sub16.anInt10495 * -1089168257
			+ (int) (Math.random()
				 * (double) ((1512108791
					      * class534_sub16.anInt10496)
					     - (-1089168257
						* class534_sub16.anInt10495))))
		       * 835810559);
	    aClass700_10468.method14131(class534_sub16, (short) 789);
	    Class171_Sub4.aClass232_9944.method4230((-241175639
						     * (class534_sub16
							.anInt10489)),
						    -1290019820);
	    Class171_Sub4.aClass232_9944
		.method4310(class534_sub16.anIntArray10479, -60173164);
	} else if (class654_sub1_sub5_sub1_sub1 != null) {
	    class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		= class654_sub1_sub5_sub1_sub1;
	    Class307 class307 = class654_sub1_sub5_sub1_sub1.aClass307_12204;
	    if (class307.anIntArray3284 != null) {
		class534_sub16.aBool10498 = true;
		class307
		    = class307.method5615(Class78.aClass103_825,
					  Class78.aClass103_825, -1466068515);
	    }
	    if (null != class307) {
		class534_sub16.anInt10474
		    = -800010841 * (class307.anInt3328 * -1821838479 + i_34_
				    << 9);
		class534_sub16.anInt10475
		    = ((-1821838479 * class307.anInt3328 + i_35_ << 9)
		       * 1156683381);
		class534_sub16.anInt10489
		    = Class308.method5656(class654_sub1_sub5_sub1_sub1,
					  -1474436649) * 1086873753;
		class534_sub16.anInt10467
		    = (class307.anInt3332 * -869231065 << 9) * -612932795;
		class534_sub16.anInt10480 = 1689126761 * class307.anInt3334;
		class534_sub16.anInt10493 = class307.anInt3344 * 932822485;
		class534_sub16.anInt10483 = -32158863 * class307.anInt3343;
		class534_sub16.anInt10491
		    = (class307.anInt3295 * -1156607845 << 9) * -2034082261;
		Class171_Sub4.aClass232_9944
		    .method4230(1315529581 * class307.anInt3304, -1671630675);
		Class171_Sub4.aClass232_9944
		    .method4230(1912925203 * class307.anInt3329, -1685772923);
		Class171_Sub4.aClass232_9944
		    .method4230(1144816973 * class307.anInt3330, -1647323869);
		Class171_Sub4.aClass232_9944
		    .method4230(-306882215 * class307.anInt3331, -1758593669);
	    }
	    aClass700_10469.method14131(class534_sub16, (short) 789);
	} else if (class654_sub1_sub5_sub1_sub2 != null) {
	    class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
		= class654_sub1_sub5_sub1_sub2;
	    class534_sub16.anInt10474
		= (i_34_ + class654_sub1_sub5_sub1_sub2.method18545((byte) 1)
		   << 9) * -800010841;
	    class534_sub16.anInt10475
		= (i_35_ + class654_sub1_sub5_sub1_sub2.method18545((byte) 1)
		   << 9) * 1156683381;
	    class534_sub16.anInt10489
		= Class206.method3937(class654_sub1_sub5_sub1_sub2,
				      2064968768) * 1086873753;
	    class534_sub16.anInt10467
		= ((class654_sub1_sub5_sub1_sub2.anInt12229 * -785793877 << 9)
		   * -612932795);
	    class534_sub16.anInt10480
		= -908420807 * class654_sub1_sub5_sub1_sub2.anInt12215;
	    class534_sub16.anInt10493 = 1916307200;
	    class534_sub16.anInt10483 = 665776896;
	    class534_sub16.anInt10491 = 0;
	    aClass9_10470.method581(class534_sub16,
				    (long) (1126388985
					    * (class654_sub1_sub5_sub1_sub2
					       .anInt11922)));
	    Class171_Sub4.aClass232_9944.method4230
		(class654_sub1_sub5_sub1_sub2.anInt12230 * -729876395,
		 -1531367281);
	    Class171_Sub4.aClass232_9944.method4230
		(-2046269725 * class654_sub1_sub5_sub1_sub2.anInt12222,
		 -1679455530);
	    Class171_Sub4.aClass232_9944.method4230
		(1734964233 * class654_sub1_sub5_sub1_sub2.anInt12227,
		 -2075147317);
	    Class171_Sub4.aClass232_9944.method4230
		(-510526957 * class654_sub1_sub5_sub1_sub2.anInt12231,
		 -1944791953);
	}
    }
    
    static void method16167(Class534_Sub16 class534_sub16, int i, int i_39_,
			    int i_40_, int i_41_) {
	if (-1 != -241175639 * class534_sub16.anInt10489
	    || class534_sub16.anIntArray10479 != null) {
	    int i_42_ = class534_sub16.anInt10480 * 63994171;
	    if (0 == 984135565 * class534_sub16.anInt10467
		|| Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub28_10754
		       .method17131(-664497469) == 0
		|| 377042463 * class534_sub16.anInt10471 != i) {
		if (class534_sub16.aClass491_10492 != null) {
		    class534_sub16.aClass491_10492.method8014(100, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    1699393094);
		    class534_sub16.aClass491_10492 = null;
		}
	    } else {
		if (null != class534_sub16.aClass491_10492
		    && ((class534_sub16.aClass491_10492.method8027((byte) 2)
			 == Class480.aClass480_5253)
			|| (class534_sub16.aClass491_10492.method8027((byte) 2)
			    == Class480.aClass480_5257))) {
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    -124293352);
		    class534_sub16.aClass491_10492 = null;
		}
		if (class534_sub16.aClass491_10492 == null) {
		    if (-241175639 * class534_sub16.anInt10489 >= 0) {
			int i_43_ = 256;
			int i_44_
			    = (int) ((0.5F
				      * (float) ((37947927
						  * class534_sub16.anInt10474)
						 - (class534_sub16.anInt10472
						    * 1285097747)))
				     + (float) (1285097747
						* class534_sub16.anInt10472));
			int i_45_
			    = (int) ((float) (-1700987663
					      * class534_sub16.anInt10473)
				     + (float) ((class534_sub16.anInt10475
						 * -374666787)
						- (class534_sub16.anInt10473
						   * -1700987663)) * 0.5F);
			class534_sub16.aClass438_10476.aFloat4864
			    = (float) i_44_;
			class534_sub16.aClass438_10476.aFloat4865
			    = (float) i_45_;
			class534_sub16.aClass491_10492
			    = (Class171_Sub4.aClass232_9944.method4229
			       (Class211.aClass211_2248, class534_sub16,
				-241175639 * class534_sub16.anInt10489, -1, 0,
				Class190.aClass190_2136.method3763(689730760),
				Class207.aClass207_2236,
				(float) (-1081773949
					 * class534_sub16.anInt10491),
				(float) (984135565
					 * class534_sub16.anInt10467),
				class534_sub16.aClass438_10476, 0, i_43_,
				false, -1607095763));
			if (class534_sub16.aClass491_10492 != null) {
			    float f = (float) i_42_ / 255.0F;
			    class534_sub16.aClass491_10492
				.method8037(f, 150, 1797686172);
			    class534_sub16.aClass491_10492
				.method8069(2092173782);
			}
		    }
		} else {
		    int i_46_
			= (int) ((float) (1285097747
					  * class534_sub16.anInt10472)
				 + ((float) ((class534_sub16.anInt10474
					      * 37947927)
					     - (1285097747
						* class534_sub16.anInt10472))
				    * 0.5F));
		    int i_47_
			= (int) (0.5F * (float) ((-374666787
						  * class534_sub16.anInt10475)
						 - (class534_sub16.anInt10473
						    * -1700987663))
				 + (float) (class534_sub16.anInt10473
					    * -1700987663));
		    class534_sub16.aClass438_10476.aFloat4864 = (float) i_46_;
		    class534_sub16.aClass438_10476.aFloat4865 = (float) i_47_;
		}
		if (class534_sub16.aClass491_10494 == null) {
		    if (class534_sub16.anIntArray10479 != null
			&& ((class534_sub16.anInt10488 -= 835810559 * i_41_)
			    * 1102416639) <= 0) {
			int i_48_
			    = ((256 == class534_sub16.anInt10493 * -1536094921
				&& (class534_sub16.anInt10483 * 1762794043
				    == 256))
			       ? 256
			       : ((int) (Math.random()
					 * (double) ((-1536094921
						      * (class534_sub16
							 .anInt10493))
						     - ((class534_sub16
							 .anInt10483)
							* 1762794043)))
				  + 1762794043 * class534_sub16.anInt10483));
			int i_49_
			    = (int) (Math.random()
				     * (double) (class534_sub16
						 .anIntArray10479).length);
			int i_50_
			    = (int) ((float) (class534_sub16.anInt10472
					      * 1285097747)
				     + (float) ((class534_sub16.anInt10474
						 * 37947927)
						- (class534_sub16.anInt10472
						   * 1285097747)) * 0.5F);
			int i_51_
			    = (int) ((float) (class534_sub16.anInt10473
					      * -1700987663)
				     + (0.5F
					* (float) (-374666787 * (class534_sub16
								 .anInt10475)
						   - (-1700987663
						      * (class534_sub16
							 .anInt10473)))));
			class534_sub16.aClass438_10478.aFloat4864
			    = (float) i_50_;
			class534_sub16.aClass438_10478.aFloat4865
			    = (float) i_51_;
			class534_sub16.aClass491_10494
			    = (Class171_Sub4.aClass232_9944.method4229
			       (Class211.aClass211_2249, class534_sub16,
				class534_sub16.anIntArray10479[i_49_], 0,
				i_42_,
				Class190.aClass190_2127.method3763(2114662975),
				Class207.aClass207_2236,
				(float) (-1081773949
					 * class534_sub16.anInt10491),
				(float) (class534_sub16.anInt10467 * 984135565
					 + (class534_sub16.anInt10491
					    * -1081773949)),
				class534_sub16.aClass438_10478, 0, i_48_,
				false, -275726212));
			if (class534_sub16.aClass491_10494 != null)
			    class534_sub16.aClass491_10494
				.method8069(1983672733);
			class534_sub16.anInt10488
			    = ((-1089168257 * class534_sub16.anInt10495
				+ (int) (Math.random()
					 * (double) ((class534_sub16.anInt10496
						      * 1512108791)
						     - (-1089168257
							* (class534_sub16
							   .anInt10495)))))
			       * 835810559);
		    }
		} else {
		    int i_52_
			= (int) (0.5F * (float) ((37947927
						  * class534_sub16.anInt10474)
						 - (class534_sub16.anInt10472
						    * 1285097747))
				 + (float) (1285097747
					    * class534_sub16.anInt10472));
		    int i_53_
			= (int) ((float) (-1700987663
					  * class534_sub16.anInt10473)
				 + 0.5F * (float) ((class534_sub16.anInt10475
						    * -374666787)
						   - (class534_sub16.anInt10473
						      * -1700987663)));
		    class534_sub16.aClass438_10478.aFloat4864 = (float) i_52_;
		    class534_sub16.aClass438_10478.aFloat4865 = (float) i_53_;
		    if ((class534_sub16.aClass491_10494.method8027((byte) 2)
			 == Class480.aClass480_5253)
			|| (class534_sub16.aClass491_10494.method8027((byte) 2)
			    == Class480.aClass480_5257)) {
			Class171_Sub4.aClass232_9944.method4234
			    (class534_sub16.aClass491_10494, 774614149);
			class534_sub16.aClass491_10494 = null;
		    }
		}
	    }
	}
    }
    
    public static void method16168
	(Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2) {
	Class534_Sub16 class534_sub16
	    = ((Class534_Sub16)
	       aClass9_10470.method579((long) (1126388985
					       * (class654_sub1_sub5_sub1_sub2
						  .anInt11922))));
	if (class534_sub16 != null) {
	    if (null != class534_sub16.aClass491_10492) {
		class534_sub16.aClass491_10492.method8014(100, 1962988118);
		Class171_Sub4.aClass232_9944
		    .method4234(class534_sub16.aClass491_10492, -854079248);
		class534_sub16.aClass491_10492 = null;
	    }
	    class534_sub16.method8892((byte) 1);
	}
    }
    
    public static void method16169
	(Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2) {
	Class534_Sub16 class534_sub16
	    = ((Class534_Sub16)
	       aClass9_10470.method579((long) (1126388985
					       * (class654_sub1_sub5_sub1_sub2
						  .anInt11922))));
	if (class534_sub16 != null) {
	    if (null != class534_sub16.aClass491_10492) {
		class534_sub16.aClass491_10492.method8014(100, 1962988118);
		Class171_Sub4.aClass232_9944
		    .method4234(class534_sub16.aClass491_10492, -1803929746);
		class534_sub16.aClass491_10492 = null;
	    }
	    class534_sub16.method8892((byte) 1);
	}
    }
    
    public static void method16170(boolean bool) {
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14135((byte) -1);
	     null != class534_sub16;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14139(1885899205)) {
	    if (class534_sub16.aClass491_10492 != null) {
		class534_sub16.aClass491_10492.method8014(150, 1962988118);
		Class171_Sub4.aClass232_9944
		    .method4234(class534_sub16.aClass491_10492, -213121137);
		class534_sub16.aClass491_10492 = null;
	    }
	    if (null != class534_sub16.aClass491_10494) {
		class534_sub16.aClass491_10494.method8014(150, 1962988118);
		Class171_Sub4.aClass232_9944
		    .method4234(class534_sub16.aClass491_10494, -731827742);
		class534_sub16.aClass491_10494 = null;
	    }
	    class534_sub16.method8892((byte) 1);
	}
	if (bool) {
	    for (Class534_Sub16 class534_sub16
		     = (Class534_Sub16) aClass700_10469.method14135((byte) -1);
		 null != class534_sub16;
		 class534_sub16 = ((Class534_Sub16)
				   aClass700_10469.method14139(1493535662))) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(150, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    -2080306892);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
	    }
	    for (Class534_Sub16 class534_sub16
		     = (Class534_Sub16) aClass9_10470.method583(-1983151651);
		 null != class534_sub16;
		 class534_sub16
		     = (Class534_Sub16) aClass9_10470.method584((byte) -41)) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(150, 1962988118);
		    Class171_Sub4.aClass232_9944
			.method4234(class534_sub16.aClass491_10492, 165897949);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
	    }
	}
    }
    
    static int method16171
	(Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1) {
	Class307 class307 = class654_sub1_sub5_sub1_sub1.aClass307_12204;
	if (class307.anIntArray3284 != null) {
	    class307 = class307.method5615(Class78.aClass103_825,
					   Class78.aClass103_825, -1466068515);
	    if (class307 == null)
		return -1;
	}
	int i = 1144816973 * class307.anInt3330;
	Class570 class570
	    = class654_sub1_sub5_sub1_sub1.method18531((byte) -9);
	int i_54_ = class654_sub1_sub5_sub1_sub1.aClass711_Sub1_11965
			.method14329(2049449634);
	if (-1 == i_54_
	    || class654_sub1_sub5_sub1_sub1.aClass711_Sub1_11965.aBool10971)
	    i = class307.anInt3304 * 1315529581;
	else if (i_54_ == class570.anInt7642 * 1074876801
		 || class570.anInt7631 * 421310407 == i_54_
		 || i_54_ == class570.anInt7633 * 541177679
		 || -921167219 * class570.anInt7632 == i_54_)
	    i = class307.anInt3331 * -306882215;
	else if (class570.anInt7634 * 1846476627 == i_54_
		 || i_54_ == class570.anInt7638 * -63558043
		 || class570.anInt7647 * -1045334803 == i_54_
		 || i_54_ == 630970333 * class570.anInt7636)
	    i = 1912925203 * class307.anInt3329;
	return i;
    }
    
    static int method16172
	(Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2) {
	int i = 1734964233 * class654_sub1_sub5_sub1_sub2.anInt12227;
	Class570 class570
	    = class654_sub1_sub5_sub1_sub2.method18531((byte) -73);
	int i_55_ = class654_sub1_sub5_sub1_sub2.aClass711_Sub1_11965
			.method14329(728188588);
	if (-1 == i_55_
	    || class654_sub1_sub5_sub1_sub2.aClass711_Sub1_11965.aBool10971)
	    i = -729876395 * class654_sub1_sub5_sub1_sub2.anInt12230;
	else if (1074876801 * class570.anInt7642 == i_55_
		 || class570.anInt7631 * 421310407 == i_55_
		 || i_55_ == class570.anInt7633 * 541177679
		 || class570.anInt7632 * -921167219 == i_55_)
	    i = class654_sub1_sub5_sub1_sub2.anInt12231 * -510526957;
	else if (i_55_ == 1846476627 * class570.anInt7634
		 || -63558043 * class570.anInt7638 == i_55_
		 || class570.anInt7647 * -1045334803 == i_55_
		 || 630970333 * class570.anInt7636 == i_55_)
	    i = -2046269725 * class654_sub1_sub5_sub1_sub2.anInt12222;
	return i;
    }
    
    public static void method16173(int i, int i_56_, int i_57_, int i_58_) {
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14135((byte) -1);
	     class534_sub16 != null;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10468.method14139(685078232))
	    Class243.method4483(class534_sub16, i, i_56_, i_57_, i_58_,
				-1788895012);
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14135((byte) -1);
	     null != class534_sub16;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14139(1148511129)) {
	    int i_59_ = 1;
	    Class570 class570
		= class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		      .method18531((byte) -109);
	    int i_60_ = class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
			    .aClass711_Sub1_11965.method14329(1183268783);
	    if (i_60_ == -1
		|| (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		    .aClass711_Sub1_11965.aBool10971))
		i_59_ = 0;
	    else if (class570.anInt7642 * 1074876801 == i_60_
		     || i_60_ == 421310407 * class570.anInt7631
		     || i_60_ == class570.anInt7633 * 541177679
		     || i_60_ == class570.anInt7632 * -921167219)
		i_59_ = 2;
	    else if (i_60_ == 1846476627 * class570.anInt7634
		     || i_60_ == class570.anInt7638 * -63558043
		     || i_60_ == class570.anInt7647 * -1045334803
		     || 630970333 * class570.anInt7636 == i_60_)
		i_59_ = 3;
	    if (i_59_ != class534_sub16.anInt10497 * 1445799511) {
		int i_61_
		    = (Class308.method5656
		       (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482,
			964108196));
		Class307 class307
		    = (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		       .aClass307_12204);
		if (null != class307.anIntArray3284)
		    class307 = class307.method5615(Class78.aClass103_825,
						   Class78.aClass103_825,
						   -1466068515);
		if (null == class307 || -1 == i_61_) {
		    class534_sub16.anInt10489 = -1086873753;
		    class534_sub16.anInt10497 = i_59_ * 1882006887;
		} else if (i_61_ != -241175639 * class534_sub16.anInt10489) {
		    boolean bool = false;
		    if (class534_sub16.aClass491_10492 != null) {
			class534_sub16.anInt10480 -= 1072686592;
			if (class534_sub16.anInt10480 * 63994171 <= 0) {
			    class534_sub16.aClass491_10492
				.method8014(100, 1962988118);
			    Class171_Sub4.aClass232_9944.method4234
				(class534_sub16.aClass491_10492, 1160397765);
			    class534_sub16.aClass491_10492 = null;
			    bool = true;
			}
		    } else
			bool = true;
		    if (bool) {
			class534_sub16.anInt10480
			    = class307.anInt3334 * 1689126761;
			class534_sub16.anInt10489 = i_61_ * 1086873753;
			class534_sub16.anInt10497 = i_59_ * 1882006887;
		    }
		} else {
		    class534_sub16.anInt10497 = i_59_ * 1882006887;
		    class534_sub16.anInt10480
			= class307.anInt3334 * 1689126761;
		}
	    }
	    Class438 class438
		= (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		       .method10807
		   ().aClass438_4885);
	    class534_sub16.anInt10472 = -973592293 * (int) class438.aFloat4864;
	    class534_sub16.anInt10474
		= ((int) class438.aFloat4864
		   + (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
			  .method18545((byte) 1)
		      << 8)) * -800010841;
	    class534_sub16.anInt10473
		= (int) class438.aFloat4865 * -1604946927;
	    class534_sub16.anInt10475
		= ((int) class438.aFloat4865
		   + (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
			  .method18545((byte) 1)
		      << 8)) * 1156683381;
	    class534_sub16.anInt10471
		= (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		   .aByte10854) * -1178810913;
	    Class243.method4483(class534_sub16, i, i_56_, i_57_, i_58_,
				-1788895012);
	}
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass9_10470.method583(-1522344863);
	     class534_sub16 != null;
	     class534_sub16
		 = (Class534_Sub16) aClass9_10470.method584((byte) -67)) {
	    int i_62_ = 1;
	    Class570 class570
		= class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
		      .method18531((byte) -96);
	    int i_63_ = class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
			    .aClass711_Sub1_11965.method14329(794408118);
	    if (-1 == i_63_
		|| (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
		    .aClass711_Sub1_11965.aBool10971))
		i_62_ = 0;
	    else if (i_63_ == 1074876801 * class570.anInt7642
		     || i_63_ == class570.anInt7631 * 421310407
		     || class570.anInt7633 * 541177679 == i_63_
		     || i_63_ == class570.anInt7632 * -921167219)
		i_62_ = 2;
	    else if (i_63_ == 1846476627 * class570.anInt7634
		     || -63558043 * class570.anInt7638 == i_63_
		     || -1045334803 * class570.anInt7647 == i_63_
		     || class570.anInt7636 * 630970333 == i_63_)
		i_62_ = 3;
	    if (i_62_ != 1445799511 * class534_sub16.anInt10497) {
		int i_64_
		    = (Class206.method3937
		       (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490,
			305947134));
		if (class534_sub16.anInt10489 * -241175639 != i_64_) {
		    boolean bool = false;
		    if (class534_sub16.aClass491_10492 != null) {
			class534_sub16.anInt10480 -= 1072686592;
			if (class534_sub16.anInt10480 * 63994171 <= 0) {
			    class534_sub16.aClass491_10492
				.method8014(100, 1962988118);
			    Class171_Sub4.aClass232_9944.method4234
				(class534_sub16.aClass491_10492, 881417211);
			    class534_sub16.aClass491_10492 = null;
			    bool = true;
			}
		    } else
			bool = true;
		    if (bool) {
			class534_sub16.anInt10480
			    = (-908420807
			       * (class534_sub16
				  .aClass654_Sub1_Sub5_Sub1_Sub2_10490
				  .anInt12215));
			class534_sub16.anInt10489 = i_64_ * 1086873753;
			class534_sub16.anInt10497 = 1882006887 * i_62_;
		    }
		} else {
		    class534_sub16.anInt10480
			= -908420807 * (class534_sub16
					.aClass654_Sub1_Sub5_Sub1_Sub2_10490
					.anInt12215);
		    class534_sub16.anInt10497 = i_62_ * 1882006887;
		}
	    }
	    Class438 class438
		= (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
		       .method10807
		   ().aClass438_4885);
	    class534_sub16.anInt10472 = (int) class438.aFloat4864 * -973592293;
	    class534_sub16.anInt10474
		= ((int) class438.aFloat4864
		   + (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
			  .method18545((byte) 1)
		      << 8)) * -800010841;
	    class534_sub16.anInt10473
		= -1604946927 * (int) class438.aFloat4865;
	    class534_sub16.anInt10475
		= ((int) class438.aFloat4865
		   + (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
			  .method18545((byte) 1)
		      << 8)) * 1156683381;
	    class534_sub16.anInt10471
		= (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
		   .aByte10854) * -1178810913;
	    Class243.method4483(class534_sub16, i, i_56_, i_57_, i_58_,
				-1788895012);
	}
    }
    
    Class534_Sub16() {
	/* empty */
    }
    
    static void method16174(Class534_Sub16 class534_sub16, int i, int i_65_,
			    int i_66_, int i_67_) {
	if (-1 != -241175639 * class534_sub16.anInt10489
	    || class534_sub16.anIntArray10479 != null) {
	    int i_68_ = class534_sub16.anInt10480 * 63994171;
	    if (0 == 984135565 * class534_sub16.anInt10467
		|| Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub28_10754
		       .method17131(-1458016648) == 0
		|| 377042463 * class534_sub16.anInt10471 != i) {
		if (class534_sub16.aClass491_10492 != null) {
		    class534_sub16.aClass491_10492.method8014(100, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    -1321982299);
		    class534_sub16.aClass491_10492 = null;
		}
	    } else {
		if (null != class534_sub16.aClass491_10492
		    && ((class534_sub16.aClass491_10492.method8027((byte) 2)
			 == Class480.aClass480_5253)
			|| (class534_sub16.aClass491_10492.method8027((byte) 2)
			    == Class480.aClass480_5257))) {
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    -1856164219);
		    class534_sub16.aClass491_10492 = null;
		}
		if (class534_sub16.aClass491_10492 == null) {
		    if (-241175639 * class534_sub16.anInt10489 >= 0) {
			int i_69_ = 256;
			int i_70_
			    = (int) ((0.5F
				      * (float) ((37947927
						  * class534_sub16.anInt10474)
						 - (class534_sub16.anInt10472
						    * 1285097747)))
				     + (float) (1285097747
						* class534_sub16.anInt10472));
			int i_71_
			    = (int) ((float) (-1700987663
					      * class534_sub16.anInt10473)
				     + (float) ((class534_sub16.anInt10475
						 * -374666787)
						- (class534_sub16.anInt10473
						   * -1700987663)) * 0.5F);
			class534_sub16.aClass438_10476.aFloat4864
			    = (float) i_70_;
			class534_sub16.aClass438_10476.aFloat4865
			    = (float) i_71_;
			class534_sub16.aClass491_10492
			    = (Class171_Sub4.aClass232_9944.method4229
			       (Class211.aClass211_2248, class534_sub16,
				-241175639 * class534_sub16.anInt10489, -1, 0,
				Class190.aClass190_2136.method3763(836549),
				Class207.aClass207_2236,
				(float) (-1081773949
					 * class534_sub16.anInt10491),
				(float) (984135565
					 * class534_sub16.anInt10467),
				class534_sub16.aClass438_10476, 0, i_69_,
				false, -737333246));
			if (class534_sub16.aClass491_10492 != null) {
			    float f = (float) i_68_ / 255.0F;
			    class534_sub16.aClass491_10492
				.method8037(f, 150, 1381758790);
			    class534_sub16.aClass491_10492
				.method8069(1931748589);
			}
		    }
		} else {
		    int i_72_
			= (int) ((float) (1285097747
					  * class534_sub16.anInt10472)
				 + ((float) ((class534_sub16.anInt10474
					      * 37947927)
					     - (1285097747
						* class534_sub16.anInt10472))
				    * 0.5F));
		    int i_73_
			= (int) (0.5F * (float) ((-374666787
						  * class534_sub16.anInt10475)
						 - (class534_sub16.anInt10473
						    * -1700987663))
				 + (float) (class534_sub16.anInt10473
					    * -1700987663));
		    class534_sub16.aClass438_10476.aFloat4864 = (float) i_72_;
		    class534_sub16.aClass438_10476.aFloat4865 = (float) i_73_;
		}
		if (class534_sub16.aClass491_10494 == null) {
		    if (class534_sub16.anIntArray10479 != null
			&& ((class534_sub16.anInt10488 -= 835810559 * i_67_)
			    * 1102416639) <= 0) {
			int i_74_
			    = ((256 == class534_sub16.anInt10493 * -1536094921
				&& (class534_sub16.anInt10483 * 1762794043
				    == 256))
			       ? 256
			       : ((int) (Math.random()
					 * (double) ((-1536094921
						      * (class534_sub16
							 .anInt10493))
						     - ((class534_sub16
							 .anInt10483)
							* 1762794043)))
				  + 1762794043 * class534_sub16.anInt10483));
			int i_75_
			    = (int) (Math.random()
				     * (double) (class534_sub16
						 .anIntArray10479).length);
			int i_76_
			    = (int) ((float) (class534_sub16.anInt10472
					      * 1285097747)
				     + (float) ((class534_sub16.anInt10474
						 * 37947927)
						- (class534_sub16.anInt10472
						   * 1285097747)) * 0.5F);
			int i_77_
			    = (int) ((float) (class534_sub16.anInt10473
					      * -1700987663)
				     + (0.5F
					* (float) (-374666787 * (class534_sub16
								 .anInt10475)
						   - (-1700987663
						      * (class534_sub16
							 .anInt10473)))));
			class534_sub16.aClass438_10478.aFloat4864
			    = (float) i_76_;
			class534_sub16.aClass438_10478.aFloat4865
			    = (float) i_77_;
			class534_sub16.aClass491_10494
			    = (Class171_Sub4.aClass232_9944.method4229
			       (Class211.aClass211_2249, class534_sub16,
				class534_sub16.anIntArray10479[i_75_], 0,
				i_68_,
				Class190.aClass190_2127.method3763(932857303),
				Class207.aClass207_2236,
				(float) (-1081773949
					 * class534_sub16.anInt10491),
				(float) (class534_sub16.anInt10467 * 984135565
					 + (class534_sub16.anInt10491
					    * -1081773949)),
				class534_sub16.aClass438_10478, 0, i_74_,
				false, -532932956));
			if (class534_sub16.aClass491_10494 != null)
			    class534_sub16.aClass491_10494
				.method8069(2019368299);
			class534_sub16.anInt10488
			    = ((-1089168257 * class534_sub16.anInt10495
				+ (int) (Math.random()
					 * (double) ((class534_sub16.anInt10496
						      * 1512108791)
						     - (-1089168257
							* (class534_sub16
							   .anInt10495)))))
			       * 835810559);
		    }
		} else {
		    int i_78_
			= (int) (0.5F * (float) ((37947927
						  * class534_sub16.anInt10474)
						 - (class534_sub16.anInt10472
						    * 1285097747))
				 + (float) (1285097747
					    * class534_sub16.anInt10472));
		    int i_79_
			= (int) ((float) (-1700987663
					  * class534_sub16.anInt10473)
				 + 0.5F * (float) ((class534_sub16.anInt10475
						    * -374666787)
						   - (class534_sub16.anInt10473
						      * -1700987663)));
		    class534_sub16.aClass438_10478.aFloat4864 = (float) i_78_;
		    class534_sub16.aClass438_10478.aFloat4865 = (float) i_79_;
		    if ((class534_sub16.aClass491_10494.method8027((byte) 2)
			 == Class480.aClass480_5253)
			|| (class534_sub16.aClass491_10494.method8027((byte) 2)
			    == Class480.aClass480_5257)) {
			Class171_Sub4.aClass232_9944.method4234
			    (class534_sub16.aClass491_10494, -1659070132);
			class534_sub16.aClass491_10494 = null;
		    }
		}
	    }
	}
    }
    
    static void method16175(Class534_Sub16 class534_sub16, int i, int i_80_,
			    int i_81_, int i_82_) {
	if (-1 != -241175639 * class534_sub16.anInt10489
	    || class534_sub16.anIntArray10479 != null) {
	    int i_83_ = class534_sub16.anInt10480 * 63994171;
	    if (0 == 984135565 * class534_sub16.anInt10467
		|| Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub28_10754
		       .method17131(1737082897) == 0
		|| 377042463 * class534_sub16.anInt10471 != i) {
		if (class534_sub16.aClass491_10492 != null) {
		    class534_sub16.aClass491_10492.method8014(100, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    -314807468);
		    class534_sub16.aClass491_10492 = null;
		}
	    } else {
		if (null != class534_sub16.aClass491_10492
		    && ((class534_sub16.aClass491_10492.method8027((byte) 2)
			 == Class480.aClass480_5253)
			|| (class534_sub16.aClass491_10492.method8027((byte) 2)
			    == Class480.aClass480_5257))) {
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    -1427735502);
		    class534_sub16.aClass491_10492 = null;
		}
		if (class534_sub16.aClass491_10492 == null) {
		    if (-241175639 * class534_sub16.anInt10489 >= 0) {
			int i_84_ = 256;
			int i_85_
			    = (int) ((0.5F
				      * (float) ((37947927
						  * class534_sub16.anInt10474)
						 - (class534_sub16.anInt10472
						    * 1285097747)))
				     + (float) (1285097747
						* class534_sub16.anInt10472));
			int i_86_
			    = (int) ((float) (-1700987663
					      * class534_sub16.anInt10473)
				     + (float) ((class534_sub16.anInt10475
						 * -374666787)
						- (class534_sub16.anInt10473
						   * -1700987663)) * 0.5F);
			class534_sub16.aClass438_10476.aFloat4864
			    = (float) i_85_;
			class534_sub16.aClass438_10476.aFloat4865
			    = (float) i_86_;
			class534_sub16.aClass491_10492
			    = (Class171_Sub4.aClass232_9944.method4229
			       (Class211.aClass211_2248, class534_sub16,
				-241175639 * class534_sub16.anInt10489, -1, 0,
				Class190.aClass190_2136.method3763(1732083949),
				Class207.aClass207_2236,
				(float) (-1081773949
					 * class534_sub16.anInt10491),
				(float) (984135565
					 * class534_sub16.anInt10467),
				class534_sub16.aClass438_10476, 0, i_84_,
				false, -821317183));
			if (class534_sub16.aClass491_10492 != null) {
			    float f = (float) i_83_ / 255.0F;
			    class534_sub16.aClass491_10492
				.method8037(f, 150, 1747156477);
			    class534_sub16.aClass491_10492
				.method8069(1949845787);
			}
		    }
		} else {
		    int i_87_
			= (int) ((float) (1285097747
					  * class534_sub16.anInt10472)
				 + ((float) ((class534_sub16.anInt10474
					      * 37947927)
					     - (1285097747
						* class534_sub16.anInt10472))
				    * 0.5F));
		    int i_88_
			= (int) (0.5F * (float) ((-374666787
						  * class534_sub16.anInt10475)
						 - (class534_sub16.anInt10473
						    * -1700987663))
				 + (float) (class534_sub16.anInt10473
					    * -1700987663));
		    class534_sub16.aClass438_10476.aFloat4864 = (float) i_87_;
		    class534_sub16.aClass438_10476.aFloat4865 = (float) i_88_;
		}
		if (class534_sub16.aClass491_10494 == null) {
		    if (class534_sub16.anIntArray10479 != null
			&& ((class534_sub16.anInt10488 -= 835810559 * i_82_)
			    * 1102416639) <= 0) {
			int i_89_
			    = ((256 == class534_sub16.anInt10493 * -1536094921
				&& (class534_sub16.anInt10483 * 1762794043
				    == 256))
			       ? 256
			       : ((int) (Math.random()
					 * (double) ((-1536094921
						      * (class534_sub16
							 .anInt10493))
						     - ((class534_sub16
							 .anInt10483)
							* 1762794043)))
				  + 1762794043 * class534_sub16.anInt10483));
			int i_90_
			    = (int) (Math.random()
				     * (double) (class534_sub16
						 .anIntArray10479).length);
			int i_91_
			    = (int) ((float) (class534_sub16.anInt10472
					      * 1285097747)
				     + (float) ((class534_sub16.anInt10474
						 * 37947927)
						- (class534_sub16.anInt10472
						   * 1285097747)) * 0.5F);
			int i_92_
			    = (int) ((float) (class534_sub16.anInt10473
					      * -1700987663)
				     + (0.5F
					* (float) (-374666787 * (class534_sub16
								 .anInt10475)
						   - (-1700987663
						      * (class534_sub16
							 .anInt10473)))));
			class534_sub16.aClass438_10478.aFloat4864
			    = (float) i_91_;
			class534_sub16.aClass438_10478.aFloat4865
			    = (float) i_92_;
			class534_sub16.aClass491_10494
			    = (Class171_Sub4.aClass232_9944.method4229
			       (Class211.aClass211_2249, class534_sub16,
				class534_sub16.anIntArray10479[i_90_], 0,
				i_83_,
				Class190.aClass190_2127.method3763(-633499320),
				Class207.aClass207_2236,
				(float) (-1081773949
					 * class534_sub16.anInt10491),
				(float) (class534_sub16.anInt10467 * 984135565
					 + (class534_sub16.anInt10491
					    * -1081773949)),
				class534_sub16.aClass438_10478, 0, i_89_,
				false, 676355308));
			if (class534_sub16.aClass491_10494 != null)
			    class534_sub16.aClass491_10494
				.method8069(2143155589);
			class534_sub16.anInt10488
			    = ((-1089168257 * class534_sub16.anInt10495
				+ (int) (Math.random()
					 * (double) ((class534_sub16.anInt10496
						      * 1512108791)
						     - (-1089168257
							* (class534_sub16
							   .anInt10495)))))
			       * 835810559);
		    }
		} else {
		    int i_93_
			= (int) (0.5F * (float) ((37947927
						  * class534_sub16.anInt10474)
						 - (class534_sub16.anInt10472
						    * 1285097747))
				 + (float) (1285097747
					    * class534_sub16.anInt10472));
		    int i_94_
			= (int) ((float) (-1700987663
					  * class534_sub16.anInt10473)
				 + 0.5F * (float) ((class534_sub16.anInt10475
						    * -374666787)
						   - (class534_sub16.anInt10473
						      * -1700987663)));
		    class534_sub16.aClass438_10478.aFloat4864 = (float) i_93_;
		    class534_sub16.aClass438_10478.aFloat4865 = (float) i_94_;
		    if ((class534_sub16.aClass491_10494.method8027((byte) 2)
			 == Class480.aClass480_5253)
			|| (class534_sub16.aClass491_10494.method8027((byte) 2)
			    == Class480.aClass480_5257)) {
			Class171_Sub4.aClass232_9944.method4234
			    (class534_sub16.aClass491_10494, -507555710);
			class534_sub16.aClass491_10494 = null;
		    }
		}
	    }
	}
    }
    
    static void method16176(Class534_Sub16 class534_sub16, int i, int i_95_,
			    int i_96_, int i_97_) {
	if (-1 != -241175639 * class534_sub16.anInt10489
	    || class534_sub16.anIntArray10479 != null) {
	    int i_98_ = class534_sub16.anInt10480 * 63994171;
	    if (0 == 984135565 * class534_sub16.anInt10467
		|| Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub28_10754
		       .method17131(-614834777) == 0
		|| 377042463 * class534_sub16.anInt10471 != i) {
		if (class534_sub16.aClass491_10492 != null) {
		    class534_sub16.aClass491_10492.method8014(100, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    -1166379388);
		    class534_sub16.aClass491_10492 = null;
		}
	    } else {
		if (null != class534_sub16.aClass491_10492
		    && ((class534_sub16.aClass491_10492.method8027((byte) 2)
			 == Class480.aClass480_5253)
			|| (class534_sub16.aClass491_10492.method8027((byte) 2)
			    == Class480.aClass480_5257))) {
		    Class171_Sub4.aClass232_9944
			.method4234(class534_sub16.aClass491_10492, 519611176);
		    class534_sub16.aClass491_10492 = null;
		}
		if (class534_sub16.aClass491_10492 == null) {
		    if (-241175639 * class534_sub16.anInt10489 >= 0) {
			int i_99_ = 256;
			int i_100_
			    = (int) ((0.5F
				      * (float) ((37947927
						  * class534_sub16.anInt10474)
						 - (class534_sub16.anInt10472
						    * 1285097747)))
				     + (float) (1285097747
						* class534_sub16.anInt10472));
			int i_101_
			    = (int) ((float) (-1700987663
					      * class534_sub16.anInt10473)
				     + (float) ((class534_sub16.anInt10475
						 * -374666787)
						- (class534_sub16.anInt10473
						   * -1700987663)) * 0.5F);
			class534_sub16.aClass438_10476.aFloat4864
			    = (float) i_100_;
			class534_sub16.aClass438_10476.aFloat4865
			    = (float) i_101_;
			class534_sub16.aClass491_10492
			    = (Class171_Sub4.aClass232_9944.method4229
			       (Class211.aClass211_2248, class534_sub16,
				-241175639 * class534_sub16.anInt10489, -1, 0,
				Class190.aClass190_2136.method3763(-906538213),
				Class207.aClass207_2236,
				(float) (-1081773949
					 * class534_sub16.anInt10491),
				(float) (984135565
					 * class534_sub16.anInt10467),
				class534_sub16.aClass438_10476, 0, i_99_,
				false, 744352144));
			if (class534_sub16.aClass491_10492 != null) {
			    float f = (float) i_98_ / 255.0F;
			    class534_sub16.aClass491_10492
				.method8037(f, 150, 1367640274);
			    class534_sub16.aClass491_10492
				.method8069(2122897203);
			}
		    }
		} else {
		    int i_102_
			= (int) ((float) (1285097747
					  * class534_sub16.anInt10472)
				 + ((float) ((class534_sub16.anInt10474
					      * 37947927)
					     - (1285097747
						* class534_sub16.anInt10472))
				    * 0.5F));
		    int i_103_
			= (int) (0.5F * (float) ((-374666787
						  * class534_sub16.anInt10475)
						 - (class534_sub16.anInt10473
						    * -1700987663))
				 + (float) (class534_sub16.anInt10473
					    * -1700987663));
		    class534_sub16.aClass438_10476.aFloat4864 = (float) i_102_;
		    class534_sub16.aClass438_10476.aFloat4865 = (float) i_103_;
		}
		if (class534_sub16.aClass491_10494 == null) {
		    if (class534_sub16.anIntArray10479 != null
			&& ((class534_sub16.anInt10488 -= 835810559 * i_97_)
			    * 1102416639) <= 0) {
			int i_104_
			    = ((256 == class534_sub16.anInt10493 * -1536094921
				&& (class534_sub16.anInt10483 * 1762794043
				    == 256))
			       ? 256
			       : ((int) (Math.random()
					 * (double) ((-1536094921
						      * (class534_sub16
							 .anInt10493))
						     - ((class534_sub16
							 .anInt10483)
							* 1762794043)))
				  + 1762794043 * class534_sub16.anInt10483));
			int i_105_
			    = (int) (Math.random()
				     * (double) (class534_sub16
						 .anIntArray10479).length);
			int i_106_
			    = (int) ((float) (class534_sub16.anInt10472
					      * 1285097747)
				     + (float) ((class534_sub16.anInt10474
						 * 37947927)
						- (class534_sub16.anInt10472
						   * 1285097747)) * 0.5F);
			int i_107_
			    = (int) ((float) (class534_sub16.anInt10473
					      * -1700987663)
				     + (0.5F
					* (float) (-374666787 * (class534_sub16
								 .anInt10475)
						   - (-1700987663
						      * (class534_sub16
							 .anInt10473)))));
			class534_sub16.aClass438_10478.aFloat4864
			    = (float) i_106_;
			class534_sub16.aClass438_10478.aFloat4865
			    = (float) i_107_;
			class534_sub16.aClass491_10494
			    = (Class171_Sub4.aClass232_9944.method4229
			       (Class211.aClass211_2249, class534_sub16,
				class534_sub16.anIntArray10479[i_105_], 0,
				i_98_,
				Class190.aClass190_2127.method3763(2048635158),
				Class207.aClass207_2236,
				(float) (-1081773949
					 * class534_sub16.anInt10491),
				(float) (class534_sub16.anInt10467 * 984135565
					 + (class534_sub16.anInt10491
					    * -1081773949)),
				class534_sub16.aClass438_10478, 0, i_104_,
				false, -375277836));
			if (class534_sub16.aClass491_10494 != null)
			    class534_sub16.aClass491_10494
				.method8069(2023418322);
			class534_sub16.anInt10488
			    = ((-1089168257 * class534_sub16.anInt10495
				+ (int) (Math.random()
					 * (double) ((class534_sub16.anInt10496
						      * 1512108791)
						     - (-1089168257
							* (class534_sub16
							   .anInt10495)))))
			       * 835810559);
		    }
		} else {
		    int i_108_
			= (int) (0.5F * (float) ((37947927
						  * class534_sub16.anInt10474)
						 - (class534_sub16.anInt10472
						    * 1285097747))
				 + (float) (1285097747
					    * class534_sub16.anInt10472));
		    int i_109_
			= (int) ((float) (-1700987663
					  * class534_sub16.anInt10473)
				 + 0.5F * (float) ((class534_sub16.anInt10475
						    * -374666787)
						   - (class534_sub16.anInt10473
						      * -1700987663)));
		    class534_sub16.aClass438_10478.aFloat4864 = (float) i_108_;
		    class534_sub16.aClass438_10478.aFloat4865 = (float) i_109_;
		    if ((class534_sub16.aClass491_10494.method8027((byte) 2)
			 == Class480.aClass480_5253)
			|| (class534_sub16.aClass491_10494.method8027((byte) 2)
			    == Class480.aClass480_5257)) {
			Class171_Sub4.aClass232_9944.method4234
			    (class534_sub16.aClass491_10494, 1150853411);
			class534_sub16.aClass491_10494 = null;
		    }
		}
	    }
	}
    }
    
    public static void method16177
	(Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1) {
	for (Class534_Sub16 class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14135((byte) -1);
	     class534_sub16 != null;
	     class534_sub16
		 = (Class534_Sub16) aClass700_10469.method14139(1385765568)) {
	    if (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		== class654_sub1_sub5_sub1_sub1) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(100, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    1947962045);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
		break;
	    }
	}
    }
    
    static final void method16178(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class410.method6711(class247, class243, class669, (byte) 100);
    }
    
    static final void method16179(Class669 class669, byte i) {
	int i_110_ = (class669.anIntArray8595
		      [(class669.anInt8600 -= 308999563) * 2088438307]);
	int i_111_ = Class44_Sub6.aClass534_Sub35_10989
			 .aClass690_Sub28_10788.method17131(-1888456810);
	if (i_111_ != i_110_) {
	    Class44_Sub6.aClass534_Sub35_10989.method16438
		(Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub28_10788,
		 i_110_, 1550383463);
	    Class672.method11096((byte) 1);
	    client.aBool11048 = false;
	}
    }
}
