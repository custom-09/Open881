/* Class44_Sub22 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class44_Sub22 extends Class44
{
    public Class44_Sub22(Class675 class675, Class672 class672,
			 Class472 class472) {
	super(class675, class672, class472, Class649.aClass649_8389, 64,
	      new Class61(com.jagex.Class90.class));
    }
    
    public static boolean method17372(int i, int i_0_) {
	return 16 == i || i == 3 || i == 14;
    }
    
    public static Class692 method17373(int i, int i_1_) {
	Class692[] class692s = Class429.method6811(1011348130);
	for (int i_2_ = 0; i_2_ < class692s.length; i_2_++) {
	    Class692 class692 = class692s[i_2_];
	    if (class692.anInt8763 * 1457930057 == i)
		return class692;
	}
	return null;
    }
}
