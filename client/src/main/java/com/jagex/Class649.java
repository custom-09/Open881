/* Class649 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class649
{
    static Class649 aClass649_8379;
    static Class649 aClass649_8380;
    static Class649 aClass649_8381;
    public static Class649 aClass649_8382;
    static Class649 aClass649_8383;
    public static Class649 aClass649_8384;
    public int anInt8385;
    public static Class649 aClass649_8386;
    public static Class649 aClass649_8387;
    public static Class649 aClass649_8388;
    public static Class649 aClass649_8389;
    static Class649 aClass649_8390;
    public static Class649 aClass649_8391;
    static Class649 aClass649_8392;
    public static Class649 aClass649_8393;
    static Class649 aClass649_8394;
    static Class649 aClass649_8395;
    public static Class649 aClass649_8396;
    static Class649 aClass649_8397;
    public static Class649 aClass649_8398 = new Class649(1);
    static Class649 aClass649_8399;
    static Class649 aClass649_8400;
    static Class649 aClass649_8401;
    static Class649 aClass649_8402;
    public static Class649 aClass649_8403;
    public static Class649 aClass649_8404;
    static Class649 aClass649_8405;
    public static Class649 aClass649_8406;
    public static Class649 aClass649_8407;
    public static Class649 aClass649_8408;
    public static Class649 aClass649_8409;
    public static Class649 aClass649_8410;
    public static Class649 aClass649_8411;
    public static Class649 aClass649_8412;
    public static Class649 aClass649_8413;
    public static Class649 aClass649_8414;
    public static Class649 aClass649_8415;
    int anInt8416;
    static Class649 aClass649_8417;
    static Class649 aClass649_8418;
    public static Class649 aClass649_8419;
    public static Class649 aClass649_8420;
    static Class649 aClass649_8421;
    static Class649 aClass649_8422;
    static Class649 aClass649_8423;
    public static Class649 aClass649_8424;
    static Class649 aClass649_8425;
    static Class649 aClass649_8426;
    static Class649 aClass649_8427;
    static Class649 aClass649_8428;
    static Class649 aClass649_8429;
    static Class649 aClass649_8430;
    static Class649 aClass649_8431;
    public static Class649 aClass649_8432;
    public static Class649 aClass649_8433;
    public static Class649 aClass649_8434;
    public static Class649 aClass649_8435;
    static Class649 aClass649_8436;
    public static Class649 aClass649_8437;
    public static Class649 aClass649_8438;
    public static Class649 aClass649_8439;
    public static Class649 aClass649_8440;
    public static Class649 aClass649_8441;
    static Class649 aClass649_8442;
    public static Class649 aClass649_8443;
    static Class649 aClass649_8444;
    static Class649 aClass649_8445;
    static Class649 aClass649_8446;
    static Class649 aClass649_8447;
    public static Class649 aClass649_8448;
    static Class649 aClass649_8449;
    static Class649 aClass649_8450;
    static Class649 aClass649_8451;
    static Class649 aClass649_8452;
    static Class649 aClass649_8453;
    public static Class649 aClass649_8454;
    static Class649 aClass649_8455;
    static Class649 aClass649_8456;
    static Class649 aClass649_8457;
    
    static void method10705(boolean bool, byte i) {
	Class114.aString1396 = Class114.aString1396.trim();
	Class114.anInt1388 = 0;
	if (Class114.aString1396.length() == 0)
	    Class114.anInt1389 = 0;
	else {
	    Class73.method1567(new StringBuilder().append("--> ").append
				   (Class114.aString1396).toString(),
			       -1333406846);
	    Class504.method8327(Class114.aString1396, false, bool, 2041963403);
	    if (!bool) {
		Class114.anInt1389 = 0;
		Class114.aString1396 = "";
	    } else
		Class114.anInt1389 = Class114.aString1396.length() * 399107939;
	}
    }
    
    public int method10706(int i) {
	return i >>> 144413887 * anInt8416;
    }
    
    public int method10707(byte i) {
	return 1 << anInt8416 * 144413887;
    }
    
    Class649(int i, int i_0_) {
	anInt8385 = -1625654503 * i;
	anInt8416 = i_0_ * 1632230207;
    }
    
    public int method10708(int i, int i_1_) {
	return i & (1 << 144413887 * anInt8416) - 1;
    }
    
    Class649(int i) {
	this(i, 0);
    }
    
    public int method10709() {
	return 1 << anInt8416 * 144413887;
    }
    
    public int method10710() {
	return 1 << anInt8416 * 144413887;
    }
    
    public int method10711(int i, int i_2_) {
	return i >>> 144413887 * anInt8416;
    }
    
    public int method10712(int i) {
	return i & (1 << 144413887 * anInt8416) - 1;
    }
    
    public int method10713(int i) {
	return i & (1 << 144413887 * anInt8416) - 1;
    }
    
    static void method10714(byte[] is, int i) {
	Class534_Sub40 class534_sub40 = new Class534_Sub40(is);
	for (;;) {
	    int i_3_ = class534_sub40.method16527(348273396);
	    if (i_3_ == 0)
		break;
	    if (1 == i_3_) {
		int i_4_ = class534_sub40.method16529((byte) 1);
		Class171_Sub4.aClass232_9944.method4240(i_4_, (byte) -25);
	    }
	}
    }
    
    static {
	aClass649_8380 = new Class649(2);
	aClass649_8420 = new Class649(3);
	aClass649_8406 = new Class649(4);
	aClass649_8438 = new Class649(5);
	aClass649_8384 = new Class649(6, 8);
	aClass649_8436 = new Class649(7);
	aClass649_8386 = new Class649(8, 8);
	aClass649_8387 = new Class649(9, 7);
	aClass649_8388 = new Class649(10, 8);
	aClass649_8389 = new Class649(11);
	aClass649_8415 = new Class649(12, 7);
	aClass649_8391 = new Class649(13, 8);
	aClass649_8392 = new Class649(14, 10);
	aClass649_8381 = new Class649(15);
	aClass649_8394 = new Class649(16);
	aClass649_8395 = new Class649(17);
	aClass649_8383 = new Class649(18);
	aClass649_8397 = new Class649(19);
	aClass649_8456 = new Class649(20);
	aClass649_8417 = new Class649(21);
	aClass649_8457 = new Class649(22);
	aClass649_8401 = new Class649(23);
	aClass649_8402 = new Class649(24);
	aClass649_8418 = new Class649(25);
	aClass649_8404 = new Class649(26, 5);
	aClass649_8405 = new Class649(27);
	aClass649_8452 = new Class649(28);
	aClass649_8407 = new Class649(29);
	aClass649_8403 = new Class649(30);
	aClass649_8409 = new Class649(31);
	aClass649_8410 = new Class649(32);
	aClass649_8411 = new Class649(33);
	aClass649_8412 = new Class649(34);
	aClass649_8413 = new Class649(35);
	aClass649_8414 = new Class649(36);
	aClass649_8446 = new Class649(37);
	aClass649_8399 = new Class649(38);
	aClass649_8400 = new Class649(39);
	aClass649_8396 = new Class649(40);
	aClass649_8419 = new Class649(41);
	aClass649_8451 = new Class649(42);
	aClass649_8421 = new Class649(43);
	aClass649_8422 = new Class649(44);
	aClass649_8423 = new Class649(45);
	aClass649_8424 = new Class649(46);
	aClass649_8425 = new Class649(47);
	aClass649_8390 = new Class649(48);
	aClass649_8427 = new Class649(49);
	aClass649_8428 = new Class649(50);
	aClass649_8429 = new Class649(51);
	aClass649_8430 = new Class649(53);
	aClass649_8431 = new Class649(54);
	aClass649_8432 = new Class649(60);
	aClass649_8433 = new Class649(61);
	aClass649_8393 = new Class649(62);
	aClass649_8435 = new Class649(63);
	aClass649_8440 = new Class649(64);
	aClass649_8437 = new Class649(65);
	aClass649_8434 = new Class649(66);
	aClass649_8439 = new Class649(67);
	aClass649_8408 = new Class649(68);
	aClass649_8441 = new Class649(69);
	aClass649_8442 = new Class649(70);
	aClass649_8443 = new Class649(72);
	aClass649_8444 = new Class649(73);
	aClass649_8445 = new Class649(74);
	aClass649_8382 = new Class649(75);
	aClass649_8447 = new Class649(76);
	aClass649_8448 = new Class649(77);
	aClass649_8449 = new Class649(78);
	aClass649_8450 = new Class649(79);
	aClass649_8454 = new Class649(80);
	aClass649_8426 = new Class649(81);
	aClass649_8453 = new Class649(82);
	aClass649_8379 = new Class649(83);
	aClass649_8455 = new Class649(84);
    }
    
    static final void method10715(Class669 class669, int i) {
	int i_5_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub5_10737
		  .method14026(i_5_, -2024064741);
    }
    
    static final void method10716(Class669 class669, int i) {
	int i_6_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.aClass214_11359.method4044(i_6_, 1481307617)
		  .method3964(1659085688);
    }
    
    static boolean method10717(int i) {
	Class680.anInt8668 = 268768563;
	Class65.aClass100_658 = client.aClass100_11094;
	return Class593.method9900((Class65.aLong663 * 5952060205682133295L
				    == -1L),
				   true, "", "",
				   5952060205682133295L * Class65.aLong663);
    }
}
