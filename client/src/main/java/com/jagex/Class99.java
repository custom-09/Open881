/* Class99 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class99
{
    int anInt1167;
    static Class203 aClass203_1168 = new Class203(4);
    public int anInt1169;
    int anInt1170;
    public int anInt1171;
    public int anInt1172;
    int anInt1173;
    public int anInt1174 = 864989343;
    public int anInt1175;
    public int anInt1176;
    int anInt1177;
    public static int anInt1178;
    
    public static Class183 method1839(Class185 class185, int i, int i_0_,
				      int i_1_, int i_2_, int i_3_) {
	long l = (long) i_3_;
	Class183 class183 = (Class183) aClass203_1168.method3871(l);
	int i_4_ = 2055;
	if (class183 == null) {
	    Class187 class187
		= Class187.method3709(Class130.aClass472_1524, i_3_, 0);
	    if (null == class187)
		return null;
	    if (class187.anInt2082 < 13)
		class187.method3723(2);
	    class183 = class185.method3329(class187, i_4_,
					   2007436411 * Class551.anInt7308, 64,
					   768);
	    aClass203_1168.method3893(class183, l);
	}
	class183 = class183.method3011((byte) 6, i_4_, true);
	if (0 != i)
	    class183.method3015(i);
	if (i_0_ != 0)
	    class183.method3017(i_0_);
	if (0 != i_1_)
	    class183.method3018(i_1_);
	if (i_2_ != 0)
	    class183.method3098(0, i_2_, 0);
	return class183;
    }
    
    public static Class183 method1840(Class185 class185, int i, int i_5_,
				      int i_6_, int i_7_, int i_8_) {
	long l = (long) i_8_;
	Class183 class183 = (Class183) aClass203_1168.method3871(l);
	int i_9_ = 2055;
	if (class183 == null) {
	    Class187 class187
		= Class187.method3709(Class130.aClass472_1524, i_8_, 0);
	    if (null == class187)
		return null;
	    if (class187.anInt2082 < 13)
		class187.method3723(2);
	    class183 = class185.method3329(class187, i_9_,
					   2007436411 * Class551.anInt7308, 64,
					   768);
	    aClass203_1168.method3893(class183, l);
	}
	class183 = class183.method3011((byte) 6, i_9_, true);
	if (0 != i)
	    class183.method3015(i);
	if (i_5_ != 0)
	    class183.method3017(i_5_);
	if (0 != i_6_)
	    class183.method3018(i_6_);
	if (i_7_ != 0)
	    class183.method3098(0, i_7_, 0);
	return class183;
    }
    
    static void method1841(int i) {
	aClass203_1168.method3876(i, (byte) 0);
    }
    
    public static Class183 method1842(Class185 class185, int i, int i_10_,
				      int i_11_, int i_12_, int i_13_) {
	long l = (long) i_13_;
	Class183 class183 = (Class183) aClass203_1168.method3871(l);
	int i_14_ = 2055;
	if (class183 == null) {
	    Class187 class187
		= Class187.method3709(Class130.aClass472_1524, i_13_, 0);
	    if (null == class187)
		return null;
	    if (class187.anInt2082 < 13)
		class187.method3723(2);
	    class183 = class185.method3329(class187, i_14_,
					   2007436411 * Class551.anInt7308, 64,
					   768);
	    aClass203_1168.method3893(class183, l);
	}
	class183 = class183.method3011((byte) 6, i_14_, true);
	if (0 != i)
	    class183.method3015(i);
	if (i_10_ != 0)
	    class183.method3017(i_10_);
	if (0 != i_11_)
	    class183.method3018(i_11_);
	if (i_12_ != 0)
	    class183.method3098(0, i_12_, 0);
	return class183;
    }
    
    static void method1843(int i) {
	Class551.anInt7308 = i * 928182963;
	aClass203_1168.method3877(111534788);
    }
    
    static void method1844(int i) {
	Class551.anInt7308 = i * 928182963;
	aClass203_1168.method3877(-2053378329);
    }
    
    public static Class183 method1845(Class185 class185, int i, int i_15_,
				      int i_16_, int i_17_, int i_18_) {
	long l = (long) i_18_;
	Class183 class183 = (Class183) aClass203_1168.method3871(l);
	int i_19_ = 2055;
	if (class183 == null) {
	    Class187 class187
		= Class187.method3709(Class130.aClass472_1524, i_18_, 0);
	    if (null == class187)
		return null;
	    if (class187.anInt2082 < 13)
		class187.method3723(2);
	    class183 = class185.method3329(class187, i_19_,
					   2007436411 * Class551.anInt7308, 64,
					   768);
	    aClass203_1168.method3893(class183, l);
	}
	class183 = class183.method3011((byte) 6, i_19_, true);
	if (0 != i)
	    class183.method3015(i);
	if (i_15_ != 0)
	    class183.method3017(i_15_);
	if (0 != i_16_)
	    class183.method3018(i_16_);
	if (i_17_ != 0)
	    class183.method3098(0, i_17_, 0);
	return class183;
    }
    
    public static Class183 method1846(Class185 class185, int i, int i_20_,
				      int i_21_, int i_22_, int i_23_) {
	long l = (long) i_23_;
	Class183 class183 = (Class183) aClass203_1168.method3871(l);
	int i_24_ = 2055;
	if (class183 == null) {
	    Class187 class187
		= Class187.method3709(Class130.aClass472_1524, i_23_, 0);
	    if (null == class187)
		return null;
	    if (class187.anInt2082 < 13)
		class187.method3723(2);
	    class183 = class185.method3329(class187, i_24_,
					   2007436411 * Class551.anInt7308, 64,
					   768);
	    aClass203_1168.method3893(class183, l);
	}
	class183 = class183.method3011((byte) 6, i_24_, true);
	if (0 != i)
	    class183.method3015(i);
	if (i_20_ != 0)
	    class183.method3017(i_20_);
	if (0 != i_21_)
	    class183.method3018(i_21_);
	if (i_22_ != 0)
	    class183.method3098(0, i_22_, 0);
	return class183;
    }
    
    static void method1847(int i) {
	Class551.anInt7308 = i * 928182963;
	aClass203_1168.method3877(-1779366855);
    }
    
    Class99() {
	/* empty */
    }
    
    static void method1848(int i) {
	Class551.anInt7308 = i * 928182963;
	aClass203_1168.method3877(-1112599343);
    }
    
    static void method1849(int i) {
	Class551.anInt7308 = i * 928182963;
	aClass203_1168.method3877(-435335229);
    }
    
    static void method1850() {
	aClass203_1168.method3877(-1491145957);
    }
    
    public static Class183 method1851(Class185 class185, int i, int i_25_,
				      int i_26_, int i_27_, int i_28_) {
	long l = (long) i_28_;
	Class183 class183 = (Class183) aClass203_1168.method3871(l);
	int i_29_ = 2055;
	if (class183 == null) {
	    Class187 class187
		= Class187.method3709(Class130.aClass472_1524, i_28_, 0);
	    if (null == class187)
		return null;
	    if (class187.anInt2082 < 13)
		class187.method3723(2);
	    class183 = class185.method3329(class187, i_29_,
					   2007436411 * Class551.anInt7308, 64,
					   768);
	    aClass203_1168.method3893(class183, l);
	}
	class183 = class183.method3011((byte) 6, i_29_, true);
	if (0 != i)
	    class183.method3015(i);
	if (i_25_ != 0)
	    class183.method3017(i_25_);
	if (0 != i_26_)
	    class183.method3018(i_26_);
	if (i_27_ != 0)
	    class183.method3098(0, i_27_, 0);
	return class183;
    }
    
    static void method1852() {
	aClass203_1168.method3877(-169221409);
    }
    
    static void method1853(int i) {
	aClass203_1168.method3876(i, (byte) 0);
    }
    
    static void method1854() {
	aClass203_1168.method3884((byte) -45);
    }
    
    static void method1855() {
	aClass203_1168.method3884((byte) 1);
    }
    
    static final void method1856(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub24_10756
		  .method17094(-1741609204);
    }
    
    public static void method1857(byte i) {
	Class574.anInt7703 = -202745631;
    }
    
    static final void method1858(Class669 class669, int i) {
	String string;
	if (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419 != null
	    && (null
		!= Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.aString12228))
	    string = Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			 .method18877(true, 1937320173);
	else
	    string = "";
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = string;
    }
    
    static final Object[] method1859(String string, Class669 class669, int i) {
	Object[] objects = new Object[string.length() + 1];
	for (int i_30_ = objects.length - 1; i_30_ >= 1; i_30_--) {
	    if (string.charAt(i_30_ - 1) == 's')
		objects[i_30_]
		    = (class669.anObjectArray8593
		       [(class669.anInt8594 -= 1460193483) * 1485266147]);
	    else if (string.charAt(i_30_ - 1) == 'l')
		objects[i_30_]
		    = new Long(class669.aLongArray8587[((class669.anInt8596
							 -= 1091885681)
							* 1572578961)]);
	    else
		objects[i_30_]
		    = new Integer(class669.anIntArray8595[((class669.anInt8600
							    -= 308999563)
							   * 2088438307)]);
	}
	int i_31_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	if (-1 != i_31_)
	    objects[0] = new Integer(i_31_);
	else
	    objects = null;
	return objects;
    }
}
