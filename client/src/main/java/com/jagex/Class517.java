/* Class517 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class517
{
    int anInt7045;
    int anInt7046;
    int anInt7047;
    int anInt7048;
    String aString7049;
    
    int method8616(byte i) {
	return anInt7045 * -1926637249;
    }
    
    String method8617() {
	return aString7049;
    }
    
    int method8618(int i) {
	return anInt7047 * -1525456743;
    }
    
    int method8619(int i) {
	return -179368965 * anInt7048;
    }
    
    String method8620() {
	return aString7049;
    }
    
    String method8621(int i) {
	return aString7049;
    }
    
    int method8622() {
	return anInt7046 * -2020985623;
    }
    
    int method8623() {
	return -179368965 * anInt7048;
    }
    
    int method8624() {
	return anInt7046 * -2020985623;
    }
    
    int method8625() {
	return anInt7045 * -1926637249;
    }
    
    String method8626() {
	return aString7049;
    }
    
    int method8627() {
	return -179368965 * anInt7048;
    }
    
    int method8628() {
	return -179368965 * anInt7048;
    }
    
    int method8629() {
	return -179368965 * anInt7048;
    }
    
    int method8630() {
	return anInt7046 * -2020985623;
    }
    
    public Class517(int i, int i_0_, int i_1_, int i_2_, String string) {
	anInt7046 = 841081689 * i;
	anInt7047 = -333122647 * i_0_;
	anInt7045 = -595290433 * i_1_;
	anInt7048 = i_2_ * -1922414797;
	aString7049 = string;
    }
    
    String method8631() {
	return aString7049;
    }
    
    int method8632(int i) {
	return anInt7046 * -2020985623;
    }
    
    static final void method8633(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class669.aClass352_8602.aByte3623;
    }
    
    static final void method8634(Class669 class669, byte i) {
	if (client.aBool11017)
	    Class403.aClass403_4148.method6601(421734097);
    }
    
    static final void method8635(Class247 class247, Class243 class243,
				 Class669 class669, byte i) {
	class247.aStringArray2487 = null;
    }
    
    static void method8636(Class534_Sub18_Sub7 class534_sub18_sub7, byte i) {
	if (class534_sub18_sub7 != null) {
	    Class72.aClass700_771.method14131(class534_sub18_sub7,
					      (short) 789);
	    Class72.anInt765 += 914117357;
	    Object object = null;
	    Class534_Sub18_Sub11 class534_sub18_sub11;
	    if (!class534_sub18_sub7.aBool11707
		&& !"".equals(class534_sub18_sub7.aString11708)) {
		long l
		    = -6387465159951953483L * class534_sub18_sub7.aLong11709;
		for (class534_sub18_sub11 = ((Class534_Sub18_Sub11)
					     Class72.aClass9_768.method579(l));
		     (class534_sub18_sub11 != null
		      && !class534_sub18_sub11.aString11793
			      .equals(class534_sub18_sub7.aString11708));
		     class534_sub18_sub11
			 = ((Class534_Sub18_Sub11)
			    Class72.aClass9_768.method582(683676784))) {
		    /* empty */
		}
		if (null == class534_sub18_sub11) {
		    class534_sub18_sub11
			= ((Class534_Sub18_Sub11)
			   Class72.aClass203_794.method3871(l));
		    if (class534_sub18_sub11 != null
			&& !class534_sub18_sub11.aString11793
				.equals(class534_sub18_sub7.aString11708))
			class534_sub18_sub11 = null;
		    if (null == class534_sub18_sub11)
			class534_sub18_sub11
			    = new Class534_Sub18_Sub11(class534_sub18_sub7
						       .aString11708);
		    Class72.aClass9_768.method581(class534_sub18_sub11, l);
		    Class72.anInt789 += -210574503;
		}
	    } else {
		class534_sub18_sub11
		    = new Class534_Sub18_Sub11(class534_sub18_sub7
					       .aString11708);
		Class72.anInt789 += -210574503;
	    }
	    if (class534_sub18_sub11.method18343(class534_sub18_sub7,
						 2005653935))
		Class526.method8761(class534_sub18_sub11, -26193475);
	}
    }
    
    public static Class675 method8637(int i, int i_3_) {
	Class675[] class675s = Class536_Sub5.method16006((byte) -44);
	for (int i_4_ = 0; i_4_ < class675s.length; i_4_++) {
	    Class675 class675 = class675s[i_4_];
	    if (-1082924039 * class675.anInt8642 == i)
		return class675;
	}
	return null;
    }
}
