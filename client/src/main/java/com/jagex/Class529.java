/* Class529 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class529
{
    public int anInt7120;
    public Class711 aClass711_7121;
    public int anInt7122;
    public int anInt7123 = -496843307;
    public int anInt7124;
    
    Class529(Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1) {
	aClass711_7121 = new Class711_Sub3(class654_sub1_sub5_sub1, false);
    }
    
    static final void method8821(Class669 class669, short i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class255.method4653(class247, class243, class669, (byte) -31);
    }
    
    static final void method8822(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class65.anInt701 * 1245290027;
    }
}
