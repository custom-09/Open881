/* Class536_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class536_Sub1 extends Class536
{
    public int anInt10329;
    public int anInt10330;
    public int anInt10331;
    public int anInt10332;
    
    void method15931(int i, int i_0_, int i_1_, int i_2_, int i_3_) {
	anInt10330 = i * -1298926391;
	anInt10329 = i_0_ * -2109868659;
	anInt10331 = -1172909789 * i_1_;
	anInt10332 = i_2_ * -1343905849;
    }
    
    void method15932(int i, int i_4_, int i_5_, int i_6_) {
	anInt10330 = i * -1298926391;
	anInt10329 = i_4_ * -2109868659;
	anInt10331 = -1172909789 * i_5_;
	anInt10332 = i_6_ * -1343905849;
    }
    
    Class536_Sub1(int i, int i_7_, int i_8_, int i_9_) {
	anInt10330 = -1298926391 * i;
	anInt10329 = i_7_ * -2109868659;
	anInt10331 = -1172909789 * i_8_;
	anInt10332 = -1343905849 * i_9_;
    }
    
    void method15933(int i, int i_10_, int i_11_, int i_12_) {
	anInt10330 = i * -1298926391;
	anInt10329 = i_10_ * -2109868659;
	anInt10331 = -1172909789 * i_11_;
	anInt10332 = i_12_ * -1343905849;
    }
    
    public static void method15934(int i, int i_13_, int i_14_, boolean bool,
				   int i_15_) {
	Class534_Sub18_Sub9 class534_sub18_sub9
	    = Class554_Sub1.aClass534_Sub18_Sub9_7354;
	Class554_Sub1.method9118(i);
	Class554_Sub1.aBool7355 = false;
	if (Class554_Sub1.aClass534_Sub18_Sub9_7354 != class534_sub18_sub9)
	    Class176.method2929(-2024655746);
	Class554_Sub1.anInt10668 = i_13_ * 2119321151;
	Class554_Sub1.anInt10680 = i_14_ * 177934039;
	Class554_Sub1.aBool10677 = bool;
    }
    
    static final void method15935(Class669 class669, byte i) {
	int i_16_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763
		  .method14026(i_16_, -2024064741);
    }
    
    static final void method15936(Class669 class669, int i)
	throws Exception_Sub2 {
	Class599.aClass298_Sub1_7871.method5365(((float) (class669
							  .anIntArray8595
							  [((class669.anInt8600
							     -= 308999563)
							    * 2088438307)])
						 / 1000.0F),
						31645619);
    }
}
