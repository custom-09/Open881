/* Class527 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.io.IOException;

public class Class527
{
    short[] aShortArray7095;
    Class316[] aClass316Array7096;
    int anInt7097;
    Class316 aClass316_7098;
    Class522 this$0;
    Class316[] aClass316Array7099;
    
    void method8764(int i) {
	for (/**/; -481611531 * anInt7097 < i; anInt7097 += 949222237) {
	    aClass316Array7096[-481611531 * anInt7097] = new Class316(3);
	    aClass316Array7099[anInt7097 * -481611531] = new Class316(3);
	}
    }
    
    void method8765(int i, short i_0_) {
	for (/**/; -481611531 * anInt7097 < i; anInt7097 += 949222237) {
	    aClass316Array7096[-481611531 * anInt7097] = new Class316(3);
	    aClass316Array7099[anInt7097 * -481611531] = new Class316(3);
	}
    }
    
    void method8766() {
	Class329.method5834(aShortArray7095, -205540606);
	for (int i = 0; i < anInt7097 * -481611531; i++) {
	    aClass316Array7096[i].method5708(-1360777581);
	    aClass316Array7099[i].method5708(-1200507559);
	}
	aClass316_7098.method5708(-1328347941);
    }
    
    Class527(Class522 class522) {
	this$0 = class522;
	aShortArray7095 = new short[2];
	aClass316Array7096 = new Class316[16];
	aClass316Array7099 = new Class316[16];
	aClass316_7098 = new Class316(8);
	anInt7097 = 0;
    }
    
    void method8767(int i) {
	for (/**/; -481611531 * anInt7097 < i; anInt7097 += 949222237) {
	    aClass316Array7096[-481611531 * anInt7097] = new Class316(3);
	    aClass316Array7099[anInt7097 * -481611531] = new Class316(3);
	}
    }
    
    int method8768(Class317 class317, int i) throws IOException {
	if (class317.method5740(aShortArray7095, 0, -792227360) == 0)
	    return aClass316Array7096[i].method5707(class317, 1225454738);
	int i_1_ = 8;
	if (class317.method5740(aShortArray7095, 1, 1093949812) == 0)
	    i_1_ += aClass316Array7099[i].method5707(class317, 1120040163);
	else
	    i_1_ += 8 + aClass316_7098.method5707(class317, 43005521);
	return i_1_;
    }
    
    void method8769(int i) {
	for (/**/; -481611531 * anInt7097 < i; anInt7097 += 949222237) {
	    aClass316Array7096[-481611531 * anInt7097] = new Class316(3);
	    aClass316Array7099[anInt7097 * -481611531] = new Class316(3);
	}
    }
    
    void method8770() {
	Class329.method5834(aShortArray7095, -357644498);
	for (int i = 0; i < anInt7097 * -481611531; i++) {
	    aClass316Array7096[i].method5708(-1636997247);
	    aClass316Array7099[i].method5708(-800716753);
	}
	aClass316_7098.method5708(-1814136712);
    }
    
    void method8771() {
	Class329.method5834(aShortArray7095, 20325057);
	for (int i = 0; i < anInt7097 * -481611531; i++) {
	    aClass316Array7096[i].method5708(-1677018029);
	    aClass316Array7099[i].method5708(-2143374007);
	}
	aClass316_7098.method5708(-1382507613);
    }
    
    void method8772(short i) {
	Class329.method5834(aShortArray7095, 32047496);
	for (int i_2_ = 0; i_2_ < anInt7097 * -481611531; i_2_++) {
	    aClass316Array7096[i_2_].method5708(-672049414);
	    aClass316Array7099[i_2_].method5708(-1632920215);
	}
	aClass316_7098.method5708(-1362077789);
    }
    
    void method8773() {
	Class329.method5834(aShortArray7095, 1809899977);
	for (int i = 0; i < anInt7097 * -481611531; i++) {
	    aClass316Array7096[i].method5708(-1595139122);
	    aClass316Array7099[i].method5708(-1879462210);
	}
	aClass316_7098.method5708(-654814208);
    }
    
    int method8774(Class317 class317, int i) throws IOException {
	if (class317.method5740(aShortArray7095, 0, -1082566840) == 0)
	    return aClass316Array7096[i].method5707(class317, 1904123452);
	int i_3_ = 8;
	if (class317.method5740(aShortArray7095, 1, -329682578) == 0)
	    i_3_ += aClass316Array7099[i].method5707(class317, 963704353);
	else
	    i_3_ += 8 + aClass316_7098.method5707(class317, 1598397006);
	return i_3_;
    }
    
    int method8775(Class317 class317, int i, byte i_4_) throws IOException {
	if (class317.method5740(aShortArray7095, 0, 1965062431) == 0)
	    return aClass316Array7096[i].method5707(class317, 526429854);
	int i_5_ = 8;
	if (class317.method5740(aShortArray7095, 1, 98639921) == 0)
	    i_5_ += aClass316Array7099[i].method5707(class317, 2074251499);
	else
	    i_5_ += 8 + aClass316_7098.method5707(class317, 978122552);
	return i_5_;
    }
    
    static final void method8776(Class669 class669, byte i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class474.method7763(class247, class669, 1876776206);
    }
    
    static final void method8777(Class669 class669, int i) {
	Class679.method13860(16711935);
    }
    
    public static void method8778(int i, boolean bool, int i_6_) {
	Class277.method5164(i,
			    Class58.aClass58_460.method1245((Class539
							     .aClass672_7171),
							    (byte) -77),
			    bool, (byte) 1);
    }
    
    public static final void method8779(String string, int i) {
	if (null != string) {
	    String string_7_
		= Class465.method7570(string, Class39.aClass76_307,
				      (byte) -26);
	    if (null != string_7_) {
		for (int i_8_ = 0; i_8_ < 2103713403 * client.anInt11329;
		     i_8_++) {
		    Class33 class33 = client.aClass33Array11299[i_8_];
		    String string_9_ = class33.aString273;
		    String string_10_
			= Class465.method7570(string_9_, Class39.aClass76_307,
					      (byte) -9);
		    if (Class470.method7644(string, string_7_, string_9_,
					    string_10_, 1906696648)) {
			client.anInt11329 -= 1662324915;
			for (int i_11_ = i_8_;
			     i_11_ < 2103713403 * client.anInt11329; i_11_++)
			    client.aClass33Array11299[i_11_]
				= client.aClass33Array11299[i_11_ + 1];
			client.anInt11261 = client.anInt11095 * -664271315;
			Class100 class100 = Class201.method3864(2095398292);
			Class534_Sub19 class534_sub19
			    = Class346.method6128(Class404.aClass404_4156,
						  class100.aClass13_1183,
						  1341391005);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			    (Class668.method11029(string, (byte) 0),
			     571515702);
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16713(string, -164238235);
			class100.method1863(class534_sub19, (byte) 72);
			break;
		    }
		}
	    }
	}
    }
    
    static void method8780(int i) {
	if (Class114.anInt1390 * 431834035 < 102)
	    Class114.anInt1390 += -2067868446;
	if (-1804400865 * Class114.anInt1387 != -1
	    && (7283764449686237077L * Class303.aLong3252
		< Class250.method4604((byte) -8))) {
	    for (int i_12_ = Class114.anInt1387 * -1804400865;
		 i_12_ < Class114.aStringArray1382.length; i_12_++) {
		if (Class114.aStringArray1382[i_12_].startsWith("pause")) {
		    int i_13_ = 5;
		    try {
			i_13_ = Integer.parseInt(Class114.aStringArray1382
						     [i_12_].substring(6));
		    } catch (Exception exception) {
			/* empty */
		    }
		    Class73.method1567(new StringBuilder().append
					   ("Pausing for ").append
					   (i_13_).append
					   (" seconds...").toString(),
				       -1989081922);
		    Class114.anInt1387 = -556419873 * (1 + i_12_);
		    Class303.aLong3252
			= (Class250.method4604((byte) -56)
			   + (long) (i_13_ * 1000)) * 6271713210732061629L;
		    return;
		}
		Class114.aString1396 = Class114.aStringArray1382[i_12_];
		Class649.method10705(false, (byte) 67);
	    }
	    Class114.anInt1387 = 556419873;
	}
	if (-1756158727 * client.anInt11193 != 0) {
	    Class114.anInt1393 -= client.anInt11193 * 643697769;
	    if (-596800939 * Class114.anInt1393
		>= Class114.anInt1385 * -1716872169)
		Class114.anInt1393
		    = -1262726981 * Class114.anInt1385 - 719528701;
	    if (Class114.anInt1393 * -596800939 < 0)
		Class114.anInt1393 = 0;
	    client.anInt11193 = 0;
	}
	for (int i_14_ = 0; i_14_ < 1779655715 * client.anInt11066; i_14_++) {
	    Interface63 interface63 = client.anInterface63Array11067[i_14_];
	    int i_15_ = interface63.method431((byte) -96);
	    char c = interface63.method424((byte) 1);
	    int i_16_ = interface63.method426((byte) 16);
	    if (84 == i_15_)
		Class649.method10705(false, (byte) 123);
	    if (80 == i_15_)
		Class649.method10705(true, (byte) 37);
	    else if (66 == i_15_ && (i_16_ & 0x4) != 0) {
		if (null != Class118.aClipboard1421) {
		    String string = "";
		    for (int i_17_
			     = Class200_Sub11.aStringArray9930.length - 1;
			 i_17_ >= 0; i_17_--) {
			if (Class200_Sub11.aStringArray9930[i_17_] != null
			    && (Class200_Sub11.aStringArray9930[i_17_].length()
				> 0))
			    string
				= new StringBuilder().append(string).append
				      (Class200_Sub11.aStringArray9930[i_17_])
				      .append
				      ('\n').toString();
		    }
		    Class118.aClipboard1421
			.setContents(new StringSelection(string), null);
		}
	    } else if (i_15_ == 67 && 0 != (i_16_ & 0x4)) {
		if (null != Class118.aClipboard1421) {
		    try {
			Transferable transferable
			    = Class118.aClipboard1421.getContents(null);
			if (transferable != null) {
			    String string
				= (String) (transferable.getTransferData
					    (DataFlavor.stringFlavor));
			    if (string != null) {
				String[] strings
				    = Class387.method6504(string, '\n',
							  277884299);
				Class634.method10532(strings, (byte) -88);
			    }
			}
		    } catch (Exception exception) {
			/* empty */
		    }
		}
	    } else if (85 == i_15_ && -1360080309 * Class114.anInt1389 > 0) {
		Class114.aString1396
		    = new StringBuilder().append
			  (Class114.aString1396.substring(0, ((-1360080309
							       * (Class114
								  .anInt1389))
							      - 1)))
			  .append
			  (Class114.aString1396
			       .substring(-1360080309 * Class114.anInt1389))
			  .toString();
		Class114.anInt1389 -= 399107939;
	    } else if (101 == i_15_ && (Class114.anInt1389 * -1360080309
					< Class114.aString1396.length()))
		Class114.aString1396
		    = new StringBuilder().append
			  (Class114.aString1396
			       .substring(0, Class114.anInt1389 * -1360080309))
			  .append
			  (Class114.aString1396.substring((Class114.anInt1389
							   * -1360080309) + 1))
			  .toString();
	    else if (i_15_ == 96 && -1360080309 * Class114.anInt1389 > 0)
		Class114.anInt1389 -= 399107939;
	    else if (97 == i_15_ && (-1360080309 * Class114.anInt1389
				     < Class114.aString1396.length()))
		Class114.anInt1389 += 399107939;
	    else if (102 == i_15_)
		Class114.anInt1389 = 0;
	    else if (103 == i_15_)
		Class114.anInt1389 = Class114.aString1396.length() * 399107939;
	    else if (104 == i_15_
		     && (2030085227 * Class114.anInt1388
			 < Class200_Sub11.aStringArray9930.length)) {
		Class114.anInt1388 += 1417472067;
		Class247.method4594(-577935767);
		Class114.anInt1389 = Class114.aString1396.length() * 399107939;
	    } else if (i_15_ == 105 && 2030085227 * Class114.anInt1388 > 0) {
		Class114.anInt1388 -= 1417472067;
		Class247.method4594(-1667019524);
		Class114.anInt1389 = Class114.aString1396.length() * 399107939;
	    } else if (Class604.method10031(c, 1462130253)
		       || "\\/.:, _-+[]~@".indexOf(c) != -1) {
		Class114.aString1396
		    = new StringBuilder().append
			  (Class114.aString1396
			       .substring(0, Class114.anInt1389 * -1360080309))
			  .append
			  (client.anInterface63Array11067[i_14_]
			       .method424((byte) 1))
			  .append
			  (Class114.aString1396
			       .substring(Class114.anInt1389 * -1360080309))
			  .toString();
		Class114.anInt1389 += 399107939;
	    }
	}
	client.anInt11066 = 0;
	client.anInt11199 = 0;
	Class422.method6785((byte) -50);
    }
}
