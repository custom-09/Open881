/* Class101 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class101
{
    float aFloat1200;
    int[] anIntArray1201;
    static final float aFloat1202 = 255.0F;
    boolean aBool1203;
    float aFloat1204 = 0.85F;
    float aFloat1205 = 1.0F - aFloat1204;
    int anInt1206 = 0;
    int anInt1207 = 0;
    Runnable aRunnable1208;
    int anInt1209;
    int anInt1210;
    boolean aBool1211;
    Class433 aClass433_1212;
    Class446 aClass446_1213;
    int[] anIntArray1214;
    Class446 aClass446_1215;
    int[] anIntArray1216;
    Class433 aClass433_1217;
    int[] anIntArray1218;
    float aFloat1219;
    float[] aFloatArray1220;
    float[] aFloatArray1221;
    float[] aFloatArray1222;
    float aFloat1223;
    Class433 aClass433_1224;
    float[] aFloatArray1225;
    float[] aFloatArray1226;
    int[] anIntArray1227;
    int[] anIntArray1228;
    Class185_Sub2 aClass185_Sub2_1229;
    float aFloat1230;
    float aFloat1231;
    float aFloat1232;
    Class119 aClass119_1233;
    float aFloat1234;
    float[] aFloatArray1235;
    int anInt1236;
    float[] aFloatArray1237;
    Class183_Sub2[] aClass183_Sub2Array1238;
    Class183_Sub2[] aClass183_Sub2Array1239;
    int[] anIntArray1240;
    float[] aFloatArray1241;
    float[] aFloatArray1242;
    float[] aFloatArray1243;
    float[] aFloatArray1244;
    boolean aBool1245 = false;
    
    void method1891(Runnable runnable, int i) {
	aRunnable1208 = runnable;
    }
    
    void method1892() {
	aClass119_1233 = new Class119(aClass185_Sub2_1229, this);
    }
    
    void method1893(int i) {
	aClass119_1233 = new Class119(aClass185_Sub2_1229, this);
    }
    
    void method1894() {
	aClass119_1233 = new Class119(aClass185_Sub2_1229, this);
    }
    
    void method1895() {
	aClass119_1233 = new Class119(aClass185_Sub2_1229, this);
    }
    
    Class101(Class185_Sub2 class185_sub2) {
	anInt1209 = 0;
	anInt1210 = 0;
	aBool1211 = true;
	aClass433_1212 = new Class433();
	aClass446_1213 = new Class446();
	aClass446_1215 = new Class446();
	aClass433_1224 = new Class433();
	aClass433_1217 = new Class433();
	anIntArray1214 = new int[Class183_Sub2.anInt9436];
	aFloatArray1235 = new float[Class183_Sub2.anInt9436];
	aFloatArray1220 = new float[Class183_Sub2.anInt9436];
	aFloatArray1221 = new float[Class183_Sub2.anInt9436];
	aFloatArray1222 = new float[Class183_Sub2.anInt9436];
	anIntArray1240 = new int[8];
	anIntArray1216 = new int[8];
	anIntArray1218 = new int[8];
	anIntArray1201 = new int[10000];
	anIntArray1227 = new int[10000];
	aFloat1223 = 1.0F;
	aFloat1234 = 0.0F;
	aFloat1219 = 1.0F;
	aFloatArray1237 = new float[2];
	aClass183_Sub2Array1238 = new Class183_Sub2[8];
	aClass183_Sub2Array1239 = new Class183_Sub2[8];
	aFloatArray1226 = new float[64];
	aFloatArray1241 = new float[64];
	aFloatArray1242 = new float[64];
	aFloatArray1243 = new float[64];
	aFloatArray1244 = new float[64];
	aFloatArray1225 = new float[3];
	aClass185_Sub2_1229 = class185_sub2;
	aClass119_1233 = new Class119(class185_sub2, this);
	for (int i = 0; i < 8; i++) {
	    aClass183_Sub2Array1238[i]
		= new Class183_Sub2(aClass185_Sub2_1229);
	    aClass183_Sub2Array1239[i]
		= new Class183_Sub2(aClass185_Sub2_1229);
	}
	anIntArray1228 = new int[Class183_Sub2.anInt9434];
	for (int i = 0; i < Class183_Sub2.anInt9434; i++)
	    anIntArray1228[i] = -1;
    }
    
    void method1896() {
	aClass119_1233 = new Class119(aClass185_Sub2_1229, this);
    }
    
    void method1897(Runnable runnable) {
	aRunnable1208 = runnable;
    }
    
    void method1898(Runnable runnable) {
	aRunnable1208 = runnable;
    }
    
    static final void method1899(int i) {
	Class459.method7443(Class254.aClass185_2683, 211999447);
	if (Class674.anInt8633 * -878424575 != client.anInt11176 * -1292249295)
	    Class453_Sub5.method16008((byte) -16);
    }
    
    static final void method1900
	(Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1, int i) {
	Class711_Sub1 class711_sub1
	    = class654_sub1_sub5_sub1.aClass711_Sub1_11965;
	if (class711_sub1.method14338((byte) 0)
	    && class711_sub1.method14393(1, (byte) 1)
	    && class711_sub1.method14343((byte) 1)) {
	    if (class711_sub1.aBool10971) {
		class711_sub1.method14386(class654_sub1_sub5_sub1.method18531
					      ((byte) -58)
					      .method9600(308999563),
					  false, true, 1337930696);
		class711_sub1.aBool10971 = class711_sub1.method14338((byte) 0);
	    }
	    class711_sub1.method14374(-1304088467);
	}
	for (int i_0_ = 0;
	     i_0_ < class654_sub1_sub5_sub1.aClass529Array11949.length;
	     i_0_++) {
	    if ((class654_sub1_sub5_sub1.aClass529Array11949[i_0_].anInt7123
		 * -1183861629)
		!= -1) {
		Class711 class711
		    = (class654_sub1_sub5_sub1.aClass529Array11949[i_0_]
		       .aClass711_7121);
		if (class711.method14336(1512169824)) {
		    Class684 class684
			= ((Class684)
			   (Class55.aClass44_Sub4_447.method91
			    ((class654_sub1_sub5_sub1.aClass529Array11949[i_0_]
			      .anInt7123) * -1183861629,
			     -861982147)));
		    Class205 class205 = class711.method14382(-1293681482);
		    if (class684.aBool8691) {
			if (716389381 * class205.anInt2221 == 3) {
			    if ((class654_sub1_sub5_sub1.anInt11937 * 86173187
				 > 0)
				&& (class654_sub1_sub5_sub1.anInt11956
				    * -1626386689) <= client.anInt11101
				&& (class654_sub1_sub5_sub1.anInt11957
				    * -1259938159) < client.anInt11101) {
				class711.method14330(-1, 1075693766);
				class654_sub1_sub5_sub1.aClass529Array11949
				    [i_0_].anInt7123
				    = -496843307;
				continue;
			    }
			} else if (1 == 716389381 * class205.anInt2221
				   && 86173187 * (class654_sub1_sub5_sub1
						  .anInt11937) > 0
				   && (class654_sub1_sub5_sub1.anInt11956
				       * -1626386689) <= client.anInt11101
				   && (class654_sub1_sub5_sub1.anInt11957
				       * -1259938159) < client.anInt11101)
			    continue;
		    }
		}
		if (class711.method14393(1, (byte) 1)
		    && class711.method14343((byte) 1)) {
		    class711.method14330(-1, 1905631765);
		    class654_sub1_sub5_sub1.aClass529Array11949[i_0_].anInt7123
			= -496843307;
		}
	    }
	}
	Class711 class711 = class654_sub1_sub5_sub1.aClass711_11948;
	do {
	    if (class711.method14338((byte) 0)) {
		Class205 class205 = class711.method14382(-934933615);
		if (3 == 716389381 * class205.anInt2221) {
		    if (class654_sub1_sub5_sub1.anInt11937 * 86173187 > 0
			&& (class654_sub1_sub5_sub1.anInt11956 * -1626386689
			    <= client.anInt11101)
			&& (class654_sub1_sub5_sub1.anInt11957 * -1259938159
			    < client.anInt11101)) {
			class654_sub1_sub5_sub1.anIntArray11946 = null;
			class711.method14330(-1, 1159215912);
			break;
		    }
		} else if (716389381 * class205.anInt2221 == 1) {
		    if (86173187 * class654_sub1_sub5_sub1.anInt11937 > 0
			&& (-1626386689 * class654_sub1_sub5_sub1.anInt11956
			    <= client.anInt11101)
			&& (class654_sub1_sub5_sub1.anInt11957 * -1259938159
			    < client.anInt11101)) {
			class711.method14333(1, (byte) 26);
			break;
		    }
		    class711.method14333(0, (byte) 21);
		}
		if (class711.method14393(1, (byte) 1)
		    && class711.method14343((byte) 1)) {
		    class654_sub1_sub5_sub1.anIntArray11946 = null;
		    class711.method14330(-1, 1772145651);
		}
	    }
	} while (false);
	for (int i_1_ = 0;
	     (i_1_
	      < class654_sub1_sub5_sub1.aClass711_Sub3_Sub1Array11968.length);
	     i_1_++) {
	    Class711_Sub3_Sub1 class711_sub3_sub1
		= class654_sub1_sub5_sub1.aClass711_Sub3_Sub1Array11968[i_1_];
	    if (class711_sub3_sub1 != null) {
		if (-628514349 * class711_sub3_sub1.anInt11918 > 0)
		    class711_sub3_sub1.anInt11918 -= -1464312229;
		else if (class711_sub3_sub1.method14393(1, (byte) 1)
			 && class711_sub3_sub1.method14343((byte) 1))
		    class654_sub1_sub5_sub1.aClass711_Sub3_Sub1Array11968[i_1_]
			= null;
	    }
	}
    }
    
    static void method1901(Class534_Sub18_Sub7 class534_sub18_sub7, int i,
			   int i_2_, boolean bool, int i_3_) {
	if (null != class534_sub18_sub7
	    && class534_sub18_sub7 != Class72.aClass700_771.aClass534_8803) {
	    int i_4_ = class534_sub18_sub7.anInt11703 * 980750511;
	    int i_5_ = -1180225371 * class534_sub18_sub7.anInt11704;
	    int i_6_ = class534_sub18_sub7.anInt11706 * -1986934021;
	    int i_7_ = (int) (-7225575275964615095L
			      * class534_sub18_sub7.aLong11702);
	    long l = -7225575275964615095L * class534_sub18_sub7.aLong11702;
	    if (i_6_ >= 2000)
		i_6_ -= 2000;
	    Class597 class597 = client.aClass512_11100.method8416((byte) 46);
	    if (i_6_ == 15) {
		Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2
		    = client.aClass654_Sub1_Sub5_Sub1_Sub2Array11279[i_7_];
		if (null != class654_sub1_sub5_sub1_sub2) {
		    client.anInt11115 = -1077600287 * i;
		    client.anInt11169 = 1219616153 * i_2_;
		    client.anInt11265 = 1639785950;
		    client.anInt11188 = 0;
		    Class534_Sub19 class534_sub19
			= Class346.method6128(Class404.aClass404_4229,
					      (client.aClass100_11264
					       .aClass13_1183),
					      1341391005);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16687
			(-823724811 * client.anInt11170, 2106009576);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			(Class479.method7916(-877455222) ? 1 : 0, 1327769381);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16579
			(-1808298539 * Class130.anInt1525, 63191484);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16507
			(-985352023 * client.anInt11219, 1644045333);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16687(i_7_, 1999256116);
		    client.aClass100_11264.method1863(class534_sub19,
						      (byte) 68);
		    Class84.method1669((class654_sub1_sub5_sub1_sub2
					.anIntArray11977[0]),
				       (class654_sub1_sub5_sub1_sub2
					.anIntArray11978[0]),
				       -787566118);
		}
	    }
	    if (i_6_ == 25) {
		Class247 class247 = Class81.method1637(i_5_, i_4_, 1521806600);
		if (class247 != null) {
		    Exception_Sub3.method17943((byte) 1);
		    Class534_Sub25 class534_sub25
			= client.method17392(class247);
		    Class208.method3945(class247,
					class534_sub25.method16271((byte) 123),
					(class534_sub25.anInt10575
					 * -1799641075),
					927529620);
		    client.aString11221
			= Class598.method9939(class247, -770958165);
		    if (client.aString11221 == null)
			client.aString11221 = "Null";
		    client.aString11222
			= new StringBuilder().append(class247.aString2551)
			      .append
			      (Class154.method2575(16777215, 1229875408))
			      .toString();
		}
	    } else {
		if (59 == i_6_) {
		    client.anInt11115 = i * -1077600287;
		    client.anInt11169 = 1219616153 * i_2_;
		    client.anInt11265 = -1327590673;
		    client.anInt11188 = 0;
		    Class534_Sub19 class534_sub19
			= Class346.method6128(Class404.aClass404_4157,
					      (client.aClass100_11264
					       .aClass13_1183),
					      1341391005);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16579
			(-1808298539 * Class130.anInt1525, -558872762);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16569
			(client.anInt11170 * -823724811, -220996549);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16569
			(class597.anInt7861 * -1166289421 + i_5_, -119709629);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16687
			(class597.anInt7859 * -424199969 + i_4_, 2065425175);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16568
			(-985352023 * client.anInt11219, -2019266541);
		    client.aClass100_11264.method1863(class534_sub19,
						      (byte) 125);
		    Class84.method1669(i_4_, i_5_, -787566118);
		}
		if (i_6_ == 17) {
		    client.anInt11115 = -1077600287 * i;
		    client.anInt11169 = 1219616153 * i_2_;
		    client.anInt11265 = 1639785950;
		    client.anInt11188 = 0;
		    Class534_Sub19 class534_sub19
			= Class346.method6128(Class404.aClass404_4262,
					      (client.aClass100_11264
					       .aClass13_1183),
					      1341391005);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16510
			(-1808298539 * Class130.anInt1525, -1758892120);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16687
			(client.anInt11170 * -823724811, 2008754261);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16560
			(Class479.method7916(-877455222) ? 1 : 0, -2031086477);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16569(i_7_, -584849516);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16569
			(i_5_ + class597.anInt7861 * -1166289421, 526127219);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16568
			(i_4_ + class597.anInt7859 * -424199969, 1190731982);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16569
			(client.anInt11219 * -985352023, -1984280197);
		    client.aClass100_11264.method1863(class534_sub19,
						      (byte) 67);
		    Class84.method1669(i_4_, i_5_, -787566118);
		}
		if (60 == i_6_) {
		    if (365872613 * client.anInt11194 > 0
			&& Class453_Sub5.method16010(2134513972))
			Class534_Sub18_Sub2.method17849
			    ((Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			      .aByte10854),
			     class597.anInt7859 * -424199969 + i_4_,
			     -1166289421 * class597.anInt7861 + i_5_,
			     -673402239);
		    else {
			client.anInt11115 = -1077600287 * i;
			client.anInt11169 = i_2_ * 1219616153;
			client.anInt11265 = -1327590673;
			client.anInt11188 = 0;
			Class534_Sub19 class534_sub19
			    = Class346.method6128(Class404.aClass404_4179,
						  (client.aClass100_11264
						   .aClass13_1183),
						  1341391005);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16687
			    (-424199969 * class597.anInt7859 + i_4_,
			     1973634277);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16507
			    (-1166289421 * class597.anInt7861 + i_5_,
			     1950708692);
			client.aClass100_11264.method1863(class534_sub19,
							  (byte) 124);
		    }
		}
		if (i_6_ == 30 && client.aClass247_11119 == null) {
		    Class536.method8908(i_5_, i_4_, -1517946915);
		    client.aClass247_11119
			= Class81.method1637(i_5_, i_4_, 820821149);
		    if (client.aClass247_11119 != null)
			Class454.method7416(client.aClass247_11119,
					    -1648178677);
		}
		if (i_6_ == 1008 || i_6_ == 1009 || i_6_ == 1010
		    || 1011 == i_6_ || 1012 == i_6_)
		    Class536_Sub4.method15993(i_6_, i_7_, i_4_, (byte) 50);
		if (i_6_ == 23) {
		    if (client.anInt11194 * 365872613 > 0
			&& Class453_Sub5.method16010(2098706754))
			Class534_Sub18_Sub2.method17849
			    ((Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			      .aByte10854),
			     i_4_ + -424199969 * class597.anInt7859,
			     i_5_ + class597.anInt7861 * -1166289421,
			     -771503026);
		    else {
			Class534_Sub19 class534_sub19
			    = Class711.method14415(i_4_, i_5_, i_7_,
						   1294038924);
			if (i_7_ == 1) {
			    class534_sub19.aClass534_Sub40_Sub1_10513
				.method16506(-1, 814880219);
			    class534_sub19.aClass534_Sub40_Sub1_10513
				.method16506(-1, 1969957723);
			    class534_sub19.aClass534_Sub40_Sub1_10513
				.method16507
				((int) client.aFloat11140, 1208242449);
			    class534_sub19.aClass534_Sub40_Sub1_10513
				.method16506(57, 637818948);
			    class534_sub19.aClass534_Sub40_Sub1_10513
				.method16506
				(client.anInt11189 * -2045337339, 1536663480);
			    class534_sub19.aClass534_Sub40_Sub1_10513
				.method16506
				(-2002513841 * client.anInt11022, 649604275);
			    class534_sub19.aClass534_Sub40_Sub1_10513
				.method16506(89, 1263449796);
			    Class438 class438
				= (Class322
				       .aClass654_Sub1_Sub5_Sub1_Sub2_3419
				       .method10807
				   ().aClass438_4885);
			    class534_sub19.aClass534_Sub40_Sub1_10513
				.method16507
				((int) class438.aFloat4864, 822901485);
			    class534_sub19.aClass534_Sub40_Sub1_10513
				.method16507
				((int) class438.aFloat4865, 794243902);
			    class534_sub19.aClass534_Sub40_Sub1_10513
				.method16506(63, 373415499);
			} else {
			    client.anInt11115 = -1077600287 * i;
			    client.anInt11169 = 1219616153 * i_2_;
			    client.anInt11265 = -1327590673;
			    client.anInt11188 = 0;
			}
			client.aClass100_11264.method1863(class534_sub19,
							  (byte) 126);
			Class84.method1669(i_4_, i_5_, -787566118);
		    }
		}
		Class404 class404 = null;
		if (9 == i_6_)
		    class404 = Class404.aClass404_4248;
		else if (i_6_ == 10)
		    class404 = Class404.aClass404_4266;
		else if (i_6_ == 11)
		    class404 = Class404.aClass404_4221;
		else if (12 == i_6_)
		    class404 = Class404.aClass404_4226;
		else if (13 == i_6_)
		    class404 = Class404.aClass404_4216;
		else if (i_6_ == 1003)
		    class404 = Class404.aClass404_4254;
		if (class404 != null) {
		    Class534_Sub6 class534_sub6
			= ((Class534_Sub6)
			   client.aClass9_11331.method579((long) i_7_));
		    if (null != class534_sub6) {
			Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1
			    = ((Class654_Sub1_Sub5_Sub1_Sub1)
			       class534_sub6.anObject10417);
			client.anInt11115 = -1077600287 * i;
			client.anInt11169 = 1219616153 * i_2_;
			client.anInt11265 = 1639785950;
			client.anInt11188 = 0;
			Class534_Sub19 class534_sub19
			    = Class346.method6128(class404,
						  (client.aClass100_11264
						   .aClass13_1183),
						  1341391005);
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16569(i_7_, -867127460);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16559
			    (Class479.method7916(-877455222) ? 1 : 0,
			     1257841122);
			client.aClass100_11264.method1863(class534_sub19,
							  (byte) 23);
			Class84.method1669((class654_sub1_sub5_sub1_sub1
					    .anIntArray11977[0]),
					   (class654_sub1_sub5_sub1_sub1
					    .anIntArray11978[0]),
					   -787566118);
		    }
		}
		if (i_6_ == 2) {
		    client.anInt11115 = i * -1077600287;
		    client.anInt11169 = i_2_ * 1219616153;
		    client.anInt11265 = 1639785950;
		    client.anInt11188 = 0;
		    Class534_Sub19 class534_sub19
			= Class346.method6128(Class404.aClass404_4174,
					      (client.aClass100_11264
					       .aClass13_1183),
					      1341391005);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16569
			(-985352023 * client.anInt11219, 887664504);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16617((int) (l >>> 32) & 0x7fffffff, 131307918);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16569
			(i_5_ + class597.anInt7861 * -1166289421, -892293782);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16687
			(-823724811 * client.anInt11170, 2097286959);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16559
			(Class479.method7916(-877455222) ? 1 : 0, 1374522746);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16579
			(Class130.anInt1525 * -1808298539, -943758698);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16569
			(i_4_ + -424199969 * class597.anInt7859, -1654888788);
		    client.aClass100_11264.method1863(class534_sub19,
						      (byte) 21);
		    Class84.method1669(i_4_, i_5_, -787566118);
		}
		Class404 class404_8_ = null;
		if (i_6_ == 44)
		    class404_8_ = Class404.aClass404_4198;
		else if (45 == i_6_)
		    class404_8_ = Class404.aClass404_4237;
		else if (i_6_ == 46)
		    class404_8_ = Class404.aClass404_4261;
		else if (i_6_ == 47)
		    class404_8_ = Class404.aClass404_4215;
		else if (i_6_ == 48)
		    class404_8_ = Class404.aClass404_4202;
		else if (49 == i_6_)
		    class404_8_ = Class404.aClass404_4196;
		else if (50 == i_6_)
		    class404_8_ = Class404.aClass404_4264;
		else if (i_6_ == 51)
		    class404_8_ = Class404.aClass404_4162;
		else if (i_6_ == 52)
		    class404_8_ = Class404.aClass404_4232;
		else if (53 == i_6_)
		    class404_8_ = Class404.aClass404_4189;
		if (class404_8_ != null) {
		    Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2
			= client.aClass654_Sub1_Sub5_Sub1_Sub2Array11279[i_7_];
		    if (null != class654_sub1_sub5_sub1_sub2) {
			client.anInt11115 = i * -1077600287;
			client.anInt11169 = 1219616153 * i_2_;
			client.anInt11265 = 1639785950;
			client.anInt11188 = 0;
			Class534_Sub19 class534_sub19
			    = Class346.method6128(class404_8_,
						  (client.aClass100_11264
						   .aClass13_1183),
						  1341391005);
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16687(i_7_, 2133496111);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16560
			    (Class479.method7916(-877455222) ? 1 : 0,
			     -1757947031);
			client.aClass100_11264.method1863(class534_sub19,
							  (byte) 70);
			Class84.method1669((class654_sub1_sub5_sub1_sub2
					    .anIntArray11977[0]),
					   (class654_sub1_sub5_sub1_sub2
					    .anIntArray11978[0]),
					   -787566118);
		    }
		}
		if (i_6_ == 57 || 1007 == i_6_)
		    Class273.method5102(i_7_, i_5_, i_4_,
					class534_sub18_sub7.aString11708,
					(byte) -33);
		if (16 == i_6_) {
		    client.anInt11115 = -1077600287 * i;
		    client.anInt11169 = 1219616153 * i_2_;
		    client.anInt11265 = 1639785950;
		    client.anInt11188 = 0;
		    Class534_Sub19 class534_sub19
			= Class346.method6128(Class404.aClass404_4229,
					      (client.aClass100_11264
					       .aClass13_1183),
					      1341391005);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16687
			(client.anInt11170 * -823724811, 1972507999);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			(Class479.method7916(-877455222) ? 1 : 0, 1523461869);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16579
			(-1808298539 * Class130.anInt1525, -188891994);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16507
			(-985352023 * client.anInt11219, 2056785489);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16687
			(1126388985 * (Class322
				       .aClass654_Sub1_Sub5_Sub1_Sub2_3419
				       .anInt11922),
			 2141928230);
		    client.aClass100_11264.method1863(class534_sub19,
						      (byte) 63);
		}
		if (8 == i_6_) {
		    Class534_Sub6 class534_sub6
			= ((Class534_Sub6)
			   client.aClass9_11331.method579((long) i_7_));
		    if (null != class534_sub6) {
			Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1
			    = ((Class654_Sub1_Sub5_Sub1_Sub1)
			       class534_sub6.anObject10417);
			client.anInt11115 = -1077600287 * i;
			client.anInt11169 = i_2_ * 1219616153;
			client.anInt11265 = 1639785950;
			client.anInt11188 = 0;
			Class534_Sub19 class534_sub19
			    = Class346.method6128(Class404.aClass404_4274,
						  (client.aClass100_11264
						   .aClass13_1183),
						  1341391005);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16507
			    (-823724811 * client.anInt11170, 507408924);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16568
			    (-985352023 * client.anInt11219, 1927822114);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16510
			    (Class130.anInt1525 * -1808298539, -1284920397);
			class534_sub19.aClass534_Sub40_Sub1_10513
			    .method16507(i_7_, 1662932807);
			class534_sub19.aClass534_Sub40_Sub1_10513.method16559
			    (Class479.method7916(-877455222) ? 1 : 0,
			     1463700680);
			client.aClass100_11264.method1863(class534_sub19,
							  (byte) 65);
			Class84.method1669((class654_sub1_sub5_sub1_sub1
					    .anIntArray11977[0]),
					   (class654_sub1_sub5_sub1_sub1
					    .anIntArray11978[0]),
					   -787566118);
		    }
		}
		if (i_6_ == 58) {
		    Class247 class247
			= Class81.method1637(i_5_, i_4_, 731049878);
		    if (null != class247)
			Class392_Sub1.method15837(class247, -1975461224);
		}
		Class404 class404_9_ = null;
		if (3 == i_6_)
		    class404_9_ = Class404.aClass404_4235;
		else if (4 == i_6_)
		    class404_9_ = Class404.aClass404_4244;
		else if (5 == i_6_)
		    class404_9_ = Class404.aClass404_4194;
		else if (i_6_ == 6)
		    class404_9_ = Class404.aClass404_4271;
		else if (i_6_ == 1001)
		    class404_9_ = Class404.aClass404_4181;
		else if (i_6_ == 1002)
		    class404_9_ = Class404.aClass404_4231;
		if (class404_9_ != null) {
		    client.anInt11115 = -1077600287 * i;
		    client.anInt11169 = i_2_ * 1219616153;
		    client.anInt11265 = 1639785950;
		    client.anInt11188 = 0;
		    Class534_Sub19 class534_sub19
			= Class346.method6128(class404_9_,
					      (client.aClass100_11264
					       .aClass13_1183),
					      1341391005);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16568
			(-1166289421 * class597.anInt7861 + i_5_, -1714352851);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16687
			(i_4_ + -424199969 * class597.anInt7859, 1942217951);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16510
			((int) (l >>> 32) & 0x7fffffff, -677770291);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16506
			(Class479.method7916(-877455222) ? 1 : 0, 886743135);
		    client.aClass100_11264.method1863(class534_sub19,
						      (byte) 58);
		    Class84.method1669(i_4_, i_5_, -787566118);
		}
		Class404 class404_10_ = null;
		if (18 == i_6_)
		    class404_10_ = Class404.aClass404_4193;
		else if (i_6_ == 19)
		    class404_10_ = Class404.aClass404_4160;
		else if (20 == i_6_)
		    class404_10_ = Class404.aClass404_4268;
		else if (21 == i_6_)
		    class404_10_ = Class404.aClass404_4184;
		else if (i_6_ == 22)
		    class404_10_ = Class404.aClass404_4233;
		else if (1004 == i_6_)
		    class404_10_ = Class404.aClass404_4185;
		if (class404_10_ != null) {
		    client.anInt11115 = i * -1077600287;
		    client.anInt11169 = i_2_ * 1219616153;
		    client.anInt11265 = 1639785950;
		    client.anInt11188 = 0;
		    Class534_Sub19 class534_sub19
			= Class346.method6128(class404_10_,
					      (client.aClass100_11264
					       .aClass13_1183),
					      1341391005);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16569
			(i_4_ + class597.anInt7859 * -424199969, 815928428);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16635
			((bool ? 2 : 0) | (Class479.method7916(-877455222) ? 1
					   : 0),
			 1925337788);
		    class534_sub19.aClass534_Sub40_Sub1_10513.method16569
			(i_5_ + -1166289421 * class597.anInt7861, -1810917205);
		    class534_sub19.aClass534_Sub40_Sub1_10513
			.method16687(i_7_, 2041872387);
		    client.aClass100_11264.method1863(class534_sub19,
						      (byte) 14);
		    Class84.method1669(i_4_, i_5_, -787566118);
		}
		if (client.aBool11218)
		    Exception_Sub3.method17943((byte) 1);
		if (null != Class236.aClass247_2372
		    && client.anInt11084 * 602951003 == 0)
		    Class454.method7416(Class236.aClass247_2372, -656206390);
	    }
	}
    }
    
    static final void method1902(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (-686202593 * Class200_Sub23.aClass534_Sub28_10040.anInt10635
	       < 512) || client.aBool11050 || client.aBool11201 ? 1 : 0;
    }
}
