/* Class318 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class318 implements Interface12
{
    Class453 aClass453_3390;
    Class341 aClass341_3391;
    public Class150 aClass150_3392;
    int anInt3393;
    int anInt3394;
    String aString3395;
    public int anInt3396;
    int anInt3397 = 1818307695;
    static int[] anIntArray3398 = new int[32];
    
    public void method78(Class534_Sub40 class534_sub40) {
	method5753(class534_sub40, false, -1116625500);
    }
    
    public void method79(Class534_Sub40 class534_sub40, byte i) {
	method5753(class534_sub40, false, 1650873871);
    }
    
    Class318(int i, Class341 class341) {
	anInt3396 = i * 383904583;
	aClass341_3391 = class341;
    }
    
    public void method82(int i) {
	/* empty */
    }
    
    void method5746(Class534_Sub40 class534_sub40, int i, boolean bool,
		    int i_0_) {
	Class328 class328
	    = (Class328) Class448.method7319(Class398.method6580((byte) -78),
					     i, 2088438307);
	switch (-1254923905 * class328.anInt3471) {
	case 13: {
	    int i_1_ = class534_sub40.method16527(-301897538);
	    aClass453_3390
		= ((Class453)
		   Class448.method7319(Class453.method7393(-1239749602), i_1_,
				       2088438307));
	    if (null == aClass453_3390)
		throw new IllegalStateException("");
	    anInt3397 = class534_sub40.method16550((byte) -54) * -1818307695;
	    if (null != aClass341_3391) {
		Interface14 interface14
		    = ((Interface14)
		       aClass341_3391.aMap3553.get(aClass453_3390));
		if (null != interface14)
		    aClass150_3392
			= ((Class150)
			   interface14.method91(anInt3397 * 2015513969,
						-308053420));
		else if (!bool)
		    throw new IllegalStateException("");
	    }
	    break;
	}
	case 11:
	    anInt3393 = class534_sub40.method16527(-1955870767) * -778147733;
	    anInt3394 = class534_sub40.method16527(431202467) * 1263787473;
	    break;
	case 4:
	    break;
	}
    }
    
    public int method5747(int i, int i_2_) throws Exception_Sub6 {
	int i_3_
	    = anIntArray3398[anInt3394 * 1133878065 - anInt3393 * 469435459];
	if (i_2_ < 0 || i_2_ > i_3_)
	    throw new Exception_Sub6((null != aString3395 ? aString3395
				      : Integer
					    .toString(anInt3396 * 206851703)),
				     i_2_, i_3_);
	i_3_ <<= anInt3393 * 469435459;
	return i & (i_3_ ^ 0xffffffff) | i_2_ << anInt3393 * 469435459 & i_3_;
    }
    
    public int method5748(int i, int i_4_, byte i_5_) throws Exception_Sub6 {
	int i_6_
	    = anIntArray3398[anInt3394 * 1133878065 - anInt3393 * 469435459];
	if (i_4_ < 0 || i_4_ > i_6_)
	    throw new Exception_Sub6((null != aString3395 ? aString3395
				      : Integer
					    .toString(anInt3396 * 206851703)),
				     i_4_, i_6_);
	i_6_ <<= anInt3393 * 469435459;
	return i & (i_6_ ^ 0xffffffff) | i_4_ << anInt3393 * 469435459 & i_6_;
    }
    
    void method5749(Class534_Sub40 class534_sub40, int i, boolean bool) {
	Class328 class328
	    = (Class328) Class448.method7319(Class398.method6580((byte) -30),
					     i, 2088438307);
	switch (-1254923905 * class328.anInt3471) {
	case 13: {
	    int i_7_ = class534_sub40.method16527(1787204323);
	    aClass453_3390
		= ((Class453)
		   Class448.method7319(Class453.method7393(-698018617), i_7_,
				       2088438307));
	    if (null == aClass453_3390)
		throw new IllegalStateException("");
	    anInt3397 = class534_sub40.method16550((byte) -27) * -1818307695;
	    if (null != aClass341_3391) {
		Interface14 interface14
		    = ((Interface14)
		       aClass341_3391.aMap3553.get(aClass453_3390));
		if (null != interface14)
		    aClass150_3392
			= ((Class150)
			   interface14.method91(anInt3397 * 2015513969,
						-94976438));
		else if (!bool)
		    throw new IllegalStateException("");
	    }
	    break;
	}
	case 11:
	    anInt3393 = class534_sub40.method16527(1446402621) * -778147733;
	    anInt3394 = class534_sub40.method16527(-267616695) * 1263787473;
	    break;
	case 4:
	    break;
	}
    }
    
    public void method80(Class534_Sub40 class534_sub40) {
	method5753(class534_sub40, false, 1465651563);
    }
    
    public int method5750(int i, int i_8_) {
	int i_9_
	    = anIntArray3398[1133878065 * anInt3394 - anInt3393 * 469435459];
	return i >> anInt3393 * 469435459 & i_9_;
    }
    
    public void method81(Class534_Sub40 class534_sub40) {
	method5753(class534_sub40, false, -888403816);
    }
    
    public void method83() {
	/* empty */
    }
    
    public void method84() {
	/* empty */
    }
    
    void method5751(Class534_Sub40 class534_sub40, int i, boolean bool) {
	Class328 class328
	    = (Class328) Class448.method7319(Class398.method6580((byte) -99),
					     i, 2088438307);
	switch (-1254923905 * class328.anInt3471) {
	case 13: {
	    int i_10_ = class534_sub40.method16527(-2077435991);
	    aClass453_3390
		= ((Class453)
		   Class448.method7319(Class453.method7393(-1283875897), i_10_,
				       2088438307));
	    if (null == aClass453_3390)
		throw new IllegalStateException("");
	    anInt3397 = class534_sub40.method16550((byte) -15) * -1818307695;
	    if (null != aClass341_3391) {
		Interface14 interface14
		    = ((Interface14)
		       aClass341_3391.aMap3553.get(aClass453_3390));
		if (null != interface14)
		    aClass150_3392
			= ((Class150)
			   interface14.method91(anInt3397 * 2015513969,
						-1975752747));
		else if (!bool)
		    throw new IllegalStateException("");
	    }
	    break;
	}
	case 11:
	    anInt3393 = class534_sub40.method16527(-553552829) * -778147733;
	    anInt3394 = class534_sub40.method16527(-646254123) * 1263787473;
	    break;
	case 4:
	    break;
	}
    }
    
    void method5752(Class534_Sub40 class534_sub40, int i, boolean bool) {
	Class328 class328
	    = (Class328) Class448.method7319(Class398.method6580((byte) -47),
					     i, 2088438307);
	switch (-1254923905 * class328.anInt3471) {
	case 13: {
	    int i_11_ = class534_sub40.method16527(-269508229);
	    aClass453_3390
		= ((Class453)
		   Class448.method7319(Class453.method7393(-752320735), i_11_,
				       2088438307));
	    if (null == aClass453_3390)
		throw new IllegalStateException("");
	    anInt3397 = class534_sub40.method16550((byte) -41) * -1818307695;
	    if (null != aClass341_3391) {
		Interface14 interface14
		    = ((Interface14)
		       aClass341_3391.aMap3553.get(aClass453_3390));
		if (null != interface14)
		    aClass150_3392
			= ((Class150)
			   interface14.method91(anInt3397 * 2015513969,
						-368278105));
		else if (!bool)
		    throw new IllegalStateException("");
	    }
	    break;
	}
	case 11:
	    anInt3393 = class534_sub40.method16527(-400514962) * -778147733;
	    anInt3394 = class534_sub40.method16527(-1894525897) * 1263787473;
	    break;
	case 4:
	    break;
	}
    }
    
    void method5753(Class534_Sub40 class534_sub40, boolean bool, int i) {
	for (;;) {
	    int i_12_ = class534_sub40.method16527(-920853962);
	    if (i_12_ == 0)
		break;
	    method5746(class534_sub40, i_12_, bool, -1539201441);
	}
    }
    
    public int method5754(int i) {
	int i_13_
	    = anIntArray3398[1133878065 * anInt3394 - anInt3393 * 469435459];
	return i >> anInt3393 * 469435459 & i_13_;
    }
    
    static {
	int i = 2;
	for (int i_14_ = 0; i_14_ < 32; i_14_++) {
	    anIntArray3398[i_14_] = i - 1;
	    i += i;
	}
    }
    
    public int method5755(int i) {
	int i_15_
	    = anIntArray3398[1133878065 * anInt3394 - anInt3393 * 469435459];
	return i >> anInt3393 * 469435459 & i_15_;
    }
    
    static final void method5756(Class669 class669, byte i) {
	int i_16_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.aClass214_11359.method4044(i_16_, 1481307617)
		  .method3961((short) 1369) ? 1 : 0;
    }
    
    static void method5757(Class669 class669, byte i) {
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = (((Class273)
		Class223.aClass53_Sub2_2316.method91((class669.anIntArray8595
						      [((2088438307
							 * class669.anInt8600)
							- 2)]),
						     -188927893))
	       .aStringArray3032
	       [class669.anIntArray8595[class669.anInt8600 * 2088438307 - 1]]);
	class669.anInt8600 -= 617999126;
    }
    
    public static int method5758(int i) {
	Class409.method6709(1768982251);
	return Class69.aClass106_726.anInt1307 * 756058023;
    }
}
