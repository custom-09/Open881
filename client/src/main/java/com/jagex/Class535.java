/* Class535 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class535 implements Interface19
{
    Class654_Sub1_Sub5_Sub1 aClass654_Sub1_Sub5_Sub1_7161;
    public static Class110_Sub1_Sub2 aClass110_Sub1_Sub2_7162;
    
    public long method133(Class150 class150) {
	return aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
		   .method26(class150.anInt1694 * -1270946121, 673320982);
    }
    
    public int method120(Class150 class150, byte i) {
	return aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
		   .method32(class150.anInt1694 * -1270946121, 1895668970);
    }
    
    public long method127(Class150 class150, byte i) {
	return aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
		   .method26(class150.anInt1694 * -1270946121, 673320982);
    }
    
    public Object method124(Class150 class150, int i) {
	return aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
		   .method45(class150.anInt1694 * -1270946121, -2116705787);
    }
    
    public void method140(Class318 class318, int i) throws Exception_Sub6 {
	method114(class318.aClass150_3392,
		  class318.method5748(method120(class318.aClass150_3392,
						(byte) -117),
				      i, (byte) 14),
		  1601718914);
    }
    
    public void method114(Class150 class150, int i, int i_0_) {
	aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
	    .method28(class150.anInt1694 * -1270946121, i, (byte) -59);
    }
    
    public void method118(Class150 class150, Object object, byte i) {
	aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987.method29
	    (class150.anInt1694 * -1270946121, object, (short) -10227);
    }
    
    public void method121(Class150 class150, int i) {
	aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
	    .method28(class150.anInt1694 * -1270946121, i, (byte) -106);
    }
    
    public void method122(Class318 class318, int i, byte i_1_)
	throws Exception_Sub6 {
	method114(class318.aClass150_3392,
		  class318.method5748(method120(class318.aClass150_3392,
						(byte) -82),
				      i, (byte) -94),
		  -1408666875);
    }
    
    public int method117(Class150 class150) {
	return aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
		   .method32(class150.anInt1694 * -1270946121, 1527077103);
    }
    
    public int method135(Class318 class318) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -87),
				   1394743353);
    }
    
    public int method123(Class318 class318) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -91),
				   2118128828);
    }
    
    public void method116(Class150 class150, long l) {
	aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
	    .method27(class150.anInt1694 * -1270946121, l);
    }
    
    public int method125(Class318 class318) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -99),
				   1569585863);
    }
    
    public int method126(Class318 class318) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -81),
				   1139446719);
    }
    
    public long method131(Class150 class150) {
	return aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
		   .method26(class150.anInt1694 * -1270946121, 673320982);
    }
    
    public void method128(Class150 class150, int i) {
	aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
	    .method28(class150.anInt1694 * -1270946121, i, (byte) -12);
    }
    
    public void method113(Class150 class150, long l) {
	aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
	    .method27(class150.anInt1694 * -1270946121, l);
    }
    
    public int method136(Class150 class150) {
	return aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
		   .method32(class150.anInt1694 * -1270946121, -1222659504);
    }
    
    public int method119(Class318 class318, int i) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -77),
				   1667295911);
    }
    
    public long method132(Class150 class150) {
	return aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
		   .method26(class150.anInt1694 * -1270946121, 673320982);
    }
    
    public void method115(Class150 class150, long l) {
	aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
	    .method27(class150.anInt1694 * -1270946121, l);
    }
    
    Class535(Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1) {
	aClass654_Sub1_Sub5_Sub1_7161 = class654_sub1_sub5_sub1;
    }
    
    public void method130(Class150 class150, long l) {
	aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
	    .method27(class150.anInt1694 * -1270946121, l);
    }
    
    public Object method134(Class150 class150) {
	return aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
		   .method45(class150.anInt1694 * -1270946121, -2063369632);
    }
    
    public Object method137(Class150 class150) {
	return aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
		   .method45(class150.anInt1694 * -1270946121, -1951796360);
    }
    
    public void method138(Class150 class150, Object object) {
	aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987.method29
	    (class150.anInt1694 * -1270946121, object, (short) -28489);
    }
    
    public void method139(Class318 class318, int i) throws Exception_Sub6 {
	method114(class318.aClass150_3392,
		  class318.method5748(method120(class318.aClass150_3392,
						(byte) -56),
				      i, (byte) -27),
		  886190898);
    }
    
    public long method129(Class150 class150) {
	return aClass654_Sub1_Sub5_Sub1_7161.anInterface4_11987
		   .method26(class150.anInt1694 * -1270946121, 673320982);
    }
    
    static boolean method8895(byte i) {
	if (-1850530127 * client.anInt11039 != 15)
	    return false;
	if (Class202.method3865((byte) 0) || Class295.method5325(-344719875))
	    return false;
	return true;
    }
    
    static void method8896(int i) {
	Class574.aBool7681 = false;
	Class574.anIntArray7698 = null;
	Class574.anIntArray7699 = null;
	Class574.aLinkedList7701.clear();
	Class574.aLinkedList7702.clear();
	Class254.aClass185_2683.method3280();
    }
    
    static final void method8897(Class669 class669, int i) {
	int i_2_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_2_, 2007113507);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class247.anInt2624 * -1513459333;
    }
    
    static final void method8898(Class669 class669, int i) {
	class669.anInt8600 -= 926998689;
	int i_3_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_4_
	    = class669.anIntArray8595[class669.anInt8600 * 2088438307 + 1];
	int i_5_
	    = class669.anIntArray8595[2088438307 * class669.anInt8600 + 2];
	int i_6_ = 31 - i_5_;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = i_3_ << i_6_ >>> i_6_ + i_4_;
    }
    
    static final void method8899(Class669 class669, int i) {
	class669.anInt8594 -= -1374580330;
	String string
	    = ((String)
	       class669.anObjectArray8593[1485266147 * class669.anInt8594]);
	String string_7_ = (String) (class669.anObjectArray8593
				     [1485266147 * class669.anInt8594 + 1]);
	if ((class669.anIntArray8595
	     [(class669.anInt8600 -= 308999563) * 2088438307])
	    == 1)
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= string;
	else
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= string_7_;
    }
}
