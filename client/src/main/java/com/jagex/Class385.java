/* Class385 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class385
{
    int anInt3968;
    public static final int anInt3969 = 1;
    public static final int anInt3970 = 2;
    public short aShort3971;
    public int anInt3972;
    public short aShort3973;
    public int anInt3974;
    public boolean aBool3975;
    public int anInt3976;
    public int anInt3977 = 0;
    int anInt3978;
    public int anInt3979 = -181262307;
    int anInt3980 = -323983324;
    public int anInt3981;
    public int anInt3982;
    static final int anInt3983 = 0;
    int anInt3984;
    int anInt3985;
    public boolean aBool3986;
    public boolean aBool3987;
    public int anInt3988;
    int anInt3989;
    int anInt3990;
    public int anInt3991;
    public int anInt3992;
    public int anInt3993;
    public int anInt3994;
    public int[] anIntArray3995;
    int anInt3996;
    public int anInt3997 = -1706942285;
    public int[] anIntArray3998;
    public short aShort3999;
    public static final int anInt4000 = -1;
    public int anInt4001;
    public int anInt4002;
    public int anInt4003;
    public boolean aBool4004;
    public int anInt4005;
    public int anInt4006;
    public int anInt4007;
    public short aShort4008;
    public boolean aBool4009;
    public int anInt4010;
    int anInt4011;
    int anInt4012;
    public int anInt4013;
    public boolean aBool4014;
    int anInt4015;
    public int[] anIntArray4016;
    public int anInt4017;
    public int[] anIntArray4018;
    public boolean aBool4019;
    public boolean aBool4020;
    int anInt4021;
    public int anInt4022;
    public int anInt4023;
    public int anInt4024;
    public int anInt4025;
    public int anInt4026;
    public int anInt4027;
    public int anInt4028;
    public int anInt4029;
    public int anInt4030;
    public int anInt4031;
    public int anInt4032;
    public boolean aBool4033;
    public int anInt4034;
    public int anInt4035;
    public int anInt4036;
    public int anInt4037;
    public int anInt4038;
    public boolean aBool4039;
    public int anInt4040;
    public int anInt4041;
    public int anInt4042;
    public int anInt4043;
    public int anInt4044;
    public int anInt4045;
    
    void method6479() {
	if (anInt4001 * 2042652149 > -2 || 1736785337 * anInt4002 > -2)
	    aBool4014 = true;
	anInt4022 = 1153904143 * (-1438701003 * anInt3985 >> 16 & 0xff);
	anInt3996 = (-1155336309 * anInt4021 >> 16 & 0xff) * -1463969015;
	anInt4024 = 111135051 * anInt3996 - anInt4022 * -186426323;
	anInt4029 = (-1438701003 * anInt3985 >> 8 & 0xff) * 726724793;
	anInt4011 = (anInt4021 * -1155336309 >> 8 & 0xff) * 1693756103;
	anInt4027 = anInt4011 * -1516100631 - anInt4029 * -503029865;
	anInt3974 = -1809171537 * (-1438701003 * anInt3985 & 0xff);
	anInt3978 = (anInt4021 * -1155336309 & 0xff) * -41740921;
	anInt4025 = -410548623 * anInt3978 - anInt3974 * -1613221479;
	anInt4031 = 23737121 * (-1438701003 * anInt3985 >> 24 & 0xff);
	anInt3968 = 403995401 * (anInt4021 * -1155336309 >> 24 & 0xff);
	anInt4030 = anInt3968 * -1461894887 - 562733505 * anInt4031;
	if (563541671 * anInt3988 != 0) {
	    anInt4032 = 2048723293 * (anInt3989 * -716860259
				      * (anInt4034 * 1843131803) / 100);
	    anInt4035 = (anInt3990 * -1530616341 * (1843131803 * anInt4034)
			 / 100 * 801489573);
	    if (anInt4032 * -1117023499 == 0)
		anInt4032 = 2048723293;
	    anInt4036
		= (((anInt3988 * 563541671 >> 16 & 0xff)
		    - (-1590427409 * anInt4022 + anInt4024 * -467462389 / 2))
		   << 8) / (anInt4032 * -1117023499) * -1436983421;
	    anInt4037
		= (((563541671 * anInt3988 >> 8 & 0xff)
		    - (2123356127 * anInt4027 / 2 + 1164973449 * anInt4029))
		   << 8) / (anInt4032 * -1117023499) * -1870729061;
	    anInt4038 = 431526401 * ((((563541671 * anInt3988 & 0xff)
				       - (anInt4025 * -1708133081 / 2
					  + 1719532367 * anInt3974))
				      << 8)
				     / (anInt4032 * -1117023499));
	    if (0 == 392975661 * anInt4035)
		anInt4035 = 801489573;
	    anInt4017
		= (((563541671 * anInt3988 >> 24 & 0xff)
		    - (anInt4030 * 1298659105 / 2 + anInt4031 * 915869921))
		   << 8) / (anInt4035 * 392975661) * -467927579;
	    Class385 class385_0_ = this;
	    class385_0_.anInt4036
		= (class385_0_.anInt4036
		   + -1436983421 * (anInt4036 * 969942827 > 0 ? -4 : 4));
	    Class385 class385_1_ = this;
	    class385_1_.anInt4037
		= (class385_1_.anInt4037
		   + (anInt4037 * -1220577901 > 0 ? -4 : 4) * -1870729061);
	    Class385 class385_2_ = this;
	    class385_2_.anInt4038
		= (class385_2_.anInt4038
		   + (1368092161 * anInt4038 > 0 ? -4 : 4) * 431526401);
	    Class385 class385_3_ = this;
	    class385_3_.anInt4017
		= (class385_3_.anInt4017
		   + (anInt4017 * -1647904787 > 0 ? -4 : 4) * -467927579);
	}
	if (-1 != anInt3979 * -2017057333) {
	    anInt4040 = 2141022519 * (-4228719 * anInt3980
				      * (1843131803 * anInt4034) / 100);
	    if (-417293177 * anInt4040 == 0)
		anInt4040 = 2141022519;
	    anInt3992 = -90279563 * ((-2017057333 * anInt3979
				      - ((anInt4005 * 770148019
					  - 579777191 * anInt3972) / 2
					 + 579777191 * anInt3972))
				     / (anInt4040 * -417293177));
	}
	if (921858437 * anInt3997 != -1) {
	    anInt4028 = (-2061968261 * anInt3984 * (anInt4034 * 1843131803)
			 / 100 * 1014192875);
	    if (0 == anInt4028 * -1191739965)
		anInt4028 = 1014192875;
	    anInt4043 = 7647549 * ((921858437 * anInt3997
				    - (anInt3981 * 1362111377
				       + (anInt3982 * -1082478985
					  - anInt3981 * 1362111377) / 2))
				   / (anInt4028 * -1191739965));
	}
	anInt4044 = anInt4012 * 26147823 - 1851772421 * anInt3976;
	anInt4045 = -957269875 * anInt4015 - anInt4026 * 488468283;
    }
    
    void method6480(Class534_Sub40 class534_sub40, byte i) {
	for (;;) {
	    int i_4_ = class534_sub40.method16527(-248479795);
	    if (0 == i_4_)
		break;
	    method6481(class534_sub40, i_4_, (byte) 16);
	}
    }
    
    void method6481(Class534_Sub40 class534_sub40, int i, byte i_5_) {
	if (i == 1) {
	    aShort3971 = (short) class534_sub40.method16529((byte) 1);
	    aShort4008 = (short) class534_sub40.method16529((byte) 1);
	    aShort3973 = (short) class534_sub40.method16529((byte) 1);
	    aShort3999 = (short) class534_sub40.method16529((byte) 1);
	    int i_6_ = 3;
	    aShort3971 <<= i_6_;
	    aShort4008 <<= i_6_;
	    aShort3973 <<= i_6_;
	    aShort3999 <<= i_6_;
	} else if (2 == i)
	    class534_sub40.method16527(2107228241);
	else if (i == 3) {
	    anInt3972 = class534_sub40.method16533(-258848859) * -816851689;
	    anInt4005 = class534_sub40.method16533(-258848859) * 1125685371;
	} else if (i == 4) {
	    anInt3977 = class534_sub40.method16527(1891784559) * 683558411;
	    anInt4007 = class534_sub40.method16586((byte) 1) * -2054930875;
	} else if (i == 5)
	    anInt3981 = (anInt3982 = (class534_sub40.method16529((byte) 1)
				      << 12 << 2) * 1048152903) * 988538247;
	else if (6 == i) {
	    anInt3985 = class534_sub40.method16533(-258848859) * -1715578851;
	    anInt4021 = class534_sub40.method16533(-258848859) * -529696733;
	} else if (i == 7) {
	    anInt4042 = class534_sub40.method16529((byte) 1) * 1747639811;
	    anInt4034 = class534_sub40.method16529((byte) 1) * -1997444973;
	} else if (i == 8) {
	    anInt3994 = class534_sub40.method16529((byte) 1) * -1303551657;
	    anInt4023 = class534_sub40.method16529((byte) 1) * 672339755;
	} else if (i == 9) {
	    int i_7_ = class534_sub40.method16527(-1705806308);
	    anIntArray3995 = new int[i_7_];
	    for (int i_8_ = 0; i_8_ < i_7_; i_8_++)
		anIntArray3995[i_8_] = class534_sub40.method16529((byte) 1);
	} else if (i == 10) {
	    int i_9_ = class534_sub40.method16527(858291305);
	    anIntArray3998 = new int[i_9_];
	    for (int i_10_ = 0; i_10_ < i_9_; i_10_++)
		anIntArray3998[i_10_] = class534_sub40.method16529((byte) 1);
	} else if (12 == i)
	    anInt4001 = class534_sub40.method16586((byte) 1) * 1260346973;
	else if (i == 13)
	    anInt4002 = class534_sub40.method16586((byte) 1) * 1115535497;
	else if (14 == i)
	    anInt4003 = class534_sub40.method16529((byte) 1) * 167340951;
	else if (i == 15)
	    anInt3991 = class534_sub40.method16529((byte) 1) * 527505809;
	else if (16 == i) {
	    aBool4004 = class534_sub40.method16527(-935906579) == 1;
	    anInt4006 = class534_sub40.method16529((byte) 1) * -2076072973;
	    anInt3993 = class534_sub40.method16529((byte) 1) * -1355956623;
	    aBool3975 = class534_sub40.method16527(2025690443) == 1;
	} else if (17 == i)
	    anInt4010 = class534_sub40.method16529((byte) 1) * 457525633;
	else if (18 == i)
	    anInt3988 = class534_sub40.method16533(-258848859) * -283946729;
	else if (i == 19)
	    anInt4041 = class534_sub40.method16527(-1960647200) * -870420359;
	else if (20 == i)
	    anInt3989 = class534_sub40.method16527(-1782722608) * -556428875;
	else if (21 == i)
	    anInt3990 = class534_sub40.method16527(-1298353788) * 375610051;
	else if (i == 22)
	    anInt3979 = class534_sub40.method16533(-258848859) * 181262307;
	else if (23 == i)
	    anInt3980 = class534_sub40.method16527(138741429) * -260937871;
	else if (24 == i)
	    aBool3987 = false;
	else if (25 == i) {
	    int i_11_ = class534_sub40.method16527(-1165324749);
	    anIntArray4016 = new int[i_11_];
	    for (int i_12_ = 0; i_12_ < i_11_; i_12_++)
		anIntArray4016[i_12_] = class534_sub40.method16529((byte) 1);
	} else if (26 == i)
	    aBool4009 = false;
	else if (i == 27)
	    anInt3997 = ((class534_sub40.method16529((byte) 1) << 12 << 2)
			 * 1706942285);
	else if (i == 28)
	    anInt3984 = class534_sub40.method16527(1844933759) * -1664572749;
	else if (i == 29) {
	    if (class534_sub40.method16527(1918878037) == 0)
		anInt4026
		    = ((anInt4015
			= class534_sub40.method16530((byte) -59) * 808818696)
		       * 1100581591);
	    else {
		anInt4026
		    = class534_sub40.method16530((byte) -113) * 1170144952;
		anInt4015
		    = class534_sub40.method16530((byte) -108) * 808818696;
	    }
	} else if (i == 30)
	    aBool4033 = true;
	else if (i == 31) {
	    anInt3981 = ((class534_sub40.method16529((byte) 1) << 12 << 2)
			 * -1680083599);
	    anInt3982 = ((class534_sub40.method16529((byte) 1) << 12 << 2)
			 * 1048152903);
	} else if (32 == i)
	    aBool3986 = false;
	else if (33 == i)
	    aBool4020 = true;
	else if (34 == i)
	    aBool4019 = false;
	else if (i == 35) {
	    if (class534_sub40.method16527(-632241328) == 0)
		anInt3976
		    = ((anInt4012
			= class534_sub40.method16530((byte) -77) * -1076897672)
		       * -289795997);
	    else {
		anInt3976 = class534_sub40.method16530((byte) -111) * 35141224;
		anInt4012
		    = class534_sub40.method16530((byte) -62) * -1076897672;
		anInt4013
		    = class534_sub40.method16527(-1473742404) * -991793813;
	    }
	} else if (i == 36)
	    aBool4039 = true;
    }
    
    Class385() {
	anInt3984 = 1046449644;
	aBool3987 = true;
	anInt3989 = 191687348;
	anInt3990 = -1093700564;
	anInt3991 = -527505809;
	anInt4001 = 1774273350;
	anInt4002 = 2063896302;
	anInt4003 = 0;
	aBool4004 = true;
	aBool3975 = true;
	anInt4006 = 2076072973;
	anInt3993 = 1355956623;
	anInt4041 = 0;
	aBool4009 = true;
	anInt4010 = -457525633;
	anInt3976 = 0;
	anInt4012 = 0;
	anInt4013 = 0;
	anInt4026 = 0;
	anInt4015 = 0;
	aBool4033 = false;
	aBool3986 = true;
	aBool4020 = false;
	aBool4019 = true;
	aBool4039 = false;
	aBool4014 = false;
    }
    
    void method6482(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(1350715836);
	    if (0 == i)
		break;
	    method6481(class534_sub40, i, (byte) 16);
	}
    }
    
    void method6483(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-518987539);
	    if (0 == i)
		break;
	    method6481(class534_sub40, i, (byte) 16);
	}
    }
    
    void method6484(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(2005288080);
	    if (0 == i)
		break;
	    method6481(class534_sub40, i, (byte) 16);
	}
    }
    
    void method6485() {
	if (anInt4001 * 2042652149 > -2 || 1736785337 * anInt4002 > -2)
	    aBool4014 = true;
	anInt4022 = 1153904143 * (-1438701003 * anInt3985 >> 16 & 0xff);
	anInt3996 = (-1155336309 * anInt4021 >> 16 & 0xff) * -1463969015;
	anInt4024 = 111135051 * anInt3996 - anInt4022 * -186426323;
	anInt4029 = (-1438701003 * anInt3985 >> 8 & 0xff) * 726724793;
	anInt4011 = (anInt4021 * -1155336309 >> 8 & 0xff) * 1693756103;
	anInt4027 = anInt4011 * -1516100631 - anInt4029 * -503029865;
	anInt3974 = -1809171537 * (-1438701003 * anInt3985 & 0xff);
	anInt3978 = (anInt4021 * -1155336309 & 0xff) * -41740921;
	anInt4025 = -410548623 * anInt3978 - anInt3974 * -1613221479;
	anInt4031 = 23737121 * (-1438701003 * anInt3985 >> 24 & 0xff);
	anInt3968 = 403995401 * (anInt4021 * -1155336309 >> 24 & 0xff);
	anInt4030 = anInt3968 * -1461894887 - 562733505 * anInt4031;
	if (563541671 * anInt3988 != 0) {
	    anInt4032 = 2048723293 * (anInt3989 * -716860259
				      * (anInt4034 * 1843131803) / 100);
	    anInt4035 = (anInt3990 * -1530616341 * (1843131803 * anInt4034)
			 / 100 * 801489573);
	    if (anInt4032 * -1117023499 == 0)
		anInt4032 = 2048723293;
	    anInt4036
		= (((anInt3988 * 563541671 >> 16 & 0xff)
		    - (-1590427409 * anInt4022 + anInt4024 * -467462389 / 2))
		   << 8) / (anInt4032 * -1117023499) * -1436983421;
	    anInt4037
		= (((563541671 * anInt3988 >> 8 & 0xff)
		    - (2123356127 * anInt4027 / 2 + 1164973449 * anInt4029))
		   << 8) / (anInt4032 * -1117023499) * -1870729061;
	    anInt4038 = 431526401 * ((((563541671 * anInt3988 & 0xff)
				       - (anInt4025 * -1708133081 / 2
					  + 1719532367 * anInt3974))
				      << 8)
				     / (anInt4032 * -1117023499));
	    if (0 == 392975661 * anInt4035)
		anInt4035 = 801489573;
	    anInt4017
		= (((563541671 * anInt3988 >> 24 & 0xff)
		    - (anInt4030 * 1298659105 / 2 + anInt4031 * 915869921))
		   << 8) / (anInt4035 * 392975661) * -467927579;
	    Class385 class385_13_ = this;
	    class385_13_.anInt4036
		= (class385_13_.anInt4036
		   + -1436983421 * (anInt4036 * 969942827 > 0 ? -4 : 4));
	    Class385 class385_14_ = this;
	    class385_14_.anInt4037
		= (class385_14_.anInt4037
		   + (anInt4037 * -1220577901 > 0 ? -4 : 4) * -1870729061);
	    Class385 class385_15_ = this;
	    class385_15_.anInt4038
		= (class385_15_.anInt4038
		   + (1368092161 * anInt4038 > 0 ? -4 : 4) * 431526401);
	    Class385 class385_16_ = this;
	    class385_16_.anInt4017
		= (class385_16_.anInt4017
		   + (anInt4017 * -1647904787 > 0 ? -4 : 4) * -467927579);
	}
	if (-1 != anInt3979 * -2017057333) {
	    anInt4040 = 2141022519 * (-4228719 * anInt3980
				      * (1843131803 * anInt4034) / 100);
	    if (-417293177 * anInt4040 == 0)
		anInt4040 = 2141022519;
	    anInt3992 = -90279563 * ((-2017057333 * anInt3979
				      - ((anInt4005 * 770148019
					  - 579777191 * anInt3972) / 2
					 + 579777191 * anInt3972))
				     / (anInt4040 * -417293177));
	}
	if (921858437 * anInt3997 != -1) {
	    anInt4028 = (-2061968261 * anInt3984 * (anInt4034 * 1843131803)
			 / 100 * 1014192875);
	    if (0 == anInt4028 * -1191739965)
		anInt4028 = 1014192875;
	    anInt4043 = 7647549 * ((921858437 * anInt3997
				    - (anInt3981 * 1362111377
				       + (anInt3982 * -1082478985
					  - anInt3981 * 1362111377) / 2))
				   / (anInt4028 * -1191739965));
	}
	anInt4044 = anInt4012 * 26147823 - 1851772421 * anInt3976;
	anInt4045 = -957269875 * anInt4015 - anInt4026 * 488468283;
    }
    
    void method6486(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(1369103782);
	    if (0 == i)
		break;
	    method6481(class534_sub40, i, (byte) 16);
	}
    }
    
    void method6487(Class534_Sub40 class534_sub40, int i) {
	if (i == 1) {
	    aShort3971 = (short) class534_sub40.method16529((byte) 1);
	    aShort4008 = (short) class534_sub40.method16529((byte) 1);
	    aShort3973 = (short) class534_sub40.method16529((byte) 1);
	    aShort3999 = (short) class534_sub40.method16529((byte) 1);
	    int i_17_ = 3;
	    aShort3971 <<= i_17_;
	    aShort4008 <<= i_17_;
	    aShort3973 <<= i_17_;
	    aShort3999 <<= i_17_;
	} else if (2 == i)
	    class534_sub40.method16527(-1794940373);
	else if (i == 3) {
	    anInt3972 = class534_sub40.method16533(-258848859) * -816851689;
	    anInt4005 = class534_sub40.method16533(-258848859) * 1125685371;
	} else if (i == 4) {
	    anInt3977 = class534_sub40.method16527(-290140235) * 683558411;
	    anInt4007 = class534_sub40.method16586((byte) 1) * -2054930875;
	} else if (i == 5)
	    anInt3981 = (anInt3982 = (class534_sub40.method16529((byte) 1)
				      << 12 << 2) * 1048152903) * 988538247;
	else if (6 == i) {
	    anInt3985 = class534_sub40.method16533(-258848859) * -1715578851;
	    anInt4021 = class534_sub40.method16533(-258848859) * -529696733;
	} else if (i == 7) {
	    anInt4042 = class534_sub40.method16529((byte) 1) * 1747639811;
	    anInt4034 = class534_sub40.method16529((byte) 1) * -1997444973;
	} else if (i == 8) {
	    anInt3994 = class534_sub40.method16529((byte) 1) * -1303551657;
	    anInt4023 = class534_sub40.method16529((byte) 1) * 672339755;
	} else if (i == 9) {
	    int i_18_ = class534_sub40.method16527(1398208156);
	    anIntArray3995 = new int[i_18_];
	    for (int i_19_ = 0; i_19_ < i_18_; i_19_++)
		anIntArray3995[i_19_] = class534_sub40.method16529((byte) 1);
	} else if (i == 10) {
	    int i_20_ = class534_sub40.method16527(-1465485532);
	    anIntArray3998 = new int[i_20_];
	    for (int i_21_ = 0; i_21_ < i_20_; i_21_++)
		anIntArray3998[i_21_] = class534_sub40.method16529((byte) 1);
	} else if (12 == i)
	    anInt4001 = class534_sub40.method16586((byte) 1) * 1260346973;
	else if (i == 13)
	    anInt4002 = class534_sub40.method16586((byte) 1) * 1115535497;
	else if (14 == i)
	    anInt4003 = class534_sub40.method16529((byte) 1) * 167340951;
	else if (i == 15)
	    anInt3991 = class534_sub40.method16529((byte) 1) * 527505809;
	else if (16 == i) {
	    aBool4004 = class534_sub40.method16527(-1250524099) == 1;
	    anInt4006 = class534_sub40.method16529((byte) 1) * -2076072973;
	    anInt3993 = class534_sub40.method16529((byte) 1) * -1355956623;
	    aBool3975 = class534_sub40.method16527(884285395) == 1;
	} else if (17 == i)
	    anInt4010 = class534_sub40.method16529((byte) 1) * 457525633;
	else if (18 == i)
	    anInt3988 = class534_sub40.method16533(-258848859) * -283946729;
	else if (i == 19)
	    anInt4041 = class534_sub40.method16527(78838991) * -870420359;
	else if (20 == i)
	    anInt3989 = class534_sub40.method16527(1176723442) * -556428875;
	else if (21 == i)
	    anInt3990 = class534_sub40.method16527(-1660432989) * 375610051;
	else if (i == 22)
	    anInt3979 = class534_sub40.method16533(-258848859) * 181262307;
	else if (23 == i)
	    anInt3980 = class534_sub40.method16527(-53194803) * -260937871;
	else if (24 == i)
	    aBool3987 = false;
	else if (25 == i) {
	    int i_22_ = class534_sub40.method16527(-600120334);
	    anIntArray4016 = new int[i_22_];
	    for (int i_23_ = 0; i_23_ < i_22_; i_23_++)
		anIntArray4016[i_23_] = class534_sub40.method16529((byte) 1);
	} else if (26 == i)
	    aBool4009 = false;
	else if (i == 27)
	    anInt3997 = ((class534_sub40.method16529((byte) 1) << 12 << 2)
			 * 1706942285);
	else if (i == 28)
	    anInt3984 = class534_sub40.method16527(395865755) * -1664572749;
	else if (i == 29) {
	    if (class534_sub40.method16527(-766073302) == 0)
		anInt4026
		    = ((anInt4015
			= class534_sub40.method16530((byte) -115) * 808818696)
		       * 1100581591);
	    else {
		anInt4026
		    = class534_sub40.method16530((byte) -68) * 1170144952;
		anInt4015 = class534_sub40.method16530((byte) -13) * 808818696;
	    }
	} else if (i == 30)
	    aBool4033 = true;
	else if (i == 31) {
	    anInt3981 = ((class534_sub40.method16529((byte) 1) << 12 << 2)
			 * -1680083599);
	    anInt3982 = ((class534_sub40.method16529((byte) 1) << 12 << 2)
			 * 1048152903);
	} else if (32 == i)
	    aBool3986 = false;
	else if (33 == i)
	    aBool4020 = true;
	else if (34 == i)
	    aBool4019 = false;
	else if (i == 35) {
	    if (class534_sub40.method16527(-672005036) == 0)
		anInt3976
		    = ((anInt4012
			= class534_sub40.method16530((byte) -39) * -1076897672)
		       * -289795997);
	    else {
		anInt3976 = class534_sub40.method16530((byte) -76) * 35141224;
		anInt4012
		    = class534_sub40.method16530((byte) -22) * -1076897672;
		anInt4013
		    = class534_sub40.method16527(1191719486) * -991793813;
	    }
	} else if (i == 36)
	    aBool4039 = true;
    }
    
    void method6488(byte i) {
	if (anInt4001 * 2042652149 > -2 || 1736785337 * anInt4002 > -2)
	    aBool4014 = true;
	anInt4022 = 1153904143 * (-1438701003 * anInt3985 >> 16 & 0xff);
	anInt3996 = (-1155336309 * anInt4021 >> 16 & 0xff) * -1463969015;
	anInt4024 = 111135051 * anInt3996 - anInt4022 * -186426323;
	anInt4029 = (-1438701003 * anInt3985 >> 8 & 0xff) * 726724793;
	anInt4011 = (anInt4021 * -1155336309 >> 8 & 0xff) * 1693756103;
	anInt4027 = anInt4011 * -1516100631 - anInt4029 * -503029865;
	anInt3974 = -1809171537 * (-1438701003 * anInt3985 & 0xff);
	anInt3978 = (anInt4021 * -1155336309 & 0xff) * -41740921;
	anInt4025 = -410548623 * anInt3978 - anInt3974 * -1613221479;
	anInt4031 = 23737121 * (-1438701003 * anInt3985 >> 24 & 0xff);
	anInt3968 = 403995401 * (anInt4021 * -1155336309 >> 24 & 0xff);
	anInt4030 = anInt3968 * -1461894887 - 562733505 * anInt4031;
	if (563541671 * anInt3988 != 0) {
	    anInt4032 = 2048723293 * (anInt3989 * -716860259
				      * (anInt4034 * 1843131803) / 100);
	    anInt4035 = (anInt3990 * -1530616341 * (1843131803 * anInt4034)
			 / 100 * 801489573);
	    if (anInt4032 * -1117023499 == 0)
		anInt4032 = 2048723293;
	    anInt4036
		= (((anInt3988 * 563541671 >> 16 & 0xff)
		    - (-1590427409 * anInt4022 + anInt4024 * -467462389 / 2))
		   << 8) / (anInt4032 * -1117023499) * -1436983421;
	    anInt4037
		= (((563541671 * anInt3988 >> 8 & 0xff)
		    - (2123356127 * anInt4027 / 2 + 1164973449 * anInt4029))
		   << 8) / (anInt4032 * -1117023499) * -1870729061;
	    anInt4038 = 431526401 * ((((563541671 * anInt3988 & 0xff)
				       - (anInt4025 * -1708133081 / 2
					  + 1719532367 * anInt3974))
				      << 8)
				     / (anInt4032 * -1117023499));
	    if (0 == 392975661 * anInt4035)
		anInt4035 = 801489573;
	    anInt4017
		= (((563541671 * anInt3988 >> 24 & 0xff)
		    - (anInt4030 * 1298659105 / 2 + anInt4031 * 915869921))
		   << 8) / (anInt4035 * 392975661) * -467927579;
	    Class385 class385_24_ = this;
	    class385_24_.anInt4036
		= (class385_24_.anInt4036
		   + -1436983421 * (anInt4036 * 969942827 > 0 ? -4 : 4));
	    Class385 class385_25_ = this;
	    class385_25_.anInt4037
		= (class385_25_.anInt4037
		   + (anInt4037 * -1220577901 > 0 ? -4 : 4) * -1870729061);
	    Class385 class385_26_ = this;
	    class385_26_.anInt4038
		= (class385_26_.anInt4038
		   + (1368092161 * anInt4038 > 0 ? -4 : 4) * 431526401);
	    Class385 class385_27_ = this;
	    class385_27_.anInt4017
		= (class385_27_.anInt4017
		   + (anInt4017 * -1647904787 > 0 ? -4 : 4) * -467927579);
	}
	if (-1 != anInt3979 * -2017057333) {
	    anInt4040 = 2141022519 * (-4228719 * anInt3980
				      * (1843131803 * anInt4034) / 100);
	    if (-417293177 * anInt4040 == 0)
		anInt4040 = 2141022519;
	    anInt3992 = -90279563 * ((-2017057333 * anInt3979
				      - ((anInt4005 * 770148019
					  - 579777191 * anInt3972) / 2
					 + 579777191 * anInt3972))
				     / (anInt4040 * -417293177));
	}
	if (921858437 * anInt3997 != -1) {
	    anInt4028 = (-2061968261 * anInt3984 * (anInt4034 * 1843131803)
			 / 100 * 1014192875);
	    if (0 == anInt4028 * -1191739965)
		anInt4028 = 1014192875;
	    anInt4043 = 7647549 * ((921858437 * anInt3997
				    - (anInt3981 * 1362111377
				       + (anInt3982 * -1082478985
					  - anInt3981 * 1362111377) / 2))
				   / (anInt4028 * -1191739965));
	}
	anInt4044 = anInt4012 * 26147823 - 1851772421 * anInt3976;
	anInt4045 = -957269875 * anInt4015 - anInt4026 * 488468283;
    }
    
    void method6489(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-477499022);
	    if (0 == i)
		break;
	    method6481(class534_sub40, i, (byte) 16);
	}
    }
    
    static final void method6490(Class669 class669, int i)
	throws Exception_Sub2 {
	class669.anInt8600 -= 617999126;
	int i_28_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_29_
	    = class669.anIntArray8595[1 + class669.anInt8600 * 2088438307];
	Class438 class438
	    = Class438.method6996((float) i_28_, (float) i_28_, (float) i_28_);
	if (class438.aFloat4864 == -1.0F)
	    class438.aFloat4864 = Float.POSITIVE_INFINITY;
	if (class438.aFloat4863 == -1.0F)
	    class438.aFloat4863 = Float.POSITIVE_INFINITY;
	if (class438.aFloat4865 == -1.0F)
	    class438.aFloat4865 = Float.POSITIVE_INFINITY;
	Class599.aClass298_Sub1_7871.method5470(class438, 1229923834);
	Class599.aClass298_Sub1_7871.method5357((float) i_29_ / 1000.0F,
						-827787691);
	class438.method7074();
    }
    
    static final void method6491(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 1358707213 * Class65.anInt706;
    }
}
