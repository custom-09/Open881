/* Class534_Sub18_Sub14 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub18_Sub14 extends Class534_Sub18
{
    public String aString11803;
    public String aString11804;
    public int anInt11805;
    public int anInt11806;
    public String aString11807;
    public int anInt11808 = Class170.method2826((byte) 24) * -1477791089;
    public String aString11809;
    public Class407 aClass407_11810;
    public int anInt11811;
    public String aString11812;
    public long aLong11813
	= Class250.method4604((byte) -51) * -3617813219191009017L;
    
    void method18392(int i, int i_0_, String string, String string_1_,
		     String string_2_, String string_3_, int i_4_,
		     String string_5_, Class407 class407) {
	anInt11808 = Class170.method2826((byte) 24) * -1477791089;
	aLong11813 = Class250.method4604((byte) -81) * -3617813219191009017L;
	anInt11805 = 1097374805 * i;
	anInt11806 = -27866335 * i_0_;
	aString11807 = string;
	aString11804 = string_1_;
	aString11809 = string_2_;
	aString11803 = string_3_;
	anInt11811 = -1177229315 * i_4_;
	aString11812 = string_5_;
	aClass407_11810 = class407;
    }
    
    void method18393(int i, int i_6_, String string, String string_7_,
		     String string_8_, String string_9_, int i_10_,
		     String string_11_, Class407 class407, int i_12_) {
	anInt11808 = Class170.method2826((byte) 24) * -1477791089;
	aLong11813 = Class250.method4604((byte) -113) * -3617813219191009017L;
	anInt11805 = 1097374805 * i;
	anInt11806 = -27866335 * i_6_;
	aString11807 = string;
	aString11804 = string_7_;
	aString11809 = string_8_;
	aString11803 = string_9_;
	anInt11811 = -1177229315 * i_10_;
	aString11812 = string_11_;
	aClass407_11810 = class407;
    }
    
    void method18394(int i, int i_13_, String string, String string_14_,
		     String string_15_, String string_16_, int i_17_,
		     String string_18_, Class407 class407) {
	anInt11808 = Class170.method2826((byte) 24) * -1477791089;
	aLong11813 = Class250.method4604((byte) -113) * -3617813219191009017L;
	anInt11805 = 1097374805 * i;
	anInt11806 = -27866335 * i_13_;
	aString11807 = string;
	aString11804 = string_14_;
	aString11809 = string_15_;
	aString11803 = string_16_;
	anInt11811 = -1177229315 * i_17_;
	aString11812 = string_18_;
	aClass407_11810 = class407;
    }
    
    Class534_Sub18_Sub14(int i, int i_19_, String string, String string_20_,
			 String string_21_, String string_22_, int i_23_,
			 String string_24_, Class407 class407) {
	anInt11805 = i * 1097374805;
	anInt11806 = i_19_ * -27866335;
	aString11807 = string;
	aString11804 = string_20_;
	aString11809 = string_21_;
	aString11803 = string_22_;
	anInt11811 = -1177229315 * i_23_;
	aString11812 = string_24_;
	aClass407_11810 = class407;
    }
    
    void method18395(int i, int i_25_, String string, String string_26_,
		     String string_27_, String string_28_, int i_29_,
		     String string_30_, Class407 class407) {
	anInt11808 = Class170.method2826((byte) 24) * -1477791089;
	aLong11813 = Class250.method4604((byte) -9) * -3617813219191009017L;
	anInt11805 = 1097374805 * i;
	anInt11806 = -27866335 * i_25_;
	aString11807 = string;
	aString11804 = string_26_;
	aString11809 = string_27_;
	aString11803 = string_28_;
	anInt11811 = -1177229315 * i_29_;
	aString11812 = string_30_;
	aClass407_11810 = class407;
    }
}
