/* Class412 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Class412 implements Iterator
{
    AbstractQueue_Sub1 anAbstractQueue_Sub1_4640;
    int anInt4641 = 0;
    int anInt4642 = 745537151 * anAbstractQueue_Sub1_4640.anInt11915;
    static int anInt4643;
    
    public boolean method6720() {
	return (-514684453 * anInt4641
		< anAbstractQueue_Sub1_4640.anInt11913 * 633845711);
    }
    
    public boolean hasNext() {
	return (-514684453 * anInt4641
		< anAbstractQueue_Sub1_4640.anInt11913 * 633845711);
    }
    
    public void method6721() {
	throw new UnsupportedOperationException();
    }
    
    public void remove() {
	throw new UnsupportedOperationException();
    }
    
    public boolean method6722() {
	return (-514684453 * anInt4641
		< anAbstractQueue_Sub1_4640.anInt11913 * 633845711);
    }
    
    public Object method6723() {
	if (1820965865 * anInt4642
	    != anAbstractQueue_Sub1_4640.anInt11915 * 114058903)
	    throw new ConcurrentModificationException();
	if (anInt4641 * -514684453
	    < anAbstractQueue_Sub1_4640.anInt11913 * 633845711) {
	    Object object = (anAbstractQueue_Sub1_4640.aClass415Array11914
			     [-514684453 * anInt4641].anObject4665);
	    anInt4641 += 711739987;
	    return object;
	}
	throw new NoSuchElementException();
    }
    
    public boolean method6724() {
	return (-514684453 * anInt4641
		< anAbstractQueue_Sub1_4640.anInt11913 * 633845711);
    }
    
    public Object next() {
	if (1820965865 * anInt4642
	    != anAbstractQueue_Sub1_4640.anInt11915 * 114058903)
	    throw new ConcurrentModificationException();
	if (anInt4641 * -514684453
	    < anAbstractQueue_Sub1_4640.anInt11913 * 633845711) {
	    Object object = (anAbstractQueue_Sub1_4640.aClass415Array11914
			     [-514684453 * anInt4641].anObject4665);
	    anInt4641 += 711739987;
	    return object;
	}
	throw new NoSuchElementException();
    }
    
    public boolean method6725() {
	return (-514684453 * anInt4641
		< anAbstractQueue_Sub1_4640.anInt11913 * 633845711);
    }
    
    public Object method6726() {
	if (1820965865 * anInt4642
	    != anAbstractQueue_Sub1_4640.anInt11915 * 114058903)
	    throw new ConcurrentModificationException();
	if (anInt4641 * -514684453
	    < anAbstractQueue_Sub1_4640.anInt11913 * 633845711) {
	    Object object = (anAbstractQueue_Sub1_4640.aClass415Array11914
			     [-514684453 * anInt4641].anObject4665);
	    anInt4641 += 711739987;
	    return object;
	}
	throw new NoSuchElementException();
    }
    
    public void method6727() {
	throw new UnsupportedOperationException();
    }
    
    Class412(AbstractQueue_Sub1 abstractqueue_sub1) {
	anAbstractQueue_Sub1_4640 = abstractqueue_sub1;
    }
    
    static void method6728(Class669 class669, int i) {
	class669.anIntArray8595[2088438307 * class669.anInt8600 - 1]
	    = (((Class273)
		Class223.aClass53_Sub2_2316.method91((class669.anIntArray8595
						      [(class669.anInt8600
							* 2088438307) - 1]),
						     29150218)).anInt3034
	       * -950684159);
    }
    
    static final void method6729(Class669 class669, int i) {
	Class150 class150
	    = (Class150) (class669.aClass534_Sub18_Sub8_8614.anObjectArray11753
			  [662605117 * class669.anInt8613]);
	Interface19 interface19
	    = ((Interface19)
	       (class669.anIntArray8591[class669.anInt8613 * 662605117] == 0
		? class669.aMap8607.get(class150.aClass453_1695)
		: class669.aMap8608.get(class150.aClass453_1695)));
	Class483 class483 = class150.aClass493_1696.method8102(1804514866);
	if (Class483.aClass483_5271 == class483) {
	    if (Class453.aClass453_4947 == class150.aClass453_1695)
		Class406.method6682(class150, 354619436);
	    interface19.method114(class150,
				  class669.anIntArray8595[((class669.anInt8600
							    -= 308999563)
							   * 2088438307)],
				  431217078);
	} else if (Class483.aClass483_5272 == class483)
	    interface19.method116(class150,
				  class669.aLongArray8587[((class669.anInt8596
							    -= 1091885681)
							   * 1572578961)]);
	else if (class483 == Class483.aClass483_5279) {
	    if (class150.aClass453_1695 == Class453.aClass453_4947)
		Class606.method10052(class150, -1700987663);
	    interface19.method118(class150,
				  (class669.anObjectArray8593
				   [((class669.anInt8594 -= 1460193483)
				     * 1485266147)]),
				  (byte) -11);
	} else
	    throw new RuntimeException();
    }
    
    static final void method6730(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_0_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_1_
	    = class669.anIntArray8595[1 + class669.anInt8600 * 2088438307];
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (-130968455
	       * client.aClass492ArrayArray11027[i_1_][i_0_].anInt5336);
    }
}
