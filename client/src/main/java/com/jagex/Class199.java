/* Class199 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public final class Class199
{
    int anInt2171;
    Class534_Sub18 aClass534_Sub18_2172 = new Class534_Sub18();
    int anInt2173;
    Class9 aClass9_2174;
    Class696 aClass696_2175 = new Class696();
    static Class169 aClass169_2176;
    
    public void method3832(Class534_Sub18 class534_sub18, long l) {
	if (1830502407 * anInt2173 == 0) {
	    Class534_Sub18 class534_sub18_0_
		= aClass696_2175.method14077((byte) -14);
	    class534_sub18_0_.method8892((byte) 1);
	    class534_sub18_0_.method16180(-421776830);
	    if (class534_sub18_0_ == aClass534_Sub18_2172) {
		class534_sub18_0_ = aClass696_2175.method14077((byte) -13);
		class534_sub18_0_.method8892((byte) 1);
		class534_sub18_0_.method16180(-421776830);
	    }
	} else
	    anInt2173 -= 1973169591;
	aClass9_2174.method581(class534_sub18, l);
	aClass696_2175.method14076(class534_sub18, (byte) 38);
    }
    
    public Class534_Sub18 method3833(long l) {
	Class534_Sub18 class534_sub18
	    = (Class534_Sub18) aClass9_2174.method579(l);
	if (null != class534_sub18)
	    aClass696_2175.method14076(class534_sub18, (byte) 34);
	return class534_sub18;
    }
    
    public void method3834(Class534_Sub18 class534_sub18, long l) {
	if (1830502407 * anInt2173 == 0) {
	    Class534_Sub18 class534_sub18_1_
		= aClass696_2175.method14077((byte) -93);
	    class534_sub18_1_.method8892((byte) 1);
	    class534_sub18_1_.method16180(-421776830);
	    if (class534_sub18_1_ == aClass534_Sub18_2172) {
		class534_sub18_1_ = aClass696_2175.method14077((byte) -60);
		class534_sub18_1_.method8892((byte) 1);
		class534_sub18_1_.method16180(-421776830);
	    }
	} else
	    anInt2173 -= 1973169591;
	aClass9_2174.method581(class534_sub18, l);
	aClass696_2175.method14076(class534_sub18, (byte) 123);
    }
    
    public void method3835(Class534_Sub18 class534_sub18, long l) {
	if (1830502407 * anInt2173 == 0) {
	    Class534_Sub18 class534_sub18_2_
		= aClass696_2175.method14077((byte) -25);
	    class534_sub18_2_.method8892((byte) 1);
	    class534_sub18_2_.method16180(-421776830);
	    if (class534_sub18_2_ == aClass534_Sub18_2172) {
		class534_sub18_2_ = aClass696_2175.method14077((byte) -26);
		class534_sub18_2_.method8892((byte) 1);
		class534_sub18_2_.method16180(-421776830);
	    }
	} else
	    anInt2173 -= 1973169591;
	aClass9_2174.method581(class534_sub18, l);
	aClass696_2175.method14076(class534_sub18, (byte) 45);
    }
    
    public Class534_Sub18 method3836(long l) {
	Class534_Sub18 class534_sub18
	    = (Class534_Sub18) aClass9_2174.method579(l);
	if (null != class534_sub18)
	    aClass696_2175.method14076(class534_sub18, (byte) 111);
	return class534_sub18;
    }
    
    public void method3837(int i) {
	aClass696_2175.method14075(958896847);
	aClass9_2174.method578((byte) -76);
	aClass534_Sub18_2172 = new Class534_Sub18();
	anInt2173 = 1909500161 * anInt2171;
    }
    
    public Class534_Sub18 method3838(long l) {
	Class534_Sub18 class534_sub18
	    = (Class534_Sub18) aClass9_2174.method579(l);
	if (null != class534_sub18)
	    aClass696_2175.method14076(class534_sub18, (byte) 10);
	return class534_sub18;
    }
    
    public void method3839(Class534_Sub18 class534_sub18, long l) {
	if (1830502407 * anInt2173 == 0) {
	    Class534_Sub18 class534_sub18_3_
		= aClass696_2175.method14077((byte) -46);
	    class534_sub18_3_.method8892((byte) 1);
	    class534_sub18_3_.method16180(-421776830);
	    if (class534_sub18_3_ == aClass534_Sub18_2172) {
		class534_sub18_3_ = aClass696_2175.method14077((byte) -100);
		class534_sub18_3_.method8892((byte) 1);
		class534_sub18_3_.method16180(-421776830);
	    }
	} else
	    anInt2173 -= 1973169591;
	aClass9_2174.method581(class534_sub18, l);
	aClass696_2175.method14076(class534_sub18, (byte) 39);
    }
    
    public Class199(int i) {
	anInt2171 = -832217417 * i;
	anInt2173 = i * 1973169591;
	int i_4_;
	for (i_4_ = 1; i_4_ + i_4_ < i; i_4_ += i_4_) {
	    /* empty */
	}
	aClass9_2174 = new Class9(i_4_);
    }
    
    static final void method3840(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub13_10761
		  .method16991(28803288) ? 1 : 0;
    }
    
    static final void method3841(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class625.aByte8166;
    }
}
