/* Class88 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class88
{
    static Class88 aClass88_851;
    static Class88 aClass88_852;
    static Class88 aClass88_853;
    static Class88 aClass88_854;
    static Class88 aClass88_855;
    static Class88 aClass88_856;
    static Class88 aClass88_857;
    static Class88 aClass88_858;
    static Class88 aClass88_859 = new Class88(7, 0);
    public int anInt860;
    static Class88 aClass88_861;
    static Class88 aClass88_862;
    static Class88 aClass88_863;
    static Class88 aClass88_864;
    static Class88 aClass88_865;
    static Class88 aClass88_866;
    static Class88 aClass88_867;
    static Class88 aClass88_868;
    static Class88 aClass88_869;
    static Class88 aClass88_870;
    static Class88 aClass88_871;
    static Class88 aClass88_872;
    static Class88 aClass88_873;
    static Class88 aClass88_874;
    static Class88 aClass88_875;
    static Class88 aClass88_876;
    static Class88 aClass88_877;
    static Class88 aClass88_878;
    static Class88 aClass88_879;
    static Class88 aClass88_880;
    static Class88 aClass88_881;
    static Class88 aClass88_882;
    int anInt883;
    public static Class44_Sub12 aClass44_Sub12_884;
    
    Class88(int i, int i_0_) {
	anInt860 = i * 1461158145;
	anInt883 = -2028737925 * i_0_;
    }
    
    public static Class88 method1710(int i) {
	Class88[] class88s = Class655.method10855(2128500171);
	Class88[] class88s_1_ = class88s;
	for (int i_2_ = 0; i_2_ < class88s_1_.length; i_2_++) {
	    Class88 class88 = class88s_1_[i_2_];
	    if (i == 553739443 * class88.anInt883)
		return class88;
	}
	return null;
    }
    
    static Class88[] method1711() {
	return (new Class88[]
		{ aClass88_873, aClass88_875, aClass88_857, aClass88_853,
		  aClass88_876, aClass88_862, aClass88_861, aClass88_856,
		  aClass88_872, aClass88_880, aClass88_858, aClass88_855,
		  aClass88_859, aClass88_867, aClass88_863, aClass88_874,
		  aClass88_866, aClass88_871, aClass88_869, aClass88_870,
		  aClass88_854, aClass88_851, aClass88_879, aClass88_881,
		  aClass88_852, aClass88_864, aClass88_877, aClass88_882,
		  aClass88_865, aClass88_878, aClass88_868 });
    }
    
    static {
	aClass88_852 = new Class88(9, 1);
	aClass88_853 = new Class88(25, 2);
	aClass88_854 = new Class88(19, 3);
	aClass88_855 = new Class88(15, 10);
	aClass88_867 = new Class88(10, 11);
	aClass88_857 = new Class88(1, 12);
	aClass88_858 = new Class88(22, 13);
	aClass88_882 = new Class88(17, 14);
	aClass88_865 = new Class88(8, 15);
	aClass88_871 = new Class88(20, 16);
	aClass88_878 = new Class88(13, 17);
	aClass88_861 = new Class88(21, 20);
	aClass88_864 = new Class88(24, 21);
	aClass88_881 = new Class88(30, 22);
	aClass88_866 = new Class88(14, 30);
	aClass88_879 = new Class88(28, 31);
	aClass88_862 = new Class88(23, 32);
	aClass88_869 = new Class88(12, 33);
	aClass88_870 = new Class88(18, 40);
	aClass88_856 = new Class88(26, 41);
	aClass88_872 = new Class88(2, 42);
	aClass88_873 = new Class88(0, 43);
	aClass88_874 = new Class88(4, 50);
	aClass88_875 = new Class88(3, 51);
	aClass88_876 = new Class88(5, 52);
	aClass88_877 = new Class88(16, 53);
	aClass88_863 = new Class88(29, 60);
	aClass88_851 = new Class88(11, 61);
	aClass88_880 = new Class88(27, 70);
	aClass88_868 = new Class88(6, 255);
    }
    
    public static Class88 method1712(int i) {
	Class88[] class88s = Class655.method10855(2128500171);
	Class88[] class88s_3_ = class88s;
	for (int i_4_ = 0; i_4_ < class88s_3_.length; i_4_++) {
	    Class88 class88 = class88s_3_[i_4_];
	    if (i == 553739443 * class88.anInt883)
		return class88;
	}
	return null;
    }
}
