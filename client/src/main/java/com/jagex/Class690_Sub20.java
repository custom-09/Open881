/* Class690_Sub20 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub20 extends Class690
{
    public static final int anInt10908 = 1;
    public static final int anInt10909 = 2;
    public static final int anInt10910 = 0;
    
    public int method14029(int i) {
	return 1;
    }
    
    public Class690_Sub20(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public void method17057(int i) {
	if (anInt8753 * 189295939 < 0 || 189295939 * anInt8753 > 2)
	    anInt8753 = method14017(2132099138) * 1823770475;
    }
    
    int method14017(int i) {
	if (aClass534_Sub35_8752.aClass690_Sub7_10733.method16938(-881188269)
	    && Class431.method6834(aClass534_Sub35_8752
				       .aClass690_Sub7_10733
				       .method16935(-1807368365),
				   (byte) 23))
	    return 1;
	return 0;
    }
    
    public int method14028(int i) {
	return 1;
    }
    
    public int method14026(int i, int i_0_) {
	return 1;
    }
    
    void method14020(int i, int i_1_) {
	anInt8753 = i * 1823770475;
    }
    
    public int method17058(int i) {
	return 189295939 * anInt8753;
    }
    
    int method14021() {
	if (aClass534_Sub35_8752.aClass690_Sub7_10733.method16938(-881188269)
	    && Class431.method6834(aClass534_Sub35_8752
				       .aClass690_Sub7_10733
				       .method16935(-1807368365),
				   (byte) -33))
	    return 1;
	return 0;
    }
    
    int method14022() {
	if (aClass534_Sub35_8752.aClass690_Sub7_10733.method16938(-881188269)
	    && Class431.method6834(aClass534_Sub35_8752
				       .aClass690_Sub7_10733
				       .method16935(-1807368365),
				   (byte) -56))
	    return 1;
	return 0;
    }
    
    int method14018() {
	if (aClass534_Sub35_8752.aClass690_Sub7_10733.method16938(-881188269)
	    && Class431.method6834(aClass534_Sub35_8752
				       .aClass690_Sub7_10733
				       .method16935(-1807368365),
				   (byte) 116))
	    return 1;
	return 0;
    }
    
    public boolean method17059(int i) {
	return true;
    }
    
    void method14025(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public int method14027(int i) {
	return 1;
    }
    
    void method14024(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public Class690_Sub20(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    public int method17060() {
	return 189295939 * anInt8753;
    }
    
    public int method14030(int i) {
	return 1;
    }
    
    public void method17061() {
	if (anInt8753 * 189295939 < 0 || 189295939 * anInt8753 > 2)
	    anInt8753 = method14017(2111367557) * 1823770475;
    }
    
    public boolean method17062() {
	return true;
    }
    
    public int method17063() {
	return 189295939 * anInt8753;
    }
    
    void method14023(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public int method17064() {
	return 189295939 * anInt8753;
    }
}
