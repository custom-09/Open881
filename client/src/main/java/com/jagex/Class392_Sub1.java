/* Class392_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class392_Sub1 extends Class392
{
    public int anInt10223;
    static int anInt10224;
    
    public Class397 method351() {
	return Class397.aClass397_4114;
    }
    
    public Class397 method348(int i) {
	return Class397.aClass397_4114;
    }
    
    Class392_Sub1(int i, Class401 class401, Class391 class391, int i_0_,
		  int i_1_, int i_2_) {
	super(i, class401, class391, i_0_, i_1_);
	anInt10223 = 361516917 * i_2_;
    }
    
    public Class397 method350() {
	return Class397.aClass397_4114;
    }
    
    public Class397 method349() {
	return Class397.aClass397_4114;
    }
    
    public static Class392 method15836(Class534_Sub40 class534_sub40) {
	Class392 class392 = Class681.method13865(class534_sub40, (byte) 37);
	int i = class534_sub40.method16532(-676065872);
	return new Class392_Sub1(class392.anInt4077 * 1909682011,
				 class392.aClass401_4078,
				 class392.aClass391_4080,
				 -1151439181 * class392.anInt4079,
				 -963484815 * class392.anInt4081, i);
    }
    
    static void method15837(Class247 class247, int i) {
	if (client.aBool11218) {
	    if (class247.anObjectArray2491 != null) {
		Class247 class247_3_
		    = Class81.method1637(-1808298539 * Class130.anInt1525,
					 client.anInt11219 * -985352023,
					 908859792);
		if (null != class247_3_) {
		    Class534_Sub41 class534_sub41 = new Class534_Sub41();
		    class534_sub41.aClass247_10818 = class247;
		    class534_sub41.aClass247_10824 = class247_3_;
		    class534_sub41.anObjectArray10819
			= class247.anObjectArray2491;
		    Class94.method1764(class534_sub41, 998770475);
		}
	    }
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4180,
				      client.aClass100_11264.aClass13_1183,
				      1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16569(1365669833 * class247.anInt2580, -123707223);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16569(class247.anInt2606 * 403396513, 1912038356);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16617(-1808298539 * Class130.anInt1525, 1618928000);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16617(-1278450723 * class247.anInt2454, 888398826);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16568(client.anInt11219 * -985352023, 846942787);
	    class534_sub19.aClass534_Sub40_Sub1_10513
		.method16687(-823724811 * client.anInt11170, 2014283694);
	    client.aClass100_11264.method1863(class534_sub19, (byte) 22);
	}
    }
}
