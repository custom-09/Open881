/* Class51 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.Iterator;
import java.util.List;

public class Class51
{
    static Class298_Sub1 aClass298_Sub1_412;
    static int anInt413;
    static boolean aBool414 = false;
    
    static void method1165() {
	Class347_Sub2 class347_sub2
	    = (Class347_Sub2) aClass298_Sub1_412.method5380((byte) -54);
	Class706_Sub3 class706_sub3
	    = (Class706_Sub3) aClass298_Sub1_412.method5381(1654072399);
	Class438 class438 = class347_sub2.method6137(-2143922585);
	Class443 class443 = class706_sub3.method17272(-946441695);
	if (null != client.aClass247_11226) {
	    int i = client.aClass247_11226.anInt2468 * -881188269;
	    int i_0_ = client.aClass247_11226.anInt2469 * -1279656873;
	    float f = 1000.0F;
	    float f_1_
		= (float) (2.0 * Math.atan((double) ((float) i / 2.0F / f)));
	    float f_2_ = (float) (2.0 * Math.atan((double) ((float) i_0_ / 2.0F
							    / f)));
	    try {
		aClass298_Sub1_412.method5370(f_1_, f_2_, (byte) -35);
	    } catch (Exception_Sub2 exception_sub2) {
		/* empty */
	    }
	}
	if (Class81.aClass563_834.method9501((byte) -119)) {
	    Class443 class443_3_ = Class443.method7137();
	    class443_3_.method7147(1.0F, 0.0F, 0.0F,
				   (float) (Class81.aClass563_834
						.method9477(475378452)
					    - (RuntimeException_Sub4.anInt12126
					       * -230736199)) / 200.0F);
	    class443.method7158(class443_3_);
	    Class438 class438_4_ = Class438.method6996(0.0F, 1.0F, 0.0F);
	    class438_4_.method7021(class443);
	    Class443 class443_5_ = Class443.method7137();
	    class443_5_.method7146(class438_4_,
				   ((float) (1453851069 * anInt413
					     - Class81.aClass563_834
						   .method9493(-1711261825))
				    / 200.0F));
	    class443.method7158(class443_5_);
	    class706_sub3.method17278(class443, (byte) 1);
	}
	anInt413 = Class81.aClass563_834.method9493(-1640805316) * 742277525;
	RuntimeException_Sub4.anInt12126
	    = Class81.aClass563_834.method9477(789554638) * -1144821879;
	class443.method7182();
	if (Class666.aClass547_8577.method8995(98, (byte) -30)) {
	    Class438 class438_6_ = Class438.method6996(0.0F, 0.0F, 25.0F);
	    class438_6_.method7021(class443);
	    class438_6_.aFloat4863 *= -1.0F;
	    class438.method7026(class438_6_);
	}
	if (Class666.aClass547_8577.method8995(99, (byte) -76)) {
	    Class438 class438_7_ = Class438.method6996(0.0F, 0.0F, -25.0F);
	    class438_7_.method7021(class443);
	    class438_7_.aFloat4863 *= -1.0F;
	    class438.method7026(class438_7_);
	}
	if (Class666.aClass547_8577.method8995(96, (byte) -69)) {
	    Class438 class438_8_ = Class438.method6996(-25.0F, 0.0F, 0.0F);
	    class438_8_.method7021(class443);
	    class438_8_.aFloat4863 *= -1.0F;
	    class438.method7026(class438_8_);
	}
	if (Class666.aClass547_8577.method8995(97, (byte) -20)) {
	    Class438 class438_9_ = Class438.method6996(25.0F, 0.0F, 0.0F);
	    class438_9_.method7021(class443);
	    class438_9_.aFloat4863 *= -1.0F;
	    class438.method7026(class438_9_);
	}
	Class534_Sub36 class534_sub36
	    = new Class534_Sub36(0, (int) class438.aFloat4864,
				 (int) class438.aFloat4863,
				 (int) class438.aFloat4865);
	class347_sub2.method15787(class534_sub36, -1062631237);
	Class597 class597 = client.aClass512_11100.method8416((byte) 127);
	int i = class597.anInt7859 * -424199969 << 9;
	int i_10_ = class597.anInt7861 * -1166289421 << 9;
	aClass298_Sub1_412.method5351(0.02F,
				      (client.aClass512_11100.method8493
				       (2002725561).anIntArrayArrayArray4995),
				      client.aClass512_11100
					  .method8552((byte) 0),
				      i, i_10_, -1200920428);
    }
    
    static boolean method1166() {
	return aBool414;
    }
    
    static void method1167() {
	Class347_Sub2 class347_sub2
	    = (Class347_Sub2) aClass298_Sub1_412.method5380((byte) -45);
	Class706_Sub3 class706_sub3
	    = (Class706_Sub3) aClass298_Sub1_412.method5381(1690915228);
	Class438 class438 = class347_sub2.method6137(-549132248);
	Class443 class443 = class706_sub3.method17272(1864306882);
	if (null != client.aClass247_11226) {
	    int i = client.aClass247_11226.anInt2468 * -881188269;
	    int i_11_ = client.aClass247_11226.anInt2469 * -1279656873;
	    float f = 1000.0F;
	    float f_12_
		= (float) (2.0 * Math.atan((double) ((float) i / 2.0F / f)));
	    float f_13_ = (float) (2.0 * Math.atan((double) ((float) i_11_
							     / 2.0F / f)));
	    try {
		aClass298_Sub1_412.method5370(f_12_, f_13_, (byte) -18);
	    } catch (Exception_Sub2 exception_sub2) {
		/* empty */
	    }
	}
	if (Class81.aClass563_834.method9501((byte) -38)) {
	    Class443 class443_14_ = Class443.method7137();
	    class443_14_.method7147(1.0F, 0.0F, 0.0F,
				    ((float) (Class81.aClass563_834
						  .method9477(1611574682)
					      - (RuntimeException_Sub4
						 .anInt12126) * -230736199)
				     / 200.0F));
	    class443.method7158(class443_14_);
	    Class438 class438_15_ = Class438.method6996(0.0F, 1.0F, 0.0F);
	    class438_15_.method7021(class443);
	    Class443 class443_16_ = Class443.method7137();
	    class443_16_.method7146(class438_15_,
				    ((float) (1453851069 * anInt413
					      - Class81.aClass563_834
						    .method9493(-2034608397))
				     / 200.0F));
	    class443.method7158(class443_16_);
	    class706_sub3.method17278(class443, (byte) 1);
	}
	anInt413 = Class81.aClass563_834.method9493(-1625646778) * 742277525;
	RuntimeException_Sub4.anInt12126
	    = Class81.aClass563_834.method9477(2126989443) * -1144821879;
	class443.method7182();
	if (Class666.aClass547_8577.method8995(98, (byte) -29)) {
	    Class438 class438_17_ = Class438.method6996(0.0F, 0.0F, 25.0F);
	    class438_17_.method7021(class443);
	    class438_17_.aFloat4863 *= -1.0F;
	    class438.method7026(class438_17_);
	}
	if (Class666.aClass547_8577.method8995(99, (byte) -38)) {
	    Class438 class438_18_ = Class438.method6996(0.0F, 0.0F, -25.0F);
	    class438_18_.method7021(class443);
	    class438_18_.aFloat4863 *= -1.0F;
	    class438.method7026(class438_18_);
	}
	if (Class666.aClass547_8577.method8995(96, (byte) -128)) {
	    Class438 class438_19_ = Class438.method6996(-25.0F, 0.0F, 0.0F);
	    class438_19_.method7021(class443);
	    class438_19_.aFloat4863 *= -1.0F;
	    class438.method7026(class438_19_);
	}
	if (Class666.aClass547_8577.method8995(97, (byte) -52)) {
	    Class438 class438_20_ = Class438.method6996(25.0F, 0.0F, 0.0F);
	    class438_20_.method7021(class443);
	    class438_20_.aFloat4863 *= -1.0F;
	    class438.method7026(class438_20_);
	}
	Class534_Sub36 class534_sub36
	    = new Class534_Sub36(0, (int) class438.aFloat4864,
				 (int) class438.aFloat4863,
				 (int) class438.aFloat4865);
	class347_sub2.method15787(class534_sub36, -1062631237);
	Class597 class597 = client.aClass512_11100.method8416((byte) 40);
	int i = class597.anInt7859 * -424199969 << 9;
	int i_21_ = class597.anInt7861 * -1166289421 << 9;
	aClass298_Sub1_412.method5351(0.02F,
				      (client.aClass512_11100.method8493
				       (328426473).anIntArrayArrayArray4995),
				      client.aClass512_11100
					  .method8552((byte) 0),
				      i, i_21_, 1683250463);
    }
    
    static void method1168() {
	aClass298_Sub1_412 = null;
	aBool414 = false;
    }
    
    static boolean method1169() {
	return aBool414;
    }
    
    static boolean method1170() {
	return aBool414;
    }
    
    static boolean method1171() {
	return aBool414;
    }
    
    Class51() throws Throwable {
	throw new Error();
    }
    
    static void method1172() {
	aClass298_Sub1_412 = null;
	aBool414 = false;
    }
    
    static void method1173() {
	aClass298_Sub1_412 = null;
	aBool414 = false;
    }
    
    static boolean method1174() {
	return aBool414;
    }
    
    static void method1175() {
	Class347_Sub2 class347_sub2
	    = (Class347_Sub2) aClass298_Sub1_412.method5380((byte) -79);
	Class706_Sub3 class706_sub3
	    = (Class706_Sub3) aClass298_Sub1_412.method5381(1578027250);
	Class438 class438 = class347_sub2.method6137(406147519);
	Class443 class443 = class706_sub3.method17272(-741388784);
	if (null != client.aClass247_11226) {
	    int i = client.aClass247_11226.anInt2468 * -881188269;
	    int i_22_ = client.aClass247_11226.anInt2469 * -1279656873;
	    float f = 1000.0F;
	    float f_23_
		= (float) (2.0 * Math.atan((double) ((float) i / 2.0F / f)));
	    float f_24_ = (float) (2.0 * Math.atan((double) ((float) i_22_
							     / 2.0F / f)));
	    try {
		aClass298_Sub1_412.method5370(f_23_, f_24_, (byte) -21);
	    } catch (Exception_Sub2 exception_sub2) {
		/* empty */
	    }
	}
	if (Class81.aClass563_834.method9501((byte) -120)) {
	    Class443 class443_25_ = Class443.method7137();
	    class443_25_.method7147(1.0F, 0.0F, 0.0F,
				    ((float) (Class81.aClass563_834
						  .method9477(1935011043)
					      - (RuntimeException_Sub4
						 .anInt12126) * -230736199)
				     / 200.0F));
	    class443.method7158(class443_25_);
	    Class438 class438_26_ = Class438.method6996(0.0F, 1.0F, 0.0F);
	    class438_26_.method7021(class443);
	    Class443 class443_27_ = Class443.method7137();
	    class443_27_.method7146(class438_26_,
				    ((float) (1453851069 * anInt413
					      - Class81.aClass563_834
						    .method9493(-1787295866))
				     / 200.0F));
	    class443.method7158(class443_27_);
	    class706_sub3.method17278(class443, (byte) 1);
	}
	anInt413 = Class81.aClass563_834.method9493(-1077767130) * 742277525;
	RuntimeException_Sub4.anInt12126
	    = Class81.aClass563_834.method9477(321692408) * -1144821879;
	class443.method7182();
	if (Class666.aClass547_8577.method8995(98, (byte) -71)) {
	    Class438 class438_28_ = Class438.method6996(0.0F, 0.0F, 25.0F);
	    class438_28_.method7021(class443);
	    class438_28_.aFloat4863 *= -1.0F;
	    class438.method7026(class438_28_);
	}
	if (Class666.aClass547_8577.method8995(99, (byte) -33)) {
	    Class438 class438_29_ = Class438.method6996(0.0F, 0.0F, -25.0F);
	    class438_29_.method7021(class443);
	    class438_29_.aFloat4863 *= -1.0F;
	    class438.method7026(class438_29_);
	}
	if (Class666.aClass547_8577.method8995(96, (byte) -59)) {
	    Class438 class438_30_ = Class438.method6996(-25.0F, 0.0F, 0.0F);
	    class438_30_.method7021(class443);
	    class438_30_.aFloat4863 *= -1.0F;
	    class438.method7026(class438_30_);
	}
	if (Class666.aClass547_8577.method8995(97, (byte) -37)) {
	    Class438 class438_31_ = Class438.method6996(25.0F, 0.0F, 0.0F);
	    class438_31_.method7021(class443);
	    class438_31_.aFloat4863 *= -1.0F;
	    class438.method7026(class438_31_);
	}
	Class534_Sub36 class534_sub36
	    = new Class534_Sub36(0, (int) class438.aFloat4864,
				 (int) class438.aFloat4863,
				 (int) class438.aFloat4865);
	class347_sub2.method15787(class534_sub36, -1062631237);
	Class597 class597 = client.aClass512_11100.method8416((byte) 42);
	int i = class597.anInt7859 * -424199969 << 9;
	int i_32_ = class597.anInt7861 * -1166289421 << 9;
	aClass298_Sub1_412.method5351(0.02F,
				      (client.aClass512_11100.method8493
				       (429533452).anIntArrayArrayArray4995),
				      client.aClass512_11100
					  .method8552((byte) 0),
				      i, i_32_, 1852405588);
    }
    
    static void method1176(Class185 class185, int i, int i_33_, int i_34_) {
	if (i >= 0 && i_33_ >= 0 && Class72.aClass433_762 != null) {
	    Class597 class597 = client.aClass512_11100.method8416((byte) 46);
	    Class433 class433 = class185.method3518();
	    Class706_Sub5.method17347(class185, (byte) 0);
	    class433.method6916(Class72.aClass446_783);
	    class433.method6839(Class72.aClass433_762);
	    class433.method6847();
	    int i_35_ = i - 894945745 * Class72.anInt777;
	    int i_36_ = i_33_ - -655548783 * Class72.anInt778;
	    if (client.aClass512_11100.method8424((byte) 14) != null) {
		if (!client.aBool11218
		    || (460977285 * Class200_Sub14.anInt9950 & 0x40) != 0) {
		    int i_37_ = -1;
		    int i_38_ = -1;
		    float f
			= ((float) i_35_ * 2.0F / (float) (1852547097
							   * Class72.anInt779)
			   - 1.0F);
		    float f_39_
			= (2.0F * (float) i_36_ / (float) (-1954074475
							   * Class72.anInt780)
			   - 1.0F);
		    class433.method6850(f, f_39_, -1.0F,
					Class72.aFloatArray769);
		    float f_40_ = (Class72.aFloatArray769[0]
				   / Class72.aFloatArray769[3]);
		    float f_41_ = (Class72.aFloatArray769[1]
				   / Class72.aFloatArray769[3]);
		    float f_42_ = (Class72.aFloatArray769[2]
				   / Class72.aFloatArray769[3]);
		    class433.method6850(f, f_39_, 1.0F,
					Class72.aFloatArray769);
		    float f_43_ = (Class72.aFloatArray769[0]
				   / Class72.aFloatArray769[3]);
		    float f_44_ = (Class72.aFloatArray769[1]
				   / Class72.aFloatArray769[3]);
		    float f_45_ = (Class72.aFloatArray769[2]
				   / Class72.aFloatArray769[3]);
		    float f_46_
			= Class33.method900(f_40_, f_41_, f_42_, f_43_, f_44_,
					    f_45_, 4, 286284420);
		    if (f_46_ > 0.0F) {
			float f_47_ = f_43_ - f_40_;
			float f_48_ = f_45_ - f_42_;
			int i_49_ = (int) (f_47_ * f_46_ + f_40_);
			int i_50_ = (int) (f_42_ + f_48_ * f_46_);
			i_37_
			    = i_49_ + (Class322
					   .aClass654_Sub1_Sub5_Sub1_Sub2_3419
					   .method18545((byte) 1) - 1
				       << 8) >> 9;
			i_38_
			    = i_50_ + (Class322
					   .aClass654_Sub1_Sub5_Sub1_Sub2_3419
					   .method18545((byte) 1) - 1
				       << 8) >> 9;
			int i_51_
			    = (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			       .aByte10854);
			if (i_51_ < 3 && ((client.aClass512_11100.method8552
					   ((byte) 0).aByteArrayArrayArray5145
					   [1][i_49_ >> 9][i_50_ >> 9])
					  & 0x2) != 0)
			    i_51_++;
		    }
		    if (-1 != i_37_ && i_38_ != -1) {
			if (client.aBool11218
			    && ((460977285 * Class200_Sub14.anInt9950 & 0x40)
				!= 0)) {
			    Class247 class247
				= Class81.method1637((-1808298539
						      * Class130.anInt1525),
						     (-985352023
						      * client.anInt11219),
						     2030685651);
			    if (null != class247)
				Class112.method2016
				    (client.aString11221,
				     new StringBuilder().append(" ").append
					 (Class29.aString263).append
					 (" ").toString(),
				     -731032933 * Class460.anInt5069, 59, -1,
				     0L, i_37_, i_38_, true, false,
				     (long) (i_37_ << 0 | i_38_), true,
				     (short) 25225);
			    else
				Exception_Sub3.method17943((byte) 1);
			} else {
			    if (Class665.aBool8573)
				Class112.method2016((Class58.aClass58_636
							 .method1245
						     (Class539.aClass672_7171,
						      (byte) -68)),
						    "", -1, 60, -1, 0L, i_37_,
						    i_38_, true, false,
						    (long) (i_37_ << 0
							    | i_38_),
						    true, (short) 9883);
			    Class112.method2016(Class106.aString1311, "",
						1361725501 * client.anInt11356,
						23, -1, 0L, i_37_, i_38_, true,
						false,
						(long) (i_37_ << 0 | i_38_),
						true, (short) 9974);
			}
		    }
		}
		Class557 class557 = (client.aClass512_11100.method8424
				     ((byte) 25).aClass557_7478);
		int i_52_ = i;
		int i_53_ = i_33_;
		List list = class557.aList7484;
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
		    Class550 class550 = (Class550) iterator.next();
		    if ((client.aBool11057
			 || ((Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			      .aByte10854)
			     == class550.aClass654_Sub1_7303.aByte10854))
			&& class550.method9025(class185, i_52_, i_53_,
					       2088438307)) {
			boolean bool = false;
			boolean bool_54_ = false;
			int i_55_;
			int i_56_;
			if (class550.aClass654_Sub1_7303
			    instanceof Class654_Sub1_Sub5) {
			    i_55_ = ((Class654_Sub1_Sub5)
				     class550.aClass654_Sub1_7303).aShort11900;
			    i_56_ = ((Class654_Sub1_Sub5)
				     class550.aClass654_Sub1_7303).aShort11901;
			} else {
			    Class438 class438
				= (class550.aClass654_Sub1_7303.method10807()
				   .aClass438_4885);
			    i_55_ = (int) class438.aFloat4864 >> 9;
			    i_56_ = (int) class438.aFloat4865 >> 9;
			}
			if (class550.aClass654_Sub1_7303
			    instanceof Class654_Sub1_Sub5_Sub1_Sub2) {
			    Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2
				= ((Class654_Sub1_Sub5_Sub1_Sub2)
				   class550.aClass654_Sub1_7303);
			    int i_57_ = class654_sub1_sub5_sub1_sub2
					    .method18545((byte) 1);
			    Class438 class438
				= (class654_sub1_sub5_sub1_sub2.method10807()
				   .aClass438_4885);
			    if ((0 == (i_57_ & 0x1)
				 && 0 == ((int) class438.aFloat4864 & 0x1ff)
				 && ((int) class438.aFloat4865 & 0x1ff) == 0)
				|| (1 == (i_57_ & 0x1)
				    && 256 == ((int) class438.aFloat4864
					       & 0x1ff)
				    && 256 == ((int) class438.aFloat4865
					       & 0x1ff))) {
				int i_58_ = ((int) class438.aFloat4864
					     - (class654_sub1_sub5_sub1_sub2
						    .method18545((byte) 1) - 1
						<< 8));
				int i_59_ = ((int) class438.aFloat4865
					     - (class654_sub1_sub5_sub1_sub2
						    .method18545((byte) 1) - 1
						<< 8));
				for (int i_60_ = 0;
				     i_60_ < client.anInt11321 * -1125820437;
				     i_60_++) {
				    Class534_Sub6 class534_sub6
					= ((Class534_Sub6)
					   (client.aClass9_11331.method579
					    ((long) client.anIntArray11088
						    [i_60_])));
				    if (null != class534_sub6) {
					Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1
					    = ((Class654_Sub1_Sub5_Sub1_Sub1)
					       class534_sub6.anObject10417);
					if ((client.anInt11101
					     != (class654_sub1_sub5_sub1_sub1
						 .anInt11969) * 1959487047)
					    && (class654_sub1_sub5_sub1_sub1
						.aBool11989)) {
					    Class438 class438_61_
						= (class654_sub1_sub5_sub1_sub1
						       .method10807
						   ().aClass438_4885);
					    int i_62_
						= ((int) (class438_61_
							  .aFloat4864)
						   - ((-1821838479
						       * (class654_sub1_sub5_sub1_sub1
							  .aClass307_12204
							  .anInt3328)) - 1
						      << 8));
					    int i_63_
						= ((int) (class438_61_
							  .aFloat4865)
						   - ((-1821838479
						       * (class654_sub1_sub5_sub1_sub1
							  .aClass307_12204
							  .anInt3328)) - 1
						      << 8));
					    if (i_62_ >= i_58_
						&& ((-1821838479
						     * (class654_sub1_sub5_sub1_sub1
							.aClass307_12204
							.anInt3328))
						    <= ((class654_sub1_sub5_sub1_sub2
							     .method18545
							 ((byte) 1))
							- (i_62_ - i_58_
							   >> 9)))
						&& i_63_ >= i_59_
						&& ((class654_sub1_sub5_sub1_sub1
						     .aClass307_12204
						     .anInt3328) * -1821838479
						    <= ((class654_sub1_sub5_sub1_sub2
							     .method18545
							 ((byte) 1))
							- (i_63_ - i_59_
							   >> 9)))) {
						Class276.method5155
						    (class654_sub1_sub5_sub1_sub1,
						     ((Class322
						       .aClass654_Sub1_Sub5_Sub1_Sub2_3419
						       .aByte10854)
						      != (class550
							  .aClass654_Sub1_7303
							  .aByte10854)),
						     1233308163);
						class654_sub1_sub5_sub1_sub1
						    .anInt11969
						    = (client.anInt11101
						       * -253074569);
					    }
					}
				    }
				}
				int i_64_ = -1843550713 * Class108.anInt1321;
				int[] is = Class108.anIntArray1322;
				for (int i_65_ = 0; i_65_ < i_64_; i_65_++) {
				    Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2_66_
					= (client
					   .aClass654_Sub1_Sub5_Sub1_Sub2Array11279
					   [is[i_65_]]);
				    if ((null
					 != class654_sub1_sub5_sub1_sub2_66_)
					&& ((class654_sub1_sub5_sub1_sub2_66_
					     .anInt11969) * 1959487047
					    != client.anInt11101)
					&& (class654_sub1_sub5_sub1_sub2_66_
					    != class654_sub1_sub5_sub1_sub2)
					&& (class654_sub1_sub5_sub1_sub2_66_
					    .aBool11989)) {
					Class438 class438_67_
					    = (class654_sub1_sub5_sub1_sub2_66_
						   .method10807
					       ().aClass438_4885);
					int i_68_
					    = ((int) class438_67_.aFloat4864
					       - (class654_sub1_sub5_sub1_sub2_66_
						      .method18545((byte) 1) - 1
						  << 8));
					int i_69_
					    = ((int) class438_67_.aFloat4865
					       - (class654_sub1_sub5_sub1_sub2_66_
						      .method18545((byte) 1) - 1
						  << 8));
					if (i_68_ >= i_58_
					    && (class654_sub1_sub5_sub1_sub2_66_
						    .method18545((byte) 1)
						<= (class654_sub1_sub5_sub1_sub2
							.method18545((byte) 1)
						    - (i_68_ - i_58_ >> 9)))
					    && i_69_ >= i_59_
					    && (class654_sub1_sub5_sub1_sub2_66_
						    .method18545((byte) 1)
						<= (class654_sub1_sub5_sub1_sub2
							.method18545((byte) 1)
						    - (i_69_ - i_59_ >> 9)))) {
					    Class294.method5319
						(class654_sub1_sub5_sub1_sub2_66_,
						 ((class550.aClass654_Sub1_7303
						   .aByte10854)
						  != (Class322
						      .aClass654_Sub1_Sub5_Sub1_Sub2_3419
						      .aByte10854)),
						 (byte) 41);
					    class654_sub1_sub5_sub1_sub2_66_
						.anInt11969
						= (-253074569
						   * client.anInt11101);
					}
				    }
				}
			    }
			    if (client.anInt11101
				== (class654_sub1_sub5_sub1_sub2.anInt11969
				    * 1959487047))
				continue;
			    Class294.method5319
				(class654_sub1_sub5_sub1_sub2,
				 (class550.aClass654_Sub1_7303.aByte10854
				  != (Class322
				      .aClass654_Sub1_Sub5_Sub1_Sub2_3419
				      .aByte10854)),
				 (byte) 45);
			    class654_sub1_sub5_sub1_sub2.anInt11969
				= client.anInt11101 * -253074569;
			}
			if (class550.aClass654_Sub1_7303
			    instanceof Class654_Sub1_Sub5_Sub1_Sub1) {
			    Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1
				= ((Class654_Sub1_Sub5_Sub1_Sub1)
				   class550.aClass654_Sub1_7303);
			    if (null != (class654_sub1_sub5_sub1_sub1
					 .aClass307_12204)) {
				Class438 class438
				    = (class654_sub1_sub5_sub1_sub1.method10807
				       ().aClass438_4885);
				if (((((class654_sub1_sub5_sub1_sub1
					.aClass307_12204.anInt3328)
				       * -1821838479)
				      & 0x1) == 0
				     && (((int) class438.aFloat4864 & 0x1ff)
					 == 0)
				     && (((int) class438.aFloat4865 & 0x1ff)
					 == 0))
				    || (1 == (((class654_sub1_sub5_sub1_sub1
						.aClass307_12204.anInt3328)
					       * -1821838479)
					      & 0x1)
					&& 256 == ((int) class438.aFloat4864
						   & 0x1ff)
					&& (((int) class438.aFloat4865 & 0x1ff)
					    == 256))) {
				    int i_70_
					= ((int) class438.aFloat4864
					   - (((class654_sub1_sub5_sub1_sub1
						.aClass307_12204.anInt3328)
					       * -1821838479) - 1
					      << 8));
				    int i_71_
					= ((int) class438.aFloat4865
					   - (((class654_sub1_sub5_sub1_sub1
						.aClass307_12204.anInt3328)
					       * -1821838479) - 1
					      << 8));
				    for (int i_72_ = 0;
					 (i_72_
					  < client.anInt11321 * -1125820437);
					 i_72_++) {
					Class534_Sub6 class534_sub6
					    = ((Class534_Sub6)
					       (client.aClass9_11331.method579
						((long) client.anIntArray11088
							[i_72_])));
					if (class534_sub6 != null) {
					    Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1_73_
						= ((Class654_Sub1_Sub5_Sub1_Sub1)
						   (class534_sub6
						    .anObject10417));
					    if ((client.anInt11101
						 != (1959487047
						     * (class654_sub1_sub5_sub1_sub1_73_
							.anInt11969)))
						&& (class654_sub1_sub5_sub1_sub1_73_
						    != class654_sub1_sub5_sub1_sub1)
						&& (class654_sub1_sub5_sub1_sub1_73_
						    .aBool11989)) {
						Class438 class438_74_
						    = (class654_sub1_sub5_sub1_sub1_73_
							   .method10807
						       ().aClass438_4885);
						int i_75_
						    = ((int) (class438_74_
							      .aFloat4864)
						       - (((class654_sub1_sub5_sub1_sub1_73_
							    .aClass307_12204
							    .anInt3328)
							   * -1821838479) - 1
							  << 8));
						int i_76_
						    = ((int) (class438_74_
							      .aFloat4865)
						       - ((-1821838479
							   * (class654_sub1_sub5_sub1_sub1_73_
							      .aClass307_12204
							      .anInt3328)) - 1
							  << 8));
						if (i_75_ >= i_70_
						    && (((class654_sub1_sub5_sub1_sub1_73_
							  .aClass307_12204
							  .anInt3328)
							 * -1821838479)
							<= (((class654_sub1_sub5_sub1_sub1
							      .aClass307_12204
							      .anInt3328)
							     * -1821838479)
							    - (i_75_ - i_70_
							       >> 9)))
						    && i_76_ >= i_71_
						    && (((class654_sub1_sub5_sub1_sub1_73_
							  .aClass307_12204
							  .anInt3328)
							 * -1821838479)
							<= ((-1821838479
							     * (class654_sub1_sub5_sub1_sub1
								.aClass307_12204
								.anInt3328))
							    - (i_76_ - i_71_
							       >> 9)))) {
						    Class276.method5155
							(class654_sub1_sub5_sub1_sub1_73_,
							 ((class550
							   .aClass654_Sub1_7303
							   .aByte10854)
							  != (Class322
							      .aClass654_Sub1_Sub5_Sub1_Sub2_3419
							      .aByte10854)),
							 1328670535);
						    class654_sub1_sub5_sub1_sub1_73_
							.anInt11969
							= (-253074569
							   * (client
							      .anInt11101));
						}
					    }
					}
				    }
				    int i_77_
					= -1843550713 * Class108.anInt1321;
				    int[] is = Class108.anIntArray1322;
				    for (int i_78_ = 0; i_78_ < i_77_;
					 i_78_++) {
					Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2
					    = (client
					       .aClass654_Sub1_Sub5_Sub1_Sub2Array11279
					       [is[i_78_]]);
					if ((class654_sub1_sub5_sub1_sub2
					     != null)
					    && ((1959487047
						 * (class654_sub1_sub5_sub1_sub2
						    .anInt11969))
						!= client.anInt11101)
					    && (class654_sub1_sub5_sub1_sub2
						.aBool11989)) {
					    Class438 class438_79_
						= (class654_sub1_sub5_sub1_sub2
						       .method10807
						   ().aClass438_4885);
					    int i_80_
						= ((int) (class438_79_
							  .aFloat4864)
						   - ((class654_sub1_sub5_sub1_sub2
							   .method18545
						       ((byte) 1)) - 1
						      << 8));
					    int i_81_
						= ((int) (class438_79_
							  .aFloat4865)
						   - ((class654_sub1_sub5_sub1_sub2
							   .method18545
						       ((byte) 1)) - 1
						      << 8));
					    if (i_80_ >= i_70_
						&& (class654_sub1_sub5_sub1_sub2
							.method18545((byte) 1)
						    <= (((class654_sub1_sub5_sub1_sub1
							  .aClass307_12204
							  .anInt3328)
							 * -1821838479)
							- (i_80_ - i_70_
							   >> 9)))
						&& i_81_ >= i_71_
						&& (class654_sub1_sub5_sub1_sub2
							.method18545((byte) 1)
						    <= (((class654_sub1_sub5_sub1_sub1
							  .aClass307_12204
							  .anInt3328)
							 * -1821838479)
							- (i_81_ - i_71_
							   >> 9)))) {
						Class294.method5319
						    (class654_sub1_sub5_sub1_sub2,
						     ((Class322
						       .aClass654_Sub1_Sub5_Sub1_Sub2_3419
						       .aByte10854)
						      != (class550
							  .aClass654_Sub1_7303
							  .aByte10854)),
						     (byte) 37);
						class654_sub1_sub5_sub1_sub2
						    .anInt11969
						    = (-253074569
						       * client.anInt11101);
					    }
					}
				    }
				}
				if ((1959487047
				     * class654_sub1_sub5_sub1_sub1.anInt11969)
				    == client.anInt11101)
				    continue;
				Class276.method5155
				    (class654_sub1_sub5_sub1_sub1,
				     ((Class322
				       .aClass654_Sub1_Sub5_Sub1_Sub2_3419
				       .aByte10854)
				      != (class550.aClass654_Sub1_7303
					  .aByte10854)),
				     1072564693);
				class654_sub1_sub5_sub1_sub1.anInt11969
				    = -253074569 * client.anInt11101;
			    }
			}
			if (class550.aClass654_Sub1_7303
			    instanceof Class654_Sub1_Sub4_Sub1) {
			    int i_82_
				= i_55_ + class597.anInt7859 * -424199969;
			    int i_83_
				= i_56_ + class597.anInt7861 * -1166289421;
			    Class534_Sub7 class534_sub7
				= ((Class534_Sub7)
				   (client.aClass9_11209.method579
				    ((long) ((class550.aClass654_Sub1_7303
					      .aByte10854) << 28
					     | i_83_ << 14 | i_82_))));
			    if (null != class534_sub7) {
				int i_84_ = 0;
				Class534_Sub23 class534_sub23
				    = ((Class534_Sub23)
				       class534_sub7.aClass700_10418
					   .method14137((byte) -94));
				while (class534_sub23 != null) {
				    Class15 class15
					= ((Class15)
					   (Class531.aClass44_Sub7_7135
						.method91
					    ((-400233975
					      * class534_sub23.anInt10548),
					     -115377055)));
				    int i_85_;
				    if (class15.aBool181)
					i_85_ = 510229545 * class15.anInt182;
				    else if (class15.aBool106)
					i_85_ = 1376086885 * (Class700
							      .aClass638_8806
							      .anInt8306);
				    else
					i_85_ = (Class700.aClass638_8806
						 .anInt8314) * -629529995;
				    if (client.aBool11218
					&& ((class550.aClass654_Sub1_7303
					     .aByte10854)
					    == (Class322
						.aClass654_Sub1_Sub5_Sub1_Sub2_3419
						.aByte10854))) {
					Class90 class90
					    = ((Class90)
					       ((Class392_Sub1.anInt10224
						 * 410979031) != -1
						? (Class534_Sub11_Sub13
						       .aClass44_Sub22_11730
						       .method91
						   (410979031 * (Class392_Sub1
								 .anInt10224),
						    -1968220311))
						: null));
					if (((460977285
					      * Class200_Sub14.anInt9950)
					     & 0x1) != 0
					    && (class90 == null
						|| ((class15.method683
						     ((Class392_Sub1.anInt10224
						       * 410979031),
						      (class90.anInt888
						       * 263946597),
						      (byte) 75))
						    != (263946597
							* class90.anInt888))))
					    Class112.method2016
						(client.aString11221,
						 new StringBuilder().append
						     (client.aString11222)
						     .append
						     (" ").append
						     (Class29.aString263)
						     .append
						     (" ").append
						     (Class154.method2575
						      (i_85_, 252547774))
						     .append
						     (class15.aString122)
						     .toString(),
						 (-731032933
						  * Class460.anInt5069),
						 17, -1,
						 (long) (-400233975
							 * (class534_sub23
							    .anInt10548)),
						 i_55_, i_56_, true, false,
						 (long) i_84_, false,
						 (short) 23037);
				    }
				    if (class550.aClass654_Sub1_7303.aByte10854
					== (Class322
					    .aClass654_Sub1_Sub5_Sub1_Sub2_3419
					    .aByte10854)) {
					String[] strings
					    = class15.aStringArray133;
					for (int i_86_ = strings.length - 1;
					     i_86_ >= 0; i_86_--) {
					    if (null != strings[i_86_]) {
						int i_87_ = 0;
						int i_88_ = (client.anInt11216
							     * 2068410197);
						if (i_86_ == 0)
						    i_87_ = 18;
						if (i_86_ == 1)
						    i_87_ = 19;
						if (2 == i_86_)
						    i_87_ = 20;
						if (i_86_ == 3)
						    i_87_ = 21;
						if (i_86_ == 4)
						    i_87_ = 22;
						if (5 == i_86_)
						    i_87_ = 1004;
						int i_89_ = (class15.method701
							     (i_86_, 2681600));
						if (i_89_ != -1)
						    i_88_ = i_89_;
						Class112.method2016
						    (strings[i_86_],
						     new StringBuilder().append
							 (Class154.method2575
							  (i_85_, 1601270780))
							 .append
							 (class15.aString122)
							 .toString(),
						     i_88_, i_87_, -1,
						     (long) (-400233975
							     * (class534_sub23
								.anInt10548)),
						     i_55_, i_56_, true, false,
						     (long) i_84_, false,
						     (short) 12597);
					    }
					}
				    }
				    class534_sub23
					= ((Class534_Sub23)
					   class534_sub7.aClass700_10418
					       .method14140((byte) 7));
				    i_84_++;
				}
			    }
			}
			if (class550.aClass654_Sub1_7303
			    instanceof Interface62) {
			    Interface62 interface62
				= (Interface62) class550.aClass654_Sub1_7303;
			    Class602 class602
				= ((Class602)
				   (client.aClass512_11100.method8428
					(-1486655428).method91
				    (interface62.method56(1268438851),
				     -1619211514)));
			    if (null != class602.anIntArray7943)
				class602
				    = class602.method9988((Class78
							   .aClass103_825),
							  (Class78
							   .aClass103_825),
							  1285711490);
			    if (class602 != null) {
				if (client.aBool11218
				    && (class550.aClass654_Sub1_7303.aByte10854
					== (Class322
					    .aClass654_Sub1_Sub5_Sub1_Sub2_3419
					    .aByte10854))) {
				    Class90 class90
					= ((Class90)
					   ((Class392_Sub1.anInt10224
					     * 410979031) != -1
					    ? (Class534_Sub11_Sub13
						   .aClass44_Sub22_11730
						   .method91
					       ((Class392_Sub1.anInt10224
						 * 410979031),
						1440714210))
					    : null));
				    if ((Class200_Sub14.anInt9950 * 460977285
					 & 0x4) != 0
					&& (null == class90
					    || ((class602.method9989
						 ((Class392_Sub1.anInt10224
						   * 410979031),
						  263946597 * class90.anInt888,
						  (byte) 58))
						!= (class90.anInt888
						    * 263946597))))
					Class112.method2016
					    (client.aString11221,
					     new StringBuilder().append
						 (client.aString11222).append
						 (" ").append
						 (Class29.aString263).append
						 (" ").append
						 (Class154.method2575
						  (65535, -98984067))
						 .append
						 (class602.aString7892)
						 .toString(),
					     -731032933 * Class460.anInt5069,
					     2, -1,
					     Class551.method9046(interface62,
								 i_55_, i_56_,
								 468659320),
					     i_55_, i_56_, true, false,
					     (long) interface62.hashCode(),
					     false, (short) 28074);
				}
				if ((Class322
				     .aClass654_Sub1_Sub5_Sub1_Sub2_3419
				     .aByte10854)
				    == (class550.aClass654_Sub1_7303
					.aByte10854)) {
				    String[] strings
					= class602.aStringArray7920;
				    if (null != strings) {
					for (int i_90_ = strings.length - 1;
					     i_90_ >= 0; i_90_--) {
					    if (strings[i_90_] != null) {
						int i_91_ = 0;
						int i_92_ = (client.anInt11216
							     * 2068410197);
						if (0 == i_90_)
						    i_91_ = 3;
						if (i_90_ == 1)
						    i_91_ = 4;
						if (i_90_ == 2)
						    i_91_ = 5;
						if (i_90_ == 3)
						    i_91_ = 6;
						if (i_90_ == 4)
						    i_91_ = 1001;
						if (5 == i_90_)
						    i_91_ = 1002;
						int i_93_
						    = (class602.method9985
						       (i_90_, 2018481806));
						if (-1 != i_93_)
						    i_92_ = i_93_;
						Class112.method2016
						    (strings[i_90_],
						     new StringBuilder().append
							 (Class154.method2575
							  (65535, 774339046))
							 .append
							 (class602.aString7892)
							 .toString(),
						     i_92_, i_91_, -1,
						     (Class551.method9046
						      (interface62, i_55_,
						       i_56_, 468659320)),
						     i_55_, i_56_, true, false,
						     (long) interface62
								.hashCode(),
						     false, (short) 27063);
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
    }
    
    static final void method1177(Class669 class669, int i) {
	if ((Class599.aClass298_Sub1_7871.method5388((byte) 15)
	     != Class305.aClass305_3265)
	    || (Class599.aClass298_Sub1_7871.method5425(663620008)
		!= Class293.aClass293_3125))
	    throw new RuntimeException();
	Class438 class438
	    = ((Class347_Sub1)
	       Class599.aClass298_Sub1_7871.method5380((byte) -52))
		  .method15760(-1461413436);
	Class438 class438_94_
	    = ((Class706_Sub4)
	       Class599.aClass298_Sub1_7871.method5381(1922503418))
		  .method17300((byte) -31);
	Class438 class438_95_ = Class438.method6994(class438);
	class438_95_.method7006(class438_94_);
	float f = Class455.method7423(class438_95_.aFloat4864,
				      class438_95_.aFloat4865, (byte) 20);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (int) (2607.5945876176133 * (double) f) & 0x3fff;
    }
    
    static final void method1178(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub10_10751
		  .method16966(-1705363794) ? 1 : 0;
    }
    
    public static int method1179(int i, int i_96_, boolean bool,
				 boolean bool_97_, int i_98_) {
	Class534_Sub5 class534_sub5
	    = Class269.method5023(i, bool_97_, (byte) 78);
	if (null == class534_sub5)
	    return 0;
	int i_99_ = 0;
	for (int i_100_ = 0; i_100_ < class534_sub5.anIntArray10414.length;
	     i_100_++) {
	    if (class534_sub5.anIntArray10414[i_100_] >= 0
		&& (class534_sub5.anIntArray10414[i_100_]
		    < 888398261 * Class531.aClass44_Sub7_7135.anInt327)) {
		Class15 class15
		    = ((Class15)
		       Class531.aClass44_Sub7_7135.method91((class534_sub5
							     .anIntArray10414
							     [i_100_]),
							    682273682));
		int i_101_
		    = class15.method683(i_96_,
					(((Class90)
					  Class534_Sub11_Sub13
					      .aClass44_Sub22_11730
					      .method91(i_96_, 104776986))
					 .anInt888) * 263946597,
					(byte) 71);
		if (bool)
		    i_99_ += class534_sub5.anIntArray10415[i_100_] * i_101_;
		else
		    i_99_ += i_101_;
	    }
	}
	return i_99_;
    }
    
    public static void method1180(Class534_Sub18 class534_sub18,
				  Class534_Sub18 class534_sub18_102_, byte i) {
	if (null != class534_sub18.aClass534_Sub18_10508)
	    class534_sub18.method16180(-421776830);
	class534_sub18.aClass534_Sub18_10508 = class534_sub18_102_;
	class534_sub18.aClass534_Sub18_10510
	    = class534_sub18_102_.aClass534_Sub18_10510;
	class534_sub18.aClass534_Sub18_10508.aClass534_Sub18_10510
	    = class534_sub18;
	class534_sub18.aClass534_Sub18_10510.aClass534_Sub18_10508
	    = class534_sub18;
    }
}
