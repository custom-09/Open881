/* Class500 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.awt.Canvas;

import jaggl.OpenGL;

public class Class500
{
    public static Class185 method8274
	(Canvas canvas, Class177 class177, Interface25 interface25,
	 Interface45 interface45, Interface48 interface48,
	 Interface46 interface46, Class472 class472, int i) {
	Class185_Sub1_Sub2 class185_sub1_sub2;
	try {
	    Class185_Sub1.method14906();
	    Class112.method2018(-1327590673).method400("jaggl", -1425884034);
	    Class481.method7927(canvas, -1298746079);
	    OpenGL opengl = new OpenGL();
	    long l = opengl.init(canvas, 8, 8, 8, 24, 0, i);
	    if (l == 0L)
		throw new RuntimeException("");
	    Class185_Sub1_Sub2 class185_sub1_sub2_0_
		= new Class185_Sub1_Sub2(opengl, canvas, l, class177,
					 interface25, interface45, interface48,
					 interface46, class472, i);
	    class185_sub1_sub2_0_.method14594();
	    class185_sub1_sub2 = class185_sub1_sub2_0_;
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Throwable throwable) {
	    throw new RuntimeException("");
	}
	return class185_sub1_sub2;
    }
    
    Class500() throws Throwable {
	throw new Error();
    }
    
    public static Class185 method8275
	(Canvas canvas, Class177 class177, Interface25 interface25,
	 Interface45 interface45, Interface48 interface48,
	 Interface46 interface46, Class472 class472, int i) {
	Class185_Sub1_Sub2 class185_sub1_sub2;
	try {
	    Class185_Sub1.method14906();
	    Class112.method2018(-1327590673).method400("jaggl", 211275496);
	    Class481.method7927(canvas, -1470277094);
	    OpenGL opengl = new OpenGL();
	    long l = opengl.init(canvas, 8, 8, 8, 24, 0, i);
	    if (l == 0L)
		throw new RuntimeException("");
	    Class185_Sub1_Sub2 class185_sub1_sub2_1_
		= new Class185_Sub1_Sub2(opengl, canvas, l, class177,
					 interface25, interface45, interface48,
					 interface46, class472, i);
	    class185_sub1_sub2_1_.method14594();
	    class185_sub1_sub2 = class185_sub1_sub2_1_;
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Throwable throwable) {
	    throw new RuntimeException("");
	}
	return class185_sub1_sub2;
    }
    
    public static Class185 method8276
	(Canvas canvas, Class177 class177, Interface25 interface25,
	 Interface45 interface45, Interface48 interface48,
	 Interface46 interface46, Class472 class472, int i) {
	Class185_Sub1_Sub2 class185_sub1_sub2;
	try {
	    Class185_Sub1.method14906();
	    Class112.method2018(-1327590673).method400("jaggl", -1574131752);
	    Class481.method7927(canvas, -1793602145);
	    OpenGL opengl = new OpenGL();
	    long l = opengl.init(canvas, 8, 8, 8, 24, 0, i);
	    if (l == 0L)
		throw new RuntimeException("");
	    Class185_Sub1_Sub2 class185_sub1_sub2_2_
		= new Class185_Sub1_Sub2(opengl, canvas, l, class177,
					 interface25, interface45, interface48,
					 interface46, class472, i);
	    class185_sub1_sub2_2_.method14594();
	    class185_sub1_sub2 = class185_sub1_sub2_2_;
	} catch (RuntimeException runtimeexception) {
	    throw runtimeexception;
	} catch (Throwable throwable) {
	    throw new RuntimeException("");
	}
	return class185_sub1_sub2;
    }
}
