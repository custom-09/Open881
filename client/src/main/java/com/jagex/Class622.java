/* Class622 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.io.IOException;

public class Class622
{
    public int anInt8127;
    public static final int anInt8128 = 1;
    public static final int anInt8129 = 2;
    public static final int anInt8130 = 3;
    public static final int anInt8131 = 0;
    public static final int anInt8132 = 1;
    public int anInt8133;
    public static final int anInt8134 = 3;
    static final int anInt8135 = 100;
    public static final int anInt8136 = 5;
    static final int anInt8137 = 3;
    public int anInt8138;
    public static final int anInt8139 = 4;
    public static final int anInt8140 = 2;
    public int anInt8141;
    public int anInt8142;
    public int anInt8143;
    public static final int anInt8144 = 0;
    public int anInt8145;
    public int[][] anIntArrayArray8146 = new int[3][5];
    
    void method10284(Class534_Sub40 class534_sub40, int i) {
	for (;;) {
	    int i_0_ = class534_sub40.method16527(-80590536);
	    if (i_0_ == 0)
		break;
	    if (1 == i_0_)
		anInt8138 = class534_sub40.method16533(-258848859) * 619929269;
	    else if (2 == i_0_)
		anInt8127
		    = class534_sub40.method16533(-258848859) * -448964977;
	    else if (3 == i_0_)
		anInt8141 = class534_sub40.method16533(-258848859) * 458017283;
	    else if (i_0_ == 4)
		anInt8142
		    = class534_sub40.method16527(-1856895531) * -1992331723;
	    else if (5 == i_0_)
		anInt8143
		    = class534_sub40.method16527(-1232041083) * -1891901395;
	    else if (6 == i_0_)
		anInt8133
		    = class534_sub40.method16533(-258848859) * -1139256357;
	    else if (7 == i_0_)
		anInt8145 = class534_sub40.method16533(-258848859) * 15357739;
	    else if (i_0_ >= 100) {
		i_0_ -= 100;
		anIntArrayArray8146[i_0_ & (int) (Math.pow(2.0, 3.0)
						  - 1.0)][i_0_ >> 3]
		    = class534_sub40.method16529((byte) 1);
	    }
	}
    }
    
    public Class622(Class472 class472) {
	byte[] is = class472.method7738((Class617.aClass617_8095.anInt8096
					 * -448671533),
					(byte) -22);
	if (is != null) {
	    /* empty */
	}
	method10284(new Class534_Sub40(is), 2123222156);
    }
    
    void method10285(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-193580654);
	    if (i == 0)
		break;
	    if (1 == i)
		anInt8138 = class534_sub40.method16533(-258848859) * 619929269;
	    else if (2 == i)
		anInt8127
		    = class534_sub40.method16533(-258848859) * -448964977;
	    else if (3 == i)
		anInt8141 = class534_sub40.method16533(-258848859) * 458017283;
	    else if (i == 4)
		anInt8142
		    = class534_sub40.method16527(-1192984025) * -1992331723;
	    else if (5 == i)
		anInt8143
		    = class534_sub40.method16527(922581425) * -1891901395;
	    else if (6 == i)
		anInt8133
		    = class534_sub40.method16533(-258848859) * -1139256357;
	    else if (7 == i)
		anInt8145 = class534_sub40.method16533(-258848859) * 15357739;
	    else if (i >= 100) {
		i -= 100;
		anIntArrayArray8146[i & (int) (Math.pow(2.0, 3.0)
					       - 1.0)][i >> 3]
		    = class534_sub40.method16529((byte) 1);
	    }
	}
    }
    
    void method10286(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-1054423188);
	    if (i == 0)
		break;
	    if (1 == i)
		anInt8138 = class534_sub40.method16533(-258848859) * 619929269;
	    else if (2 == i)
		anInt8127
		    = class534_sub40.method16533(-258848859) * -448964977;
	    else if (3 == i)
		anInt8141 = class534_sub40.method16533(-258848859) * 458017283;
	    else if (i == 4)
		anInt8142
		    = class534_sub40.method16527(1595164429) * -1992331723;
	    else if (5 == i)
		anInt8143
		    = class534_sub40.method16527(1317337298) * -1891901395;
	    else if (6 == i)
		anInt8133
		    = class534_sub40.method16533(-258848859) * -1139256357;
	    else if (7 == i)
		anInt8145 = class534_sub40.method16533(-258848859) * 15357739;
	    else if (i >= 100) {
		i -= 100;
		anIntArrayArray8146[i & (int) (Math.pow(2.0, 3.0)
					       - 1.0)][i >> 3]
		    = class534_sub40.method16529((byte) 1);
	    }
	}
    }
    
    static final void method10287(Class669 class669, int i) {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub3_10767,
	     (class669.anIntArray8595
	      [(class669.anInt8600 -= 308999563) * 2088438307]) == 1 ? 2 : 0,
	     685132623);
	client.aClass512_11100.method8441(1431096479);
	Class672.method11096((byte) 1);
	client.aBool11048 = false;
    }
    
    static final void method10288(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = -1;
    }
    
    static final void method10289(Class247 class247, Class243 class243,
				  Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	if (Class546.method8989(string, class669, -219410121) != null)
	    string = string.substring(0, string.length() - 1);
	class247.anObjectArray2491
	    = Class99.method1859(string, class669, -753127554);
	class247.aBool2561 = true;
    }
    
    static Class534_Sub42_Sub2 method10290(int i, int i_1_, int i_2_, long l,
					   int i_3_, int i_4_) {
	synchronized (Class534_Sub42_Sub2.aClass534_Sub42_Sub2Array11877) {
	    Class534_Sub42_Sub2 class534_sub42_sub2;
	    if (-1194496119 * Class534_Sub18_Sub3.anInt11375 == 0)
		class534_sub42_sub2 = new Class534_Sub42_Sub2();
	    else
		class534_sub42_sub2
		    = (Class534_Sub42_Sub2.aClass534_Sub42_Sub2Array11877
		       [((Class534_Sub18_Sub3.anInt11375 -= -1383614791)
			 * -1194496119)]);
	    class534_sub42_sub2.anInt11876 = i * -1091003251;
	    class534_sub42_sub2.anInt11878 = -397312265 * i_1_;
	    class534_sub42_sub2.anInt11879 = i_2_ * -374658177;
	    class534_sub42_sub2.aLong11880 = 2565003256494833653L * l;
	    class534_sub42_sub2.anInt11881 = 674620361 * i_3_;
	    Class534_Sub42_Sub2 class534_sub42_sub2_5_ = class534_sub42_sub2;
	    return class534_sub42_sub2_5_;
	}
    }
    
    public static final void method10291(boolean bool, int i) {
	Class100[] class100s = client.aClass100Array11096;
	for (int i_6_ = 0; i_6_ < class100s.length; i_6_++) {
	    Class100 class100 = class100s[i_6_];
	    try {
		class100.method1868(1805858475);
	    } catch (IOException ioexception) {
		/* empty */
	    }
	    class100.method1866((byte) -33);
	}
	Class440.method7101(1327637973);
	Class53.method1193((byte) 7);
	Class11.method611(false, -799160718);
	client.aClass512_11100.method8455(1881099737);
	client.aClass512_11100.method8434(1508032778);
	client.aClass512_11100.method8438(245176744);
	Class176.method2929(-2024655746);
	Class269.method5020(357558912);
	Class278.method5224(true, -901785400);
	Class229.method4208(-1986053454);
	Class513.method8582(1087795413);
	client.anInt11037 = 2083494349;
	if (bool)
	    Class673.method11110(14, 609777370);
	else {
	    Class673.method11110(15, 130685367);
	    try {
		Class31.method887(Class305.anApplet3271, "loggedout",
				  -1405577713);
	    } catch (Throwable throwable) {
		/* empty */
	    }
	}
    }
}
