/* Class314_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class314_Sub1 extends Class314
{
    Class278 aClass278_9992;
    Class534_Sub12_Sub1 aClass534_Sub12_Sub1_9993;
    Class269 aClass269_9994;
    protected static Class38[] aClass38Array9995;
    
    public void method5694() {
	aClass269_9994.method4913();
	aClass269_9994.method4933(aClass534_Sub12_Sub1_9993, 0,
				  anInterface38_3375, 1353342427);
    }
    
    boolean method15639(byte i) throws Exception_Sub4 {
	aClass269_9994 = aClass185_Sub1_3376.method14589("BatchedSprite");
	aClass534_Sub12_Sub1_9993
	    = aClass269_9994.method4909("SpriteSampler", 466774272);
	aClass278_9992 = aClass269_9994.method4914("Normal", -1370128325);
	aClass269_9994.method4919(aClass278_9992);
	return true;
    }
    
    public void method5693() {
	aClass269_9994.method4913();
	aClass269_9994.method4933(aClass534_Sub12_Sub1_9993, 0,
				  anInterface38_3375, -1576830247);
    }
    
    boolean method15640() throws Exception_Sub4 {
	aClass269_9994 = aClass185_Sub1_3376.method14589("BatchedSprite");
	aClass534_Sub12_Sub1_9993
	    = aClass269_9994.method4909("SpriteSampler", 123093375);
	aClass278_9992 = aClass269_9994.method4914("Normal", -2075559237);
	aClass269_9994.method4919(aClass278_9992);
	return true;
    }
    
    public void method5692() {
	aClass269_9994.method4913();
	aClass269_9994.method4933(aClass534_Sub12_Sub1_9993, 0,
				  anInterface38_3375, 507575950);
    }
    
    boolean method15641() throws Exception_Sub4 {
	aClass269_9994 = aClass185_Sub1_3376.method14589("BatchedSprite");
	aClass534_Sub12_Sub1_9993
	    = aClass269_9994.method4909("SpriteSampler", 71566610);
	aClass278_9992 = aClass269_9994.method4914("Normal", -1312183991);
	aClass269_9994.method4919(aClass278_9992);
	return true;
    }
    
    boolean method15642() throws Exception_Sub4 {
	aClass269_9994 = aClass185_Sub1_3376.method14589("BatchedSprite");
	aClass534_Sub12_Sub1_9993
	    = aClass269_9994.method4909("SpriteSampler", 1425573866);
	aClass278_9992 = aClass269_9994.method4914("Normal", -1732725303);
	aClass269_9994.method4919(aClass278_9992);
	return true;
    }
    
    public Class314_Sub1(Class185_Sub1 class185_sub1) throws Exception_Sub4 {
	super(class185_sub1);
	method15639((byte) 8);
    }
    
    static final void method15643(Class669 class669, int i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_0_, 1849953365);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_0_ >> 16];
	Class690_Sub24.method17100(class247, class243, class669, -228283357);
    }
}
