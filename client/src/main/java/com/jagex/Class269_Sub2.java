/* Class269_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public final class Class269_Sub2 extends Class269
{
    boolean aBool10112;
    Class278_Sub2 aClass278_Sub2_10113;
    Class185_Sub1_Sub1 aClass185_Sub1_Sub1_10114;
    
    Class269_Sub2(Class185_Sub1_Sub1 class185_sub1_sub1, Class261 class261) {
	super((Class185_Sub1) class185_sub1_sub1, class261);
	aClass185_Sub1_Sub1_10114 = class185_sub1_sub1;
	aBool10112 = false;
    }
    
    Class278 method4958(Class185_Sub1 class185_sub1, Class277 class277) {
	return new Class278_Sub2((Class185_Sub1_Sub1) class185_sub1, this,
				 class277);
    }
    
    public boolean method4919(Class278 class278) {
	if (aClass278_Sub2_10113 == class278)
	    return true;
	if (!class278.method5182())
	    return false;
	aClass278_Sub2_10113 = (Class278_Sub2) class278;
	anInt2953 = method4917(class278, (byte) -3) * -1164303385;
	if (anInt2953 * 567951319 == -1)
	    throw new IllegalArgumentException();
	if (aBool10112) {
	    aClass185_Sub1_Sub1_10114
		.method18009(aClass278_Sub2_10113.aLong10118);
	    aClass185_Sub1_Sub1_10114
		.method18025(aClass278_Sub2_10113.aLong10119);
	    aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523
		= aClass278_Sub2_10113;
	}
	return true;
    }
    
    Class534_Sub12_Sub1 method4922(Class266 class266) {
	return new Class534_Sub12_Sub1_Sub2(this, class266);
    }
    
    public void method5015() {
	if (aClass278_Sub2_10113 == null)
	    throw new RuntimeException_Sub3();
	aClass185_Sub1_Sub1_10114.method18009(aClass278_Sub2_10113.aLong10118);
	aClass185_Sub1_Sub1_10114.method18025(aClass278_Sub2_10113.aLong10119);
	aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523 = aClass278_Sub2_10113;
	aBool10112 = true;
    }
    
    public void method4910() {
	aClass185_Sub1_Sub1_10114.method18009(0L);
	aClass185_Sub1_Sub1_10114.method18025(0L);
	aBool10112 = false;
	aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523 = null;
	aClass185_Sub1_Sub1_10114.method14668(1);
	aClass185_Sub1_Sub1_10114.method14669(null);
	aClass185_Sub1_Sub1_10114.method14668(0);
	aClass185_Sub1_Sub1_10114.method14669(null);
    }
    
    public boolean method4911() {
	return (aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523
		== aClass278_Sub2_10113);
    }
    
    public void method4950() {
	aClass185_Sub1_Sub1_10114.method18009(0L);
	aClass185_Sub1_Sub1_10114.method18025(0L);
	aBool10112 = false;
	aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523 = null;
	aClass185_Sub1_Sub1_10114.method14668(1);
	aClass185_Sub1_Sub1_10114.method14669(null);
	aClass185_Sub1_Sub1_10114.method14668(0);
	aClass185_Sub1_Sub1_10114.method14669(null);
    }
    
    public void method4962() {
	aClass185_Sub1_Sub1_10114.method18009(0L);
	aClass185_Sub1_Sub1_10114.method18025(0L);
	aBool10112 = false;
	aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523 = null;
	aClass185_Sub1_Sub1_10114.method14668(1);
	aClass185_Sub1_Sub1_10114.method14669(null);
	aClass185_Sub1_Sub1_10114.method14668(0);
	aClass185_Sub1_Sub1_10114.method14669(null);
    }
    
    public void method4972() {
	aClass185_Sub1_Sub1_10114.method18009(0L);
	aClass185_Sub1_Sub1_10114.method18025(0L);
	aBool10112 = false;
	aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523 = null;
	aClass185_Sub1_Sub1_10114.method14668(1);
	aClass185_Sub1_Sub1_10114.method14669(null);
	aClass185_Sub1_Sub1_10114.method14668(0);
	aClass185_Sub1_Sub1_10114.method14669(null);
    }
    
    public void method5018() {
	if (aClass278_Sub2_10113 == null)
	    throw new RuntimeException_Sub3();
	aClass185_Sub1_Sub1_10114.method18009(aClass278_Sub2_10113.aLong10118);
	aClass185_Sub1_Sub1_10114.method18025(aClass278_Sub2_10113.aLong10119);
	aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523 = aClass278_Sub2_10113;
	aBool10112 = true;
    }
    
    public boolean method4953() {
	return (aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523
		== aClass278_Sub2_10113);
    }
    
    Class278 method4956(Class185_Sub1 class185_sub1, Class277 class277) {
	return new Class278_Sub2((Class185_Sub1_Sub1) class185_sub1, this,
				 class277);
    }
    
    Class278 method5012(Class185_Sub1 class185_sub1, Class277 class277) {
	return new Class278_Sub2((Class185_Sub1_Sub1) class185_sub1, this,
				 class277);
    }
    
    Class278 method4957(Class185_Sub1 class185_sub1, Class277 class277) {
	return new Class278_Sub2((Class185_Sub1_Sub1) class185_sub1, this,
				 class277);
    }
    
    Class278 method4959(Class185_Sub1 class185_sub1, Class277 class277) {
	return new Class278_Sub2((Class185_Sub1_Sub1) class185_sub1, this,
				 class277);
    }
    
    public boolean method4970(Class278 class278) {
	if (aClass278_Sub2_10113 == class278)
	    return true;
	if (!class278.method5182())
	    return false;
	aClass278_Sub2_10113 = (Class278_Sub2) class278;
	anInt2953 = method4917(class278, (byte) 112) * -1164303385;
	if (anInt2953 * 567951319 == -1)
	    throw new IllegalArgumentException();
	if (aBool10112) {
	    aClass185_Sub1_Sub1_10114
		.method18009(aClass278_Sub2_10113.aLong10118);
	    aClass185_Sub1_Sub1_10114
		.method18025(aClass278_Sub2_10113.aLong10119);
	    aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523
		= aClass278_Sub2_10113;
	}
	return true;
    }
    
    public void method5001() {
	if (aClass278_Sub2_10113 == null)
	    throw new RuntimeException_Sub3();
	aClass185_Sub1_Sub1_10114.method18009(aClass278_Sub2_10113.aLong10118);
	aClass185_Sub1_Sub1_10114.method18025(aClass278_Sub2_10113.aLong10119);
	aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523 = aClass278_Sub2_10113;
	aBool10112 = true;
    }
    
    public void method4913() {
	if (aClass278_Sub2_10113 == null)
	    throw new RuntimeException_Sub3();
	aClass185_Sub1_Sub1_10114.method18009(aClass278_Sub2_10113.aLong10118);
	aClass185_Sub1_Sub1_10114.method18025(aClass278_Sub2_10113.aLong10119);
	aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523 = aClass278_Sub2_10113;
	aBool10112 = true;
    }
    
    public boolean method4969(Class278 class278) {
	if (aClass278_Sub2_10113 == class278)
	    return true;
	if (!class278.method5182())
	    return false;
	aClass278_Sub2_10113 = (Class278_Sub2) class278;
	anInt2953 = method4917(class278, (byte) -66) * -1164303385;
	if (anInt2953 * 567951319 == -1)
	    throw new IllegalArgumentException();
	if (aBool10112) {
	    aClass185_Sub1_Sub1_10114
		.method18009(aClass278_Sub2_10113.aLong10118);
	    aClass185_Sub1_Sub1_10114
		.method18025(aClass278_Sub2_10113.aLong10119);
	    aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523
		= aClass278_Sub2_10113;
	}
	return true;
    }
    
    public boolean method4968(Class278 class278) {
	if (aClass278_Sub2_10113 == class278)
	    return true;
	if (!class278.method5182())
	    return false;
	aClass278_Sub2_10113 = (Class278_Sub2) class278;
	anInt2953 = method4917(class278, (byte) -88) * -1164303385;
	if (anInt2953 * 567951319 == -1)
	    throw new IllegalArgumentException();
	if (aBool10112) {
	    aClass185_Sub1_Sub1_10114
		.method18009(aClass278_Sub2_10113.aLong10118);
	    aClass185_Sub1_Sub1_10114
		.method18025(aClass278_Sub2_10113.aLong10119);
	    aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523
		= aClass278_Sub2_10113;
	}
	return true;
    }
    
    Class534_Sub12_Sub1 method4915(Class266 class266) {
	return new Class534_Sub12_Sub1_Sub2(this, class266);
    }
    
    public boolean method5004() {
	return (aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523
		== aClass278_Sub2_10113);
    }
    
    Class278 method4960(Class185_Sub1 class185_sub1, Class277 class277) {
	return new Class278_Sub2((Class185_Sub1_Sub1) class185_sub1, this,
				 class277);
    }
    
    public void method5016() {
	if (aClass278_Sub2_10113 == null)
	    throw new RuntimeException_Sub3();
	aClass185_Sub1_Sub1_10114.method18009(aClass278_Sub2_10113.aLong10118);
	aClass185_Sub1_Sub1_10114.method18025(aClass278_Sub2_10113.aLong10119);
	aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523 = aClass278_Sub2_10113;
	aBool10112 = true;
    }
    
    public void method5017() {
	if (aClass278_Sub2_10113 == null)
	    throw new RuntimeException_Sub3();
	aClass185_Sub1_Sub1_10114.method18009(aClass278_Sub2_10113.aLong10118);
	aClass185_Sub1_Sub1_10114.method18025(aClass278_Sub2_10113.aLong10119);
	aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523 = aClass278_Sub2_10113;
	aBool10112 = true;
    }
    
    public boolean method4967(Class278 class278) {
	if (aClass278_Sub2_10113 == class278)
	    return true;
	if (!class278.method5182())
	    return false;
	aClass278_Sub2_10113 = (Class278_Sub2) class278;
	anInt2953 = method4917(class278, (byte) -14) * -1164303385;
	if (anInt2953 * 567951319 == -1)
	    throw new IllegalArgumentException();
	if (aBool10112) {
	    aClass185_Sub1_Sub1_10114
		.method18009(aClass278_Sub2_10113.aLong10118);
	    aClass185_Sub1_Sub1_10114
		.method18025(aClass278_Sub2_10113.aLong10119);
	    aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523
		= aClass278_Sub2_10113;
	}
	return true;
    }
    
    public void method4955() {
	if (aClass278_Sub2_10113 == null)
	    throw new RuntimeException_Sub3();
	aClass185_Sub1_Sub1_10114.method18009(aClass278_Sub2_10113.aLong10118);
	aClass185_Sub1_Sub1_10114.method18025(aClass278_Sub2_10113.aLong10119);
	aClass185_Sub1_Sub1_10114.aClass278_Sub2_11523 = aClass278_Sub2_10113;
	aBool10112 = true;
    }
}
