/* Class279 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class279 implements Interface6
{
    public static Class110_Sub1_Sub2 aClass110_Sub1_Sub2_3053;
    
    public Interface13 method63(int i, Interface14 interface14) {
	return new Class273(i, interface14);
    }
    
    public Interface13 method62(int i, Interface14 interface14) {
	return new Class273(i, interface14);
    }
    
    public Class method59(short i) {
	return com.jagex.Class273.class;
    }
    
    public Interface13 method64(int i, Interface14 interface14) {
	return new Class273(i, interface14);
    }
    
    Class279() {
	/* empty */
    }
    
    public Interface13 method58(int i, Interface14 interface14, byte i_0_) {
	return new Class273(i, interface14);
    }
    
    public Interface13 method60(int i, Interface14 interface14) {
	return new Class273(i, interface14);
    }
    
    public Class method61() {
	return com.jagex.Class273.class;
    }
    
    static final void method5226(Class669 class669, int i) {
	int i_1_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class606_Sub1 class606_sub1 = Class521.method8688(i_1_, 230897623);
	if (class606_sub1 != null) {
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= class606_sub1.anInt7985 * -1395637313;
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= class606_sub1.aString10858;
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= class606_sub1.method16908((byte) 112);
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= class606_sub1.method16906(-479731018);
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 2098404975 * class606_sub1.anInt7981;
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= class606_sub1.anInt10856 * -663639827;
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= class606_sub1.aString10857;
	} else {
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= -1;
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= "";
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= "";
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= "";
	}
    }
    
    static final void method5227(Class669 class669, int i) {
	int i_2_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_2_, 628310725);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_2_ >> 16];
	Class469.method7637(class247, class243, class669, -1986370619);
    }
    
    static void method5228(Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1,
			   int i) {
	if (class654_sub1_sub5_sub1 instanceof Class654_Sub1_Sub5_Sub1_Sub1) {
	    Class654_Sub1_Sub5_Sub1_Sub1 class654_sub1_sub5_sub1_sub1
		= (Class654_Sub1_Sub5_Sub1_Sub1) class654_sub1_sub5_sub1;
	    if (class654_sub1_sub5_sub1_sub1.aClass307_12204 != null)
		Class276.method5155(class654_sub1_sub5_sub1_sub1,
				    ((Class322
				      .aClass654_Sub1_Sub5_Sub1_Sub2_3419
				      .aByte10854)
				     != (class654_sub1_sub5_sub1_sub1
					 .aByte10854)),
				    1591619626);
	} else if (class654_sub1_sub5_sub1
		   instanceof Class654_Sub1_Sub5_Sub1_Sub2) {
	    Class654_Sub1_Sub5_Sub1_Sub2 class654_sub1_sub5_sub1_sub2
		= (Class654_Sub1_Sub5_Sub1_Sub2) class654_sub1_sub5_sub1;
	    Class294.method5319(class654_sub1_sub5_sub1_sub2,
				((Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
				  .aByte10854)
				 != class654_sub1_sub5_sub1_sub2.aByte10854),
				(byte) 39);
	}
    }
}
