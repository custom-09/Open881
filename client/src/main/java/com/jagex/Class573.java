/* Class573 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.LinkedList;

public class Class573
{
    int anInt7668;
    int anInt7669;
    int anInt7670;
    float aFloat7671;
    LinkedList aLinkedList7672;
    public static Class163 aClass163_7673;
    
    Class573(int i, LinkedList linkedlist, int i_0_, int i_1_, float f) {
	anInt7669 = -891726949 * i;
	aLinkedList7672 = linkedlist;
	anInt7670 = i_0_ * -1086277771;
	anInt7668 = 1787761333 * i_1_;
	aFloat7671 = f;
    }
    
    static boolean method9640(byte i) {
	return Class114.aBool1383;
    }
    
    static final void method9641(Class669 class669, int i) {
	class669.anInt8600 -= 308999563;
    }
    
    static final void method9642(Class669 class669, byte i) {
	boolean bool = false;
	if (client.aBool11017) {
	    try {
		Object object
		    = (Class403.aClass403_4144.method6604
		       ((new Object[]
			 { Integer
			       .valueOf(1757658941 * Class200_Sub4.anInt9897),
			   Boolean.valueOf((Class322
					    .aClass654_Sub1_Sub5_Sub1_Sub2_3419
					    .aByte12214) == 1),
			   Integer.valueOf(class669.anIntArray8595
					   [((class669.anInt8600 -= 308999563)
					     * 2088438307)]) }),
			-2090823473));
		if (null != object)
		    bool = ((Boolean) object).booleanValue();
	    } catch (Throwable throwable) {
		/* empty */
	    }
	}
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = bool ? 1 : 0;
    }
    
    static void method9643(Class100 class100, Class247 class247, int i,
			   int i_2_, int i_3_, int i_4_) {
	if (1 == i) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4249,
				      class100.aClass13_1183, 1341391005);
	    Class407.method6688(class534_sub19, i_2_, i_3_,
				403396513 * class247.anInt2606, (byte) 5);
	    class100.method1863(class534_sub19, (byte) 5);
	}
	if (2 == i) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4188,
				      class100.aClass13_1183, 1341391005);
	    Class407.method6688(class534_sub19, i_2_, i_3_,
				class247.anInt2606 * 403396513, (byte) 89);
	    class100.method1863(class534_sub19, (byte) 25);
	}
	if (3 == i) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4210,
				      class100.aClass13_1183, 1341391005);
	    Class407.method6688(class534_sub19, i_2_, i_3_,
				403396513 * class247.anInt2606, (byte) 13);
	    class100.method1863(class534_sub19, (byte) 122);
	}
	if (i == 4) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4197,
				      class100.aClass13_1183, 1341391005);
	    Class407.method6688(class534_sub19, i_2_, i_3_,
				403396513 * class247.anInt2606, (byte) 32);
	    class100.method1863(class534_sub19, (byte) 115);
	}
	if (5 == i) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4250,
				      class100.aClass13_1183, 1341391005);
	    Class407.method6688(class534_sub19, i_2_, i_3_,
				class247.anInt2606 * 403396513, (byte) 4);
	    class100.method1863(class534_sub19, (byte) 67);
	}
	if (6 == i) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4172,
				      class100.aClass13_1183, 1341391005);
	    Class407.method6688(class534_sub19, i_2_, i_3_,
				403396513 * class247.anInt2606, (byte) 28);
	    class100.method1863(class534_sub19, (byte) 82);
	}
	if (i == 7) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4164,
				      class100.aClass13_1183, 1341391005);
	    Class407.method6688(class534_sub19, i_2_, i_3_,
				class247.anInt2606 * 403396513, (byte) 41);
	    class100.method1863(class534_sub19, (byte) 103);
	}
	if (8 == i) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4253,
				      class100.aClass13_1183, 1341391005);
	    Class407.method6688(class534_sub19, i_2_, i_3_,
				403396513 * class247.anInt2606, (byte) 106);
	    class100.method1863(class534_sub19, (byte) 6);
	}
	if (9 == i) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4224,
				      class100.aClass13_1183, 1341391005);
	    Class407.method6688(class534_sub19, i_2_, i_3_,
				class247.anInt2606 * 403396513, (byte) 8);
	    class100.method1863(class534_sub19, (byte) 19);
	}
	if (10 == i) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4211,
				      class100.aClass13_1183, 1341391005);
	    Class407.method6688(class534_sub19, i_2_, i_3_,
				class247.anInt2606 * 403396513, (byte) 24);
	    class100.method1863(class534_sub19, (byte) 38);
	}
    }
}
