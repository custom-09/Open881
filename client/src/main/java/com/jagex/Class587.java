/* Class587 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class587 implements Interface68
{
    int anInt7802;
    
    public void method356(int i) {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4168,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16687(-786710461 * anInt7802, 1957565343);
	client.aClass100_11264.method1863(class534_sub19, (byte) 64);
    }
    
    public void method142() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4168,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16687(-786710461 * anInt7802, 1966383503);
	client.aClass100_11264.method1863(class534_sub19, (byte) 46);
    }
    
    public void method286() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4168,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16687(-786710461 * anInt7802, 1961415412);
	client.aClass100_11264.method1863(class534_sub19, (byte) 42);
    }
    
    Class587(int i) {
	anInt7802 = 1469484139 * i;
    }
    
    public void method439() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4168,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16687(-786710461 * anInt7802, 2054044781);
	client.aClass100_11264.method1863(class534_sub19, (byte) 92);
    }
    
    public void method203() {
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4168,
				  client.aClass100_11264.aClass13_1183,
				  1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513
	    .method16687(-786710461 * anInt7802, 2142314765);
	client.aClass100_11264.method1863(class534_sub19, (byte) 124);
    }
    
    static final void method9863(Class669 class669, int i) {
	Class534_Sub29 class534_sub29 = Class502_Sub2.method15982((byte) 86);
	if (class534_sub29 == null) {
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= -1;
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= -1;
	} else {
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 1592054281 * class534_sub29.anInt10652;
	    int i_0_ = (class534_sub29.anInt10650 * 197477433 << 28
			| (Class554.anInt7368
			   + class534_sub29.anInt10655 * 1412374331) << 14
			| (class534_sub29.anInt10653 * -958626707
			   + Class554.anInt7369));
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= i_0_;
	}
    }
    
    static final void method9864(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub11_10749
		  .method16976(2088438307) == 1 ? 1 : 0;
    }
    
    static final void method9865(Class669 class669, byte i) {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub9_10748,
	     (class669.anIntArray8595
	      [(class669.anInt8600 -= 308999563) * 2088438307]) == 1 ? 1 : 0,
	     -72891075);
	Class672.method11096((byte) 1);
	client.aBool11048 = false;
    }
    
    static void method9866(Class433 class433, byte i) {
	Class72.aClass433_787.method6842(class433);
    }
    
    static final void method9867(Class669 class669, byte i) {
	throw new RuntimeException("");
    }
    
    static final void method9868(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class669.aClass534_Sub26_8606.method16300(((String)
							 (class669
							  .anObjectArray8593
							  [((class669.anInt8594
							     -= 1460193483)
							    * 1485266147)])),
							1454834009);
    }
}
