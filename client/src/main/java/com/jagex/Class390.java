/* Class390 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class390 implements Interface47
{
    public String aString4060;
    public Class401 aClass401_4061;
    public Class391 aClass391_4062;
    public int anInt4063;
    public int anInt4064;
    public int anInt4065;
    public int anInt4066;
    public int anInt4067;
    public int anInt4068;
    public int anInt4069;
    public int anInt4070;
    public int anInt4071;
    public int anInt4072;
    
    public Class397 method351() {
	return Class397.aClass397_4116;
    }
    
    public Class397 method349() {
	return Class397.aClass397_4116;
    }
    
    Class390(String string, Class401 class401, Class391 class391, int i,
	     int i_0_, int i_1_, int i_2_, int i_3_, int i_4_, int i_5_,
	     int i_6_, int i_7_, int i_8_) {
	aString4060 = string;
	aClass401_4061 = class401;
	aClass391_4062 = class391;
	anInt4065 = 1294675587 * i;
	anInt4064 = -470001307 * i_0_;
	anInt4063 = i_1_ * -1154027331;
	anInt4066 = 191357863 * i_2_;
	anInt4067 = i_3_ * -341865107;
	anInt4068 = -1558876151 * i_4_;
	anInt4069 = -741087003 * i_5_;
	anInt4070 = i_6_ * 298934645;
	anInt4071 = -114876607 * i_7_;
	anInt4072 = i_8_ * -838278333;
    }
    
    public Class397 method350() {
	return Class397.aClass397_4116;
    }
    
    public Class397 method348(int i) {
	return Class397.aClass397_4116;
    }
    
    public static Class390 method6535(Class534_Sub40 class534_sub40) {
	String string = class534_sub40.method16541((byte) -125);
	Class401 class401 = (Class72.method1560(2143417123)
			     [class534_sub40.method16527(-113663786)]);
	Class391 class391 = (Class705.method14234(-930124583)
			     [class534_sub40.method16527(-1690994644)]);
	int i = class534_sub40.method16530((byte) -5);
	int i_9_ = class534_sub40.method16530((byte) -102);
	int i_10_ = class534_sub40.method16527(-385389899);
	int i_11_ = class534_sub40.method16527(926661201);
	int i_12_ = class534_sub40.method16527(781905052);
	int i_13_ = class534_sub40.method16529((byte) 1);
	int i_14_ = class534_sub40.method16529((byte) 1);
	int i_15_ = class534_sub40.method16550((byte) 107);
	int i_16_ = class534_sub40.method16533(-258848859);
	int i_17_ = class534_sub40.method16533(-258848859);
	return new Class390(string, class401, class391, i, i_9_, i_10_, i_11_,
			    i_12_, i_13_, i_14_, i_15_, i_16_, i_17_);
    }
    
    static final void method6536(Class669 class669, int i) {
	class669.aLongArray8587
	    [(class669.anInt8596 += 1091885681) * 1572578961 - 1]
	    = ((Long) (class669.aClass534_Sub18_Sub8_8614.anObjectArray11753
		       [class669.anInt8613 * 662605117]))
		  .longValue();
    }
    
    static final void method6537(Class669 class669, byte i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class263.method4831(class247, class243, class669,
			    ((Class574.aClass534_Sub26_7710
			      == class669.aClass534_Sub26_8606)
			     ? Class253.aClass253_2665
			     : Class253.aClass253_2663),
			    (byte) -95);
    }
    
    static final void method6538(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class637.method10554(class247, class243, class669, -1607672642);
    }
    
    static final void method6539(Class669 class669, int i) {
	int i_18_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Character.toLowerCase((char) i_18_);
    }
    
    static final void method6540(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub30_10739
		  .method17143(-1569503751);
    }
    
    static final void method6541(Class669 class669, int i) {
	int i_19_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_19_, 1575160924);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_19_ >> 16];
	Class453.method7403(class247, class243, class669, -145349806);
    }
}
