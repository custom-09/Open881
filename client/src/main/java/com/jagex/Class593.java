/* Class593 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class593 implements Interface76
{
    int anInt7820;
    static Class593 aClass593_7821 = new Class593(0);
    public static Class593 aClass593_7822;
    static Class593 aClass593_7823 = new Class593(1);
    
    static Class593[] method9890() {
	return (new Class593[]
		{ aClass593_7823, aClass593_7822, aClass593_7821 });
    }
    
    public int method93() {
	return 1091886091 * anInt7820;
    }
    
    static {
	aClass593_7822 = new Class593(2);
    }
    
    public int method22() {
	return 1091886091 * anInt7820;
    }
    
    static Class593[] method9891() {
	return (new Class593[]
		{ aClass593_7823, aClass593_7822, aClass593_7821 });
    }
    
    static Class593[] method9892() {
	return (new Class593[]
		{ aClass593_7823, aClass593_7822, aClass593_7821 });
    }
    
    static Class593[] method9893() {
	return (new Class593[]
		{ aClass593_7823, aClass593_7822, aClass593_7821 });
    }
    
    public int method53() {
	return 1091886091 * anInt7820;
    }
    
    static Class593[] method9894() {
	return (new Class593[]
		{ aClass593_7823, aClass593_7822, aClass593_7821 });
    }
    
    static Class593[] method9895() {
	return (new Class593[]
		{ aClass593_7823, aClass593_7822, aClass593_7821 });
    }
    
    Class593(int i) {
	anInt7820 = i * 1497485219;
    }
    
    static final void method9896(Class669 class669, int i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_0_, -371368504);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_0_ >> 16];
	Class105.method1942(class247, class243, class669, -941488835);
    }
    
    static final void method9897(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_1_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_2_
	    = class669.anIntArray8595[1 + 2088438307 * class669.anInt8600];
	Class41 class41
	    = ((Class41)
	       Class667.aClass44_Sub21_8582.method91(i_1_, 1550712818));
	if (class41.aClass493_314 == Class493.aClass493_5496) {
	    /* empty */
	}
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = class41.method1040(i_2_, (byte) 8);
    }
    
    static final void method9898(Class669 class669, byte i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	Class501.method8278(string, 1814984227);
    }
    
    static final void method9899(Class669 class669, short i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
    
    static boolean method9900(boolean bool, boolean bool_3_, String string,
			      String string_4_, long l) {
	Class65.aBool661 = bool;
	if (!bool_3_)
	    Class65.anInt662 = -1651628635;
	Class65.aBool680 = bool_3_;
	Class65.aString694 = string;
	Class65.aString665 = string_4_;
	Class65.aLong663 = -8972729624098644529L * l;
	if (!Class65.aBool680 && (Class65.aString694.equals("")
				  || Class65.aString665.equals(""))) {
	    Class522.method8720(3, -2052205503);
	    Class275.method5151(-1312689955);
	    return false;
	}
	if (Class680.anInt8668 * 513656689 != 131) {
	    Class65.anInt705 = 0;
	    Class65.anInt677 = 217400935;
	    Class65.anInt706 = -1087612101;
	}
	Class65.aClass100_658.aBool1198 = false;
	Class522.method8720(-3, -2109926882);
	Class65.anInt692 = -937912598;
	Class65.anInt698 = 0;
	Class65.anInt678 = 0;
	return true;
    }
}
